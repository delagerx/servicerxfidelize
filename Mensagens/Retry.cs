﻿using Akka.Actor;
using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mensagens
{
    public class Retry
    {        
        public IActorRef Receiver { get; private set; }
        public object Mensagem { get; private set; }
        public IActorRef Sender { get; private set; }

        public Retry(IActorRef receiver, object mensagem, IActorRef sender)
        {
            Receiver = receiver;
            Mensagem = mensagem;
            Sender = sender;
        }
    }
}
