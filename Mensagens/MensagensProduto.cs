﻿using Entidades;
using Newtonsoft.Json;

namespace Mensagens
{
    public class MensagensProduto
    {
        public class EnviarProdutoParaWms : MensagemBase, IMensagemIntegracao
        {
            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public EnviarProdutoParaWms(MensagemIntegracao mensagemIntegracao)
            {
                MensagemIntegracao = mensagemIntegracao;
            }
        }

        public class ProdutoEncontrado : MensagemBase, IMensagemIntegracao
        {
            public MensagemIntegracao MensagemIntegracao { get; private set; }

            [JsonIgnore]
            public Produto Produto { get; private set; }

            public ProdutoEncontrado(MensagemIntegracao mensagem, Produto produto)
            {
                MensagemIntegracao = mensagem;
                Produto = produto;
            }
        }

        public class ProdutoNaoEncontrado : MensagemBase, IMensagemIntegracao
        {
            public long IdProduto { get; private set; }
            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public ProdutoNaoEncontrado(MensagemIntegracao mensagem, long idProduto)
            {
                MensagemIntegracao = mensagem;
                IdProduto = idProduto;
            }
        }

        public class BuscarProdutoNoErp : MensagemBase, IMensagemIntegracao
        {
            public long IdProduto { get; private set; }
            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public BuscarProdutoNoErp(MensagemIntegracao mensagem, long idProduto)
            {
                MensagemIntegracao = mensagem;
                IdProduto = idProduto;
            }
        }

        public class SalvarProdutoNoWms : MensagemBase, IMensagemIntegracao
        {
            [JsonIgnore]
            public Produto Produto { get; private set; }
            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public SalvarProdutoNoWms(MensagemIntegracao mensagem, Produto produto)
            {
                MensagemIntegracao = mensagem;
                Produto = produto;
            }
        }
    }
}
