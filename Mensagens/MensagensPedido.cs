﻿using Entidades;
using Entidades.Enum;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Mensagens
{
    public class MensagensPedido
    {
        public class EnviarPedidoParaWms : MensagemBase, IMensagemIntegracao
        {
            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public EnviarPedidoParaWms(MensagemIntegracao mensagemIntegracao)
            {
                MensagemIntegracao = mensagemIntegracao;
            }
        }

        public class PedidoEncontrado : MensagemBase, IMensagemIntegracao
        {
            public MensagemIntegracao MensagemIntegracao { get; private set; }

            [JsonIgnore]
            public Pedido Pedido { get; private set; }

            public PedidoEncontrado(MensagemIntegracao mensagem, Pedido pedido)
            {
                MensagemIntegracao = mensagem;
                Pedido = pedido;
            }
        }

        public class PedidoNaoEncontrado : MensagemBase, IMensagemIntegracao
        {
            public long IdPedido { get; private set; }
            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public PedidoNaoEncontrado(MensagemIntegracao mensagem, long idPedido)
            {
                MensagemIntegracao = mensagem;
                IdPedido = idPedido;
            }
        }

        public class BuscarPedidoNoErp : MensagemBase, IMensagemIntegracao
        {
            public long IdPedido { get; private set; }
            public Operacao Operacao { get; private set; }
            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public BuscarPedidoNoErp(MensagemIntegracao mensagem, Operacao operacao, long idPedido)
            {
                MensagemIntegracao = mensagem;
                Operacao = operacao;
                IdPedido = idPedido;
            }
        }

        public class SalvarPedidoNoWms : MensagemBase, IMensagemIntegracao
        {
            public Pedido Pedido { get; private set; }
            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public SalvarPedidoNoWms(MensagemIntegracao mensagem, Pedido pedido)
            {
                MensagemIntegracao = mensagem;
                Pedido = pedido;
            }
        }

        public class VerificarNovosPedidos : MensagemBase { }

        public class NovosPedidosEncontrados : MensagemBase
        {
            [JsonIgnore]
            public IList<Pedido> Pedidos { get; private set; }

            public NovosPedidosEncontrados(IList<Pedido> pedidos)
            {
                Pedidos = pedidos;
            }
        }

        public class SalvarPedidoNoErp : MensagemBase, IMensagemIntegracao
        {
            public Pedido Pedido { get; private set; }

            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public SalvarPedidoNoErp(Pedido pedido, MensagemIntegracao mensagemIntegracao)
            {
                MensagemIntegracao = mensagemIntegracao;
                Pedido = pedido;
            }
        }

        public class PedidoSalvoNoErp : MensagemBase, IMensagemIntegracao
        {
            [JsonIgnore]
            public Pedido Pedido { get; private set; }

            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public PedidoSalvoNoErp(Pedido pedido, MensagemIntegracao mensagemIntegracao)
            {
                MensagemIntegracao = mensagemIntegracao;
                Pedido = pedido;
            }
        }

        public class PedidoComRejeicao : MensagemBase, IMensagemIntegracao
        {
            [JsonIgnore]
            public Pedido Pedido { get; private set; }

            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public PedidoComRejeicao(Pedido pedido, MensagemIntegracao mensagemIntegracao)
            {
                MensagemIntegracao = mensagemIntegracao;
                Pedido = pedido;
            }
        }

        public class FinalizarIntegracaoDoPedidoNoWms : MensagemBase, IMensagemIntegracao
        {
            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public int IdWms { get; private set; }

            public FinalizarIntegracaoDoPedidoNoWms(MensagemIntegracao mensagem, int idWms)
            {
                MensagemIntegracao = mensagem;
                IdWms = idWms;
            }
        }
    }
}
