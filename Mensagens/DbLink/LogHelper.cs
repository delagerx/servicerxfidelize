﻿using NLog;
using System;

namespace DbLink
{
    public static class LogHelper
    {
        private static readonly Logger _logger;

        static LogHelper()
        {
            _logger = LogManager.GetCurrentClassLogger();
        }

        public static void LogInfo(string message, bool writeConsole = true)
        {
            _logger.Info(message);

            if (writeConsole)
                WriteLine(message);
        }

        public static void LogDebug(string message, bool writeConsole = true)
        {
            _logger.Debug(message);

            if (writeConsole)
                WriteLine(message);
        }

        public static void LogError(string mensagemExcecao, bool writeConsole = true)
        {
            _logger.Error(mensagemExcecao);

            if (writeConsole)
                WriteLine(mensagemExcecao);
        }

        public static void LogFormattedInfo(string className, string methodName, string message)
        {
            string stringFormat = "{{ \"className\": \"{0}\", \"methodName\": \"{1}\", \"message\": \"{2}\" }}";

            LogInfo(string.Format(stringFormat, className, methodName, message));
        }

        private static void WriteLine(string message)
        {
            Console.Out.WriteLine(string.Format("{0} - {1}.", DateTime.Now, message));
        }
    }
}
