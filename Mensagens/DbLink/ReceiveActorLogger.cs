﻿using Akka.Actor;
using Akka.Monitoring;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Runtime.Serialization;

namespace DbLink
{
    public class ReceiveActorLogger : ReceiveActor, ILogReceive
    {
        protected override void PreStart()
        {
            Context.IncrementActorCreated();
            base.PreStart();
        }

        protected override void PostStop()
        {
            Context.IncrementActorStopped();
            base.PostStop();
        }

        protected void Receive<TMessage>(Action<TMessage> action)
        {
            Context.IncrementMessagesReceived();
            Context.Gauge(String.Format("mailbox.{0}", Context.Self.GetType().Name), Context.Props.Mailbox.Length);
            base.Receive<TMessage>(msg => MonitorMessage(msg, x => action(x)));
        }

        protected void MonitorMessage<TMessage>(TMessage message, Action<TMessage> action)
        {
            var actorName = this.GetType().Name;
            var startTime = DateTime.Now;

            LogHelper.LogInfo(string.Format("{{ \"actor\": \"{0}\", \"event\": \"messageReceived\", {1} }}", actorName, message));

            try
            {
                action(message);
            }
            catch(Exception ex)
            {
                LogErrorProcessingMessage(message, ex);
                throw ex;
            }
            finally
            {
                int processTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds);

                TimeSpan elapsedTime = TimeSpan.FromMilliseconds(0);

                if (message is Mensagens.IMensagemIntegracao)
                {
                    var m = message as Mensagens.IMensagemIntegracao;
                    elapsedTime = DateTime.Now.Subtract(m.MensagemIntegracao.DbLinkInicioProcessamento);
                }

                LogHelper.LogInfo(string.Format("{{ \"actor\": \"{0}\" , \"event\": \"messageProcessed\", {1}, \"processTime\": {2}, \"elapsedTime\": {3} }}", actorName, message, processTime, Convert.ToInt64(elapsedTime.Milliseconds)));
            }

        }

        protected void LogErrorProcessingMessage(object message, Exception ex)
        {
            var actorName = this.GetType().Name;

            var serializedException = JsonConvert.SerializeObject(ex, Formatting.Indented, 
                new JsonSerializerSettings {
                    PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                    NullValueHandling = NullValueHandling.Ignore
                });

            string baseExceptionMessage = ex.GetBaseException().Message.Replace("\r\n", " ");

            LogHelper.LogError($"{{ \"actor\": \"{actorName}\", \"exceptionMessage\": \"{baseExceptionMessage}\", \"event\": \"errorProcessingMessage\", {message}, \"exception\": {serializedException}  }}");
            IncrementErrors();
        }

        protected void IncrementErrors()
        {
            Context.IncrementCounter(string.Format("{0}.errorsHandled", this.GetType().Name));
        }
    }
}