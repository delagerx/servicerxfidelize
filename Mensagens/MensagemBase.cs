﻿using System;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System.Runtime.Serialization;

namespace Mensagens
{
    public abstract class MensagemBase
    {
        [NonSerializedAttribute]
        protected string Json = null;

        public override string ToString()
        {
            if (Json == null)
            {
                Json = GenerateJson().ToString().Substring(1);
                Json = Json.Substring(0, Json.Length - 1);
            }
                

            return Json;
        }

        protected virtual JObject GenerateJson()
        {
            var json = JObject.Parse(string.Format("{{ 'AkkaMessage': '{0}'}}", this.GetType().Name));
            json.Merge(JObject.FromObject(this));
            return json;
        }

        [OnError]
        internal void OnError(StreamingContext context, ErrorContext errorContext)
        {
            errorContext.Handled = true;
        }
    }
}