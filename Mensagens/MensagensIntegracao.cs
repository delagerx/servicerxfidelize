﻿using Entidades;
using Entidades.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Mensagens
{
    public class MensagensIntegracao
    {
        public class BuscarNovasMensagens : MensagemBase { }

        public class BuscarMensagensProcessadas : MensagemBase { }

        public class BuscarMensagensPendentes : MensagemBase { }

        public class NovaMensagemEncontrada : MensagemBase, IMensagemIntegracao
        {
            public long IdMensagem { get; private set; }
            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public NovaMensagemEncontrada(long idMensagem, MensagemIntegracao mensagemIntegracao)
            {
                IdMensagem = idMensagem;
                MensagemIntegracao = mensagemIntegracao;
            }
        }

        public class MensagemProcessadaEncontrada : MensagemBase, IMensagemIntegracao
        {
            public long IdMensagem { get; private set; }
            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public MensagemProcessadaEncontrada(long idMensagem, MensagemIntegracao mensagemIntegracao)
            {
                IdMensagem = idMensagem;
                MensagemIntegracao = mensagemIntegracao;
            }
        }

        public class MensagensPendentesEncontradas : MensagemBase
        {
            public MensagensPendentesEncontradas(IList<ResultadoConsulta> mensagens)
            {
                Mensagens = mensagens;
            }

            public IList<ResultadoConsulta> Mensagens { get; set; }

            public class ResultadoConsulta
            {
                public string TabelaRelacionada { get; set; }

                public string SistemaOrigem { get; set; }

                public int QuantidadeDeMensagens { get; set; }

            }
        }

        public class MensagemProcessada : MensagemBase, IMensagemIntegracao
        {
            public long? CodigoRejeicao { get; private set; }
            public string MotivoRejeicao { get; private set; }
            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public MensagemProcessada(MensagemIntegracao mensagemIntegracao, long? codigoRejeicao = null, string motivoRejeicao = null)
            {
                MensagemIntegracao = mensagemIntegracao;
                CodigoRejeicao = codigoRejeicao;
                MotivoRejeicao = motivoRejeicao;
            }
        }

        public class MensagemCriada : MensagemBase, IMensagemIntegracao
        {
            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public MensagemCriada(MensagemIntegracao mensagem)
            {
                MensagemIntegracao = mensagem;
            }
        }

        public class MensagemAtualizada : MensagemBase, IMensagemIntegracao
        {
            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public MensagemAtualizada(MensagemIntegracao mensagem)
            {
                MensagemIntegracao = mensagem;
            }
        }

        public class SalvarMensagemIntegracaoProcessada : MensagemBase, IMensagemIntegracao
        {
            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public SalvarMensagemIntegracaoProcessada(MensagemIntegracao mensagemIntegracao)
            {
                MensagemIntegracao = mensagemIntegracao;
            }
        }

        public class CriarMensagemIntegracao : MensagemBase, IMensagemIntegracao
        {
            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public CriarMensagemIntegracao(MensagemIntegracao mensagemIntegracao)
            {
                MensagemIntegracao = mensagemIntegracao;
            }
        }
    }
}
