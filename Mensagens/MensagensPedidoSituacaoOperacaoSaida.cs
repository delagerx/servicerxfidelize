﻿using Entidades;
using Entidades.Enum;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Mensagens
{
    public class MensagensPedidoSituacaoOperacaoSaida
    {
        public class BuscarNovosRegistrosNoWms : MensagemBase { }

        public class NovosRegistrosEncontrados : MensagemBase
        {
            [JsonIgnore]
            public IList<PedidoSituacaoOperacaoSaida> Registros { get; private set; }

            public NovosRegistrosEncontrados(IList<PedidoSituacaoOperacaoSaida> registros)
            {
                Registros = registros;
            }
        }

        public class SalvarRegistroNoErp : MensagemBase, IMensagemIntegracao
        {
            public PedidoSituacaoOperacaoSaida PedidoSituacaoOperacaoSaida { get; private set; }

            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public SalvarRegistroNoErp(PedidoSituacaoOperacaoSaida pedidoSituacaoOperacaoSaida, MensagemIntegracao mensagemIntegracao)
            {
                MensagemIntegracao = mensagemIntegracao;
                PedidoSituacaoOperacaoSaida = pedidoSituacaoOperacaoSaida;
            }
        }

        public class RegistroSalvoNoErp : MensagemBase, IMensagemIntegracao
        {
            [JsonIgnore]
            public PedidoSituacaoOperacaoSaida PedidoSituacaoOperacaoSaida { get; private set; }

            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public RegistroSalvoNoErp(PedidoSituacaoOperacaoSaida pedidoSituacaoOperacaoSaida, MensagemIntegracao mensagemIntegracao)
            {
                MensagemIntegracao = mensagemIntegracao;
                PedidoSituacaoOperacaoSaida = pedidoSituacaoOperacaoSaida;
            }
        }

        public class FinalizarIntegracaoDoPedidoSituacaoNoWms : MensagemBase, IMensagemIntegracao
        {
            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public int IdWms { get; private set; }

            public int IdSituacao { get; private set; }

            public FinalizarIntegracaoDoPedidoSituacaoNoWms(MensagemIntegracao mensagem, int idWms, int idSituacao)
            {
                MensagemIntegracao = mensagem;
                IdWms = idWms;
                IdSituacao = idSituacao;
            }
        }
    }
}
