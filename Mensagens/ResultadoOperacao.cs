﻿using Entidades;
using Newtonsoft.Json;
using System;

namespace Mensagens
{
    public class ResultadoOperacao
    {
        public class Sucesso : MensagemBase, IMensagemIntegracao
        {
            public MensagemIntegracao MensagemIntegracao { get; private set; }

            public Sucesso(MensagemIntegracao mensagemIntegracao)
            {
                MensagemIntegracao = mensagemIntegracao;
            }
        }

        public class Rejeicao : MensagemBase, IMensagemIntegracao
        {
            public MensagemIntegracao MensagemIntegracao { get; private set; }
            public long CodigoRejeicao { get; private set; }
            public string MensagemRejeicao { get; private set; }

            public Rejeicao(MensagemIntegracao mensagemIntegracao, long codigoRejeicao, string mensagemRejeicao)
            {
                MensagemIntegracao = mensagemIntegracao;
                CodigoRejeicao = codigoRejeicao;
                MensagemRejeicao = mensagemRejeicao;
            }
        }

        public class Erro : MensagemBase, IMensagemIntegracao
        {
            public MensagemIntegracao MensagemIntegracao { get; private set; }
            public Exception Excecao { get; private set; }

            public Erro(MensagemIntegracao mensagemIntegracao, Exception excecao)
            {
                MensagemIntegracao = mensagemIntegracao;
                Excecao = excecao;
            }
        }
    }
}
