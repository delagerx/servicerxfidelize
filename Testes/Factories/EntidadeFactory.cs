﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testes.Factories
{
    public static class EntidadeFactory
    {
        public static Entidade Gerar()
        {
            Random random = new Random();

            return new Entidade
            {
                Id = random.Next(1, 100),
                Endereco = "Endereco",
                CodEntidadePolo = "CodEntidadePolo",
                RazaoSocial = "RazaoSocial",
                Tipo = "Tipo",
                Bandeira = "Bandeira"
            };
        }
    }
}
