﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testes.Factories
{
    public static class PedidoFactory
    {
        public static Pedido Gerar(long? id = null)
        {
            Random random = new Random();

            return new PedidoOperacaoEntrada { Id = id ?? random.Next() };
        }
    }
}
