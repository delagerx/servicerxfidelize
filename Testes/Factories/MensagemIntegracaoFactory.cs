﻿using Entidades;
using Entidades.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testes.Factories
{
    public static class MensagemIntegracaoFactory
    {
        public static MensagemIntegracao Gerar(string tabelaRelacionada = TabelaRelacionada.PedidoOperacaoEntrada, long origem = (long)Origem.ERP, int? id = null)
        {
            Random random = new Random();

            return new MensagemIntegracao(
                id: id ?? random.Next(1, 100), 
                origem: origem, 
                tabelaRelacionada: tabelaRelacionada,
                idTabelaRelacionada: random.Next()
            );
        }
    }
}
