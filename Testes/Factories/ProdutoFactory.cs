﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testes.Factories
{
    public static class ProdutoFactory
    {
        public static Produto Gerar()
        {
            Random random = new Random();

            return new Produto
            {
                Id = random.Next(1, 100),
                Altura = random.Next(),
                CasasDecimais = random.Next(),
                CodFornPolo = "CodFornPolo",
                CodProdutoPolo = "CodProdutoPolo",
                CodSituacao = random.Next(),
                Comprimento = random.Next(),
                ControleLote = true,
                Descricao = "Descricao",
                EmbCompra = random.Next(),
                EmbSaida = random.Next(),
                Generico = "Generico",
                Grupo = "Grupo",
                Largura = random.Next(),
                LinhaProduto = "LinhaProduto",
                Peso = random.Next(),
                Preco = random.Next(),
                Subgrupo = "Subgrupo",
                Unidade = "Unidade",
                Validade = random.Next()
            };
        }
    }
}
