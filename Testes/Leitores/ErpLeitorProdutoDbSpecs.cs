﻿using Akka.Actor;
using Akka.TestKit;
using Akka.TestKit.Xunit2;
using Core;
using Core.Exportacao.Agentes;
using Core.Integracao;
using Entidades;
using Integracao.ERP;
using Integracao.WMS.Escritores;
using Mensagens;
using Moq;
using Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Testes.Factories;
using Xunit;

namespace Testes.Agentes
{
    public class ErpLeitorProdutoDbSpecs : TestKit
    {
        private Mock<IProdutoRepository> _repositoryMock;
        private IActorRef _leitorProduto;

        public ErpLeitorProdutoDbSpecs() : base()
        {
            _repositoryMock = new Mock<IProdutoRepository>();

            _leitorProduto = Sys.ActorOf(Props.Create(() => new ErpLeitorProdutoDb(_repositoryMock.Object)), "leitorProduto"); 
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo BuscarProdutoNoErp deve consultar o Produto a partir do respositório e enviá-lo através de uma mensagem do tipo ProdutoEncontrado para o ator Sender")]
        public void Mensagem_BuscarProdutoNoErp_Recebida()
        {
            MensagemIntegracao novaMensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            Produto produto = ProdutoFactory.Gerar();

            _repositoryMock.Setup(t => t.Get(produto.Id)).Returns(produto);

            _leitorProduto.Tell(new MensagensProduto.BuscarProdutoNoErp(novaMensagemIntegracao, produto.Id));

            ExpectMsg<MensagensProduto.ProdutoEncontrado>(mensagem => 
                mensagem.MensagemIntegracao == novaMensagemIntegracao && 
                mensagem.Produto == produto);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo BuscarProdutoNoErp deve consultar o Produto a partir do respositório e caso ele não possa ser encontrado, deve enviar uma mensagem do tipo ProdutoNaoEncontrado para o ator Sender")]
        public void Mensagem_BuscarProdutoNoErp_Recebida_Produto_Nao_Encontrado()
        {
            MensagemIntegracao novaMensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            Produto produto = ProdutoFactory.Gerar();

            _repositoryMock.Setup(t => t.Get(produto.Id)).Returns(null as Produto);

            _leitorProduto.Tell(new MensagensProduto.BuscarProdutoNoErp(novaMensagemIntegracao, produto.Id));

            ExpectMsg<MensagensProduto.ProdutoNaoEncontrado>(mensagem =>
                mensagem.MensagemIntegracao == novaMensagemIntegracao &&
                mensagem.IdProduto == produto.Id);
        }
    }
}
