﻿using Akka.Actor;
using Akka.TestKit.Xunit2;
using Entidades;
using Integracao.ERP;
using Mensagens;
using Moq;
using Testes.Factories;
using Xunit;

namespace Testes.Agentes
{
    public class ErpLeitorEntidadeRestSpecs : TestKit
    {
        private Mock<IRestClient<Entidade>> _leitorBaseMock;
        private IActorRef _leitorEntidade;

        public ErpLeitorEntidadeRestSpecs()
            : base()
        {
            _leitorBaseMock = new Mock<IRestClient<Entidade>>();
            _leitorEntidade = Sys.ActorOf(Props.Create(() => new ErpLeitorEntidadeRest(_leitorBaseMock.Object)), "leitorEntidade");
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo BuscarEntidadeNoErp deve consultar a Entidade a partir do webservice e enviá-la através de uma mensagem do tipo EntidadeEncontrada para o ator Sender")]
        public void Mensagem_BuscarEntidadeNoErp_Recebida()
        {
            var novaMensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            var entidade = EntidadeFactory.Gerar();

            _leitorBaseMock.Setup(method => method.Buscar("entidade", entidade.Id.ToString())).Returns(entidade);

            _leitorEntidade.Tell(new MensagensEntidade.BuscarEntidadeNoErp(novaMensagemIntegracao, entidade.Id));

            ExpectMsg<MensagensEntidade.EntidadeEncontrada>(msg => msg.MensagemIntegracao == novaMensagemIntegracao && msg.Entidade.Id == entidade.Id);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo BuscarEntidadeNoErp deve consultar a Entidade a partir do webservice e caso ela não possa ser encontrada, deve enviar uma mensagem do tipo EntidadeNaoEncontrada para o ator Sender")]
        public void Mensagem_BuscarEntidadeNoErp_Recebida_Entidade_Nao_Encontrada()
        {
            var novaMensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            var entidade = EntidadeFactory.Gerar();

            _leitorBaseMock.Setup(t => t.Buscar("entidade", entidade.Id.ToString())).Returns(null as Entidade);

            _leitorEntidade.Tell(new MensagensEntidade.BuscarEntidadeNoErp(novaMensagemIntegracao, entidade.Id));

            ExpectMsg<MensagensEntidade.EntidadeNaoEncontrada>(msg => msg.MensagemIntegracao == novaMensagemIntegracao && msg.IdEntidade == entidade.Id);
        }
    }
}
