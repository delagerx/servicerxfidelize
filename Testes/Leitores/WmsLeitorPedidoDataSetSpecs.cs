﻿using Akka.Actor;
using Akka.TestKit;
using Akka.TestKit.Xunit2;
using Core;
using Core.Exportacao.Agentes;
using Core.Integracao;
using Entidades;
using Integracao.ERP;
using Integracao.WMS.Clientes;
using Integracao.WMS.Escritores;
using Mensagens;
using Moq;
using Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Testes.Factories;
using Xunit;

namespace Testes.Agentes
{
    public class WmsLeitorPedidoDataSetSpecs : TestKit
    {
        private Mock<IClienteIntegracaoPedido> _clienteIntegracaoPedidoMock;
        private IActorRef _wmsLeitorPedido;

        public WmsLeitorPedidoDataSetSpecs() : base()
        {
            _clienteIntegracaoPedidoMock = new Mock<IClienteIntegracaoPedido>();

            _wmsLeitorPedido = Sys.ActorOf(Props.Create(() => new WmsLeitorPedidoDataSet(_clienteIntegracaoPedidoMock.Object)), "wmsLeitorPedido"); 
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo VerificarNovosPedidos deve consultar os Pedidos que ainda não foram integrados no ERP a partir do webservice e enviá-los através de uma mensagem do tipo NovosPedidosEncontrados para o ator Sender")]
        public void Mensagem_VerificarNovosPedidos_Recebida()
        {
            IList<Pedido> pedidos = new List<Pedido>();

            pedidos.Add(PedidoFactory.Gerar());
            pedidos.Add(PedidoFactory.Gerar());
            pedidos.Add(PedidoFactory.Gerar());

            _clienteIntegracaoPedidoMock.Setup(t => t.BuscarNovosPedidos()).Returns(pedidos);

            _wmsLeitorPedido.Tell(new MensagensPedido.VerificarNovosPedidos());

            ExpectMsg<MensagensPedido.NovosPedidosEncontrados>(mensagem => mensagem.Pedidos == pedidos);
        }
    }
}
