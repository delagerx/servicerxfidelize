﻿using Akka.Actor;
using Akka.TestKit.Xunit2;
using Entidades;
using Integracao.ERP;
using Mensagens;
using Moq;
using Testes.Factories;
using Xunit;

namespace Testes.Agentes
{
    public class ErpLeitorProdutoRestSpecs : TestKit
    {
        private Mock<IRestClient<Produto>> _leitorBaseMock;
        private IActorRef _leitorProduto;

        public ErpLeitorProdutoRestSpecs()
            : base()
        {
            _leitorBaseMock = new Mock<IRestClient<Produto>>();
            _leitorProduto = Sys.ActorOf(Props.Create(() => new ErpLeitorProdutoRest(_leitorBaseMock.Object)), "leitorProduto");
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo BuscarProdutoNoErp deve consultar a Produto a partir do webservice e enviá-la através de uma mensagem do tipo ProdutoEncontrada para o ator Sender")]
        public void Mensagem_BuscarProdutoNoErp_Recebida()
        {
            var novaMensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            var produto = ProdutoFactory.Gerar();

            _leitorBaseMock.Setup(method => method.Buscar("produto", produto.Id.ToString())).Returns(produto);

            _leitorProduto.Tell(new MensagensProduto.BuscarProdutoNoErp(novaMensagemIntegracao, produto.Id));

            ExpectMsg<MensagensProduto.ProdutoEncontrado>(msg => msg.MensagemIntegracao == novaMensagemIntegracao && msg.Produto.Id == produto.Id);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo BuscarProdutoNoErp deve consultar a Produto a partir do webservice e caso ela não possa ser encontrada, deve enviar uma mensagem do tipo ProdutoNaoEncontrada para o ator Sender")]
        public void Mensagem_BuscarProdutoNoErp_Recebida_Produto_Nao_Encontrada()
        {
            var novaMensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            var produto = ProdutoFactory.Gerar();

            _leitorBaseMock.Setup(t => t.Buscar("produto", produto.Id.ToString())).Returns(null as Produto);

            _leitorProduto.Tell(new MensagensProduto.BuscarProdutoNoErp(novaMensagemIntegracao, produto.Id));

            ExpectMsg<MensagensProduto.ProdutoNaoEncontrado>(msg => msg.MensagemIntegracao == novaMensagemIntegracao && msg.IdProduto == produto.Id);
        }
    }
}
