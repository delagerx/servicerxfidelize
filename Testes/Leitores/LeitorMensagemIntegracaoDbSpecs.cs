﻿using Akka.Actor;
using Akka.TestKit;
using Akka.TestKit.Xunit2;
using Core;
using Core.Exportacao.Agentes;
using Core.Importacao;
using Core.Integracao;
using Entidades;
using Entidades.Enum;
using Integracao.ERP;
using Integracao.WMS.Escritores;
using Mensagens;
using Moq;
using Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Testes.Factories;
using Xunit;

namespace Testes.Agentes
{
    public class LeitorMensagemIntegracaoDbSpecs : TestKit
    {
        private Mock<IMensagemIntegracaoRepository> _repositoryMock;
        private IActorRef _leitorMensagemIntegracao;
        private IActorRef _coordenadorImportacao;
        private TestProbe _coordenadorImportacaoProbe;

        public LeitorMensagemIntegracaoDbSpecs() : base()
        {
            _repositoryMock = new Mock<IMensagemIntegracaoRepository>();

            _leitorMensagemIntegracao = Sys.ActorOf(Props.Create(() => new ErpLeitorMensagemIntegracaoDb(_repositoryMock.Object)), "leitorMensagemIntegracao"); 
            
            _coordenadorImportacaoProbe = CreateTestProbe("coordenadorImportacao");
            _coordenadorImportacao = Sys.ActorOf(Props.Create<CoordenadorImportacao>(_coordenadorImportacaoProbe));
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo BuscarNovasMensagens deve enviar uma mensagem do tipo NovaMensagemEncontrada para o ator CoordenadorImportacao para cada mensagem encontrada")]
        public void Mensagem_EnviarEntidadeParaWms_Recebida()
        {
            IList<MensagemIntegracao> mensagensNaoProcessadas = new List<MensagemIntegracao>();

            mensagensNaoProcessadas.Add(MensagemIntegracaoFactory.Gerar(id: 1));
            mensagensNaoProcessadas.Add(MensagemIntegracaoFactory.Gerar(id: 2));
            mensagensNaoProcessadas.Add(MensagemIntegracaoFactory.Gerar(id: 3));
            
            _repositoryMock.Setup(t => t.Items).Returns(mensagensNaoProcessadas.AsQueryable());

            _leitorMensagemIntegracao.Tell(new MensagensIntegracao.BuscarNovasMensagens(), _coordenadorImportacaoProbe);
            
            _coordenadorImportacaoProbe.ExpectMsg<MensagensIntegracao.NovaMensagemEncontrada>(mensagem => mensagem.IdMensagem == mensagensNaoProcessadas[0].Id);
            _coordenadorImportacaoProbe.ExpectMsg<MensagensIntegracao.NovaMensagemEncontrada>(mensagem => mensagem.IdMensagem == mensagensNaoProcessadas[1].Id);
            _coordenadorImportacaoProbe.ExpectMsg<MensagensIntegracao.NovaMensagemEncontrada>(mensagem => mensagem.IdMensagem == mensagensNaoProcessadas[2].Id);
        }
    }
}
