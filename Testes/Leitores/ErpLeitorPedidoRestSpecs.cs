﻿using Akka.Actor;
using Akka.TestKit.Xunit2;
using Entidades;
using Entidades.Enum;
using Integracao.ERP;
using Mensagens;
using Moq;
using Testes.Factories;
using Xunit;

namespace Testes.Agentes
{
    public class ErpLeitorPedidoRestSpecs : TestKit
    {
        private Mock<IRestClient<PedidoOperacaoEntrada>> _leitorPedidoOperacaoEntradaMock;
        private Mock<IRestClient<PedidoOperacaoSaida>> _leitorPedidoOperacaoSaidaMock;
        private Mock<IRestClient<PedidoOperacaoMovimentacao>> _leitorPedidoOperacaoMovimentacaoMock;

        private IActorRef _leitorPedido;

        public ErpLeitorPedidoRestSpecs()
            : base()
        {
            _leitorPedidoOperacaoEntradaMock = new Mock<IRestClient<PedidoOperacaoEntrada>>();
            _leitorPedidoOperacaoSaidaMock = new Mock<IRestClient<PedidoOperacaoSaida>>();
            _leitorPedidoOperacaoMovimentacaoMock = new Mock<IRestClient<PedidoOperacaoMovimentacao>>();

            _leitorPedido = Sys.ActorOf(Props.Create(() => new ErpLeitorPedidoRest(
                _leitorPedidoOperacaoEntradaMock.Object,
                _leitorPedidoOperacaoSaidaMock.Object,
                _leitorPedidoOperacaoMovimentacaoMock.Object)), "leitorPedido");
        }

        #region OPERACAO: Entrada

        [Fact(DisplayName = "Ao receber uma mensagem do tipo BuscarPedidoNoErp deve consultar a Pedido a partir do webservice e enviá-la através de uma mensagem do tipo PedidoEncontrada para o ator Sender")]
        public void Mensagem_BuscarPedidoTipoEntradaNoErp_Recebida()
        {
            var novaMensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            var pedido = new PedidoOperacaoEntrada { Id = 10 };

            _leitorPedidoOperacaoEntradaMock.Setup(method => method.Buscar("pedido_operacao_entrada", pedido.Id.ToString())).Returns(pedido);

            _leitorPedido.Tell(new MensagensPedido.BuscarPedidoNoErp(novaMensagemIntegracao, Operacao.Entrada, pedido.Id));

            ExpectMsg<MensagensPedido.PedidoEncontrado>(msg => msg.MensagemIntegracao == novaMensagemIntegracao && msg.Pedido.Id == pedido.Id);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo BuscarPedidoNoErp deve consultar a Pedido a partir do webservice e caso ela não possa ser encontrada, deve enviar uma mensagem do tipo PedidoNaoEncontrada para o ator Sender")]
        public void Mensagem_BuscarPedidoTipoEntradaNoErp_Recebida_Pedido_Nao_Encontrada()
        {
            var novaMensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            var pedido = new PedidoOperacaoEntrada { Id = 10 };

            _leitorPedidoOperacaoEntradaMock.Setup(method => method.Buscar("pedido_operacao_entrada", pedido.Id.ToString())).Returns(null as PedidoOperacaoEntrada);

            _leitorPedido.Tell(new MensagensPedido.BuscarPedidoNoErp(novaMensagemIntegracao, Operacao.Entrada, pedido.Id));

            ExpectMsg<MensagensPedido.PedidoNaoEncontrado>(msg => msg.MensagemIntegracao == novaMensagemIntegracao && msg.IdPedido == pedido.Id);
        }

        #endregion

        #region OPERACAO: Movimentacao

        [Fact(DisplayName = "Ao receber uma mensagem do tipo BuscarPedidoNoErp deve consultar a Pedido a partir do webservice e enviá-la através de uma mensagem do tipo PedidoEncontrada para o ator Sender")]
        public void Mensagem_BuscarPedidoTipoMovimentacaoNoErp_Recebida()
        {
            var novaMensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            var pedido = new PedidoOperacaoMovimentacao { Id = 10 };

            _leitorPedidoOperacaoMovimentacaoMock.Setup(method => method.Buscar("pedido_operacao_movimentacao", pedido.Id.ToString())).Returns(pedido);

            _leitorPedido.Tell(new MensagensPedido.BuscarPedidoNoErp(novaMensagemIntegracao, Operacao.Movimentacao, pedido.Id));

            ExpectMsg<MensagensPedido.PedidoEncontrado>(msg => msg.MensagemIntegracao == novaMensagemIntegracao && msg.Pedido.Id == pedido.Id);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo BuscarPedidoNoErp deve consultar a Pedido a partir do webservice e caso ela não possa ser encontrada, deve enviar uma mensagem do tipo PedidoNaoEncontrada para o ator Sender")]
        public void Mensagem_BuscarPedidoTipoMovimentacaoNoErp_Recebida_Pedido_Nao_Encontrada()
        {
            var novaMensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            var pedido = new PedidoOperacaoMovimentacao { Id = 10 };

            _leitorPedidoOperacaoMovimentacaoMock.Setup(method => method.Buscar("pedido_operacao_movimentacao", pedido.Id.ToString())).Returns(null as PedidoOperacaoMovimentacao);

            _leitorPedido.Tell(new MensagensPedido.BuscarPedidoNoErp(novaMensagemIntegracao, Operacao.Movimentacao, pedido.Id));

            ExpectMsg<MensagensPedido.PedidoNaoEncontrado>(msg => msg.MensagemIntegracao == novaMensagemIntegracao && msg.IdPedido == pedido.Id);
        }

        #endregion

        #region OPERACAO: Saida

        [Fact(DisplayName = "Ao receber uma mensagem do tipo BuscarPedidoNoErp deve consultar a Pedido a partir do webservice e enviá-la através de uma mensagem do tipo PedidoEncontrada para o ator Sender")]
        public void Mensagem_BuscarPedidoTipoSaidaNoErp_Recebida()
        {
            var novaMensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            var pedido = new PedidoOperacaoSaida { Id = 10 };

            _leitorPedidoOperacaoSaidaMock.Setup(method => method.Buscar("pedido_operacao_saida", pedido.Id.ToString())).Returns(pedido);

            _leitorPedido.Tell(new MensagensPedido.BuscarPedidoNoErp(novaMensagemIntegracao, Operacao.Saida, pedido.Id));

            ExpectMsg<MensagensPedido.PedidoEncontrado>(msg => msg.MensagemIntegracao == novaMensagemIntegracao && msg.Pedido.Id == pedido.Id);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo BuscarPedidoNoErp deve consultar a Pedido a partir do webservice e caso ela não possa ser encontrada, deve enviar uma mensagem do tipo PedidoNaoEncontrada para o ator Sender")]
        public void Mensagem_BuscarPedidoTipoSaidaNoErp_Recebida_Pedido_Nao_Encontrada()
        {
            var novaMensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            var pedido = new PedidoOperacaoSaida { Id = 10 };

            _leitorPedidoOperacaoSaidaMock.Setup(method => method.Buscar("pedido_operacao_saida", pedido.Id.ToString())).Returns(null as PedidoOperacaoSaida);

            _leitorPedido.Tell(new MensagensPedido.BuscarPedidoNoErp(novaMensagemIntegracao, Operacao.Saida, pedido.Id));

            ExpectMsg<MensagensPedido.PedidoNaoEncontrado>(msg => msg.MensagemIntegracao == novaMensagemIntegracao && msg.IdPedido == pedido.Id);
        }

        #endregion
    }
}
