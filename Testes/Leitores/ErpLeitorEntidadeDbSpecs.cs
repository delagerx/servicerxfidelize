﻿using Akka.Actor;
using Akka.TestKit;
using Akka.TestKit.Xunit2;
using Core;
using Core.Exportacao.Agentes;
using Core.Integracao;
using Entidades;
using Integracao.ERP;
using Integracao.WMS.Escritores;
using Mensagens;
using Moq;
using Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Testes.Factories;
using Xunit;

namespace Testes.Agentes
{
    public class ErpLeitorEntidadeDbSpecs : TestKit
    {
        private Mock<IEntidadeRepository> _repositoryMock;
        private IActorRef _leitorEntidade;

        public ErpLeitorEntidadeDbSpecs() : base()
        {
            _repositoryMock = new Mock<IEntidadeRepository>();

            _leitorEntidade = Sys.ActorOf(Props.Create(() => new ErpLeitorEntidadeDb(_repositoryMock.Object)), "leitorEntidade"); 
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo BuscarEntidadeNoErp deve consultar a Entidade a partir do respositório e enviá-la através de uma mensagem do tipo EntidadeEncontrada para o ator Sender")]
        public void Mensagem_BuscarEntidadeNoErp_Recebida()
        {
            MensagemIntegracao novaMensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            Entidade entidade = EntidadeFactory.Gerar();

            _repositoryMock.Setup(t => t.Get(entidade.Id)).Returns(entidade);

            _leitorEntidade.Tell(new MensagensEntidade.BuscarEntidadeNoErp(novaMensagemIntegracao, entidade.Id));

            ExpectMsg<MensagensEntidade.EntidadeEncontrada>(mensagem => 
                mensagem.MensagemIntegracao == novaMensagemIntegracao && 
                mensagem.Entidade == entidade);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo BuscarEntidadeNoErp deve consultar a Entidade a partir do respositório e caso ela não possa ser encontrada, deve enviar uma mensagem do tipo EntidadeNaoEncontrada para o ator Sender")]
        public void Mensagem_BuscarEntidadeNoErp_Recebida_Entidade_Nao_Encontrada()
        {
            MensagemIntegracao novaMensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            Entidade entidade = EntidadeFactory.Gerar();

            _repositoryMock.Setup(t => t.Get(entidade.Id)).Returns(null as Entidade);

            _leitorEntidade.Tell(new MensagensEntidade.BuscarEntidadeNoErp(novaMensagemIntegracao, entidade.Id));

            ExpectMsg<MensagensEntidade.EntidadeNaoEncontrada>(mensagem =>
                mensagem.MensagemIntegracao == novaMensagemIntegracao &&
                mensagem.IdEntidade == entidade.Id);
        }
    }
}
