﻿using Akka.Actor;
using Akka.TestKit;
using Akka.TestKit.Xunit2;
using Core;
using Core.Exportacao.Agentes;
using Core.Importacao;
using Core.Importacao.Agentes;
using Entidades;
using Entidades.Enum;
using Integracao.ERP;
using Integracao.WMS.Escritores;
using Mensagens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Testes.Factories;
using Xunit;

namespace Testes.Agentes
{
    public class CoordenadorImportacaoSpecs : TestKit
    {
        private IActorRef _coordenadorImportacao;
        private IActorRef _agenteEntidade;
        private IActorRef _agenteProduto;
        private IActorRef _agentePedidoErp;
        private IActorRef _agentePedidoWms;
        private IActorRef _agenteMensagemIntegracao;
        private TestProbe _agenteMensagemIntegracaoProbe;
        private TestProbe _agenteEntidadeProbe;
        private TestProbe _agenteProdutoProbe;
        private TestProbe _agentePedidoErpProbe;
        private TestProbe _agentePedidoWmsProbe;

        public CoordenadorImportacaoSpecs() : base()
        {
            _coordenadorImportacao = Sys.ActorOf<CoordenadorImportacao>("coordenadorImportacao");

            _agenteMensagemIntegracaoProbe = CreateTestProbe("agenteMensagemIntegracao");
            _agenteMensagemIntegracao = Sys.ActorOf(Props.Create<AgenteMensagemIntegracao>(_agenteMensagemIntegracaoProbe));

            _agenteEntidadeProbe = CreateTestProbe("agenteEntidade");
            _agenteEntidade = Sys.ActorOf(Props.Create<AgenteEntidade>(_agenteEntidadeProbe));

            _agenteProdutoProbe = CreateTestProbe("agenteProduto");
            _agenteProduto = Sys.ActorOf(Props.Create<AgenteProduto>(_agenteProdutoProbe));

            _agentePedidoWmsProbe = CreateTestProbe("agentePedidoWms");
            _agentePedidoWms = Sys.ActorOf(Props.Create<AgentePedidoWms>(_agentePedidoWmsProbe));

            _agentePedidoErpProbe = CreateTestProbe("agentePedidoErp");
            _agentePedidoErp = Sys.ActorOf(Props.Create<AgentePedidoErp>(_agentePedidoErpProbe));
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo NovaMensagemEncontrada cuja tabela relacionada seja Entidade deve enviar uma mensagem do tipo EnviarEntidadeParaWms para o ator AgenteEntidade")]
        public void Mensagem_NovaMensagemEncontrada_Entidade_Recebida()
        {
            // os espaços no parâmetro tabelaRelacionada são propositais para cobrir casos identificados no banco de dados do cliente onde existem muitos registros com espaços em branco sobrando
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar(tabelaRelacionada: "Entidade             ");

            _coordenadorImportacao.Tell(new MensagensIntegracao.NovaMensagemEncontrada(mensagemIntegracao.Id, mensagemIntegracao));

            _agenteEntidadeProbe.ExpectMsg<MensagensEntidade.EnviarEntidadeParaWms>(mensagem => mensagem.MensagemIntegracao == mensagemIntegracao);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo NovaMensagemEncontrada cuja tabela relacionada seja Produto deve enviar uma mensagem do tipo EnviarProdutoParaWms para o ator AgenteProduto")]
        public void Mensagem_NovaMensagemEncontrada_Produto_Recebida()
        {
            // os espaços no parâmetro tabelaRelacionada são propositais para cobrir casos identificados no banco de dados do cliente onde existem muitos registros com espaços em branco sobrando
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar(tabelaRelacionada: "Produto            ");

            _coordenadorImportacao.Tell(new MensagensIntegracao.NovaMensagemEncontrada(mensagemIntegracao.Id, mensagemIntegracao));

            _agenteProdutoProbe.ExpectMsg<MensagensProduto.EnviarProdutoParaWms>(mensagem => mensagem.MensagemIntegracao == mensagemIntegracao);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo NovaMensagemEncontrada cuja tabela relacionada seja PedidoOperacaoEntrada deve enviar uma mensagem do tipo EnviarPedidoParaWms para o ator AgentePedido")]
        public void Mensagem_NovaMensagemEncontrada_Pedido_Recebida_PedidoOperacaoEntrada()
        {
            // os espaços no parâmetro tabelaRelacionada são propositais para cobrir casos identificados no banco de dados do cliente onde existem muitos registros com espaços em branco sobrando
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar(tabelaRelacionada: "Pedido_operacao_entrada            ");

            _coordenadorImportacao.Tell(new MensagensIntegracao.NovaMensagemEncontrada(mensagemIntegracao.Id, mensagemIntegracao));

            _agentePedidoWmsProbe.ExpectMsg<MensagensPedido.EnviarPedidoParaWms>(mensagem => mensagem.MensagemIntegracao == mensagemIntegracao);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo NovaMensagemEncontrada cuja tabela relacionada seja PedidoOperacaoSaida deve enviar uma mensagem do tipo EnviarPedidoParaWms para o ator AgentePedido")]
        public void Mensagem_NovaMensagemEncontrada_Pedido_PedidoOperacaoSaida()
        {
            // os espaços no parâmetro tabelaRelacionada são propositais para cobrir casos identificados no banco de dados do cliente onde existem muitos registros com espaços em branco sobrando
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar(tabelaRelacionada: "Pedido_operacao_saida            ");

            _coordenadorImportacao.Tell(new MensagensIntegracao.NovaMensagemEncontrada(mensagemIntegracao.Id, mensagemIntegracao));

            _agentePedidoWmsProbe.ExpectMsg<MensagensPedido.EnviarPedidoParaWms>(mensagem => mensagem.MensagemIntegracao == mensagemIntegracao);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo NovaMensagemEncontrada cuja tabela relacionada seja PedidoOperacaoMovimentacao deve enviar uma mensagem do tipo EnviarPedidoParaWms para o ator AgentePedido")]
        public void Mensagem_NovaMensagemEncontrada_Pedido_Recebida_PedidoOperacaoMovimentacao()
        {
            // os espaços no parâmetro tabelaRelacionada são propositais para cobrir casos identificados no banco de dados do cliente onde existem muitos registros com espaços em branco sobrando
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar(tabelaRelacionada: "Pedido_operacao_movimentacao            ");

            _coordenadorImportacao.Tell(new MensagensIntegracao.NovaMensagemEncontrada(mensagemIntegracao.Id, mensagemIntegracao));

            _agentePedidoWmsProbe.ExpectMsg<MensagensPedido.EnviarPedidoParaWms>(mensagem => mensagem.MensagemIntegracao == mensagemIntegracao);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo MensagensOperacao.Sucesso deve encaminhar a mesma mensagem para o ator AgenteMensagemIntegracao")]
        public void Mensagem_Sucesso_Recebida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();

            _coordenadorImportacao.Tell(new ResultadoOperacao.Sucesso(mensagemIntegracao));

            _agenteMensagemIntegracaoProbe.ExpectMsg<ResultadoOperacao.Sucesso>(mensagem => mensagem.MensagemIntegracao == mensagemIntegracao);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo MensagensOperacao.Rejeicao deve encaminhar a mesma mensagem para o ator AgenteMensagemIntegracao")]
        public void Mensagem_Rejeicao_Recebida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();

            _coordenadorImportacao.Tell(new ResultadoOperacao.Rejeicao(mensagemIntegracao, 1, "rejeição"));

            _agenteMensagemIntegracaoProbe.ExpectMsg<ResultadoOperacao.Rejeicao>(mensagem =>
                mensagem.MensagemIntegracao == mensagemIntegracao &&
                mensagem.CodigoRejeicao == 1 &&
                mensagem.MensagemRejeicao == "rejeição");
        }
    }
}
