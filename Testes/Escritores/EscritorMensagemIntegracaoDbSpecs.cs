﻿using Akka.Actor;
using Akka.TestKit;
using Akka.TestKit.Xunit2;
using Core;
using Core.Exportacao.Agentes;
using Core.Integracao;
using Entidades;
using Integracao.ERP;
using Integracao.WMS.Escritores;
using Mensagens;
using Moq;
using Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Testes.Factories;
using Xunit;

namespace Testes.Agentes
{
    public class EscritorMensagemIntegracaoDbSpecs : TestKit
    {
        private Mock<IMensagemIntegracaoRepository> _repositoryMock;
        private IActorRef _escritorMensagemIntegracao;

        public EscritorMensagemIntegracaoDbSpecs() : base()
        {
            _repositoryMock = new Mock<IMensagemIntegracaoRepository>();

            _escritorMensagemIntegracao = Sys.ActorOf(Props.Create(() => new ErpEscritorMensagemIntegracaoDb(_repositoryMock.Object)), "escritorMensagemIntegracao"); 
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo SalvarMensagemIntegracaoProcessada, caso a mensagem integração já exista na base de dados, deve atualizar as informações da mensagem a partir do repositório")]
        public void Mensagem_SalvarMensagemIntegracaoProcessada_Recebida_MensagemIntegracao_Existente()
        {
            MensagemIntegracao mensagemIntegracaoExistente = MensagemIntegracaoFactory.Gerar();

            _repositoryMock.Setup(t => t.Update(mensagemIntegracaoExistente));

            _escritorMensagemIntegracao.Tell(new MensagensIntegracao.SalvarMensagemIntegracaoProcessada(mensagemIntegracaoExistente));

            ExpectMsg<MensagensIntegracao.MensagemAtualizada>(mensagem => mensagem.MensagemIntegracao == mensagemIntegracaoExistente);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo SalvarMensagemIntegracaoProcessada, caso a mensagem integração não exista na base de dados")]
        public void Mensagem_SalvarMensagemIntegracaoProcessada_Recebida_Nova_MensagemIntegracao()
        {
            MensagemIntegracao novaMensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            
            novaMensagemIntegracao.Id = 0;

            _repositoryMock.Setup(t => t.Add(novaMensagemIntegracao));

            _escritorMensagemIntegracao.Tell(new MensagensIntegracao.SalvarMensagemIntegracaoProcessada(novaMensagemIntegracao));
        }
    }
}
