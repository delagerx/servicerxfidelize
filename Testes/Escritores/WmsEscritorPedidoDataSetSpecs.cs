﻿using Akka.Actor;
using Akka.TestKit;
using Akka.TestKit.Xunit2;
using Core;
using Core.Exportacao.Agentes;
using Core.Integracao;
using Entidades;
using Integracao.ERP;
using Integracao.WMS.Clientes;
using Integracao.WMS.Escritores;
using Mensagens;
using Moq;
using Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Testes.Factories;
using Xunit;

namespace Testes.Agentes
{
    public class WmsEscritorPedidoDataSetSpecs : TestKit
    {
        private Mock<IClienteIntegracaoPedido> _clienteIntegracaoPedidoMock;
        private IActorRef _wmsEscritorPedido;

        public WmsEscritorPedidoDataSetSpecs() : base()
        {
            _clienteIntegracaoPedidoMock = new Mock<IClienteIntegracaoPedido>();

            _wmsEscritorPedido = Sys.ActorOf(Props.Create(() => new WmsEscritorPedidoDataSet(_clienteIntegracaoPedidoMock.Object)), "wmsEscritorPedido"); 
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo SalvarPedidoNoWms deve enviar uma mensagem do tipo ResultadoOperacao.Sucesso para o ator Sender caso o Pedido tenha sido salvo com sucesso")]
        public void Mensagem_SalvarPedidoNoWms_Recebida_Sucesso()
        {
            Pedido pedido = PedidoFactory.Gerar();

            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();

            _clienteIntegracaoPedidoMock.Setup(t => t.Salvar(pedido, mensagemIntegracao));

            _wmsEscritorPedido.Tell(new MensagensPedido.SalvarPedidoNoWms(mensagemIntegracao, pedido));

            ExpectMsg<ResultadoOperacao.Sucesso>(mensagem => mensagem.MensagemIntegracao == mensagemIntegracao);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo SalvarPedidoNoWms deve enviar uma mensagem do tipo ResultadoOperacao.Rejeicao para o ator Sender caso o Pedido tenha sido salvo com rejeição")]
        public void Mensagem_SalvarPedidoNoWms_Recebida_Rejeicao()
        {
            Pedido pedido = PedidoFactory.Gerar();

            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();

            mensagemIntegracao.CodRejeicao = 1;
            mensagemIntegracao.MotivoRejeicao = "Mensagem rejeição";

            _clienteIntegracaoPedidoMock.Setup(t => t.Salvar(pedido, mensagemIntegracao));

            _wmsEscritorPedido.Tell(new MensagensPedido.SalvarPedidoNoWms(mensagemIntegracao, pedido));

            ExpectMsg<ResultadoOperacao.Rejeicao>(mensagem => 
                mensagem.MensagemIntegracao == mensagemIntegracao &&
                mensagem.CodigoRejeicao == 1 &&
                mensagem.MensagemRejeicao == "Mensagem rejeição");
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo SalvarPedidoNoWms deve enviar uma mensagem do tipo ResultadoOperacao.Erro para o ator Sender caso alguma exceção tenha sido disparada")]
        public void Mensagem_SalvarPedidoNoWms_Recebida_Erro()
        {
            Pedido pedido = PedidoFactory.Gerar();

            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            
            Exception excecao = new Exception("erro na operação");

            _clienteIntegracaoPedidoMock.Setup(t => t.Salvar(pedido, mensagemIntegracao)).Throws(excecao);

            _wmsEscritorPedido.Tell(new MensagensPedido.SalvarPedidoNoWms(mensagemIntegracao, pedido));

            ExpectMsg<ResultadoOperacao.Erro>(mensagem => mensagem.MensagemIntegracao == mensagemIntegracao && mensagem.Excecao == excecao);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo FinalizarIntegracaoDoPedidoNoWms deve enviar uma mensagem do tipo ResultadoOperacao.Sucesso para o ator Sender caso o Pedido tenha sido integrado com sucesso")]
        public void Mensagem_FinalizarIntegracaoDoPedidoNoWms_Recebida_Sucesso()
        {
            Pedido pedido = PedidoFactory.Gerar();

            pedido.IdWms = 100;

            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();

            _clienteIntegracaoPedidoMock.Setup(t => t.ConcluirPedido(mensagemIntegracao, pedido.IdWms)).Returns(true);

            _wmsEscritorPedido.Tell(new MensagensPedido.FinalizarIntegracaoDoPedidoNoWms(mensagemIntegracao, pedido.IdWms));

            ExpectMsg<ResultadoOperacao.Sucesso>(mensagem => mensagem.MensagemIntegracao == mensagemIntegracao);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo FinalizarIntegracaoDoPedidoNoWms deve enviar uma mensagem do tipo ResultadoOperacao.Rejeicao para o ator Sender caso o webservice tenha retornado False ao integrar o pedido")]
        public void Mensagem_FinalizarIntegracaoDoPedidoNoWms_Recebida_Rejeicao()
        {
            Pedido pedido = PedidoFactory.Gerar();

            pedido.IdWms = 100;

            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();

            _clienteIntegracaoPedidoMock.Setup(t => t.ConcluirPedido(mensagemIntegracao, pedido.IdWms)).Returns(false);

            _wmsEscritorPedido.Tell(new MensagensPedido.FinalizarIntegracaoDoPedidoNoWms(mensagemIntegracao, pedido.IdWms));

            ExpectMsg<ResultadoOperacao.Erro>(mensagem =>
                mensagem.MensagemIntegracao == mensagemIntegracao &&
                mensagem.Excecao.Message == "O webservice retornou false ao tentar concluir a integração do pedido.");
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo FinalizarIntegracaoDoPedidoNoWms deve enviar uma mensagem do tipo ResultadoOperacao.Erro para o ator Sender caso alguma exceção tenha sido disparada")]
        public void Mensagem_FinalizarIntegracaoDoPedidoNoWms_Recebida_Erro()
        {
            Pedido pedido = PedidoFactory.Gerar();

            pedido.IdWms = 100;

            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();

            Exception excecao = new Exception("erro na operação");

            _clienteIntegracaoPedidoMock.Setup(t => t.ConcluirPedido(mensagemIntegracao, pedido.IdWms)).Throws(excecao);

            _wmsEscritorPedido.Tell(new MensagensPedido.FinalizarIntegracaoDoPedidoNoWms(mensagemIntegracao, pedido.IdWms));

            ExpectMsg<ResultadoOperacao.Erro>(mensagem => mensagem.MensagemIntegracao == mensagemIntegracao && mensagem.Excecao == excecao);
        }
    }
}
