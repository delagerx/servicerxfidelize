﻿using Akka.Actor;
using Akka.TestKit;
using Akka.TestKit.Xunit2;
using Core;
using Core.Exportacao.Agentes;
using Core.Integracao;
using Entidades;
using Entidades.Enum;
using Integracao.ERP;
using Integracao.WMS.Escritores;
using Mensagens;
using Moq;
using Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Testes.Factories;
using Xunit;

namespace Testes.Agentes
{
    public class ErpEscritorPedidoDbSpecs : TestKit
    {
        private Mock<IPedidoOperacaoEntradaRepository> _pedidoOperacaoEntradaRepositorioMock;
        private Mock<IPedidoOperacaoSaidaRepository> _pedidoOperacaoSaidaRepositorioMock;
        private Mock<IPedidoOperacaoMovimentacaoRepository> _pedidoOperacaoMovimentacaoRepositorioMock;
        private Mock<IEntidadeRepository> _entidadeRepositorioMock;
        private Mock<IProdutoRepository> _produtoRepositorioMock;
        
        private IActorRef _erpEscritorPedido;

        public ErpEscritorPedidoDbSpecs() : base()
        {
            _pedidoOperacaoEntradaRepositorioMock = new Mock<IPedidoOperacaoEntradaRepository>();
            _pedidoOperacaoSaidaRepositorioMock = new Mock<IPedidoOperacaoSaidaRepository>();
            _pedidoOperacaoMovimentacaoRepositorioMock = new Mock<IPedidoOperacaoMovimentacaoRepository>();
            _entidadeRepositorioMock = new Mock<IEntidadeRepository>();
            _produtoRepositorioMock = new Mock<IProdutoRepository>();

            _erpEscritorPedido = Sys.ActorOf(
                Props.Create(() => 
                    new ErpEscritorPedidoDb(
                        _pedidoOperacaoEntradaRepositorioMock.Object,
                        _pedidoOperacaoMovimentacaoRepositorioMock.Object,
                        _pedidoOperacaoSaidaRepositorioMock.Object)
                ),
                "erpEscritorPedido");
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo SalvarPedidoNoErp com um pedido do tipo PedidoOperacaoEntrada, deve retornar uma mensagem do tipo PedidoSalvoNoErp para o ator Sender")]
        public void Mensagem_SalvarPedidoNoErp_Recebida_PedidoOperacaoEntrada()
        {
            PedidoOperacaoEntrada pedido = new PedidoOperacaoEntrada();
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar(TabelaRelacionada.PedidoOperacaoSaida, Origem.WMSRX);
            Entidade entidade = EntidadeFactory.Gerar();
            pedido.Entidade = entidade;

            _pedidoOperacaoEntradaRepositorioMock.Setup(t => t.Update(pedido));
            _entidadeRepositorioMock.Setup(t => t.Items).Returns(new List<Entidade>() { entidade }.AsQueryable());

            _erpEscritorPedido.Tell(new MensagensPedido.SalvarPedidoNoErp(pedido, mensagemIntegracao));

            ExpectMsg<MensagensPedido.PedidoSalvoNoErp>(mensagem => mensagem.Pedido == pedido);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo SalvarPedidoNoErp com um pedido do tipo PedidoOperacaoSaida, deve retornar uma mensagem do tipo PedidoSalvoNoErp para o ator Sender")]
        public void Mensagem_SalvarPedidoNoErp_Recebida_PedidoOperacaoSaida()
        {
            PedidoOperacaoSaida pedido = new PedidoOperacaoSaida();
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar(TabelaRelacionada.PedidoOperacaoSaida, Origem.WMSRX);
            Entidade entidade = EntidadeFactory.Gerar();
            pedido.Entidade = entidade;

            _pedidoOperacaoSaidaRepositorioMock.Setup(t => t.Update(pedido));
            _entidadeRepositorioMock.Setup(t => t.Items).Returns(new List<Entidade>() { entidade }.AsQueryable());

            _erpEscritorPedido.Tell(new MensagensPedido.SalvarPedidoNoErp(pedido, mensagemIntegracao));

            ExpectMsg<MensagensPedido.PedidoSalvoNoErp>(mensagem => mensagem.Pedido == pedido);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo SalvarPedidoNoErp com um pedido do tipo PedidoOperacaoMovimentacao, deve retornar uma mensagem do tipo PedidoSalvoNoErp para o ator Sender")]
        public void Mensagem_SalvarPedidoNoErp_Recebida_PedidoOperacaoMovimentacao()
        {
            PedidoOperacaoMovimentacao pedido = new PedidoOperacaoMovimentacao();
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar(TabelaRelacionada.PedidoOperacaoSaida, Origem.WMSRX);
            Entidade entidade = EntidadeFactory.Gerar();
            pedido.Entidade = entidade;

            _pedidoOperacaoMovimentacaoRepositorioMock.Setup(t => t.Update(pedido));
            _entidadeRepositorioMock.Setup(t => t.Items).Returns(new List<Entidade>() { entidade }.AsQueryable());

            _erpEscritorPedido.Tell(new MensagensPedido.SalvarPedidoNoErp(pedido, mensagemIntegracao));

            ExpectMsg<MensagensPedido.PedidoSalvoNoErp>(mensagem => mensagem.Pedido == pedido);
        }
    }
}
