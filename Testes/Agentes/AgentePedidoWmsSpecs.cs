﻿using Akka.Actor;
using Akka.TestKit;
using Akka.TestKit.Xunit2;
using Core.Importacao;
using Core.Importacao.Agentes;
using Entidades;
using Entidades.Enum;
using Integracao.ERP;
using Integracao.WMS.Escritores;
using Mensagens;
using System;
using Core.Validacao.Infraestrutura.Interfaces;
using Moq;
using Testes.Factories;
using Xunit;

namespace Testes.Agentes
{
    public class AgentePedidoWmsSpecs : TestKit
    {
        private IActorRef _agentePedidoWms;
        private IActorRef _erpLeitorPedido;
        private IActorRef _wmsEscritorPedido;
        private IActorRef _coordenadorImportacao;
        private TestProbe _erpLeitorPedidoProbe;
        private TestProbe _wmsEscritorPedidoProbe;
        private TestProbe _coordenadorImportacaoProbe;
        private Mock<IImportacaoValidator<Pedido>> _pedidoValidadorMock;

        public AgentePedidoWmsSpecs() : base()
        {
            _pedidoValidadorMock = new Mock<IImportacaoValidator<Pedido>>();
            _agentePedidoWms = Sys.ActorOf(Props.Create( () => new AgentePedidoWms(_pedidoValidadorMock.Object)));

            _erpLeitorPedidoProbe = CreateTestProbe("erpLeitorPedido");
            _erpLeitorPedido = Sys.ActorOf(Props.Create<ErpLeitorPedidoDb>(_erpLeitorPedidoProbe));

            _wmsEscritorPedidoProbe = CreateTestProbe("wmsEscritorPedido");
            _wmsEscritorPedido = Sys.ActorOf(Props.Create<WmsEscritorPedidoDataSet>(_wmsEscritorPedidoProbe));

            _coordenadorImportacaoProbe = CreateTestProbe("coordenadorImportacao");
            _coordenadorImportacao = Sys.ActorOf(Props.Create<CoordenadorImportacao>(_coordenadorImportacaoProbe));
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo EnviarPedidoParaWms, com tabela relacionada do tipo pedido_operacao_entrada, deve enviar uma mensagem do tipo BuscarPedidoNoErp para o ator ErpLeitorPedidoRest")]
        public void Mensagem_EnviarPedidoParaWms_Recebida_PedidoOperacaoEntrada()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar(tabelaRelacionada: "PEDIDO_OPERACAO_ENTRADA");

            _agentePedidoWms.Tell(new MensagensPedido.EnviarPedidoParaWms(mensagemIntegracao));

            _erpLeitorPedidoProbe.ExpectMsg<MensagensPedido.BuscarPedidoNoErp>(mensagem => 
                mensagem.MensagemIntegracao == mensagemIntegracao && 
                mensagem.Operacao == Operacao.Entrada &&
                mensagem.IdPedido == mensagemIntegracao.IdTabelaRelacionada);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo EnviarPedidoParaWms, com tabela relacionada do tipo pedido_operacao_saida, deve enviar uma mensagem do tipo BuscarPedidoNoErp para o ator ErpLeitorPedidoRest")]
        public void Mensagem_EnviarPedidoParaWms_Recebida_PedidoOperacaoSaida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar(tabelaRelacionada: "PEDIDO_OPERACAO_SAIDA");

            _agentePedidoWms.Tell(new MensagensPedido.EnviarPedidoParaWms(mensagemIntegracao));

            _erpLeitorPedidoProbe.ExpectMsg<MensagensPedido.BuscarPedidoNoErp>(mensagem =>
                mensagem.MensagemIntegracao == mensagemIntegracao &&
                mensagem.Operacao == Operacao.Saida &&
                mensagem.IdPedido == mensagemIntegracao.IdTabelaRelacionada);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo EnviarPedidoParaWms, com tabela relacionada do tipo pedido_operacao_movimentacao, deve enviar uma mensagem do tipo BuscarPedidoNoErp para o ator ErpLeitorPedidoRest")]
        public void Mensagem_EnviarPedidoParaWms_Recebida_PedidoOperacaoMovimentacao()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar(tabelaRelacionada: "PEDIDO_OPERACAO_MOVIMENTACAO");

            _agentePedidoWms.Tell(new MensagensPedido.EnviarPedidoParaWms(mensagemIntegracao));

            _erpLeitorPedidoProbe.ExpectMsg<MensagensPedido.BuscarPedidoNoErp>(mensagem =>
                mensagem.MensagemIntegracao == mensagemIntegracao &&
                mensagem.Operacao == Operacao.Movimentacao &&
                mensagem.IdPedido == mensagemIntegracao.IdTabelaRelacionada);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo PedidoEncontrado deve enviar uma mensagem do tipo SalvarPedidoNoWms para o ator WmsEscritorPedidoDataSet")]
        public void Mensagem_PedidoEncontrado_Recebida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            Pedido pedido = PedidoFactory.Gerar();

            pedido.Entidade = new Entidade() { Id = 1 };
            pedido.IdEntidade = 1;

            _agentePedidoWms.Tell(new MensagensPedido.PedidoEncontrado(mensagemIntegracao, pedido));

            _wmsEscritorPedidoProbe.ExpectMsg<MensagensPedido.SalvarPedidoNoWms>(mensagem =>
                mensagem.MensagemIntegracao == mensagemIntegracao &&
                mensagem.Pedido == pedido);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo MensagensOperacao.Sucesso deve encaminhar a mesma mensagem para o ator CoordenadorImportacao")]
        public void Mensagem_Sucesso_Recebida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();

            _agentePedidoWms.Tell(new ResultadoOperacao.Sucesso(mensagemIntegracao));

            _coordenadorImportacaoProbe.ExpectMsg<ResultadoOperacao.Sucesso>(mensagem => mensagem.MensagemIntegracao == mensagemIntegracao);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo MensagensOperacao.Erro deve encaminhar a mesma mensagem para o ator CoordenadorImportacao")]
        public void Mensagem_Erro_Recebida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            Exception excecao = new Exception("erro na operação");

            _agentePedidoWms.Tell(new ResultadoOperacao.Erro(mensagemIntegracao, excecao));

            _coordenadorImportacaoProbe.ExpectMsg<ResultadoOperacao.Erro>(mensagem => mensagem.MensagemIntegracao == mensagemIntegracao && mensagem.Excecao == excecao);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo MensagensOperacao.Rejeicao deve encaminhar a mesma mensagem para o ator CoordenadorImportacao")]
        public void Mensagem_Rejeicao_Recebida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();

            _agentePedidoWms.Tell(new ResultadoOperacao.Rejeicao(mensagemIntegracao, 1, "rejeição"));

            _coordenadorImportacaoProbe.ExpectMsg<ResultadoOperacao.Rejeicao>(mensagem => 
                mensagem.MensagemIntegracao == mensagemIntegracao &&
                mensagem.CodigoRejeicao == 1 &&
                mensagem.MensagemRejeicao == "rejeição");
        }
    }
}
