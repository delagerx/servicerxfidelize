﻿using Akka.Actor;
using Akka.TestKit;
using Akka.TestKit.Xunit2;
using Core;
using Core.Exportacao.Agentes;
using Core.Importacao;
using Core.Integracao;
using Entidades;
using Entidades.Enum;
using Integracao.ERP;
using Integracao.WMS.Escritores;
using Mensagens;
using Moq;
using Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Testes.Factories;
using Xunit;

namespace Testes.Agentes
{
    public class AgenteMensagemIntegracaoSpecs : TestKit
    {
        private Mock<IMensagemIntegracaoRepository> _repositoryMock;
        private IActorRef _agenteMensagemIntegracao;
        private IActorRef _leitorMensagemIntegracao;
        private IActorRef _escritorMensagemIntegracao;
        private IActorRef _coordenadorImportacao;
        private TestProbe _leitorMensagemIntegracaoProbe;
        private TestProbe _escritorMensagemIntegracaoProbe;
        private TestProbe _coordenadorImportacaoProbe;

        public AgenteMensagemIntegracaoSpecs() : base()
        {
            _repositoryMock = new Mock<IMensagemIntegracaoRepository>();

            _agenteMensagemIntegracao = Sys.ActorOf(Props.Create(() => new AgenteMensagemIntegracao()), "agenteMensagemIntegracao");

            _leitorMensagemIntegracaoProbe = CreateTestProbe("leitorMensagemIntegracao");
            _leitorMensagemIntegracao = _leitorMensagemIntegracaoProbe.ActorOf(Props.Create(() => new ErpLeitorMensagemIntegracaoDb(_repositoryMock.Object))); 

            _escritorMensagemIntegracaoProbe = CreateTestProbe("escritorMensagemIntegracao");
            _escritorMensagemIntegracao = _escritorMensagemIntegracaoProbe.ActorOf(Props.Create(() => new ErpEscritorMensagemIntegracaoDb(_repositoryMock.Object))); 

            _coordenadorImportacaoProbe = CreateTestProbe("coordenadorImportacao");
            _coordenadorImportacao = Sys.ActorOf(Props.Create<CoordenadorImportacao>(_coordenadorImportacaoProbe));
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo BuscarNovasMensagens deve encaminhar a mesma mensagem para o ator LeitorMensagemIntegracaoDb")]
        public void Mensagem_BuscarNovasMensagens_Recebida()
        {
            MensagensIntegracao.BuscarNovasMensagens novaMensagem = new MensagensIntegracao.BuscarNovasMensagens();

            _agenteMensagemIntegracao.Tell(novaMensagem);

            _leitorMensagemIntegracaoProbe.ExpectMsg<MensagensIntegracao.BuscarNovasMensagens>(mensagem => mensagem == novaMensagem);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo NovaMensagemEncontrada deve encaminhar a mesma mensagem para o ator CoordenadorImportacao")]
        public void Mensagem_NovaMensagemEncontrada_Recebida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();

            _agenteMensagemIntegracao.Tell(new MensagensIntegracao.NovaMensagemEncontrada(mensagemIntegracao.Id, mensagemIntegracao));

            _coordenadorImportacaoProbe.ExpectMsg<MensagensIntegracao.NovaMensagemEncontrada>(mensagem => 
                mensagem.IdMensagem == mensagemIntegracao.Id  &&
                mensagem.MensagemIntegracao == mensagemIntegracao);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo ResultadoOperacao.Sucesso deve enviar uma mensagem do tipo SalvarMensagemIntegracaoProcessada para o ator EscritorMensagemIntegracaoDb")]
        public void Mensagem_ResultadoOperacao_Sucesso_Recebida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();

            // coloca o estado inicial da mensagem como rejeitada para garantir que sempre que a mensagem for processada com sucesso o flag de rejeição será definido como false
            mensagemIntegracao.Rejeitado = (int)Rejeitado.Sim;

            _repositoryMock.Setup(t => t.Get(mensagemIntegracao.Id)).Returns(mensagemIntegracao);

            _agenteMensagemIntegracao.Tell(new ResultadoOperacao.Sucesso(mensagemIntegracao));

            _escritorMensagemIntegracaoProbe.ExpectMsg<MensagensIntegracao.SalvarMensagemIntegracaoProcessada>(mensagem => mensagem.MensagemIntegracao.DataProcessamento != null);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo ResultadoOperacao.Sucesso deve enviar uma mensagem do tipo SalvarMensagemIntegracaoProcessada para o ator EscritorMensagemIntegracaoDb")]
        public void Mensagem_ResultadoOperacao_Rejeicao_Recebida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();

            _repositoryMock.Setup(t => t.Get(mensagemIntegracao.Id)).Returns(mensagemIntegracao);

            int codigoRejeicao = 100000;
            string mensagemRejeicao = "Operação Rejeitada";

            _agenteMensagemIntegracao.Tell(new ResultadoOperacao.Rejeicao(mensagemIntegracao, codigoRejeicao, mensagemRejeicao));

            _escritorMensagemIntegracaoProbe.ExpectMsg<MensagensIntegracao.SalvarMensagemIntegracaoProcessada>(mensagem => 
                mensagem.MensagemIntegracao.Id == mensagemIntegracao.Id &&
                mensagem.MensagemIntegracao.DataProcessamento != null &&
                mensagem.MensagemIntegracao.Rejeitado == (int)Rejeitado.Nao &&
                mensagem.MensagemIntegracao.CodRejeicao == codigoRejeicao &&
                mensagem.MensagemIntegracao.MotivoRejeicao == mensagemRejeicao);

            codigoRejeicao = 200000;

            _agenteMensagemIntegracao.Tell(new ResultadoOperacao.Rejeicao(mensagemIntegracao, codigoRejeicao, mensagemRejeicao));

            _escritorMensagemIntegracaoProbe.ExpectMsg<MensagensIntegracao.SalvarMensagemIntegracaoProcessada>(mensagem =>
                mensagem.MensagemIntegracao.Id == mensagemIntegracao.Id &&
                mensagem.MensagemIntegracao.DataProcessamento != null &&
                mensagem.MensagemIntegracao.Rejeitado == (int)Rejeitado.Sim &&
                mensagem.MensagemIntegracao.CodRejeicao == codigoRejeicao &&
                mensagem.MensagemIntegracao.MotivoRejeicao == mensagemRejeicao);
        }
    }
}
