﻿using Akka.Actor;
using Akka.TestKit;
using Akka.TestKit.Xunit2;
using Core;
using Core.Exportacao.Agentes;
using Core.Importacao;
using Core.Importacao.Agentes;
using Entidades;
using Integracao.ERP;
using Integracao.WMS.Escritores;
using Mensagens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Testes.Factories;
using Xunit;

namespace Testes.Agentes
{
    public class AgenteProdutoSpecs : TestKit
    {
        private IActorRef _agenteProduto;
        private IActorRef _erpLeitorProduto;
        private IActorRef _wmsEscritorProduto;
        private IActorRef _coordenadorImportacao;
        private TestProbe _erpLeitorProdutoProbe;
        private TestProbe _wmsEscritorProdutoProbe;
        private TestProbe _coordenadorImportacaoProbe;

        public AgenteProdutoSpecs() : base()
        {
            _agenteProduto = Sys.ActorOf<AgenteProduto>("agenteProduto");

            _erpLeitorProdutoProbe = CreateTestProbe("erpLeitorProduto");
            _erpLeitorProduto = Sys.ActorOf(Props.Create<ErpLeitorProdutoDb>(_erpLeitorProdutoProbe));

            _wmsEscritorProdutoProbe = CreateTestProbe("wmsEscritorProduto");
            _wmsEscritorProduto = Sys.ActorOf(Props.Create<WmsEscritorProdutoDataSet>(_wmsEscritorProdutoProbe));

            _coordenadorImportacaoProbe = CreateTestProbe("coordenadorImportacao");
            _coordenadorImportacao = Sys.ActorOf(Props.Create<CoordenadorImportacao>(_coordenadorImportacaoProbe));
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo EnviarProdutoParaWms deve enviar uma mensagem do tipo BuscarProdutoNoErp para o ator ErpLeitorProdutoRest")]
        public void Mensagem_EnviarProdutoParaWms_Recebida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();

            _agenteProduto.Tell(new MensagensProduto.EnviarProdutoParaWms(mensagemIntegracao));

            _erpLeitorProdutoProbe.ExpectMsg<MensagensProduto.BuscarProdutoNoErp>(mensagem => 
                mensagem.MensagemIntegracao == mensagemIntegracao && 
                mensagem.IdProduto == mensagemIntegracao.IdTabelaRelacionada);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo ProdutoEncontrado deve enviar uma mensagem do tipo SalvarProdutoNoWms para o ator WmsEscritorProdutoDataSet")]
        public void Mensagem_ProdutoEncontrado_Recebida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            Produto produto = ProdutoFactory.Gerar();

            _agenteProduto.Tell(new MensagensProduto.ProdutoEncontrado(mensagemIntegracao, produto));

            _wmsEscritorProdutoProbe.ExpectMsg<MensagensProduto.SalvarProdutoNoWms>(mensagem =>
                mensagem.MensagemIntegracao == mensagemIntegracao &&
                mensagem.Produto == produto);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo MensagensOperacao.Sucesso deve encaminhar a mesma mensagem para o ator CoordenadorImportacao")]
        public void Mensagem_Sucesso_Recebida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();

            _agenteProduto.Tell(new ResultadoOperacao.Sucesso(mensagemIntegracao));

            _coordenadorImportacaoProbe.ExpectMsg<ResultadoOperacao.Sucesso>(mensagem => mensagem.MensagemIntegracao == mensagemIntegracao);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo MensagensOperacao.Erro deve encaminhar a mesma mensagem para o ator CoordenadorImportacao")]
        public void Mensagem_Erro_Recebida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            Exception excecao = new Exception("erro na operação");

            _agenteProduto.Tell(new ResultadoOperacao.Erro(mensagemIntegracao, excecao));

            _coordenadorImportacaoProbe.ExpectMsg<ResultadoOperacao.Erro>(mensagem => mensagem.MensagemIntegracao == mensagemIntegracao && mensagem.Excecao == excecao);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo MensagensOperacao.Rejeicao deve encaminhar a mesma mensagem para o ator CoordenadorImportacao")]
        public void Mensagem_Rejeicao_Recebida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();

            _agenteProduto.Tell(new ResultadoOperacao.Rejeicao(mensagemIntegracao, 1, "rejeição"));

            _coordenadorImportacaoProbe.ExpectMsg<ResultadoOperacao.Rejeicao>(mensagem => 
                mensagem.MensagemIntegracao == mensagemIntegracao &&
                mensagem.CodigoRejeicao == 1 &&
                mensagem.MensagemRejeicao == "rejeição");
        }
    }
}
