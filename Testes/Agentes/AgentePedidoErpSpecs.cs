﻿using Akka.Actor;
using Akka.TestKit;
using Akka.TestKit.Xunit2;
using Core;
using Core.Exportacao.Agentes;
using Core.Integracao;
using Entidades;
using Integracao.ERP;
using Integracao.WMS.Escritores;
using Mensagens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Testes.Factories;
using Xunit;

namespace Testes.Agentes
{
    public class AgentePedidoErpSpecs : TestKit
    {
        private IActorRef _agentePedidoErp;
        private IActorRef _agenteMensagemIntegracao;
        private IActorRef _wmsLeitorPedido;
        private IActorRef _erpEscritorPedido;
        private IActorRef _escritorMensagemIntegracao;
        private IActorRef _wmsEscritorPedido;
        private TestProbe _agentePedidoErpProbe;
        private TestProbe _agenteMensagemIntegracaoProbe;
        private TestProbe _wmsLeitorPedidoProbe;
        private TestProbe _erpEscritorPedidoProbe;
        private TestProbe _escritorMensagemIntegracaoProbe;
        private TestProbe _wmsEscritorPedidoProbe;

        public AgentePedidoErpSpecs() : base()
        {
            _agentePedidoErpProbe = CreateTestProbe("agentePedidoErp");
            _agentePedidoErp = Sys.ActorOf(Props.Create<AgentePedidoErp>(_agentePedidoErpProbe));

            _agenteMensagemIntegracaoProbe = CreateTestProbe("agenteMensagemIntegracao");
            _agenteMensagemIntegracao = Sys.ActorOf(Props.Create<AgenteMensagemIntegracao>(_agenteMensagemIntegracaoProbe));

            _wmsLeitorPedidoProbe = CreateTestProbe("wmsLeitorPedido");
            _wmsLeitorPedido = Sys.ActorOf(Props.Create<WmsLeitorPedidoDataSet>(_wmsLeitorPedidoProbe));

            _erpEscritorPedidoProbe = CreateTestProbe("erpEscritorPedido");
            _erpEscritorPedido = Sys.ActorOf(Props.Create<ErpEscritorPedidoDb>(_erpEscritorPedidoProbe));

            _escritorMensagemIntegracaoProbe = CreateTestProbe("escritorMensagemIntegracao");
            _escritorMensagemIntegracao = Sys.ActorOf(Props.Create<ErpEscritorMensagemIntegracaoDb>(_escritorMensagemIntegracaoProbe));

            _wmsEscritorPedidoProbe = CreateTestProbe("wmsEscritorPedido");
            _wmsEscritorPedido = Sys.ActorOf(Props.Create<WmsEscritorPedidoDataSet>(_wmsEscritorPedidoProbe));
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo VerificarNovosPedidos deve encaminhar a mesma mensagem para o ator WmsLeitorPedidoDataSet")]
        public void Mensagem_VerificarNovosPedidos_Recebida()
        {
            _agentePedidoErp.Tell(new MensagensPedido.VerificarNovosPedidos());

            _wmsLeitorPedidoProbe.ExpectMsg<MensagensPedido.VerificarNovosPedidos>(mensagem => mensagem == mensagem);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo NovosPedidosEncontrados deve enviar uma mensagem do tipo SalvarPedidoNoErp para o ator ErpEscritorPedidoDb para cada pedido encontrado")]
        public void Mensagem_NovosPedidosEncontrados_Recebida()
        {
            IList<Pedido> pedidos = new List<Pedido>();

            pedidos.Add(PedidoFactory.Gerar(1));

            _agentePedidoErp.Tell(new MensagensPedido.NovosPedidosEncontrados(pedidos), _erpEscritorPedidoProbe);

            _erpEscritorPedidoProbe.ExpectMsg<MensagensPedido.SalvarPedidoNoErp>(mensagem => mensagem.Pedido == pedidos[0]);

        }

        //[Fact(DisplayName = "Ao receber uma mensagem do tipo MensagemCriada deve enviar uma mensagem do tipo FinalizarIntegracaoDoPedidoNoWms para o ator WmsEscritorPedido")]
        //public void Mensagem_MensagemCriada_Recebida()
        //{
        //    MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();

        //    _agentePedidoErp.Tell(new MensagensIntegracao.MensagemCriada(mensagemIntegracao));

        //    _wmsEscritorPedidoProbe.ExpectMsg<MensagensPedido.FinalizarIntegracaoDoPedidoNoWms>(mensagem =>
        //        mensagem.IdPedido == mensagemIntegracao.IdTabelaRelacionada &&
        //        mensagem.CodOperacaoLogisticaPolo == mensagemIntegracao.cod
        //        );
        //}

        [Fact(DisplayName = "Ao receber uma mensagem do tipo ResultadoOperacao.Sucesso deve encaminhar a mesma mensagem para o ator AgenteMensagemIntegracao")]
        public void Mensagem_Sucesso_Recebida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            _agentePedidoErp.Tell(new ResultadoOperacao.Sucesso(mensagemIntegracao), _agenteMensagemIntegracaoProbe);
        }
    }
}
