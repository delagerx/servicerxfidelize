﻿using Akka.Actor;
using Akka.TestKit;
using Akka.TestKit.Xunit2;
using Core;
using Core.Exportacao.Agentes;
using Core.Importacao;
using Core.Importacao.Agentes;
using Entidades;
using Integracao.ERP;
using Integracao.WMS.Escritores;
using Mensagens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Validacao.Infraestrutura.Interfaces;
using Moq;
using Testes.Factories;
using Xunit;

namespace Testes.Agentes
{
    public class AgenteEntidadeSpecs : TestKit
    {
        private IActorRef _agenteEntidade;
        private IActorRef _erpLeitorEntidade;
        private IActorRef _wmsEscritorEntidade;
        private IActorRef _coordenadorImportacao;
        private TestProbe _erpLeitorEntidadeProbe;
        private TestProbe _wmsEscritorEntidadeProbe;
        private TestProbe _coordenadorImportacaoProbe;
        private Mock<IImportacaoValidator<Entidade>> _entidadeValidadorMock;

        public AgenteEntidadeSpecs() : base()
        {
            _entidadeValidadorMock = new Mock<IImportacaoValidator<Entidade>>();
            _agenteEntidade = Sys.ActorOf(Props.Create( () => new AgenteEntidade(_entidadeValidadorMock.Object)));

            _erpLeitorEntidadeProbe = CreateTestProbe("erpLeitorEntidade");
            _erpLeitorEntidade = Sys.ActorOf(Props.Create<ErpLeitorEntidadeDb>(_erpLeitorEntidadeProbe));

            _wmsEscritorEntidadeProbe = CreateTestProbe("wmsEscritorEntidade");
            _wmsEscritorEntidade = Sys.ActorOf(Props.Create<WmsEscritorEntidadeDataSet>(_wmsEscritorEntidadeProbe));

            _coordenadorImportacaoProbe = CreateTestProbe("coordenadorImportacao");
            _coordenadorImportacao = Sys.ActorOf(Props.Create<CoordenadorImportacao>(_coordenadorImportacaoProbe));
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo EnviarEntidadeParaWms deve enviar uma mensagem do tipo BuscarEntidadeNoErp para o ator ErpLeitorEntidadeRest")]
        public void Mensagem_EnviarEntidadeParaWms_Recebida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();

            _agenteEntidade.Tell(new MensagensEntidade.EnviarEntidadeParaWms(mensagemIntegracao));

            _erpLeitorEntidadeProbe.ExpectMsg<MensagensEntidade.BuscarEntidadeNoErp>(mensagem => 
                mensagem.MensagemIntegracao == mensagemIntegracao && 
                mensagem.IdEntidade == mensagemIntegracao.IdTabelaRelacionada);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo EntidadeEncontrada deve enviar uma mensagem do tipo SalvarEntidadeNoWms para o ator WmsEscritorEntidadeDataSet")]
        public void Mensagem_EntidadeEncontrada_Recebida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            
            Entidade entidade = EntidadeFactory.Gerar();

            _entidadeValidadorMock.Setup(v => v.Validar(entidade));

            _agenteEntidade.Tell(new MensagensEntidade.EntidadeEncontrada(mensagemIntegracao, entidade));

            _wmsEscritorEntidadeProbe.ExpectMsg<MensagensEntidade.SalvarEntidadeNoWms>(mensagem =>
                mensagem.MensagemIntegracao == mensagemIntegracao &&
                mensagem.Entidade == entidade);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo MensagensOperacao.Sucesso deve encaminhar a mesma mensagem para o ator CoordenadorImportacao")]
        public void Mensagem_Sucesso_Recebida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();

            _agenteEntidade.Tell(new ResultadoOperacao.Sucesso(mensagemIntegracao));

            _coordenadorImportacaoProbe.ExpectMsg<ResultadoOperacao.Sucesso>(mensagem => mensagem.MensagemIntegracao == mensagemIntegracao);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo MensagensOperacao.Erro deve encaminhar a mesma mensagem para o ator CoordenadorImportacao")]
        public void Mensagem_Erro_Recebida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();
            Exception excecao = new Exception("erro na operação");

            _agenteEntidade.Tell(new ResultadoOperacao.Erro(mensagemIntegracao, excecao));

            _coordenadorImportacaoProbe.ExpectMsg<ResultadoOperacao.Erro>(mensagem => mensagem.MensagemIntegracao == mensagemIntegracao && mensagem.Excecao == excecao);
        }

        [Fact(DisplayName = "Ao receber uma mensagem do tipo MensagensOperacao.Rejeicao deve encaminhar a mesma mensagem para o ator CoordenadorImportacao")]
        public void Mensagem_Rejeicao_Recebida()
        {
            MensagemIntegracao mensagemIntegracao = MensagemIntegracaoFactory.Gerar();

            _agenteEntidade.Tell(new ResultadoOperacao.Rejeicao(mensagemIntegracao, 1, "rejeição"));

            _coordenadorImportacaoProbe.ExpectMsg<ResultadoOperacao.Rejeicao>(mensagem => 
                mensagem.MensagemIntegracao == mensagemIntegracao &&
                mensagem.CodigoRejeicao == 1 &&
                mensagem.MensagemRejeicao == "rejeição");
        }
    }
}
