﻿using Entidades;
using Core.Validacao.Infraestrutura.Interfaces;
using System.Linq;
using Core.Validacao.Infraestrutura.Excecoes;
using Core.Validacao.Infraestrutura.Mensagens;

namespace Core.Validacao.Importacao
{
    public class PedidoImportacaoValidator : IImportacaoValidator<Pedido>
    {
        public void Validar(Pedido pedido)
        {
            PedidoPossuiEntidadeAssociada(pedido);
            ItensDoPedidoPossuemProdutoAssociado(pedido);
        }

        private void PedidoPossuiEntidadeAssociada(Pedido pedido)
        {
            if (pedido.Entidade == null || pedido.IdEntidade == 0)
                throw new ImportacaoInvalidaException(MensagensValidacao.PEDIDO_NAO_POSSUI_ENTIDADE_ASSOCIADA);
        }

        private void ItensDoPedidoPossuemProdutoAssociado(Pedido pedido)
        {
            bool pedidoPossuiItensSemProdutoAssociado = false;

            if (pedido is PedidoOperacaoEntrada)
                pedidoPossuiItensSemProdutoAssociado = ((PedidoOperacaoEntrada)pedido).PedidoItemOperacaoEntradaList.Any(item => item.Produto == null || item.IdProduto == 0);
            else if (pedido is PedidoOperacaoMovimentacao)
                pedidoPossuiItensSemProdutoAssociado = ((PedidoOperacaoMovimentacao)pedido).PedidoItemOperacaoMovimentacaoList.Any(item => item.Produto == null || item.IdProduto == 0);
            else if (pedido is PedidoOperacaoSaida)
                pedidoPossuiItensSemProdutoAssociado = ((PedidoOperacaoSaida)pedido).PedidoItemOperacaoSaidaList.Any(item => item.Produto == null || item.IdProduto == 0);

            if (pedidoPossuiItensSemProdutoAssociado)
                throw new ImportacaoInvalidaException(MensagensValidacao.PEDIDO_POSSUI_ITENS_SEM_PRODUTO_ASSOCIADO);
        }
    }
}
