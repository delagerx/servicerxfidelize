﻿using System;
using Entidades;
using Persistencia;
using Core.Validacao.Infraestrutura.Interfaces;
using System.Linq;
using Core.Validacao.Infraestrutura.Excecoes;
using Core.Validacao.Infraestrutura.Mensagens;

namespace Core.Validacao.Importacao
{
    public class EntidadeImportacaoValidator : IImportacaoValidator<Entidade>
    {
        private readonly IProdutoRepository _produtoRepository;

        public EntidadeImportacaoValidator(IProdutoRepository produtoRepository)
        {
            _produtoRepository = produtoRepository;
        }

        public void Validar(Entidade entidade)
        {
            GrupoShelfLifeSaidaExisteNaTabelaDeProdutos(entidade.ShelfLifeSaida);
        }

        private void GrupoShelfLifeSaidaExisteNaTabelaDeProdutos(ShelfLifeSaida shelfLifeSaida)
        {
            if (shelfLifeSaida == null)
                return;

            bool existeProdutoCadastradoComMesmoGrupo = _produtoRepository.Items.Any(produto => produto.Grupo == shelfLifeSaida.Grupo);

            if (!existeProdutoCadastradoComMesmoGrupo)
                throw new ImportacaoInvalidaException(MensagensValidacao.GRUPO_SHELF_LIFE_NAO_EXISTE_NA_TABELA_DE_PRODUTOS);
        }
    }
}
