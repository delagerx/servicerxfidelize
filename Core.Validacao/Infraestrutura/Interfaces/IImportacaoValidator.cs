﻿using Entidades;

namespace Core.Validacao.Infraestrutura.Interfaces
{
    public interface IImportacaoValidator<T> where T : EntidadeBase
    {
        void Validar(T registro);
    }
}
