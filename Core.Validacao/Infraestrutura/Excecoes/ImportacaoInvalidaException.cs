﻿using System;

namespace Core.Validacao.Infraestrutura.Excecoes
{
    public class ImportacaoInvalidaException : Exception
    {
        public ImportacaoInvalidaException() : base()
        {
        }

        public ImportacaoInvalidaException(string mensagem) : base(mensagem)
        {
        }
    }
}
