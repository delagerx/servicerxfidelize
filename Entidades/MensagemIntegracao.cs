﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace Entidades
{
    public class MensagemIntegracao : EntidadeBase
    {
        public long Origem { get; set; }

        public string TabelaRelacionada { get; set; }

        public long IdTabelaRelacionada { get; set; }

        public DateTime? DataCadastro { get; set; }

        public DateTime? DataProcessamento { get; set; }

        public DateTime? DataAnaliseProcessamento { get; set; }

        public long Rejeitado { get; set; }

        public long? CodRejeicao { get; set; }

        public string MotivoRejeicao { get; set; }

        public Guid DbLinkId { get; private set; }

        public DateTime DbLinkInicioProcessamento { get; private set; }

        public MensagemIntegracao()
        {
            DbLinkId = Guid.NewGuid();
            DbLinkInicioProcessamento = DateTime.Now;
        }

        public MensagemIntegracao(long origem, string tabelaRelacionada, long idTabelaRelacionada)
        {
            Origem = origem;
            TabelaRelacionada = tabelaRelacionada;
            IdTabelaRelacionada = idTabelaRelacionada;
            DbLinkId = Guid.NewGuid();
            DbLinkInicioProcessamento = DateTime.Now;
        }

        public MensagemIntegracao(long id, long origem, string tabelaRelacionada, long idTabelaRelacionada)
            : this(origem, tabelaRelacionada, idTabelaRelacionada)
        {
            Id = id;
            DbLinkId = Guid.NewGuid();
            DbLinkInicioProcessamento = DateTime.Now;
        }

        public bool FoiRejeitado()
        {
            return CodRejeicao.HasValue && CodRejeicao != 0;
        }

        public MensagemIntegracao CriarCopia()
        {
            return new MensagemIntegracao
            {
                Id = this.Id,
                CodRejeicao = this.CodRejeicao,
                MotivoRejeicao = this.MotivoRejeicao,
                Origem = this.Origem,
                TabelaRelacionada = this.TabelaRelacionada,
                IdTabelaRelacionada = this.IdTabelaRelacionada,
                DataCadastro = this.DataCadastro,
                DataProcessamento = this.DataProcessamento,
                DataAnaliseProcessamento = this.DataAnaliseProcessamento,
                Rejeitado = this.Rejeitado,
                DbLinkInicioProcessamento = this.DbLinkInicioProcessamento,
                DbLinkId = this.DbLinkId
            };
        }
    }
}