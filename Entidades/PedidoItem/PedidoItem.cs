﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Entidades
{
    public abstract class PedidoItem : EntidadeBase
    {
        public virtual Produto Produto { get; set; }
        public long IdProduto { get; set; }
        public decimal Quantidade { get; set; }
        public decimal? Separado { get; set; }
        public long? CodMotivo { get; set; }
        public string Motivo { get; set; }
        public long? NumeroItem { get; set; }
        public string Obs { get; set; }
        public long? OrdemConf { get; set; }
    }
}











