﻿using System.Configuration;

namespace Entidades.Enum
{
    public static class Configuracao
    {
        public static int Origem
        {
            get
            {
                string valorDaConfiguracao = ConfigurationManager.AppSettings["Origem"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? 8 : int.Parse(valorDaConfiguracao);
            }
        }

        public static int IntervaloConsultaMensagens
        {
            get
            {
                string valorDaConfiguracao = ConfigurationManager.AppSettings["IntervaloConsultaMensagens"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? 60 : int.Parse(valorDaConfiguracao);
            }
        }

        public static int FrequenciaExportacaoPeriodicaProdutoAuditoriaMovInv
        {
            get
            {
                string valorDaConfiguracao = ConfigurationManager.AppSettings["FrequenciaExportacaoPeriodicaProdutoAuditoriaMovInv"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? 60 : int.Parse(valorDaConfiguracao);
            }
        }

        public static int TempoRetroativoConsultaPeriodicaProdutoAuditoriaMovInv
        {
            get
            {
                string valorDaConfiguracao = ConfigurationManager.AppSettings["TempoRetroativoConsultaPeriodicaProdutoAuditoriaMovInv"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? 60 : int.Parse(valorDaConfiguracao);
            }
        }

        public static int TempoRetroativoConsultaDiariaProdutoAuditoriaMovInv
        {
            get
            {
                string valorDaConfiguracao = ConfigurationManager.AppSettings["TempoRetroativoConsultaDiariaProdutoAuditoriaMovInv"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? 60 : int.Parse(valorDaConfiguracao);
            }
        }

        public static bool DesabilitarPedidoEndereco
        {
            get
            {
                string valorDaConfiguracao = ConfigurationManager.AppSettings["DesabilitarPedidoEndereco"];
                return "true".Equals(valorDaConfiguracao);
            }
        }

        public static int CodigoOperacaoLogistica
        {
            get
            {
                string valorDaConfiguracao = ConfigurationManager.AppSettings["CodigoOperacaoLogistica"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? 0 : int.Parse(valorDaConfiguracao);
            }
        }

        public static int MensagensSimultaneasProcessadas
        {
            get
            {
                string valorDaConfiguracao = ConfigurationManager.AppSettings["MensagensSimultaneasProcessadas"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? 100 : int.Parse(valorDaConfiguracao);
            }
        }

        public static bool ImportarProduto
        {
            get
            {
                var valorDaConfiguracao = ConfigurationManager.AppSettings["ImportarProduto"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? true : bool.Parse(valorDaConfiguracao);
            }
        }

        public static bool ImportarEntidade
        {
            get
            {
                var valorDaConfiguracao = ConfigurationManager.AppSettings["ImportarEntidade"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? true : bool.Parse(valorDaConfiguracao);
            }
        }

        public static bool ImportarPedidoCancelamento
        {
            get
            {
                var valorDaConfiguracao = ConfigurationManager.AppSettings["ImportarPedidoCancelamento"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? true : bool.Parse(valorDaConfiguracao);
            }
        }

        public static bool ImportarPedidoOperacaoEntrada
        {
            get
            {
                var valorDaConfiguracao = ConfigurationManager.AppSettings["ImportarPedidoOperacaoEntrada"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? true : bool.Parse(valorDaConfiguracao);
            }
        }

        public static bool ImportarPedidoOperacaoSaida
        {
            get
            {
                var valorDaConfiguracao = ConfigurationManager.AppSettings["ImportarPedidoOperacaoSaida"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? true : bool.Parse(valorDaConfiguracao);
            }
        }

        public static bool ImportarPedidoOperacaoMovimentacao
        {
            get
            {
                var valorDaConfiguracao = ConfigurationManager.AppSettings["ImportarPedidoOperacaoMovimentacao"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? true : bool.Parse(valorDaConfiguracao);
            }
        }

        public static bool ImportarTituloOperacaoSaida
        {
            get
            {
                var valorDaConfiguracao = ConfigurationManager.AppSettings["ImportarTituloOperacaoSaida"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? true : bool.Parse(valorDaConfiguracao);
            }
        }

        public static bool ExportarDiarioLoteEstoque
        {
            get
            {
                var valorDaConfiguracao = ConfigurationManager.AppSettings["ExportarDiarioLoteEstoque"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? true : bool.Parse(valorDaConfiguracao);
            }
        }

        public static bool ExportarDiarioProdutoEstoque
        {
            get
            {
                var valorDaConfiguracao = ConfigurationManager.AppSettings["ExportarDiarioProdutoEstoque"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? true : bool.Parse(valorDaConfiguracao);
            }
        }

        public static bool ExportarPedidoOperacaoEntrada
        {
            get
            {
                var valorDaConfiguracao = ConfigurationManager.AppSettings["ExportarPedidoOperacaoEntrada"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? true : bool.Parse(valorDaConfiguracao);
            }
        }

        public static bool ExportarPedidoOperacaoSaida
        {
            get
            {
                var valorDaConfiguracao = ConfigurationManager.AppSettings["ExportarPedidoOperacaoSaida"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? true : bool.Parse(valorDaConfiguracao);
            }
        }

        public static bool ExportarPedidoOperacaoMovimentacao
        {
            get
            {
                var valorDaConfiguracao = ConfigurationManager.AppSettings["ExportarPedidoOperacaoMovimentacao"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? true : bool.Parse(valorDaConfiguracao);
            }
        }

        public static bool ExportarPedidoSituacaoOperacaoSaida
        {
            get
            {
                var valorDaConfiguracao = ConfigurationManager.AppSettings["ExportarPedidoSituacaoOperacaoSaida"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? true : bool.Parse(valorDaConfiguracao);
            }
        }

        public static bool ExportarProdutoAuditoriaMovInv
        {
            get
            {
                var valorDaConfiguracao = ConfigurationManager.AppSettings["ExportarProdutoAuditoriaMovInv"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? true : bool.Parse(valorDaConfiguracao);
            }
        }

        public static string RestServiceAddress
        {
            get
            {
                string valorDaConfiguracao = ConfigurationManager.AppSettings["RestServiceAddress"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? string.Empty : valorDaConfiguracao;
            }
        }

        public static int TempoTimeoutRequestJava
        {
            get
            {
                string valorDaConfiguracao = ConfigurationManager.AppSettings["TempoTimeoutRequestJava"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? 30 : int.Parse(valorDaConfiguracao);
            }
        }

        public static string DatabaseSchema
        {
            get
            {
                string valorDaConfiguracao = ConfigurationManager.AppSettings["DatabaseSchema"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? string.Empty : valorDaConfiguracao;
            }
        }

        public static string ActorStatsDMonitorHost
        {
            get
            {
                string valorDaConfiguracao = ConfigurationManager.AppSettings["ActorStatsDMonitorHost"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? string.Empty : valorDaConfiguracao;
            }
        }

        public static string DatabaseAccessMethod
        {
            get
            {
                string valorDaConfiguracao = ConfigurationManager.AppSettings["DatabaseAccessMethod"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? string.Empty : valorDaConfiguracao;
            }
        }

        public static int ActorPoolSize
        {
            get
            {
                string valorDaConfiguracao = ConfigurationManager.AppSettings["ActorPoolSize"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? 5 : int.Parse(valorDaConfiguracao);
            }
        }

        public static int IntervaloDeReenvioDeMensagens
        {
            get
            {
                string valorDaConfiguracao = ConfigurationManager.AppSettings["IntervaloDeReenvioDeMensagens"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? 15 : int.Parse(valorDaConfiguracao);
            }
        }

        public static bool AtualizarProdutosAoSalvarPedidos
        {
            get
            {
                var valorDaConfiguracao = ConfigurationManager.AppSettings["AtualizarProdutosAoSalvarPedidos"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? true : bool.Parse(valorDaConfiguracao);
            }
        }

        public static string HorarioProcessamentoDiarioProdutoEstoque
        {
            get
            {
                string valorDaConfiguracao = ConfigurationManager.AppSettings["HorarioProcessamentoDiarioProdutoEstoque"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? string.Empty : valorDaConfiguracao;
            }
        }

        public static string HorarioProcessamentoDiarioLoteEstoque
        {
            get
            {
                string valorDaConfiguracao = ConfigurationManager.AppSettings["HorarioProcessamentoDiarioLoteEstoque"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? string.Empty : valorDaConfiguracao;
            }
        }

        public static string HorarioExportacaoDiariaProdutoAuditoriaMovInv
        {
            get
            {
                string valorDaConfiguracao = ConfigurationManager.AppSettings["HorarioExportacaoDiariaProdutoAuditoriaMovInv"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? string.Empty : valorDaConfiguracao;
            }
        }

        public static int IntervaloDeLimpezaDaFilaDeExportacao
        {
            get
            {
                string valorDaConfiguracao = ConfigurationManager.AppSettings["IntervaloDeLimpezaDaFilaDeExportacao"];
                return string.IsNullOrEmpty(valorDaConfiguracao) ? 600 : int.Parse(valorDaConfiguracao);
            }
        }
    }
}
