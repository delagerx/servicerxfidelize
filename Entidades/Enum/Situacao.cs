﻿namespace Entidades.Enum
{
    public enum Situacao
    {
        PENDENTE = 0,
        LIBERADO = 1,
        EM_MOVIMENTACAO = 2,
        MOVIMENTADO = 3,
        CONCLUIDO = 4,
        CANCELADO = 5,
        NENHUM_ITEM_ATENDIDO = 7,
        SOLICITACAO_DE_CANCELAMENTO = 8,
        PENDENTE_NUMERO_DE_SERIE_ENTREGA = 9,
        PULADO = 10,
        PENDENTE_NUMERO_DE_SERIE = 11,
        VINCULANDO_LOTE = 13,
        PENDENTE_CONFERENTE = 14
    }
}
