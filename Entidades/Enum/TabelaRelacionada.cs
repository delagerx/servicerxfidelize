﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Enum
{
    public class TabelaRelacionada
    {
        public const string DiarioLoteEstoque = "diario_lote_estoque";
        public const string DiarioProdutoEstoque = "diario_produto_estoque";
        public const string Entidade = "entidade";
        public const string Produto = "produto";
        public const string PedidoCancelamento = "pedido_cancelamento";
        public const string PedidoOperacaoEntrada = "pedido_operacao_entrada";
        public const string PedidoOperacaoSaida = "pedido_operacao_saida";
        public const string PedidoOperacaoMovimentacao = "pedido_operacao_movimentacao";
        public const string PedidoSituacaoOperacaoSaida = "pedido_situacao_operacao_saida";
        public const string ProdutoAuditoriaMovInv = "produto_auditoria_mov_inv";
        public const string TituloOperacaoSaida = "titulo_operacao_saida";
    }
}
