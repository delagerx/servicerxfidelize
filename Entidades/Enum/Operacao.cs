﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Enum
{
    public enum Operacao
    {
        Entrada,
        Saida,
        Movimentacao
    }
}
