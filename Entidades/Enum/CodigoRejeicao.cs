﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Enum
{
    public class CodigoRejeicao
    {
        public const int ErroDeValidacao = 300000;
        public const int ErroDeConstraint = 300001;
        public const int ErroInternoDoWms = 400000;

        public const int InicioFaixaDeRejeicao = 200000;
    }
}
