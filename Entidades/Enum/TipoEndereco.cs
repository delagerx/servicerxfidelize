﻿namespace Entidades.Enum
{
    public class TipoEndereco
    {
        public const int Residencial = 1;
        public const int Comercial = 2;
        public const int Faturamento = 3;
        public const int Entrega = 4;
        public const int Cobranca = 5;
    }
}
