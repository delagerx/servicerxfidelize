﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entidades
{
    public abstract class Pedido : EntidadeBase
    {
        public int IdWms { get; set; }
        public string CodPedidoPolo { get; set; }
        public string CodOperacaoLogisticaPolo { get; set; }
        public long IdEntidade { get; set; }
        public long? CodPrenota { get; set; }
        public long Operacao { get; set; }
        public long? CodSituacao { get; set; }
        public string Situacao { get; set; }
        public DateTime? Digitacao { get; set; }
        public DateTime? DataLiberacao { get; set; }
        public DateTime? Prenota { get; set; }
        public DateTime? EncerramentoPN { get; set; }
        public string QuemDigitou { get; set; }
        public string QuemLiberou { get; set; }
        public long? CodOrigemPed { get; set; }
        public string OrigemPed { get; set; }
        public long? Volumes { get; set; }
        public decimal ValorTotal { get; set; }
        public string ObsPN { get; set; }
        public string Suboperacao { get; set; }
        public string MotivoSuboperacao { get; set; }
        public string NumeroTitulo { get; set; }
        public string SerieNf { get; set; }
        public string CodTituloPolo { get; set; }
        public float? Peso { get; set; }
        public long? QtdItens { get; set; }
        public string Obs { get; set; }
        public int? IdCancelamento { get; set; }
        public DateTime? DataSolicitacaoCancelamento { get; set; }
        public DateTime? DataConfirmacaoCancelamento { get; set; }


        public getOrder()
        {

        }


        public putOrderResponse()
        {

        }
        //Request para a Fidelize 
        public ResquestOrder()
        {

        }


    }
}