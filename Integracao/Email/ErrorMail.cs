﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Configuration;

namespace ConsoleApplication.Email
{
    public class ErrorMail
    {
        public void SendErrorMail(string task, string error, int errorCode = -1, int codPedido = 0)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["ServidorSMTP"]);

                mail.From = new MailAddress(ConfigurationManager.AppSettings["Remetente"]);
                mail.To.Add(ConfigurationManager.AppSettings["Destinatario"]);

                if(Int32.Parse(ConfigurationManager.AppSettings["CC"]) == 1)
                {
                    mail.CC.Add(ConfigurationManager.AppSettings["DestinatarioCC"]);
                }

                mail.Subject = "Relatório Informativo - Integração";
                mail.Body = createBody(task, error, errorCode, codPedido);
                mail.IsBodyHtml = true;

                SmtpServer.Port = Int32.Parse(ConfigurationManager.AppSettings["PortaSMTP"]);
                SmtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["Remetente"], ConfigurationManager.AppSettings["SenhaRemetente"]);
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void SendErrorMail(string task, string error, int errorCode = -1, string codInvoice = "0")
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["ServidorSMTP"]);

                mail.From = new MailAddress(ConfigurationManager.AppSettings["Remetente"]);
                mail.To.Add(ConfigurationManager.AppSettings["Destinatario"]);

                if (Int32.Parse(ConfigurationManager.AppSettings["CC"]) == 1)
                {
                    mail.CC.Add(ConfigurationManager.AppSettings["DestinatarioCC"]);
                }

                mail.Subject = "Relatório Informativo - Integração";
                mail.Body = createBody(task, error, errorCode, codInvoice);
                mail.IsBodyHtml = true;

                SmtpServer.Port = Int32.Parse(ConfigurationManager.AppSettings["PortaSMTP"]);
                SmtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["Remetente"], ConfigurationManager.AppSettings["SenhaRemetente"]);
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string createBody(string task, string error, int errorCode = -1, int codPedido = 0)
        {
            StringBuilder body = new StringBuilder();
            string taskName = "";

            switch(task)
            {
                case "TaskFluxo1":
                    taskName = "Fluxo 1";
                    break;

                case "TaskFluxo2":
                    taskName = "Fluxo 2";
                    break;

                case "TaskFluxo3":
                    taskName = "Fluxo 3";
                    break;

                case "TaskFluxo4":
                    taskName = "Fluxo 4";
                    break;

                case "TaskFluxo5":
                    taskName = "Fluxo 5";
                    break;

                case "TaskFluxo6":
                    taskName = "Fluxo 6";
                    break;

                default:
                    break;
            }

            
            if (errorCode != -1)
            {
                body.AppendLine("Olá, <br />");
                body.AppendLine("você está recebendo este e-mail pois ocorreu um erro no sistema de integração. Pode ser que hajam parâmetros do pedido não preenchidos ou incorretos. Os detalhes estão exibidos abaixo. <br /><br />");
                body.AppendLine("<b>Data:</b> " + DateTime.Now.ToString("dd/MM/yyyy - HH:mm:ss") + "<br />");
                if (codPedido != 0)
                    body.AppendLine("<b>Pedido:</b> " + codPedido + "<br />");
            }
            else
            {
                body.AppendLine("Olá, <br />");
                body.AppendLine("você está recebendo este e-mail pois ocorreu um erro no sistema de integração. Os detalhes estão exibidos abaixo. <br /><br />");
                body.AppendLine("<b>Data:</b> " + DateTime.Now.ToString("dd/MM/yyyy - HH:mm:ss") + "<br />");
                if (codPedido != 0)
                    body.AppendLine("<b>Pedido:</b> " + codPedido + "<br />");
            }
            body.AppendLine("<b>Módulo:</b> " + taskName + "<br />");
            body.AppendLine("<b>Código do Erro:</b> " + errorCode + "<br />");
            body.AppendLine("<b>Descrição do Erro:</b> " + error + "<br />");
            body.AppendLine("<br />");
            body.AppendLine("Atenciosamente, <br />");
            body.AppendLine("<b> Equipe de Suporte </b> <br />");
            body.AppendLine("Delage Consultoria & Sistemas <br />");
            body.AppendLine("http://www.delage.com.br <br />");
            body.AppendLine("+55 (21) 2529-3200 <br />");
            body.AppendLine("<img src='https://static.wixstatic.com/media/e524d9_9758452a7911461a96d3802b72b9e475~mv2.gif'>");

            return body.ToString();
        }

        public string createBody(string task, string error, int errorCode = -1, string codInvoice = "0")
        {
            StringBuilder body = new StringBuilder();
            string taskName = "";

            switch (task)
            {
                case "TaskFluxo1":
                    taskName = "Fluxo 1";
                    break;

                case "TaskFluxo2":
                    taskName = "Fluxo 2";
                    break;

                case "TaskFluxo3":
                    taskName = "Fluxo 3";
                    break;

                case "TaskFluxo4":
                    taskName = "Fluxo 4";
                    break;

                case "TaskFluxo5":
                    taskName = "Fluxo 5";
                    break;

                case "TaskFluxo6":
                    taskName = "Fluxo 6";
                    break;

                default:
                    break;
            }


            if (errorCode != -1)
            {
                body.AppendLine("Olá, <br />");
                body.AppendLine("você está recebendo este e-mail pois ocorreu um erro no sistema de integração. Pode ser que hajam parâmetros do pedido não preenchidos ou incorretos. Os detalhes estão exibidos abaixo. <br /><br />");
                body.AppendLine("<b>Data:</b> " + DateTime.Now.ToString("dd/MM/yyyy - HH:mm:ss") + "<br />");
                if (codInvoice != "0")
                    body.AppendLine("<b>Pedido:</b> " + codInvoice + "<br />");
            }
            else
            {
                body.AppendLine("Olá, <br />");
                body.AppendLine("você está recebendo este e-mail pois ocorreu um erro no sistema de integração. Os detalhes estão exibidos abaixo. <br /><br />");
                body.AppendLine("<b>Data:</b> " + DateTime.Now.ToString("dd/MM/yyyy - HH:mm:ss") + "<br />");
                if (codInvoice != "0")
                    body.AppendLine("<b>Pedido:</b> " + codInvoice + "<br />");
            }
            body.AppendLine("<b>Módulo:</b> " + taskName + "<br />");
            body.AppendLine("<b>Código do Erro:</b> " + errorCode + "<br />");
            body.AppendLine("<b>Descrição do Erro:</b> " + error + "<br />");
            body.AppendLine("<br />");
            body.AppendLine("Atenciosamente, <br />");
            body.AppendLine("<b> Equipe de Suporte </b> <br />");
            body.AppendLine("Delage Consultoria & Sistemas <br />");
            body.AppendLine("http://www.delage.com.br <br />");
            body.AppendLine("+55 (21) 2529-3200 <br />");
            body.AppendLine("<img src='https://static.wixstatic.com/media/e524d9_9758452a7911461a96d3802b72b9e475~mv2.gif'>");

            return body.ToString();
        }
    }
}
