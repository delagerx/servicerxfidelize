﻿using Core.Models.Invoices;
using Core.Models.Order;
using Integracao.Models.Orders;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integracao.Conversores
{
    public class ConvertJson
    {

        public static List<OrderApi> ConvertRxToAPi(string rxjson)
        {
            string content = rxjson.Replace("client_CNPJ", "client_identification")
             .Replace("subsidiary_CNPJ", "wholesaler")
             .Replace("client_Code", "client_code")
             .Replace("quantity", "ordered_quantity")
             .Replace("percent_Discount", "wholesaler_discount")
             .Replace("id_Commercial_condition", "id_commercial_condition");


            List<OrderApi> contentObj = JsonConvert.DeserializeObject<List<OrderApi>>(content);
            return contentObj;
        }


        public static OrderRx ConvertApiToRx(string rxjson)
        {
            string content = rxjson
             .Replace("wholesaler_discount", "percent_Discount")
             .Replace("client_identification", "client_CNPJ")
             .Replace("wholesaler", "subsidiary_CNPJ")
             .Replace("client_code", "client_Code")
             .Replace("ordered_quantity", "quantity")
             .Replace("status", "statusOrder");


            OrderRx contentObjRx = JsonConvert.DeserializeObject<OrderRx>(content);
            return contentObjRx;
        }


        public static OrderRx ConvertApiToRxReturn(string rxjson)
        {
            string content = rxjson
             .Replace("wholesaler_discount", "percent_Discount")
             .Replace("client_identification", "client_CNPJ")
             .Replace("wholesaler", "subsidiary_CNPJ")
             .Replace("client_code", "client_Code")
             .Replace("ordered_quantity", "quantity")
             .Replace("unit_net_price", "unitprice")
             .Replace("industry_order_code", "Id_Commercial_condition")
             .Replace("status", "statusOrder")
             .Replace("product_reason", "statusProduct");

            OrderRx contentObjRx = JsonConvert.DeserializeObject<OrderRx>(content);
            return contentObjRx;
        }


        /*
       
             */

        public static OrderRx ConvertApiToRxStatus(string rxjson)
        {
            string content = rxjson.Replace("grouped_order_code", "id")
             .Replace("client_identification", "Client_CNPJ")
             .Replace("wholesaler_discount", "discount")
             .Replace("wholesaler", "Subsidiary_CNPJ")
             .Replace("client_code", "client_Code")
             .Replace("commercial_condition", "Commercial_condition")
             .Replace("status", "StatusOrder")
             .Replace("total_products", "Total_products")
             .Replace("ean", "EAN")
             .Replace("ordered_quantity", "quantity")
             .Replace("unit_net_price", "unitPrice")
             .Replace("product_reason", "statusProduct")
             .Replace("monitored", "Monitored");
            OrderRx contentObjRx = JsonConvert.DeserializeObject<OrderRx>(content);
            return contentObjRx;
        }

        public static List<OrderApi> ConvertRxToAPiStatus(string rxjson)
        {
            Console.WriteLine(rxjson);
            string content = rxjson.Replace("motiveOrder", "order_motive")
             .Replace("responseQuantity", "response_quantity")
             .Replace("percent_Discount", "percent_discount")
             .Replace("discount_Value", "unit_discount_price")
             .Replace("unitPrice", "unit_net_price")
             .Replace("discount_Total_value", "discount_value")
             .Replace("statusProduct", "product_reason");

            Console.WriteLine(content);

            List<OrderApi> contentObj = JsonConvert.DeserializeObject<List<OrderApi>>(content);
            Console.WriteLine(content);
            OrderStatusRetorno orderstatusRetorno = new OrderStatusRetorno();
            ProductReasonRetorno productReasonRetorno = new ProductReasonRetorno();

            foreach (var item in contentObj)
            {
                foreach (var produto in item.Products)
                {
                    string statusProd = produto.Product_Reason;

                    produto.Product_Reason = productReasonRetorno.getStringProductReason(int.Parse(statusProd));
                }

                string motive = item.Order_Motive;
                Console.WriteLine(motive);
                item.Order_Motive = orderstatusRetorno.getStringStatusRetorno(int.Parse(motive));
            }
            return contentObj;
        }

        //Invoice 
        public static OrderRx ConvertApiToRxInvoiceReturn(string invoiceapijson)
        {
            string content = invoiceapijson.Replace("client_identification", "client_CNPJ")
             .Replace("wholesaler", "subsidiary_CNPJ")
             .Replace("client_code", "client_Code")
             .Replace("grouped_order_code", "OrderId")
             .Replace("commercial_condition", "commercial_condition")
             .Replace("status", "StatusOrder");
            OrderRx contentObjRx = JsonConvert.DeserializeObject<OrderRx>(content);
            return contentObjRx;
        }


        public static List<InvoiceApi> ConvertRxToAPiInvoice(string invoicerxjson)
        {


            string content = invoicerxjson.Replace("emission", "invoice_issue_date")
             .Replace("invoice_products_total_value", "products_total_value")
             .Replace("orderId", "id")
             .Replace("old_Invoice_Number","removed_invoice")
             .Replace("tax_substitution_icms_tax_calc_base", "base_calculation_icms_tax_substitution")
             .Replace("icms_tax_calc_base", "base_calculation_icms")
             .Replace("invoice_danfe_key", "danfe")
             .Replace("invoice_Value", "invoice_value")
            .Replace("invoice_discount", "invoice_discount_value")
            .Replace("invoice_passed_along_icms_tax_value", "icms_value_transferred")
            .Replace("products", "invoice_products")
            .Replace("invoice_icms_tax_retained_total_value", "icms_total_value_withheld")
            .Replace("invoice_icms_tax_total_value", "icms_total_value")
            .Replace("invoice_volume_amount", "volume_quantity");
            Console.WriteLine(content);
            content = ConvertJson.ConvertRxToApiInvoiceProducts(content);

            List<InvoiceApi> contentObj = null;
            try
            {
                contentObj = JsonConvert.DeserializeObject<List<InvoiceApi>>(content);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return contentObj;
        }

        public static string ConvertRxToApiInvoiceProducts(string content)
        {
            string invProd = content.Replace("quantity", "invoice_quantity")
                .Replace("percent_discount_product", "percent_discount")
                .Replace("discountValueProduct", "unit_discount_price")
                .Replace("unitPriceProduct", "unit_net_price")
                .Replace("Icms_tax_calc_base", "base_calculation_icms")
                .Replace("Tax_substitution_icms_tax_calc_base", "base_calculation_icms_tax_substitution")
                .Replace("icms_percentage", "percent_icms_aliquot")
                .Replace("ipi_percentage", "percent_ipi_aliquot")
                .Replace("passed_along_icms_tax_value", "icms_transferred_value")
                .Replace("icms_tax_value", "icms_value")
                .Replace("tax_substitution_value", "st_value")
                .Replace("cfop_code", "cfop")
                .Replace("tax_substitution_flag", "tax_substitution_flag")
                .Replace("positive_list_flag", "positive_list_identifier")
                .Replace("classification", "product_classification")
                .Replace("passed_along_value", "transfer_value")
                .Replace("total_value", "product_total_value")
                .Replace("total_value_with_taxes", "product_total_value_charges")
                .Replace("tax_substitution_code", "tax_status_code")
                .Replace("financial_discount_percentage", "percent_financial_discount")
                .Replace("financial_discount_value", "percent_unit_financial_discount")
                .Replace("monitored", "type")
                .Replace("Id_Commercial_condition", "id_Commercial_condition");
            return invProd;
        }
        public static List<InvoiceApi> ConvertRxToAPiInvoiceReversal(string invoicerxjson)
        {


            string content = invoicerxjson.Replace("emission", "invoice_issue_date")
             .Replace("invoice_products_total_value", "products_total_value")
             .Replace("orderId", "id")
             .Replace("tax_substitution_icms_tax_calc_base", "base_calculation_icms_tax_substitution")
             .Replace("icms_tax_calc_base", "base_calculation_icms")
             .Replace("invoice_danfe_key", "danfe")
             .Replace("invoice_Value", "invoice_value")
            .Replace("invoice_discount", "invoice_discount_value")
            .Replace("invoice_passed_along_icms_tax_value", "icms_value_transferred")
            .Replace("products", "invoice_products")
            .Replace("invoice_icms_tax_retained_total_value", "icms_total_value_withheld")
            .Replace("invoice_icms_tax_total_value", "icms_total_value")
            .Replace("invoice_volume_amount", "volume_quantity");
            Console.WriteLine(content);
            content = ConvertJson.ConvertRxToApiInvoiceProducts(content);

            List<InvoiceApi> contentObj = null;
            try
            {
                contentObj = JsonConvert.DeserializeObject<List<InvoiceApi>>(content);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return contentObj;
        }
    }
}
