﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Integracao.ERP
{
    public interface IRestClient<TEntity> : IDisposable
    {
        /*
        TResponse Get<TResponse>(string rota, Dictionary<string, object> parametros = null);

        TResponse Post<TRequest, TResponse>(string rota, TRequest request, string dbLinkId);

        TEntity Buscar(string routeName, string id);

        IList<TEntity> Listar(string routeName, Dictionary<string, object> routeParameters = null);

        IList<TEntity> Listar(string routeName, int page, int pageSize, Dictionary<string, object> routeParameters = null);

        long Salvar(string routeName, TEntity entity, string dbLinkId);
    }

    public class RestClient<TEntity> : IRestClient<TEntity>
    {
        private Uri UrlBase;

        public RestClient()
        {
            if (string.IsNullOrWhiteSpace(Configuracao.RestServiceAddress))
                throw new Exception("Configure o endereço do serviço REST de conexão com o DB2 no arquivo de configuração do projeto utilizando a chave RestServiceAddress");

            UrlBase = new Uri(Configuracao.RestServiceAddress);
        }

        public TEntity Buscar(string routeName, string id)
        {
            HttpClient Client = new HttpClient();

            Client.BaseAddress = UrlBase;
            Client.Timeout = TimeSpan.FromMinutes(Configuracao.TempoTimeoutRequestJava);

            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, string.Format(@"{0}/{1}", routeName, id));

            Task<HttpResponseMessage> requestTask = Task.Run(() => Client.SendAsync(requestMessage));

            requestTask.Wait(-1);

            var contentTask = Task.Run(() => requestTask.Result.Content.ReadAsStringAsync());

            VerificarErroNoRequest(requestTask, contentTask);

            contentTask.Wait(-1);

            return JsonConvert.DeserializeObject<TEntity>(contentTask.Result);
        }

        public IList<TEntity> Listar(string routeName, Dictionary<string, object> routeParameters = null)
        {
            HttpClient Client = new HttpClient();

            Client.BaseAddress = UrlBase;
            Client.Timeout = TimeSpan.FromMinutes(Configuracao.TempoTimeoutRequestJava);

            if (routeParameters != null && routeParameters.Count > 0)
            {
                routeName = string.Format("{0}?", routeName);

                foreach (var parameter in routeParameters)
                    routeName = string.Format("{0}{1}={2}&", routeName, parameter.Key, parameter.Value);

                routeName = routeName.Remove(routeName.Length - 1);
            }

            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, routeName);

            Task<HttpResponseMessage> requestTask = Task.Run(() => Client.SendAsync(requestMessage));

            requestTask.Wait(-1);

            Task<string> contentTask = Task.Run(() => requestTask.Result.Content.ReadAsStringAsync());

            VerificarErroNoRequest(requestTask, contentTask);

            contentTask.Wait(-1);

            return JsonConvert.DeserializeObject<IList<TEntity>>(contentTask.Result);
        }

        public IList<TEntity> Listar(string routeName, int page, int pageSize, Dictionary<string, object> routeParameters = null)
        {
            if (routeParameters == null)
                routeParameters = new Dictionary<string, object>();

            routeParameters.Add("page", page);
            routeParameters.Add("pageSize", pageSize);

            return Listar(routeName, routeParameters);
        }

        public long Salvar(string routeName, TEntity entity, string dbLinkId)
        {
            HttpClient Client = new HttpClient();

            Client.BaseAddress = UrlBase;
            Client.Timeout = TimeSpan.FromMinutes(Configuracao.TempoTimeoutRequestJava);

            Client.DefaultRequestHeaders.Accept.Clear();

            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            string jsonSerializedObject = JsonConvert.SerializeObject(entity, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });

            StringContent requestContent = new StringContent(jsonSerializedObject, Encoding.UTF8, "application/json");
            requestContent.Headers.Add("X-dbLinkId", dbLinkId);

            Uri requestUrl = new Uri(UrlBase, routeName);

            Task<HttpResponseMessage> requestTask = Task.Run(() => Client.PostAsync(requestUrl, requestContent));

            requestTask.Wait(-1);

            var contentTask = Task.Run(() => requestTask.Result.Content.ReadAsStringAsync());

            VerificarErroDeValidacao(requestTask, contentTask);
            VerificarErroNoRequest(requestTask, contentTask);

            contentTask.Wait(-1);

            return JsonConvert.DeserializeObject<long>(contentTask.Result);
        }

        public void Dispose()
        {

        }

        public TResponse Get<TResponse>(string rota, Dictionary<string, object> parametros = null)
        {
            HttpClient Client = new HttpClient
            {
                BaseAddress = UrlBase,
                Timeout = TimeSpan.FromMinutes(Configuracao.TempoTimeoutRequestJava)
            };
            
            if (parametros != null && parametros.Count > 0)
            {
                rota = string.Format("{0}?", rota);

                foreach (var parameter in parametros)
                    rota = string.Format("{0}{1}={2}&", rota, parameter.Key, parameter.Value);

                rota = rota.Remove(rota.Length - 1);
            }

            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, rota);

            Task<HttpResponseMessage> requestTask = Task.Run(() => Client.SendAsync(requestMessage));

            requestTask.Wait(-1);

            Task<string> contentTask = Task.Run(() => requestTask.Result.Content.ReadAsStringAsync());

            VerificarErroNoRequest(requestTask, contentTask);

            contentTask.Wait(-1);

            return JsonConvert.DeserializeObject<TResponse>(contentTask.Result);
        }

        public TResponse Post<TRequest, TResponse>(string rota, TRequest request, string dbLinkId)
        {
            HttpClient httpClient = new HttpClient
            {
                BaseAddress = UrlBase,
                Timeout = TimeSpan.FromMinutes(Configuracao.TempoTimeoutRequestJava)
            };
            
            httpClient.DefaultRequestHeaders.Accept.Clear();

            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            string jsonSerializedObject = JsonConvert.SerializeObject(request, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });

            StringContent requestContent = new StringContent(jsonSerializedObject, Encoding.UTF8, "application/json");

            requestContent.Headers.Add("X-dbLinkId", dbLinkId);

            Uri requestUrl = new Uri(UrlBase, rota);

            Task<HttpResponseMessage> requestTask = Task.Run(() => httpClient.PostAsync(requestUrl, requestContent));

            requestTask.Wait(-1);

            var contentTask = Task.Run(() => requestTask.Result.Content.ReadAsStringAsync());

            VerificarErroDeValidacao(requestTask, contentTask);

            VerificarErroNoRequest(requestTask, contentTask);

            contentTask.Wait(-1);

            return JsonConvert.DeserializeObject<TResponse>(contentTask.Result);
        }

        private static void VerificarErroNoRequest(Task<HttpResponseMessage> requestTask, Task<string> contentTask)
        {
            if (requestTask.Result.StatusCode == HttpStatusCode.InternalServerError)
                throw new Exception(string.Format("Ocorreu um erro no request ao webservice de comunicação com a base DB2: {0}", contentTask.Result));
        }

        private static void VerificarErroDeValidacao(Task<HttpResponseMessage> requestTask, Task<string> contentTask)
        {
            if (requestTask.Result.StatusCode == (HttpStatusCode)422)
                throw new ValidationException(contentTask.Result);
        }
        */
    }
}
