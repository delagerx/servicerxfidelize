﻿namespace Integracao.ERP.Rest.Infraestrutura
{
    public class RestRoutes
    {
        public class Entidade
        {
            public const string Buscar = "entidade";
        }

        public class DiarioLoteEstoque
        {
            public const string Salvar = "diario_lote_estoque";
        }

        public class DiarioProdutoEstoque
        {
            public const string Salvar = "diario_produto_estoque";
        }

        public class MensagemIntegracao
        {
            public const string Salvar = "mensagem_integracao";
            public const string Buscar = "mensagem_integracao";
            public const string MensagensProcessadas = "mensagem_integracao/processadas";
            public const string MensagensPendentes = "mensagem_integracao/pendentes";
        }

        public class PedidoCancelamento
        {
            public const string Buscar = "pedido_cancelamento";
        }

        public class PedidoOperacaoEntrada
        {
            public const string Buscar = "pedido_operacao_entrada";
            public const string Salvar = "pedido_operacao_entrada";
        }

        public class PedidoOperacaoMovimentacao
        {
            public const string Buscar = "pedido_operacao_movimentacao";
            public const string Salvar = "pedido_operacao_movimentacao";
        }

        public class PedidoOperacaoSaida
        {
            public const string Buscar = "pedido_operacao_saida";
            public const string Salvar = "pedido_operacao_saida";
        }

        public class PedidoSituacaoOperacaoSaida
        {
            public const string Salvar = "pedido_situacao_operacao_saida";
        }
        
        public class Produto
        {
            public const string Buscar = "produto";
        }

        public class ProdutoAuditoriaMovInv
        {
            public const string Buscar = "produto_auditoria_mov_inv";
            public const string Salvar = "produto_auditoria_mov_inv";
        }

        public class TituloOperacaoSaida
        {
            public const string Buscar = "titulo_operacao_saida";
        }
    }
}
