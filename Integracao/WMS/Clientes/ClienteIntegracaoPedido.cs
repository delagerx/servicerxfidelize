﻿using Entidades;
using Entidades.Enum;
using Integracao.Conversores;
using Integracao.WMSOper;
using System.Collections.Generic;

namespace Integracao.WMS.Clientes
{
    public interface IClienteIntegracaoPedido
    {
        void Salvar(Pedido pedido, MensagemIntegracao mensagemIntegracao);
        IList<Pedido> BuscarNovosPedidos();
        bool ConcluirPedido(MensagemIntegracao msg, int idWms);
    }

    public class ClienteIntegracaoPedido : ClienteIntegracaoBase, IClienteIntegracaoPedido
    {
        public void Salvar(Pedido pedido, MensagemIntegracao mensagemIntegracao)
        {
            RxWMSOperPedido pedidoDataSet = null;

            if (pedido is PedidoOperacaoEntrada)
                pedidoDataSet = ConversorPedido.ConverterParaDataTable(pedido as PedidoOperacaoEntrada);

            if (pedido is PedidoOperacaoSaida)
                pedidoDataSet = ConversorPedido.ConverterParaDataTable(pedido as PedidoOperacaoSaida);

            if (pedido is PedidoOperacaoMovimentacao)
                pedidoDataSet = ConversorPedido.ConverterParaDataTable(pedido as PedidoOperacaoMovimentacao);

            var request = new GravaRxWMSOperPedidoRequest(pedidoDataSet);
            LogarXmlDeRequest(mensagemIntegracao, nameof(ClienteIntegracaoPedido), nameof(_wmsClient.GravaRxWMSOperPedido), ObterXml(request.ds.GetXml));

            var response = _wmsClient.GravaRxWMSOperPedido(request);
            pedidoDataSet = response.ds;
            bool resultadoOperacao = response.GravaRxWMSOperPedidoResult;

            LogarXmlDeResponse(mensagemIntegracao, nameof(ClienteIntegracaoPedido), nameof(_wmsClient.GravaRxWMSOperPedido), ObterXml(response.ds.GetXml));

            ConversorPedido.ObterResultadoOperacao(mensagemIntegracao, pedidoDataSet, resultadoOperacao);
        }

        public IList<Pedido> BuscarNovosPedidos()
        {
            List<Pedido> pedidos = new List<Pedido>();

            if (Configuracao.ExportarPedidoOperacaoEntrada)
                pedidos.AddRange(BuscarPedidosPorTipoOperacao(Operacao.Entrada));

            if (Configuracao.ExportarPedidoOperacaoSaida)
                pedidos.AddRange(BuscarPedidosPorTipoOperacao(Operacao.Saida));

            if (Configuracao.ExportarPedidoOperacaoMovimentacao)
                pedidos.AddRange(BuscarPedidosPorTipoOperacao(Operacao.Movimentacao));

           return pedidos;
        }

        private IList<Pedido> BuscarPedidosPorTipoOperacao(Operacao operacao)
        {
            IList<Pedido> pedidos = new List<Pedido>();

            string codigoOperacaoEntrada = ConversorPedido.ObterCodigosPorTipoOperacao(operacao);

            LogarXmlDeRequest(null, nameof(ClienteIntegracaoPedido), nameof(_wmsClient.GetRxWMSOperPedido), operacao.ToString());
            RxWMSOperPedido pedidoDataSet = _wmsClient.GetRxWMSOperPedido(codOperacao: null, Operacao: codigoOperacaoEntrada, origem_ped: Configuracao.Origem.ToString());
            LogarXmlDeResponse(null, nameof(ClienteIntegracaoPedido), nameof(_wmsClient.GetRxWMSOperPedido), pedidoDataSet.GetXml());

            foreach (var pedidoRow in pedidoDataSet.pedido)
            {
                if (operacao == Operacao.Entrada)
                    pedidos.Add(ConversorPedido.ConverterParaPedidoOperacaoEntrada(pedidoDataSet, pedidoRow.cod_pedido));

                if (operacao == Operacao.Movimentacao)
                    pedidos.Add(ConversorPedido.ConverterParaPedidoOperacaoMovimentacao(pedidoDataSet, pedidoRow.cod_pedido));

                if (operacao == Operacao.Saida)
                    pedidos.Add(ConversorPedido.ConverterParaPedidoOperacaoSaida(pedidoDataSet, pedidoRow.cod_pedido));
            }

            return pedidos;
        }

        public bool ConcluirPedido(MensagemIntegracao msg, int idWms)
        {
            try
            {
                LogarXmlDeRequest(msg, nameof(ClienteIntegracaoPedido), nameof(_wmsClient.concluiRxWMSOperPedido), idWms.ToString());
                var pedidoConcluidoNoWms = _wmsClient.concluiRxWMSOperPedido(idWms, null);
                LogarXmlDeResponse(msg, nameof(ClienteIntegracaoPedido), nameof(_wmsClient.concluiRxWMSOperPedido), pedidoConcluidoNoWms.ToString());

                if (!pedidoConcluidoNoWms)
                    ReconectarAoWms();

                return pedidoConcluidoNoWms;
            }
            catch
            {
                ReconectarAoWms();

                throw;
            }
        }
    }
}
