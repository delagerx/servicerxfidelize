﻿using Entidades;
using Integracao.Conversores;
using Integracao.WMSOper;

namespace Integracao.WMS.Clientes
{
    public interface IClienteIntegracaoProduto
    {
        void Salvar(Produto produto, MensagemIntegracao mensagemIntegracao);
    }

    public class ClienteIntegracaoProduto : ClienteIntegracaoBase, IClienteIntegracaoProduto
    {
        public void Salvar(Produto produto, MensagemIntegracao mensagemIntegracao)
        {
            RxWMSOperPedido pedidoDataSet = ConversorProduto.ConverterParaDataTable(produto);

            var request = new GravaRxWMSOperProdutoRequest(pedidoDataSet);
            LogarXmlDeRequest(mensagemIntegracao, nameof(ClienteIntegracaoProduto), nameof(_wmsClient.GravaRxWMSOperProduto), ObterXml(request.ds.GetXml));

            var response = _wmsClient.GravaRxWMSOperProduto(request);
            pedidoDataSet = response.ds;
            bool resultadoOperacao = response.GravaRxWMSOperProdutoResult;

            LogarXmlDeResponse(mensagemIntegracao, nameof(ClienteIntegracaoProduto), nameof(_wmsClient.GravaRxWMSOperProduto), ObterXml(response.ds.GetXml));

            ConversorProduto.ObterResultadoOperacao(mensagemIntegracao, pedidoDataSet, resultadoOperacao);
        }
    }
}
