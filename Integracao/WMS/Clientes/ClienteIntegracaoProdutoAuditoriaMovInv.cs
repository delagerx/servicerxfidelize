﻿using Entidades;
using Integracao.Conversores;
using Integracao.WMSOper;
using System;
using System.Collections.Generic;

namespace Integracao.WMS.Clientes
{
    public interface IClienteIntegracaoProdutoAuditoriaMovInv
    {
        IList<ProdutoAuditoriaMovInv> BuscarNovosRegistros(DateTime dataInicialParaConsulta);
    }

    public class ClienteIntegracaoProdutoAuditoriaMovInv : ClienteIntegracaoBase, IClienteIntegracaoProdutoAuditoriaMovInv
    {
        public IList<ProdutoAuditoriaMovInv> BuscarNovosRegistros(DateTime dataInicialParaConsulta)
        {
            List<ProdutoAuditoriaMovInv> registrosEncontrados = new List<ProdutoAuditoriaMovInv>();

            LogarXmlDeRequest(null, nameof(ClienteIntegracaoProdutoAuditoriaMovInv), nameof(_wmsClient.GetRxWMSOperInventarioAuditoriaAnalitico), null);
            
            var request = new GetRxWMSOperInventarioAuditoriaAnaliticoRequest { data_retroativo = dataInicialParaConsulta };

            var response = _wmsClient.GetRxWMSOperInventarioAuditoriaAnalitico(request);

            LogarXmlDeResponse(null, nameof(ClienteIntegracaoProdutoAuditoriaMovInv), nameof(_wmsClient.GetRxWMSOperInventarioAuditoriaAnalitico), ObterXml(response.GetRxWMSOperInventarioAuditoriaAnaliticoResult.GetXml));

            foreach (RxWMSOperEstoque.produto_movimentoRow produtoAuditoriaMovInvRow in response.GetRxWMSOperInventarioAuditoriaAnaliticoResult.produto_movimento)
                registrosEncontrados.Add(ConversorProdutoAuditoriaMovInv.ConverterParaModelo(produtoAuditoriaMovInvRow));

            return registrosEncontrados;
        }
    }
}
