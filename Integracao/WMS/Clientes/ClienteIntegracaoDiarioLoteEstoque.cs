﻿using Entidades;
using Integracao.Conversores;
using Integracao.WMSOper;
using System.Collections.Generic;

namespace Integracao.WMS.Clientes
{
    public interface IClienteIntegracaoDiarioLoteEstoque
    {
        IList<DiarioLoteEstoque> BuscarNovosRegistros();
    }

    public class ClienteIntegracaoDiarioLoteEstoque : ClienteIntegracaoBase, IClienteIntegracaoDiarioLoteEstoque
    {
        public IList<DiarioLoteEstoque> BuscarNovosRegistros()
        {
            List<DiarioLoteEstoque> registrosEncontrados = new List<DiarioLoteEstoque>();

            LogarXmlDeRequest(null, nameof(ClienteIntegracaoDiarioLoteEstoque), nameof(_wmsClient.GetRxWMSOperProdutoEstoque), null);

            var response = _wmsClient.GetRxWMSOperEstoque(new GetRxWMSOperEstoqueRequest());

            LogarXmlDeResponse(null, nameof(ClienteIntegracaoDiarioLoteEstoque), nameof(_wmsClient.GetRxWMSOperProdutoEstoque), ObterXml(response.GetRxWMSOperEstoqueResult.GetXml));

            foreach (var diarioProdutoEstoqueRow in response.GetRxWMSOperEstoqueResult.produto_lote_estoque)
                registrosEncontrados.Add(ConversorDiarioLoteEstoque.ConverterParaModelo(diarioProdutoEstoqueRow));

            return registrosEncontrados;
        }
    }
}
