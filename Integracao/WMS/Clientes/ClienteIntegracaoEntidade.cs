﻿using Entidades;
using Integracao.Conversores;
using Integracao.WMSOper;

namespace Integracao.WMS.Clientes
{
    public interface IClienteIntegracaoEntidade
    {
        void Salvar(Entidade entidade, MensagemIntegracao mensagemIntegracao);
    }

    public class ClienteIntegracaoEntidade : ClienteIntegracaoBase, IClienteIntegracaoEntidade
    {
        public void Salvar(Entidade entidade, MensagemIntegracao mensagemIntegracao)
        {
            RxWMSOperPedido pedidoDataSet = ConversorEntidade.ConverterParaDataTable(entidade);

            var request = new GravaRxWMSOperEntidadeRequest(pedidoDataSet);
            LogarXmlDeRequest(mensagemIntegracao, nameof(ClienteIntegracaoEntidade), nameof(_wmsClient.GravaRxWMSOperEntidade), ObterXml(request.ds.GetXml));

            var response = _wmsClient.GravaRxWMSOperEntidade(request);
            pedidoDataSet = response.ds;
            bool resultadoOperacao = response.GravaRxWMSOperEntidadeResult;

            LogarXmlDeResponse(mensagemIntegracao, nameof(ClienteIntegracaoEntidade), nameof(_wmsClient.GravaRxWMSOperEntidade), ObterXml(response.ds.GetXml));

            ConversorEntidade.ObterResultadoOperacao(mensagemIntegracao, pedidoDataSet, resultadoOperacao);
        }
    }
}
