﻿using Entidades;
using Entidades.Enum;
using Integracao.Conversores;
using Integracao.WMSOper;
using System.Collections.Generic;

namespace Integracao.WMS.Clientes
{
    public interface IClienteIntegracaoPedidoSituacao
    {
        IList<PedidoSituacaoOperacaoSaida> BuscarNovosRegistros();
        bool ConcluirExportacao(MensagemIntegracao mensagem, int codPedido, int codSituacao);
    }

    public class ClienteIntegracaoPedidoSituacao : ClienteIntegracaoBase, IClienteIntegracaoPedidoSituacao
    {
        public IList<PedidoSituacaoOperacaoSaida> BuscarNovosRegistros()
        {
            IList<PedidoSituacaoOperacaoSaida> registros = new List<PedidoSituacaoOperacaoSaida>();

            LogarXmlDeRequest(null, nameof(ClienteIntegracaoPedidoSituacao), nameof(_wmsClient.GetRxWMSOperSituacaoPedido), null);
            
            RXWMSOperPedidoSimplificado pedidoSituacaoDataSet = _wmsClient.GetRxWMSOperSituacaoPedido(codOperacaoLogistica: null, Operacao: null, cod_pedido_polo: null, origem_ped: Configuracao.Origem.ToString());

            LogarXmlDeResponse(null, nameof(ClienteIntegracaoPedidoSituacao), nameof(_wmsClient.GetRxWMSOperSituacaoPedido), pedidoSituacaoDataSet.GetXml());

            foreach (var pedidoSituacaoRow in pedidoSituacaoDataSet.Pedido)
                registros.Add(ConversorPedidoSituacaoOperacaoSaida.ConverterParaModelo(pedidoSituacaoDataSet, pedidoSituacaoRow.cod_pedido));

            return registros;
        }

        public bool ConcluirExportacao(MensagemIntegracao mensagem, int codPedido, int codSituacao)
        {
            LogarXmlDeRequest(mensagem, nameof(ClienteIntegracaoPedidoSituacao), nameof(_wmsClient.concluiRxWMSOperSituacaoPedido), $"{{ codPedido: {codPedido.ToString()}, codSituacao: {codSituacao}}}");

            var response = _wmsClient.concluiRxWMSOperSituacaoPedido(codPedido, codSituacao, null);

            LogarXmlDeResponse(mensagem, nameof(ClienteIntegracaoPedidoSituacao), nameof(_wmsClient.concluiRxWMSOperSituacaoPedido), response.ToString());

            return response;
        }
    }
}
