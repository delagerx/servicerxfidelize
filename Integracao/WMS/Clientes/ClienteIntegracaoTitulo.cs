﻿using Entidades;
using Integracao.Conversores;
using Integracao.WMSOper;

namespace Integracao.WMS.Clientes
{
    public interface IClienteIntegracaoTituloOperacaoSaida
    {
        void Salvar(TituloOperacaoSaida tituloOperacaoSaida, MensagemIntegracao mensagemIntegracao);
    }

    public class ClienteIntegracaoTituloOperacaoSaida : ClienteIntegracaoBase, IClienteIntegracaoTituloOperacaoSaida
    {
        public void Salvar(TituloOperacaoSaida tituloOperacaoSaida, MensagemIntegracao mensagemIntegracao)
        {
            RXWMSOperPedidoSimplificado pedidoDataSet = ConversorTituloOperacaoSaida.ConverterParaDataTable(tituloOperacaoSaida);

            var request = new GravaRxWMSOperNumeroTituloRequest(pedidoDataSet);

            LogarXmlDeRequest(mensagemIntegracao, nameof(ClienteIntegracaoTituloOperacaoSaida), nameof(_wmsClient.GravaRxWMSOperNumeroTitulo), ObterXml(request.ds.GetXml));

            var response = _wmsClient.GravaRxWMSOperNumeroTitulo(request);

            pedidoDataSet = response.ds;

            bool resultadoOperacao = response.GravaRxWMSOperNumeroTituloResult;

            LogarXmlDeResponse(mensagemIntegracao, nameof(ClienteIntegracaoTituloOperacaoSaida), nameof(_wmsClient.GravaRxWMSOperNumeroTitulo), ObterXml(response.ds.GetXml));

            ConversorTituloOperacaoSaida.ObterResultadoOperacao(mensagemIntegracao, pedidoDataSet, resultadoOperacao);
        }
    }
}
