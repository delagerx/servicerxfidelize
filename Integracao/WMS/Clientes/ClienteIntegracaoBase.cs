﻿using DbLink;
using Entidades;
using Integracao.WMSOper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Xml.Linq;

namespace Integracao.WMS.Clientes
{
    public class ClienteIntegracaoBase
    {
        protected WMSOperSoap _wmsClient;

        public ClienteIntegracaoBase()
        {
            _wmsClient = new WMSOperSoapClient("WMSOperSoap");
        }

        protected void ReconectarAoWms()
        {
            _wmsClient = new WMSOperSoapClient("WMSOperSoap");
        }

        protected string ObterXml(Func<string> getXmlAction)
        {
            try
            {
                return getXmlAction();
            }
            catch(Exception e)
            {
                LogHelper.LogError(e.Message);
                return string.Format("Ocorreu um erro ao obter o XML do dataset: {0}", e.Message);
            }
        }

        protected void LogarXmlDeRequest(MensagemIntegracao mensagemIntegracao, string actorName, string methodName, string xml)
        {
            LogarXml(mensagemIntegracao, actorName, methodName, xml, "request");
        }

        protected void LogarXmlDeResponse(MensagemIntegracao mensagemIntegracao, string actorName, string methodName, string xml)
        {
            LogarXml(mensagemIntegracao, actorName, methodName, xml, "response");
        }

        private void LogarXml(MensagemIntegracao mensagemIntegracao, string actorName, string methodName, string xml, string type)
        {
            string json = null;

            try
            {
                XDocument xdoc = XDocument.Parse(xml);
                json = JsonConvert.SerializeXNode(xdoc.FirstNode);
            }
            catch(Exception e)
            {
                json = xml;
            }

            var msgIntegracao = "null";

            if (mensagemIntegracao != null)
                msgIntegracao = JObject.FromObject(mensagemIntegracao).ToString();
            
            LogHelper.LogInfo(string.Format("{{ \"actor\": \"{0}\", \"methodName\": \"{1}\", \"operation\": \"{2}\", \"mensagemIntegracao\": {3}, \"data\": {4} }}", actorName, methodName, type, msgIntegracao, JsonConvert.SerializeObject(json)));
        }
    }
}
