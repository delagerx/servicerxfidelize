﻿using Entidades;
using Integracao.Conversores;
using Integracao.WMSOper;
using System.Collections.Generic;

namespace Integracao.WMS.Clientes
{
    public interface IClienteIntegracaoDiarioProdutoEstoque
    {
        IList<DiarioProdutoEstoque> BuscarNovosRegistros();
    }

    public class ClienteIntegracaoDiarioProdutoEstoque : ClienteIntegracaoBase, IClienteIntegracaoDiarioProdutoEstoque
    {

        public IList<DiarioProdutoEstoque> BuscarNovosRegistros()
        {
            List<DiarioProdutoEstoque> registrosEncontrados = new List<DiarioProdutoEstoque>();

            LogarXmlDeRequest(null, nameof(ClienteIntegracaoDiarioProdutoEstoque), nameof(_wmsClient.GetRxWMSOperProdutoEstoque), null);

            var response = _wmsClient.GetRxWMSOperEstoque(new GetRxWMSOperEstoqueRequest());

            LogarXmlDeResponse(null, nameof(ClienteIntegracaoDiarioProdutoEstoque), nameof(_wmsClient.GetRxWMSOperProdutoEstoque), ObterXml(response.GetRxWMSOperEstoqueResult.GetXml));

            foreach (RxWMSOperEstoque.produto_estoqueRow diarioProdutoEstoqueRow in response.GetRxWMSOperEstoqueResult.produto_estoque)
                registrosEncontrados.Add(ConversorDiarioProdutoEstoque.ConverterParaModelo(diarioProdutoEstoqueRow));

            return registrosEncontrados;
        }
    }
}
