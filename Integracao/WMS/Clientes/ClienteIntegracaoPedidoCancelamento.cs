﻿using Entidades;
using Integracao.Conversores;
using Integracao.WMSOper;

namespace Integracao.WMS.Clientes
{
    public interface IClienteIntegracaoPedidoCancelamento
    {
        PedidoCancelamento Salvar(PedidoCancelamento pedidoCancelamento, MensagemIntegracao mensagemIntegracao);
    }

    public class ClienteIntegracaoPedidoCancelamento : ClienteIntegracaoBase, IClienteIntegracaoPedidoCancelamento
    {
        public PedidoCancelamento Salvar(PedidoCancelamento pedidoCancelamento, MensagemIntegracao mensagemIntegracao)
        {
            RXWMSOperPedidoSimplificado pedidoDataSet = ConversorPedidoCancelamento.ConverterParaDataTable(pedidoCancelamento);

            var request = new GravaRxWMSOperPedidoCancelaEmMassaRequest(pedidoDataSet);

            LogarXmlDeRequest(mensagemIntegracao, nameof(ClienteIntegracaoPedidoCancelamento), nameof(_wmsClient.GravaRxWMSOperPedidoCancelaEmMassa), ObterXml(request.ds.GetXml));

            var response = _wmsClient.GravaRxWMSOperPedidoCancelaEmMassa(request);
            pedidoDataSet = response.ds;
            bool resultadoOperacao = response.GravaRxWMSOperPedidoCancelaEmMassaResult;

            LogarXmlDeResponse(mensagemIntegracao, nameof(ClienteIntegracaoPedidoCancelamento), nameof(_wmsClient.GravaRxWMSOperPedidoCancelaEmMassa), ObterXml(response.ds.GetXml));

            ConversorPedidoCancelamento.ObterResultadoOperacao(mensagemIntegracao, pedidoDataSet, resultadoOperacao);

            return pedidoCancelamento;
        }
    }
}
