﻿using Akka.Actor;
using DbLink;
using Integracao.WMS.Clientes;
using Mensagens;
using System;
using Entidades.Enum;
using Mensagens.Resources;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace Integracao.WMS.Escritores
{
    public class WmsEscritorEntidadeDataSet : ReceiveActorLogger
    {
        private readonly IClienteIntegracaoEntidade _clienteIntegracaoEntidade;

        private WmsEscritorEntidadeDataSet()
        {
            Receive<MensagensEntidade.SalvarEntidadeNoWms>(msg => SalvarEntidadeNoWms(msg));
        }

        public WmsEscritorEntidadeDataSet(IClienteIntegracaoEntidade clienteIntegracaoEntidade) : this()
        {
            _clienteIntegracaoEntidade = clienteIntegracaoEntidade;
        }

        private void SalvarEntidadeNoWms(MensagensEntidade.SalvarEntidadeNoWms msg)
        {
            try
            {
                _clienteIntegracaoEntidade.Salvar(msg.Entidade, msg.MensagemIntegracao);

                if (msg.MensagemIntegracao.FoiRejeitado())
                    Sender.Tell(new ResultadoOperacao.Rejeicao(msg.MensagemIntegracao, msg.MensagemIntegracao.CodRejeicao.Value, msg.MensagemIntegracao.MotivoRejeicao));
                else
                    Sender.Tell(new ResultadoOperacao.Sucesso(msg.MensagemIntegracao));
            }
            catch (ValidationException ex)
            {
                Sender.Tell(new ResultadoOperacao.Rejeicao(msg.MensagemIntegracao, CodigoRejeicao.ErroDeValidacao, ex.Message));
            }
            catch (ConstraintException ex)
            {
                Sender.Tell(new ResultadoOperacao.Rejeicao(msg.MensagemIntegracao, CodigoRejeicao.ErroDeConstraint, ex.Message));
            }
            catch (NoNullAllowedException ex)
            {
                Sender.Tell(new ResultadoOperacao.Rejeicao(msg.MensagemIntegracao, CodigoRejeicao.ErroDeValidacao, string.Format(MensagensValidacao.CAMPO_DO_DATATABLE_OBRIGATORIO, ex.Message)));
            }
            catch (Exception ex)
            {
                Sender.Tell(new ResultadoOperacao.Erro(msg.MensagemIntegracao, ex));
            }
        }
    }
}
