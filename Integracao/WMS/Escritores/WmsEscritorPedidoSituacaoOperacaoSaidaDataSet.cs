﻿using System;
using DbLink;
using Integracao.WMS.Clientes;
using Mensagens;
using Entidades;
using Akka.Actor;

namespace Integracao.WMS.Escritores
{
    public class WmsEscritorPedidoSituacaoOperacaoSaidaDataSet : ReceiveActorLogger
    {
        private readonly IClienteIntegracaoPedidoSituacao _clienteIntegracaoPedidoSituacao;

        public WmsEscritorPedidoSituacaoOperacaoSaidaDataSet()
        {
            Receive<MensagensExportacao.ConcluirExportacaoNoWms<PedidoSituacaoOperacaoSaida>>(ConcluirExportacaoDoRegistroNoWms);
        }

        public WmsEscritorPedidoSituacaoOperacaoSaidaDataSet(IClienteIntegracaoPedidoSituacao clienteIntegracaoPedidoSituacao) : this()
        {
            _clienteIntegracaoPedidoSituacao = clienteIntegracaoPedidoSituacao;
        }

        private void ConcluirExportacaoDoRegistroNoWms(MensagensExportacao.ConcluirExportacaoNoWms<PedidoSituacaoOperacaoSaida> mensagem)
        {
            try
            {
                bool resultadoOperacao = _clienteIntegracaoPedidoSituacao.ConcluirExportacao(mensagem.MensagemIntegracao, mensagem.Registro.CodPedido, mensagem.Registro.CodSituacao);

                if (resultadoOperacao)
                    Sender.Tell(new MensagensExportacao.RegistroExportadoComSucesso<PedidoSituacaoOperacaoSaida>(mensagem.Registro, mensagem.MensagemIntegracao));
                else
                    throw new Exception("O webservice retornou false ao tentar concluir a integração do registro no WMS.");
            }
            catch (Exception ex)
            {
                Sender.Tell(new MensagensExportacao.ErroNaExportacao<PedidoSituacaoOperacaoSaida>(mensagem.Registro, mensagem.MensagemIntegracao, ex));
            }
        }
    }
}
