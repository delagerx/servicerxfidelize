﻿using Akka.Actor;
using DbLink;
using Entidades;
using Entidades.Enum;
using Integracao.WMS.Clientes;
using Mensagens;
using Mensagens.Resources;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace Integracao.WMS.Escritores
{
    public class WmsEscritorProdutoDataSet : ReceiveActorLogger
    {
        private readonly IClienteIntegracaoProduto _clienteIntegracaoProduto;

        private WmsEscritorProdutoDataSet()
        {
            Receive<MensagensProduto.SalvarProdutoNoWms>(msg => SalvarProdutoNoWms(msg));
        }

        public WmsEscritorProdutoDataSet(IClienteIntegracaoProduto clienteIntegracaoProduto) : this()
        {
            _clienteIntegracaoProduto = clienteIntegracaoProduto;
        }

        private void SalvarProdutoNoWms(MensagensProduto.SalvarProdutoNoWms msg)
        {
            try
            {
                _clienteIntegracaoProduto.Salvar(msg.Produto, msg.MensagemIntegracao);

                if (msg.MensagemIntegracao.FoiRejeitado())
                    Sender.Tell(new ResultadoOperacao.Rejeicao(msg.MensagemIntegracao, msg.MensagemIntegracao.CodRejeicao.Value, msg.MensagemIntegracao.MotivoRejeicao));
                else
                    Sender.Tell(new ResultadoOperacao.Sucesso(msg.MensagemIntegracao));
            }
            catch (ValidationException ex)
            {
                Sender.Tell(new ResultadoOperacao.Rejeicao(msg.MensagemIntegracao, CodigoRejeicao.ErroDeValidacao, ex.Message));
            }
            catch (ConstraintException ex)
            {
                Sender.Tell(new ResultadoOperacao.Rejeicao(msg.MensagemIntegracao, CodigoRejeicao.ErroDeConstraint, ex.Message));
            }
            catch (NoNullAllowedException ex)
            {
                Sender.Tell(new ResultadoOperacao.Rejeicao(msg.MensagemIntegracao, CodigoRejeicao.ErroDeValidacao, string.Format(MensagensValidacao.CAMPO_DO_DATATABLE_OBRIGATORIO, ex.Message)));
            }
            catch (Exception ex)
            {
                Sender.Tell(new ResultadoOperacao.Erro(msg.MensagemIntegracao, ex));
            }
        }
    }
}
