﻿using Akka.Actor;
using DbLink;
using Entidades;
using Integracao.WMS.Clientes;
using Mensagens;
using System;
using Entidades.Enum;
using Mensagens.Resources;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace Integracao.WMS.Escritores
{
    public class WmsEscritorPedidoCancelamentoDataSet : ReceiveActorLogger
    {
        private readonly IClienteIntegracaoPedidoCancelamento _clienteIntegracaoPedidoCancelamento;

        private WmsEscritorPedidoCancelamentoDataSet()
        {
            Receive<MensagensPedidoCancelamento.SalvarPedidoCancelamentoNoWms>(msg => SalvarPedidoCancelamentoNoWms(msg));
        }

        public WmsEscritorPedidoCancelamentoDataSet(IClienteIntegracaoPedidoCancelamento clienteIntegracaoPedidoCancelamento) : this()
        {
            _clienteIntegracaoPedidoCancelamento = clienteIntegracaoPedidoCancelamento;
        }

        private void SalvarPedidoCancelamentoNoWms(MensagensPedidoCancelamento.SalvarPedidoCancelamentoNoWms msg)
        {
            try
            {
                PedidoCancelamento pedidoCancelamento = _clienteIntegracaoPedidoCancelamento.Salvar(msg.PedidoCancelamento, msg.MensagemIntegracao);

                if (msg.MensagemIntegracao.FoiRejeitado())
                    Sender.Tell(new ResultadoOperacao.Rejeicao(msg.MensagemIntegracao, msg.MensagemIntegracao.CodRejeicao.Value, msg.MensagemIntegracao.MotivoRejeicao));
                else
                    Sender.Tell(new ResultadoOperacao.Sucesso(msg.MensagemIntegracao));
            }
            catch (ValidationException ex)
            {
                Sender.Tell(new ResultadoOperacao.Rejeicao(msg.MensagemIntegracao, CodigoRejeicao.ErroDeValidacao, ex.Message));
            }
            catch (ConstraintException ex)
            {
                Sender.Tell(new ResultadoOperacao.Rejeicao(msg.MensagemIntegracao, CodigoRejeicao.ErroDeConstraint, ex.Message));
            }
            catch (NoNullAllowedException ex)
            {
                Sender.Tell(new ResultadoOperacao.Rejeicao(msg.MensagemIntegracao, CodigoRejeicao.ErroDeValidacao, string.Format(MensagensValidacao.CAMPO_DO_DATATABLE_OBRIGATORIO, ex.Message)));
            }
            catch (Exception ex)
            {
                Sender.Tell(new ResultadoOperacao.Erro(msg.MensagemIntegracao, ex));
            }
        }
    }
}
