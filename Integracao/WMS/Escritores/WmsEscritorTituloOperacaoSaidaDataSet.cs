﻿using Akka.Actor;
using DbLink;
using Entidades;
using Entidades.Enum;
using Integracao.WMS.Clientes;
using Mensagens;
using Mensagens.Resources;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace Integracao.WMS.Escritores
{
    public class WmsEscritorTituloOperacaoSaidaDataSet : ReceiveActorLogger
    {
        private readonly IClienteIntegracaoTituloOperacaoSaida _clienteIntegracaoTituloOperacaoSaida;

        private WmsEscritorTituloOperacaoSaidaDataSet()
        {
            Receive<MensagensTituloOperacaoSaida.SalvarRegistroNoWms>(SalvarTituloOperacaoSaidaNoWms);
        }

        public WmsEscritorTituloOperacaoSaidaDataSet(IClienteIntegracaoTituloOperacaoSaida clienteIntegracaoTituloOperacaoSaida) : this()
        {
            _clienteIntegracaoTituloOperacaoSaida = clienteIntegracaoTituloOperacaoSaida;
        }

        private void SalvarTituloOperacaoSaidaNoWms(MensagensTituloOperacaoSaida.SalvarRegistroNoWms msg)
        {
            try
            {
                _clienteIntegracaoTituloOperacaoSaida.Salvar(msg.TituloOperacaoSaida, msg.MensagemIntegracao);

                if (msg.MensagemIntegracao.FoiRejeitado())
                    Sender.Tell(new ResultadoOperacao.Rejeicao(msg.MensagemIntegracao, msg.MensagemIntegracao.CodRejeicao.Value, msg.MensagemIntegracao.MotivoRejeicao));
                else
                    Sender.Tell(new ResultadoOperacao.Sucesso(msg.MensagemIntegracao));
            }
            catch (ValidationException ex)
            {
                Sender.Tell(new ResultadoOperacao.Rejeicao(msg.MensagemIntegracao, CodigoRejeicao.ErroDeValidacao, ex.Message));
            }
            catch (ConstraintException ex)
            {
                Sender.Tell(new ResultadoOperacao.Rejeicao(msg.MensagemIntegracao, CodigoRejeicao.ErroDeConstraint, ex.Message));
            }
            catch (NoNullAllowedException ex)
            {
                Sender.Tell(new ResultadoOperacao.Rejeicao(msg.MensagemIntegracao, CodigoRejeicao.ErroDeValidacao, string.Format(MensagensValidacao.CAMPO_DO_DATATABLE_OBRIGATORIO, ex.Message)));
            }
            catch (Exception ex)
            {
                Sender.Tell(new ResultadoOperacao.Erro(msg.MensagemIntegracao, ex));
            }
        }
    }
}
