﻿using Akka.Actor;
using DbLink;
using Entidades;
using Entidades.Enum;
using Integracao.WMS.Clientes;
using Mensagens;
using Mensagens.Resources;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace Integracao.WMS.Escritores
{
    public class WmsEscritorPedidoDataSet : ReceiveActorLogger
    {
        private readonly IClienteIntegracaoPedido _clienteIntegracaoPedido;

        private WmsEscritorPedidoDataSet()
        {
            Receive<MensagensPedido.SalvarPedidoNoWms>(msg => SalvarPedidoNoWms(msg));
            Receive<MensagensExportacao.ConcluirExportacaoNoWms<Pedido>>(ConcluirExportacaoDoPedidoNoWms);
        }

        public WmsEscritorPedidoDataSet(IClienteIntegracaoPedido clienteIntegracaoPedido) : this()
        {
            _clienteIntegracaoPedido = clienteIntegracaoPedido;
        }

        private void SalvarPedidoNoWms(MensagensPedido.SalvarPedidoNoWms msg)
        {
            try
            {
                _clienteIntegracaoPedido.Salvar(msg.Pedido, msg.MensagemIntegracao);

                if (msg.MensagemIntegracao.FoiRejeitado())
                    Sender.Tell(new ResultadoOperacao.Rejeicao(msg.MensagemIntegracao, msg.MensagemIntegracao.CodRejeicao.Value, msg.MensagemIntegracao.MotivoRejeicao));
                else
                    Sender.Tell(new ResultadoOperacao.Sucesso(msg.MensagemIntegracao));
            }
            catch (ValidationException ex)
            {
                Sender.Tell(new ResultadoOperacao.Rejeicao(msg.MensagemIntegracao, CodigoRejeicao.ErroDeValidacao, ex.Message));
            }
            catch (ConstraintException ex)
            {
                Sender.Tell(new ResultadoOperacao.Rejeicao(msg.MensagemIntegracao, CodigoRejeicao.ErroDeConstraint, ex.Message));
            }
            catch(NoNullAllowedException ex)
            {
                Sender.Tell(new ResultadoOperacao.Rejeicao(msg.MensagemIntegracao, CodigoRejeicao.ErroDeValidacao, string.Format(MensagensValidacao.CAMPO_DO_DATATABLE_OBRIGATORIO, ex.Message)));
            }
            catch (Exception ex)
            {
                Sender.Tell(new ResultadoOperacao.Erro(msg.MensagemIntegracao, ex));
            }
        }

        private void ConcluirExportacaoDoPedidoNoWms(MensagensExportacao.ConcluirExportacaoNoWms<Pedido> mensagem)
        {
            try
            {
                bool resultadoOperacao = _clienteIntegracaoPedido.ConcluirPedido(mensagem.MensagemIntegracao, mensagem.Registro.IdWms);

                if (resultadoOperacao)
                    Sender.Tell(new MensagensExportacao.RegistroExportadoComSucesso<Pedido>(mensagem.Registro, mensagem.MensagemIntegracao));
                else
                    Sender.Tell(new MensagensExportacao.ErroAoConcluirExportacaoNoWms<Pedido>(mensagem.Registro, mensagem.MensagemIntegracao));
            }
            catch (Exception ex)
            {
                Sender.Tell(new MensagensExportacao.ErroNaExportacao<Pedido>(mensagem.Registro, mensagem.MensagemIntegracao, ex));
            }
        }
    }
}
