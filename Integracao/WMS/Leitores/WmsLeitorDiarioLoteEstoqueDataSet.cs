﻿using Akka.Actor;
using DbLink;
using Entidades;
using Integracao.WMS.Clientes;
using Mensagens;
using System;
using System.Collections.Generic;

namespace Integracao.WMS.Escritores
{
    public class WmsLeitorDiarioLoteEstoqueDataSet : ReceiveActorLogger
    {
        private readonly IClienteIntegracaoDiarioLoteEstoque _clienteIntegracao;

        public WmsLeitorDiarioLoteEstoqueDataSet(IClienteIntegracaoDiarioLoteEstoque clienteIntegracao = null)
        {
            _clienteIntegracao = clienteIntegracao ?? new ClienteIntegracaoDiarioLoteEstoque();

            Receive<MensagensDiarioLoteEstoque.BuscarNovosRegistrosNoWms>(msg => BuscarNovosRegistrosNoWms(msg));
        }

        private void BuscarNovosRegistrosNoWms(MensagensDiarioLoteEstoque.BuscarNovosRegistrosNoWms msg)
        {
            try
            {
                IList<DiarioLoteEstoque> novosRegistros = _clienteIntegracao.BuscarNovosRegistros();

                if(novosRegistros.Count > 0)
                    Sender.Tell(new MensagensDiarioLoteEstoque.NovosRegistrosEncontrados(novosRegistros));
            }
            catch (Exception ex)
            {
                LogErrorProcessingMessage(msg, new Exception("Falha ao buscar novos registros de diario_produto_estoque no WMS", ex));
            }
        }
    }
}
