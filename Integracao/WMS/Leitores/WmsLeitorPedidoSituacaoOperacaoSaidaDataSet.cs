﻿using System;
using DbLink;
using Entidades;
using Mensagens;
using Akka.Actor;
using Integracao.WMS.Clientes;
using System.Collections.Generic;

namespace Integracao.WMS.Leitores
{
    public class WmsLeitorPedidoSituacaoOperacaoSaidaDataSet : ReceiveActorLogger
    {
        private readonly IClienteIntegracaoPedidoSituacao _clienteIntegracao;

        private WmsLeitorPedidoSituacaoOperacaoSaidaDataSet()
        {
            Receive<MensagensExportacao.BuscarNovosRegistrosNoWms>(BuscarNovosRegistrosNoWms);
        }

        public WmsLeitorPedidoSituacaoOperacaoSaidaDataSet(IClienteIntegracaoPedidoSituacao clienteIntegracao) : this()
        {
            _clienteIntegracao = clienteIntegracao;
        }

        private void BuscarNovosRegistrosNoWms(MensagensExportacao.BuscarNovosRegistrosNoWms mensagem)
        {
            try
            {
                IList<PedidoSituacaoOperacaoSaida> novosRegistros = _clienteIntegracao.BuscarNovosRegistros();

                if (novosRegistros.Count > 0)
                    Sender.Tell(new MensagensExportacao.NovosRegistrosEncontradosNoWms<PedidoSituacaoOperacaoSaida>(novosRegistros));
            }
            catch (Exception ex)
            {
                LogErrorProcessingMessage(mensagem, new Exception("Falha ao buscar novos registros de pedido_situacao_operacao_saida no WMS", ex));
            }
        }
    }
}
