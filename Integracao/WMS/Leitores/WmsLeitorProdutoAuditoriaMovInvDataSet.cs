﻿using Akka.Actor;
using DbLink;
using Entidades;
using Integracao.WMS.Clientes;
using Mensagens;
using System;
using System.Collections.Generic;

namespace Integracao.WMS.Escritores
{
    public class WmsLeitorProdutoAuditoriaMovInvDataSet : ReceiveActorLogger
    {
        private readonly IClienteIntegracaoProdutoAuditoriaMovInv _clienteIntegracao;

        private WmsLeitorProdutoAuditoriaMovInvDataSet()
        {
            Receive<MensagensProdutoAuditoriaMovInv.BuscarNovosRegistrosNoWms>(msg => BuscarNovosRegistrosNoWms(msg));
        }

        public WmsLeitorProdutoAuditoriaMovInvDataSet(IClienteIntegracaoProdutoAuditoriaMovInv clienteIntegracao) : this()
        {
            _clienteIntegracao = clienteIntegracao;
        }

        private void BuscarNovosRegistrosNoWms(MensagensProdutoAuditoriaMovInv.BuscarNovosRegistrosNoWms mensagem)
        {
            try
            {
                IList<ProdutoAuditoriaMovInv> novosRegistros = _clienteIntegracao.BuscarNovosRegistros(mensagem.ObterDataInicialParaConsulta());

                if(novosRegistros.Count > 0)
                    Sender.Tell(new MensagensProdutoAuditoriaMovInv.NovosRegistrosEncontrados(novosRegistros));
            }
            catch (Exception ex)
            {
                LogErrorProcessingMessage(mensagem, new Exception("Falha ao buscar novos registros de produto_auditoria_mov_inv no WMS", ex));
            }
        }
    }
}
