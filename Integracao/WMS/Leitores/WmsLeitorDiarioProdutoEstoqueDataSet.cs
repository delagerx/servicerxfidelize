﻿using Akka.Actor;
using DbLink;
using Entidades;
using Integracao.WMS.Clientes;
using Mensagens;
using System;
using System.Collections.Generic;

namespace Integracao.WMS.Escritores
{
    public class WmsLeitorDiarioProdutoEstoqueDataSet : ReceiveActorLogger
    {
        private readonly IClienteIntegracaoDiarioProdutoEstoque _clienteIntegracao;

        private WmsLeitorDiarioProdutoEstoqueDataSet()
        {
            Receive<MensagensDiarioProdutoEstoque.BuscarNovosRegistrosNoWms>(msg => BuscarNovosRegistrosNoWms(msg));
        }

        public WmsLeitorDiarioProdutoEstoqueDataSet(IClienteIntegracaoDiarioProdutoEstoque clienteIntegracao) : this()
        {
            _clienteIntegracao = clienteIntegracao;
        }

        private void BuscarNovosRegistrosNoWms(MensagensDiarioProdutoEstoque.BuscarNovosRegistrosNoWms msg)
        {
            try
            {
                IList<DiarioProdutoEstoque> novosRegistros = _clienteIntegracao.BuscarNovosRegistros();

                if(novosRegistros.Count > 0)
                    Sender.Tell(new MensagensDiarioProdutoEstoque.NovosRegistrosEncontrados(novosRegistros));
            }
            catch (Exception ex)
            {
                LogErrorProcessingMessage(msg, new Exception("Falha ao buscar novos registros de diario_produto_estoque no WMS", ex));
            }
        }
    }
}
