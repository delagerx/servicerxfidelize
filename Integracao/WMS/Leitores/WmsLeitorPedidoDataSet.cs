﻿using Akka.Actor;
using DbLink;
using Entidades;
using Integracao.WMS.Clientes;
using Mensagens;
using System;
using System.Collections.Generic;

namespace Integracao.WMS.Escritores
{
    public class WmsLeitorPedidoDataSet : ReceiveActorLogger
    {
        private readonly IClienteIntegracaoPedido _clienteIntegracaoPedido;

        private WmsLeitorPedidoDataSet()
        {
            Receive<MensagensExportacao.BuscarNovosRegistrosNoWms>(VerificarNovosPedidos);
        }

        public WmsLeitorPedidoDataSet(IClienteIntegracaoPedido clienteIntegracaoPedido) : this()
        {
            _clienteIntegracaoPedido = clienteIntegracaoPedido;
        }

        private void VerificarNovosPedidos(MensagensExportacao.BuscarNovosRegistrosNoWms mensagem)
        {
            try
            {
                IList<Pedido> novosPedidos = _clienteIntegracaoPedido.BuscarNovosPedidos();

                if(novosPedidos.Count > 0)
                    Sender.Tell(new MensagensExportacao.NovosRegistrosEncontradosNoWms<Pedido>(novosPedidos));
            }
            catch (Exception ex)
            {
                LogErrorProcessingMessage(mensagem, new Exception("Falha ao buscar novos pedidos no WMS", ex));
            }
        }
    }
}
