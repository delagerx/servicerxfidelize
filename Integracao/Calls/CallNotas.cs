﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Core.Models.Order;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Integracao.Models.Orders;
using Core.Models.Invoices;
using Integracao.LogClass;
using ConsoleApplication.Email;

namespace Integracao.Calls
{
    public class CallNotas
    {
        private static string filial = ConfigurationManager.AppSettings["Filiais"];
        private static string situacaoInvoice = ConfigurationManager.AppSettings["situacaoInvoice"];
        private static string[] condicaoComercial = ConfigurationManager.AppSettings["CondicaoCommercialFidelize"].Split(',');
        private static Log logNotas = new Log("CallNotasLog", "CallNotasLog");
        private static ErrorMail errorMail = new ErrorMail();
        private static string tipo = ConfigurationManager.AppSettings["Tipo"];

        public static async Task<bool> RequestInvoiceRxAsync()
        {

            
                var client = new HttpClient();                                        //Confirmar se é necessário incluir a situação 
                client.Timeout = TimeSpan.FromMinutes(10);
                var codInvoice = "-1";
                var url = ConfigurationManager.AppSettings["ServiceRx"] + "/Invoice?subsidiaryIds=" + filial; //?situacion=" + situacaoInvoice;
                var request = new HttpRequestMessage(HttpMethod.Get, url);

            try
            {
                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var content = await response.Content.ReadAsStringAsync();
                    if (content != null && !content.Equals("[]"))
                        logNotas.WriteEntry("Novo Objeto de RX To API - Fluxo 5");
                    else
                        logNotas.WriteEntry("Nenhum objeto RX To API criado - Fluxo 5 ");
                    List<InvoiceApi> InvoicesApi = Conversores.ConvertJson.ConvertRxToAPiInvoice(content);
                    List<OrderRx> OrderRXReturn = new List<OrderRx>();
                    OrderStatus RetornoStatus = new OrderStatus();
                    foreach (InvoiceApi Invoice in InvoicesApi)
                    {
                        if (Invoice.Invoice_number != null)
                            codInvoice = Invoice.Invoice_number;
                        var JsonNotas = JsonConvert.SerializeObject(Invoice, Formatting.None,
                               new JsonSerializerSettings
                               {
                                   NullValueHandling = NullValueHandling.Ignore
                               });
                        var JsonApi = new StringContent(JsonNotas, Encoding.UTF8, "application/json");
                        var urlapi = ConfigurationManager.AppSettings["ServiceApi"] + "/createInvoice";
                        using (var responseApi = client.PostAsync(urlapi, JsonApi).Result)
                        {
                            if (responseApi.StatusCode == System.Net.HttpStatusCode.BadRequest)
                            {
                                string log = await responseApi.Content.ReadAsStringAsync();
                                if (log != null && !log.Equals("[]"))
                                    logNotas.WriteEntry("Fluxo 5 - Log: " + log);
                                if (Int32.Parse(ConfigurationManager.AppSettings["ChaveEnvio"]) == 1)
                                    errorMail.SendErrorMail("TaskFluxo5", log, (int)responseApi.StatusCode, codInvoice);

                                continue;
                            }
                            responseApi.EnsureSuccessStatusCode();
                            var contentApi = await responseApi.Content.ReadAsStringAsync();
                            if (contentApi == null)
                            {
                                continue;
                            }
                            var Jo = JObject.Parse(contentApi);
                            Jo["status"] = RetornoStatus.getIndiceStatus(Jo["status"]);
                            OrderRx orderRX = Conversores.ConvertJson.ConvertApiToRxInvoiceReturn(Jo.ToString());

                            //foreach (var cond in condicaoComercial)
                            //{
                            //if (string.IsNullOrEmpty(orderRX.Commercial_condition))
                            //{
                            //orderRX.id_Commercial_condition = null;
                            //}
                            //else //if (cond.Contains(orderRX.Commercial_condition))
                            //{
                            //string condAux = cond;
                            //var index = condAux.IndexOf('/');
                            //orderRX.id_Commercial_condition = int.Parse(condAux.Remove(index, condAux.Length - index));
                            orderRX.id_Commercial_condition = Invoice.id_Commercial_condition;
                            //}
                            //}

                            orderRX.Log_id = Invoice.Log_id;
                            OrderRXReturn.Add(orderRX);
                        }
                    }
                    if (OrderRXReturn.Count > 0)
                        foreach (OrderRx order in OrderRXReturn)
                        {
                            logNotas.WriteEntry("Novo Objeto de RX To API - Fluxo 5" + (JsonConvert.SerializeObject(order)));
                        }
                    var JsonNotasReturn = JsonConvert.SerializeObject(OrderRXReturn, Newtonsoft.Json.Formatting.None,
                               new JsonSerializerSettings
                               {
                                   NullValueHandling = NullValueHandling.Ignore
                               });
                    var JsonPutApi = new StringContent(JsonNotasReturn, Encoding.UTF8, "application/json");
                    var urlPut = ConfigurationManager.AppSettings["ServiceRx"] + "/Invoice" + "?type=" + tipo;
                    using (var responseApi = client.PutAsync(urlPut, JsonPutApi).Result)
                    {
                        if (!responseApi.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                        {
                            logNotas.WriteEntry("Falha no Fluxo 5, devido a: " + responseApi.Content);
                            return false;
                        }
                        responseApi.EnsureSuccessStatusCode();
                        Console.WriteLine("Terminou Fluxo 5");
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                string mensagem, log;
                if (ex.InnerException != null)
                {
                    mensagem = ex.InnerException.Message + ";<br/>" + ex.StackTrace;
                    log = ex.InnerException.Message + ";\n" + ex.StackTrace;
                }
                else
                {
                    mensagem = ex.Message + ";<br/>" + ex.StackTrace;
                    log = ex.Message + ";\n" + ex.StackTrace;
                }
                if (Int32.Parse(ConfigurationManager.AppSettings["ChaveEnvio"]) == 1)
                    errorMail.SendErrorMail("TaskFluxo4", mensagem, -1, (int.Parse(codInvoice) != -1) ? int.Parse(codInvoice) : 0);

                if (ex.InnerException != null)
                    logNotas.WriteEntry("Falha no Fluxo 5, devido a: " + ex.InnerException.Message);
                else
                    logNotas.WriteEntry("Falha no Fluxo 5, devido a: " + ex.Message);

                throw ex;
            }

        }

        public static async Task<bool> RequestInvoiceReversalRxAsync()
        {
            var client = new HttpClient();                                        //Confirmar se é necessário incluir a situação 
            client.Timeout = TimeSpan.FromMinutes(10);
            var codInvoice = "-1";
            var url = ConfigurationManager.AppSettings["ServiceRx"] + "/InvoiceReversal?subsidiaryIds=" + filial + "&type="+tipo;
            var request = new HttpRequestMessage(HttpMethod.Get, url);

            try
            {
                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var content = await response.Content.ReadAsStringAsync();
                    if (content != null && !content.Equals("[]"))
                        logNotas.WriteEntry("Novo Objeto de RX To API - Fluxo 7");
                    else
                        logNotas.WriteEntry("Nenhum objeto RX To API criado - Fluxo 7");
                    List<InvoiceApi> InvoicesApi = Conversores.ConvertJson.ConvertRxToAPiInvoice(content);
                    List<OrderRx> OrderRXReturn = new List<OrderRx>();
                    OrderStatus RetornoStatus = new OrderStatus();
                    foreach (InvoiceApi Invoice in InvoicesApi)
                    {
                        if (Invoice.Invoice_number != null)
                            codInvoice = Invoice.Invoice_number;
                        var JsonNotas = JsonConvert.SerializeObject(Invoice, Formatting.None,
                               new JsonSerializerSettings
                               {
                                   NullValueHandling = NullValueHandling.Ignore
                               });
                        var JsonApi = new StringContent(JsonNotas, Encoding.UTF8, "application/json");
                        var urlapi = ConfigurationManager.AppSettings["ServiceApi"] + "/createInvoiceReversal";
                        using (var responseApi = client.PostAsync(urlapi, JsonApi).Result)
                        {
                            if (responseApi.StatusCode == System.Net.HttpStatusCode.BadRequest)
                            {
                                string log = await responseApi.Content.ReadAsStringAsync();
                                if (log != null && !log.Equals("[]"))
                                    logNotas.WriteEntry("Fluxo 7 - Log: " + log);
                                if (Int32.Parse(ConfigurationManager.AppSettings["ChaveEnvio"]) == 1)
                                    errorMail.SendErrorMail("TaskFluxo7", log, (int)responseApi.StatusCode, codInvoice);

                                continue;
                            }
                            responseApi.EnsureSuccessStatusCode();
                            var contentApi = await responseApi.Content.ReadAsStringAsync();
                            var Jo = JObject.Parse(contentApi);
                            Jo["status"] = RetornoStatus.getIndiceStatus(Jo["status"]);
                            OrderRx orderRX = Conversores.ConvertJson.ConvertApiToRxInvoiceReturn(Jo.ToString());

                            //foreach (var cond in condicaoComercial)
                            //{
                            //if (string.IsNullOrEmpty(orderRX.Commercial_condition))
                            //{
                            //orderRX.id_Commercial_condition = null;
                            //}
                            //else //if (cond.Contains(orderRX.Commercial_condition))
                            //{
                            //string condAux = cond;
                            //var index = condAux.IndexOf('/');
                            //orderRX.id_Commercial_condition = int.Parse(condAux.Remove(index, condAux.Length - index));
                            orderRX.id_Commercial_condition = Invoice.id_Commercial_condition;
                            //}
                            //}
                            orderRX.Log_id = Invoice.Log_id;
                            OrderRXReturn.Add(orderRX);
                        }
                    }
                    var JsonNotasReturn = JsonConvert.SerializeObject(OrderRXReturn, Newtonsoft.Json.Formatting.None,
                               new JsonSerializerSettings
                               {
                                   NullValueHandling = NullValueHandling.Ignore
                               });
                    var JsonPutApi = new StringContent(JsonNotasReturn, Encoding.UTF8, "application/json");
                    var urlPut = ConfigurationManager.AppSettings["ServiceRx"] + "/InvoiceReversal" + "& type = " + tipo;
                    using (var responseApi = client.PutAsync(urlPut, JsonPutApi).Result)
                    {
                        if (!responseApi.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                        {
                            logNotas.WriteEntry("Falha no Fluxo 7, devido a: " + responseApi.Content);
                            return false;
                        }
                        responseApi.EnsureSuccessStatusCode();
                        Console.WriteLine("Terminou Fluxo 7");
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                string mensagem, log;
                if (ex.InnerException != null)
                {
                    mensagem = ex.InnerException.Message + ";<br/>" + ex.StackTrace;
                    log = ex.InnerException.Message + ";\n" + ex.StackTrace;
                }
                else
                {
                    mensagem = ex.Message + ";<br/>" + ex.StackTrace;
                    log = ex.Message + ";\n" + ex.StackTrace;
                }
                if (Int32.Parse(ConfigurationManager.AppSettings["ChaveEnvio"]) == 1)
                    errorMail.SendErrorMail("TaskFluxo4", mensagem, -1, (int.Parse(codInvoice) != -1) ? int.Parse(codInvoice) : 0);

                if (ex.InnerException != null)
                    logNotas.WriteEntry("Falha no Fluxo 7, devido a: " + ex.InnerException.Message);
                else
                    logNotas.WriteEntry("Falha no Fluxo 7, devido a: " + ex.Message);

                throw ex;
            }

        }
    }
}