﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Core.Models.Order;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Integracao.Models.Orders;
using Integracao.LogClass;
using ConsoleApplication.Email;
using System.Diagnostics;

namespace Integracao.Calls
{
    public class CallPedidos
    {
        private static string filial = ConfigurationManager.AppSettings["Filiais"];
        private static string situacaoConsulta = ConfigurationManager.AppSettings["SituacaoConsulta"];
        private static string situacaoRetorno = ConfigurationManager.AppSettings["SituacaoRetorno"];

        private static string situacaoCancelamento = ConfigurationManager.AppSettings["SituacaoCancelamento"];
        private static string tipo = ConfigurationManager.AppSettings["Tipo"];
        private static string tipoConsulta = ConfigurationManager.AppSettings["TipoConsulta"];
        private static string[] condicaoComercial = ConfigurationManager.AppSettings["CondicaoCommercialFidelize"].Split(',');
        private static string condicaoComercialString = ConfigurationManager.AppSettings["CondicaoCommercialFidelize"];
        private static Log logPedido = new Log("CallPedidoLog", "CallPedidoLog");
        private static ErrorMail errorMail = new ErrorMail();


        public static async Task<bool> RequestOrderRxAsync()
        {
            var client = new HttpClient
            {
                Timeout = TimeSpan.FromMinutes(1000)
            };
            var codPedido = -1;
            var url = ConfigurationManager.AppSettings["ServiceRx"] + "/Order?situation=" + situacaoConsulta + "&type=" + tipo + "&subsidiaryIds=" + filial + "&typeQuery=" + tipoConsulta + "&conditions=" + condicaoComercialString;
            var request = new HttpRequestMessage(HttpMethod.Get, url);

            try
            {
                using (var response = await client.SendAsync(request))
                {
                    var content = await response.Content.ReadAsStringAsync();
                    if (content != null && !content.Equals("[]"))
                    {
                        logPedido.WriteEntry("Novo Objeto de RX To API - Fluxo 1 ");
                    }
                    else
                    {
                        logPedido.WriteEntry("Nenhum objeto RX To API criado - Fluxo 1 ");
                    }
                    List<OrderApi> OrdersApi = Conversores.ConvertJson.ConvertRxToAPi(content);
                    List<OrderRx> ordersRX = new List<OrderRx>();
                    OrderStatus orderstatus = new OrderStatus();
                    foreach (OrderApi Order in OrdersApi)
                    {
                        if (Order.Client_Code != null)
                            codPedido = Order.Client_Code.Value;

                        Order.WholeSaler = Order.WholeSaler
                            .Replace("/", "")
                            .Replace("-", "");

                        Order.Client_Identification = Order.Client_Identification
                         .Replace("/", "")
                         .Replace("-", "");                       


                        var JsonApi = new StringContent(JsonConvert.SerializeObject(Order, Formatting.None,
                                new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    Formatting = Formatting.Indented                                    
                                }), Encoding.UTF8, "application/json");

                        var urlapi = ConfigurationManager.AppSettings["ServiceApi"] + "/createOrder";
                        using (var responseApi = client.PostAsync(urlapi, JsonApi).Result)
                        {
                            if (responseApi.StatusCode == System.Net.HttpStatusCode.BadRequest)
                            {
                                string log = await responseApi.Content.ReadAsStringAsync();
                                if (int.Parse(ConfigurationManager.AppSettings["ChaveEnvio"]) == 1)
                                    errorMail.SendErrorMail("TaskFluxo1", log, (int)responseApi.StatusCode, codPedido);
                                logPedido.WriteEntry("Falha no Fluxo 1. Comando: 'createOrder'. Devido a: " + log);
                                continue;
                            }
                            responseApi.EnsureSuccessStatusCode();
                            var contentApi = await responseApi.Content.ReadAsStringAsync();
                            var Jo = JObject.Parse(contentApi);
                            Jo["status"] = orderstatus.getIndiceStatus(Jo["status"]);
                            OrderRx OrderRx = Conversores.ConvertJson.ConvertApiToRx(Jo.ToString());
                            OrderRx.Log_id = Order.Log_id;

                            //foreach (var cond in condicaoComercial)
                            //{
                            //if(string.IsNullOrEmpty(OrderRx.Commercial_condition))
                            //{
                            //OrderRx.id_Commercial_condition = null;
                            //foreach (var prod in OrderRx.Products)
                            //{
                            //    prod.Id_Commercial_condition = null;
                            //}
                            //}
                            //else //if (cond.Contains(OrderRx.Commercial_condition))
                            //{
                            //string condAux = cond;
                            //var index = condAux.IndexOf('/');
                            //var condId = condAux.Remove(index, condAux.Length - index);

                            //OrderRx.id_Commercial_condition = int.Parse(condId);

                            OrderRx.id_Commercial_condition = Order.id_Commercial_condition;
                            //}                               
                            //}
                            ordersRX.Add(OrderRx);
                        }
                    }

                    if (ordersRX.Count > 0)
                        foreach (OrderRx order in ordersRX)
                        {
                            logPedido.WriteEntry("Novo Objeto de RX To API - Fluxo 1: " + (JsonConvert.SerializeObject(order)));
                        }
                    var JsonPutApi = new StringContent(JsonConvert.SerializeObject(ordersRX, Newtonsoft.Json.Formatting.None,
                               new JsonSerializerSettings
                               {
                                   NullValueHandling = NullValueHandling.Ignore
                               }), Encoding.UTF8, "application/json");

                    var urlPut = ConfigurationManager.AppSettings["ServiceRx"] + "/Order?situation=" + situacaoConsulta + "&type=" + tipo + "&subsidiaryIds=" + filial;
                    using (var responseApi = client.PutAsync(urlPut, JsonPutApi).Result)
                    {
                        string log = await responseApi.Content.ReadAsStringAsync();
                        if (!responseApi.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                        {
                            logPedido.WriteEntry("Falha no Fluxo 1. Comando 'Order'. Devido a: " + responseApi.Content);
                            return false;
                        }
                        responseApi.EnsureSuccessStatusCode();
                        Console.WriteLine("Terminou Fluxo 1");
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                string mensagem, log;
                if (ex.InnerException != null)
                {
                    mensagem = ex.InnerException.Message + ";<br/>" + ex.StackTrace;
                    log = ex.InnerException.Message + ";\n" + ex.StackTrace;
                }
                else
                {
                    mensagem = ex.Message + ";<br/>" + ex.StackTrace;
                    log = ex.Message + ";\n" + ex.StackTrace;
                }
                if (Int32.Parse(ConfigurationManager.AppSettings["ChaveEnvio"]) == 1)
                    errorMail.SendErrorMail("TaskFluxo1", mensagem, -1, (codPedido != -1) ? codPedido : 0);

                if (ex.InnerException != null)
                    logPedido.WriteEntry("InnerException. Falha no Fluxo 1 devido a: " + ex.InnerException.Message);
                else
                {
                    int line = (new StackTrace(ex, true)).GetFrame(0).GetFileLineNumber();
                    logPedido.WriteEntry("OuterException. Falha no Fluxo 1 devido a: " + ex.Message + ";\n" + "Linha: " + line + ";\n" + ex.StackTrace);
                }

                throw ex;
            }
        }

        private static List<OrderProductRx> isCut(IEnumerable<OrderProductRx> products)
        {
            List<OrderProductRx> CutProducts = new List<OrderProductRx>();
            foreach (var item in products)
            {
                item.MustCut = isCut(item.StatusProduct);
                CutProducts.Add(item);
            }
            return CutProducts;
        }

        private static bool isCut(int? statusProduct)
        {
            List<string> cut = new List<string>(ConfigurationManager.AppSettings["SituacaoCorte"].Split(','));
            return cut.Contains(statusProduct.GetValueOrDefault().ToString());
        }

        public static async Task<bool> UpdateOrderStatus()
        {
            var client = new HttpClient();
            client.Timeout = TimeSpan.FromMinutes(100);
            var codPedido = -1;
            var url = ConfigurationManager.AppSettings["ServiceRx"] + "/Order/getOrderReturn?subsidiaryIds=" + filial;
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            try
            {
                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var content = await response.Content.ReadAsStringAsync();
                    if (content != null && !content.Equals("[]"))
                        logPedido.WriteEntry("Novo Objeto de RX To API  - Fluxo 3: ");
                    List<OrderApi> OrdersApi = JsonConvert.DeserializeObject<List<OrderApi>>(content);
                    List<OrderRx> ordersRX = new List<OrderRx>();
                    OrderStatus orderstatus = new OrderStatus();
                    ProductReason reason = new ProductReason();
                    foreach (OrderApi Order in OrdersApi)
                    {
                        if (Order.Client_Code != null)
                            codPedido = Order.Client_Code.Value;
                        var JsonApi = new StringContent(JsonConvert.SerializeObject(Order, Newtonsoft.Json.Formatting.None,
                                new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore
                                }), Encoding.UTF8, "application/json");
                        var urlapi = ConfigurationManager.AppSettings["ServiceApi"] + "/getOrder";
                        using (var responseApi = client.PostAsync(urlapi, JsonApi).Result)
                        {
                            if (responseApi.StatusCode == System.Net.HttpStatusCode.BadRequest)
                            {
                                string log = await responseApi.Content.ReadAsStringAsync();
                                if (Int32.Parse(ConfigurationManager.AppSettings["ChaveEnvio"]) == 1)
                                    errorMail.SendErrorMail("TaskFluxo3", log, (int)responseApi.StatusCode, codPedido);
                                logPedido.WriteEntry("Falha no Fluxo 3, devido a: " + log);
                                continue;
                            }
                            responseApi.EnsureSuccessStatusCode();
                            var contentApi = await responseApi.Content.ReadAsStringAsync();
                            if (contentApi != null && !contentApi.Equals("[]"))
                                logPedido.WriteEntry("Fluxo 3 - Json recebido: " + contentApi);
                            var Jo = JObject.Parse(contentApi);
                            Jo["status"] = orderstatus.getIndiceStatus(Jo["status"]);

                            foreach (JToken product in Jo["products"])
                            {
                                product["product_reason"] = reason.getIndiceReason(product["product_reason"]);
                            }
                            OrderRx OrderRx = Conversores.ConvertJson.ConvertApiToRxReturn(Jo.ToString());
                            //foreach (var cond in condicaoComercial)
                            //{
                            //if (string.IsNullOrEmpty(OrderRx.Commercial_condition))
                            //{
                            //OrderRx.id_Commercial_condition = null;                                    
                            //}
                            //else //if (cond.Contains(OrderRx.Commercial_condition))
                            //{
                            //string condAux = cond;
                            //var index = condAux.IndexOf('/');
                            //OrderRx.id_Commercial_condition = int.Parse(condAux.Remove(index, condAux.Length - index));
                            OrderRx.id_Commercial_condition = Order.id_Commercial_condition;
                            //}
                            //}
                            OrderRx.Products = isCut(OrderRx.Products);
                            OrderRx.Log_id = Order.Log_id;    
                            foreach(var product in OrderRx.Products)
                            {
                                product.Id_Order_Log = Order.Log_id;
                            }
                            ordersRX.Add(OrderRx);
                        }
                    }

                    if (ordersRX.Count > 0)
                    {
                        foreach (OrderRx order in ordersRX)
                        {
                            logPedido.WriteEntry("Novo Objeto de API To RX - Fluxo 3: " + (JsonConvert.SerializeObject(order)));
                        }
                    }
                    var JsonPutApi = new StringContent(JsonConvert.SerializeObject(ordersRX, Formatting.None,
                           new JsonSerializerSettings
                           {
                               NullValueHandling = NullValueHandling.Ignore,
                               Formatting = Formatting.Indented
                           }), Encoding.UTF8, "application/json");
                    var urlPut = ConfigurationManager.AppSettings["ServiceRx"] + "/Order/OrderReturn?situation=" + situacaoConsulta + "&subsidiaryIds=" + filial;
                    using (var responseApi = client.PutAsync(urlPut, JsonPutApi).Result)
                    {
                        if (!responseApi.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                        {
                            logPedido.WriteEntry("Falha no Fluxo 3, devido a: " + responseApi.Content);
                            return false;
                        }
                        responseApi.EnsureSuccessStatusCode();
                        Console.WriteLine("Terminou Fluxo 3");
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                string mensagem, log;
                if (ex.InnerException != null)
                {
                    mensagem = ex.InnerException.Message + ";<br/>" + ex.StackTrace;
                    log = ex.InnerException.Message + ";\n" + ex.StackTrace;
                }
                else
                {
                    mensagem = ex.Message + ";<br/>" + ex.StackTrace;
                    log = ex.Message + ";\n" + ex.StackTrace;
                }
                if (Int32.Parse(ConfigurationManager.AppSettings["ChaveEnvio"]) == 1)
                    errorMail.SendErrorMail("TaskFluxo3", mensagem, -1, (codPedido != -1) ? codPedido : 0);

                if (ex.InnerException != null)
                    logPedido.WriteEntry("Falha no Fluxo 3, devido a: " + ex.InnerException.Message);
                else 
                    logPedido.WriteEntry("Falha no Fluxo 3, devido a: " + ex.Message);

                throw ex;
            }
        }
        //fluxo 4
        public static async Task<bool> RequestOrderRetornoRxAsync()
        {
            var client = new HttpClient();
            client.Timeout = TimeSpan.FromMinutes(20);
            var codPedido = -1;
            var url = ConfigurationManager.AppSettings["ServiceRx"] + "/Order/getOrderResponse?subsidiaryIds=" + filial + "&situation = " + situacaoRetorno + "&typeQuery=" + tipoConsulta;
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            try
            {
                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var content = await response.Content.ReadAsStringAsync();
                    List<OrderRx> ordersRX = new List<OrderRx>();
                    if (content != null && !content.Equals("[]"))
                        logPedido.WriteEntry("Novo Objeto de RX To API - Fluxo 4 ");
                    List<OrderApi> OrdersApi = Conversores.ConvertJson.ConvertRxToAPiStatus(content);
                    OrderStatus orderStatus = new OrderStatus();
                    ProductReason productReason = new ProductReason();
                    foreach (OrderApi Order in OrdersApi)
                    {
                        if (Order.Client_Code != null)
                            codPedido = Order.Client_Code.Value;
                        if (Order.WholeSaler != null)
                        {
                            Order.WholeSaler = Order.WholeSaler
                            .Replace("/", "")
                            .Replace("-", "");
                        }
                        if (Order.Client_Identification != null)
                        {

                            Order.Client_Identification = Order.Client_Identification
                             .Replace("/", "")
                             .Replace("-", "");
                        }


                        var JsonApi = new StringContent(JsonConvert.SerializeObject(Order, Formatting.None,
                                new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore
                                }), Encoding.UTF8, "application/json");

                        var urlapi = ConfigurationManager.AppSettings["ServiceApi"] + "/createResponse";
                        using (var responseApi = client.PostAsync(urlapi, JsonApi).Result)
                        {

                            if (responseApi.StatusCode == System.Net.HttpStatusCode.BadRequest)
                            {
                                string log = await responseApi.Content.ReadAsStringAsync();
                                if (log != null && !log.Equals("[]"))
                                    logPedido.WriteEntry("Fluxo 4 - Log: " + log);
                                if (Int32.Parse(ConfigurationManager.AppSettings["ChaveEnvio"]) == 1)
                                    errorMail.SendErrorMail("TaskFluxo4", log, (int)responseApi.StatusCode, codPedido);

                                continue;
                            }

                            responseApi.EnsureSuccessStatusCode();
                            var contentApi = await responseApi.Content.ReadAsStringAsync();
                            if (contentApi == null)
                            {
                                continue;
                            }
                            var JsonObj = JObject.Parse(contentApi);
                            JsonObj["status"] = orderStatus.getIndiceStatus(JsonObj["status"]);
                            foreach (JToken product in JsonObj["products"])
                            {
                                product["product_reason"] = productReason.getIndiceReason(product["product_reason"]);
                            }

                            OrderRx OrderRx = Conversores.ConvertJson.ConvertApiToRxStatus(JsonObj.ToString());

                            //foreach (var cond in condicaoComercial)
                            //{
                            //if (string.IsNullOrEmpty(OrderRx.Commercial_condition))
                            //{
                            //OrderRx.id_Commercial_condition = null;
                            //}
                            //else if (cond.Contains(OrderRx.Commercial_condition))
                            //{
                            //string condAux = cond;
                            //var index = condAux.IndexOf('/');
                            //OrderRx.id_Commercial_condition = int.Parse(condAux.Remove(index, condAux.Length - index));
                            OrderRx.id_Commercial_condition = Order.id_Commercial_condition;
                            //}
                            //}

                            OrderRx.Log_id = Order.Log_id;
                            foreach (var product in OrderRx.Products)
                            {
                                product.Id_Order_Log = Order.Log_id;
                            }

                            ordersRX.Add(OrderRx);

                        }

                    }
                    if (ordersRX.Count > 0)
                        foreach (OrderRx order in ordersRX)
                        {
                            logPedido.WriteEntry("Novo Objeto de RX To API - Fluxo 4: " + (JsonConvert.SerializeObject(order)));
                        }
                    var JsonPutApi = new StringContent(JsonConvert.SerializeObject(ordersRX, Newtonsoft.Json.Formatting.None,
                               new JsonSerializerSettings
                               {
                                   NullValueHandling = NullValueHandling.Ignore
                               }), Encoding.UTF8, "application/json");
                    var urlPut = ConfigurationManager.AppSettings["ServiceRx"] + "/Order/OrderResponse?situation=" + situacaoRetorno + "&type=" + tipo + "&subsidiaryIds=" + filial;
                    using (var responseApi = client.PutAsync(urlPut, JsonPutApi).Result)
                    {
                        if (!responseApi.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                        {
                            logPedido.WriteEntry("Falha no Fluxo 4, devido a: " + responseApi.Content);
                            return false;
                        }
                        responseApi.EnsureSuccessStatusCode();
                        Console.WriteLine("Terminou Fluxo 4");
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                string mensagem, log;
                if (ex.InnerException != null)
                {
                    mensagem = ex.InnerException.Message + ";<br/>" + ex.StackTrace;
                    log = ex.InnerException.Message + ";\n" + ex.StackTrace;
                }
                else
                {
                    mensagem = ex.Message + ";<br/>" + ex.StackTrace;
                    log = ex.Message + ";\n" + ex.StackTrace;
                }
                if (Int32.Parse(ConfigurationManager.AppSettings["ChaveEnvio"]) == 1)
                    errorMail.SendErrorMail("TaskFluxo4", mensagem, -1, (codPedido != -1) ? codPedido : 0);

                if(ex.InnerException != null)
                    logPedido.WriteEntry("Falha no Fluxo 4, devido a: " + ex.InnerException.Message);
                else
                    logPedido.WriteEntry("Falha no Fluxo 4, devido a: " + ex.Message);

                throw ex;
            }
        }

        //fluxo 6
        public static async Task<bool> RequestCancellationRxAsync()
        {
            var client = new HttpClient();
            var codPedido = -1;
            var url = ConfigurationManager.AppSettings["ServiceRx"] + "/Order/getOrderCancelation?subsidiaryIds=" + filial + "&situationCancellation=" + situacaoCancelamento;
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            try
            {
                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var content = await response.Content.ReadAsStringAsync();
                    if (content != null && !content.Equals("[]"))
                        logPedido.WriteEntry("Novo Objeto de RX To API - Fluxo 6: " + content);
                    List<OrderRx> ordersRX = new List<OrderRx>();
                    List<OrderApi> OrdersApi = JsonConvert.DeserializeObject<List<OrderApi>>(content);
                    OrderStatus orderStatus = new OrderStatus();
                    ProductReason productReason = new ProductReason();
                    foreach (OrderApi Order in OrdersApi)
                    {
                        if (Order.Client_Code != null)
                            codPedido = Order.Client_Code.Value;
                        if (Order.WholeSaler != null)
                        {
                            Order.WholeSaler = Order.WholeSaler
                            .Replace("/", "")
                            .Replace("-", "");
                        }
                        if (Order.Client_Identification != null)
                        {

                            Order.Client_Identification = Order.Client_Identification
                             .Replace("/", "")
                             .Replace("-", "");
                        }

                        var JsonApi = new StringContent(JsonConvert.SerializeObject(Order, Formatting.None,
                                new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore
                                }), Encoding.UTF8, "application/json");

                        var urlapi = ConfigurationManager.AppSettings["ServiceApi"] + "/createCancellation";
                        using (var responseApi = client.PostAsync(urlapi, JsonApi).Result)
                        {
                            if (responseApi.StatusCode == System.Net.HttpStatusCode.BadRequest)
                            {
                                string log = await responseApi.Content.ReadAsStringAsync();
                                if (log != null && !log.Equals("[]"))
                                    logPedido.WriteEntry("Fluxo 6 - Log: " + log);
                                if (Int32.Parse(ConfigurationManager.AppSettings["ChaveEnvio"]) == 1)
                                    errorMail.SendErrorMail("TaskFluxo6", log, (int)responseApi.StatusCode, codPedido);

                                continue;
                            }
                            responseApi.EnsureSuccessStatusCode();
                            var contentApi = await responseApi.Content.ReadAsStringAsync();
                            if (contentApi == null)
                            {
                                continue;
                            }
                            var JsonObj = JObject.Parse(contentApi);
                            JsonObj["status"] = orderStatus.getIndiceStatus(JsonObj["status"]);
                            OrderRx OrderRx = Conversores.ConvertJson.ConvertApiToRxStatus(JsonObj.ToString());

                            //foreach (var cond in condicaoComercial)
                            //{
                            //if (string.IsNullOrEmpty(OrderRx.Commercial_condition))
                            //{
                            //OrderRx.id_Commercial_condition = null;
                            //}
                            //else if (cond.Contains(OrderRx.Commercial_condition))
                            //{
                            //string condAux = cond;
                            //var index = condAux.IndexOf('/');
                            //OrderRx.id_Commercial_condition = int.Parse(condAux.Remove(index, condAux.Length - index));
                            OrderRx.id_Commercial_condition = Order.id_Commercial_condition;
                            //}
                            //}

                            OrderRx.Log_id = Order.Log_id;
                            //foreach (var product in OrderRx.Products)
                            //{
                            //    product.Id_Order_Log = Order.Log_id;
                            //}

                            ordersRX.Add(OrderRx);
                        }
                    }
                    if (ordersRX.Count > 0)
                        foreach (OrderRx order in ordersRX)
                        {
                            logPedido.WriteEntry("Novo Objeto de RX To API - Fluxo 6" + (JsonConvert.SerializeObject(order)));
                        }
                    var JsonPutApi = new StringContent(JsonConvert.SerializeObject(ordersRX, Formatting.None,
                               new JsonSerializerSettings
                               {
                                   NullValueHandling = NullValueHandling.Ignore
                               }), Encoding.UTF8, "application/json");
                    var urlPut = ConfigurationManager.AppSettings["ServiceRx"] + "/Order/OrderCancelation?&subsidiaryIds=" + filial;
                    using (var responseApi = client.PutAsync(urlPut, JsonPutApi).Result)
                    {
                        if (!responseApi.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                        {
                            logPedido.WriteEntry("Falha no Fluxo 6, devido a: " + responseApi.Content);
                            return false;
                        }
                        responseApi.EnsureSuccessStatusCode();
                        Console.WriteLine("Terminou Fluxo 6");
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                string mensagem, log;
                if (ex.InnerException != null)
                {
                    mensagem = ex.InnerException.Message + ";<br/>" + ex.StackTrace;
                    log = ex.InnerException.Message + ";\n" + ex.StackTrace;
                }
                else
                {
                    mensagem = ex.Message + ";<br/>" + ex.StackTrace;
                    log = ex.Message + ";\n" + ex.StackTrace;
                }
                if (Int32.Parse(ConfigurationManager.AppSettings["ChaveEnvio"]) == 1)
                    errorMail.SendErrorMail("TaskFluxo6", mensagem, -1, (codPedido != -1) ? codPedido : 0);

                if(ex.InnerException != null)
                    logPedido.WriteEntry("Falha no Fluxo 6, devido a: " + ex.InnerException.Message);
                else
                    logPedido.WriteEntry("Falha no Fluxo 6, devido a: " + ex.Message);
                throw ex;
            }
        }
    }
}