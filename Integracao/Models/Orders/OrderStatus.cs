﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Integracao.Models.Orders
{
    public class OrderStatus
    {
        private Dictionary<int, string> listOrderStatus = new Dictionary<int, string>();

        public OrderStatus()
        {
            listOrderStatus[1] = "AWAITING_PROCESSING";
            listOrderStatus[2] = "PROCESSING";
            listOrderStatus[3] = "PROCESSED";
            listOrderStatus[4] = "AWAITING_ORDER_RESPONSE";
            listOrderStatus[5] = "AWAITING_ORDER_INVOICE";
            listOrderStatus[6] = "INVOICE_RECEIVED";
            listOrderStatus[7] = "AWAITING_CANCELLATION_PROCESSING";
            listOrderStatus[8] = "PROCESSING_CANCELLATION";
            listOrderStatus[9] = "CANCELLED";

        }

        public int getIndiceStatus(string status)
        {
            return listOrderStatus.Where(item => item.Value == status).Select(item => item.Key).Single();
        }

        public string getStringStatus(int indice)
        {
            return listOrderStatus[indice];
        }

        internal JToken getIndiceStatus(JToken value)
        {
            return this.getIndiceStatus(value.ToString());
        }
    }
}
