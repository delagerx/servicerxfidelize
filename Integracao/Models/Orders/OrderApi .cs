﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace Core.Models.Order
{
    public class OrderApi : IEquatable<OrderApi>
    {
        [JsonProperty(PropertyName = "id")]
        public int? Id { get; set; }
        [JsonProperty(PropertyName = "grouped_order_code")]
        public int? Grouped_order_code { get; set; }
        [JsonProperty(PropertyName = "client_identification")]
        public string Client_Identification { get; set; }
        [JsonProperty(PropertyName = "wholesaler")]
        public string WholeSaler { get; set; }
        [JsonProperty(PropertyName = "client_code")]
        public int? Client_Code { get; set; }
        [JsonProperty(PropertyName = "commercial_condition")]
        public string Commercial_condition { get; set; }        
        [JsonProperty(PropertyName = "id_commercial_condition")]
        public int? id_Commercial_condition { get; set; }
        [JsonProperty(PropertyName = "status")]
        public int? Status { get; set; }
        [JsonProperty(PropertyName = "total_products")]
        public int? Total_products { get; set; }
        [JsonProperty(PropertyName = "order_motive")]
        public string Order_Motive { get; set; }
        [JsonProperty(PropertyName = "products")]
        public IEnumerable<OrderProductApi> Products { get; set; }
        [JsonProperty(PropertyName = "total_value")]
        public double? Total_Value { get; set; }
        [JsonProperty(PropertyName = "discount_value")]
        public double? Discount_Value { get; set; }

        
        public int? Log_id { get; set; }
        [JsonIgnore]
        public List<Error> Errors { get; set; }
        [JsonIgnore]
        public int? SubsidiaryId { get; set; }
        [JsonIgnore]
        public int? Situation { get; set; }
        

        
        protected OrderApi()
        {
            Errors = new List<Error>();
        }


        public bool Equals(OrderApi other)
        {
            return this.Equals(other);
        }

        public override bool Equals(object obj)
        {
            var api = obj as OrderApi;
            return api != null &&
                   EqualityComparer<int?>.Default.Equals(Id, api.Id) &&
                   EqualityComparer<int?>.Default.Equals(Grouped_order_code, api.Grouped_order_code) &&
                   Client_Identification == api.Client_Identification &&
                   WholeSaler == api.WholeSaler &&
                   EqualityComparer<int?>.Default.Equals(Client_Code, api.Client_Code) &&
                   Commercial_condition == api.Commercial_condition &&
                   EqualityComparer<int?>.Default.Equals(Status, api.Status) &&
                   EqualityComparer<int?>.Default.Equals(Total_products, api.Total_products) &&
                   EqualityComparer<string>.Default.Equals(Order_Motive, api.Order_Motive) &&
                   EqualityComparer<IEnumerable<OrderProductApi>>.Default.Equals(Products, api.Products) &&
                   EqualityComparer<List<Error>>.Default.Equals(Errors, api.Errors) &&
                   EqualityComparer<int?>.Default.Equals(SubsidiaryId, api.SubsidiaryId) &&
                   EqualityComparer<int?>.Default.Equals(Situation, api.Situation);
        }

        public override int GetHashCode()
        {
            var hashCode = -64236821;
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Id);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Grouped_order_code);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Client_Identification);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(WholeSaler);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Client_Code);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Commercial_condition);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Status);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Total_products);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Order_Motive);
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<OrderProductApi>>.Default.GetHashCode(Products);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<Error>>.Default.GetHashCode(Errors);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(SubsidiaryId);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Situation);
            return hashCode;
        }
    }
}
