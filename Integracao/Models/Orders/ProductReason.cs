﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Integracao.Models.Orders
{
    public class ProductReason
    {
        private  Dictionary<int, string>  listProductReasons = new Dictionary<int, string>();

        public ProductReason() {
            listProductReasons[1] = "AWAITING_PROCESSING";
            listProductReasons[2] = "ORDER_CREATED_ON_INDUSTRY_OL_PORTAL";
            listProductReasons[3] = "NO_RESPONSE_FROM_INDUSTRY_OL_PORTAL";
            listProductReasons[4] = "PRODUCT_DOES_NOT_BELONG_TO_OL_FIDELIZE";
            listProductReasons[5] = "COMMERCIAL_POLICIES_NOT_AVAILABLE_FOR_ORDER_CONDITIONS";
            listProductReasons[6] = "COMMERCIAL_POLICIES_NOT_FOUND_FOR_PRODUCT_ON_INDUSTRY_OL_PORTAL";
            listProductReasons[7] = "WHOLESALER_DISCOUNT_REQUIRED";
            listProductReasons[8] = "WHOLESALER_DISCOUNT_OUT_OF_RANGE";
            listProductReasons[9] = "PRODUCT_DISABLED_ON_INDUSTRY_OL_PORTAL";
            listProductReasons[10] = "CLIENT_NOT_FOUND_ON_INDUSTRY_OL_PORTAL";
            listProductReasons[11] = "WHOLESALER_BRANCH_NOT_AVAILABLE_FOR_ORDER_CONDITIONS";
            listProductReasons[12] = "PRODUCTS_NOT_AVAILABLES";
            listProductReasons[13] = "ERROR_ADDING_PRODUCT_TO_ORDER";
            listProductReasons[14] = "ERROR_SENDING_ORDER";
            listProductReasons[15] = "WHOLESALER_BRANCH_NOT_ALLOWED_TO_INDUSTRY";
            listProductReasons[16] = "DUPLICATE_ORDER_ON_INDUSTRY_OL_PORTAL";
        }

        public int getIndiceReason(string status)
        {
            return listProductReasons.Where(item => item.Value == status).Select(item => item.Key).Single();
        }

        public string getStringProductReason(int indice) {
            return listProductReasons[indice];
        }

        internal JToken getIndiceReason(JToken value)
        {
            try
            {
                return this.getIndiceReason(value.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
