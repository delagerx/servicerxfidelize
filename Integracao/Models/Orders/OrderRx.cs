﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Core.Models.Order
{
    public class OrderRx : IEquatable<OrderApi>
    {
        [JsonProperty(PropertyName = "id")]
        public int? Id { get; set; }
        [JsonProperty(PropertyName = "grouped_order_code")]
        public int? Grouped_order_code { get; set; }
        [JsonProperty(PropertyName = "client_CNPJ")]
        public string Client_CNPJ { get; set; }
        [JsonProperty(PropertyName = "client_Code")]
        public int? Client_Code { get; set; }
        [JsonProperty(PropertyName = "motiveOrder")]
        public int? MotiveOrder { get; set; }
        [JsonProperty(PropertyName = "subsidiary_CNPJ")]
        public string Subsidiary_CNPJ { get; set; }
        [JsonProperty(PropertyName = "total_value")]
        public double? Total_Value { get; set; }
        [JsonProperty(PropertyName = "subsidiaryId")]
        public int? SubsidiaryId { get; set; }
        [JsonProperty(PropertyName = "discount_value")]
        public double? Discount_Value { get; set; }
        [JsonProperty(PropertyName = "commercial_condition")]
        public string Commercial_condition { get; set; }
        [JsonProperty(PropertyName = "id_commercial_condition")]
        public int? id_Commercial_condition { get; set; }
        [JsonProperty(PropertyName = "total_products")]
        public int? Total_products { get; set; }
        [JsonProperty(PropertyName = "situation")]
        public int? Situation { get; set; }
        [JsonProperty(PropertyName = "StatusOrder")]
        public int? StatusOrder { get; set; }
        [JsonProperty(PropertyName = "products")]
        public IEnumerable<OrderProductRx> Products { get; set; }
        [JsonProperty(PropertyName = "errors")]
        public List<Error> Errors { get; set; }
        
        public int? Log_id { get; set; }

        protected OrderRx()
        {
            Errors = new List<Error>();
        }

        public bool Equals(OrderApi other)
        {
            return this.Equals(other);
        }

        public override bool Equals(object obj)
        {
            var rx = obj as OrderRx;
            return rx != null &&
                   EqualityComparer<int?>.Default.Equals(Id, rx.Id) &&
                   EqualityComparer<int?>.Default.Equals(Grouped_order_code, rx.Grouped_order_code) &&
                   Client_CNPJ == rx.Client_CNPJ &&
                   EqualityComparer<int?>.Default.Equals(Client_Code, rx.Client_Code) &&
                   EqualityComparer<int?>.Default.Equals(MotiveOrder, rx.MotiveOrder) &&
                   Subsidiary_CNPJ == rx.Subsidiary_CNPJ &&
                   Total_Value == rx.Total_Value &&
                   EqualityComparer<int?>.Default.Equals(SubsidiaryId, rx.SubsidiaryId) &&
                   EqualityComparer<double?>.Default.Equals(Discount_Value, rx.Discount_Value) &&
                   Commercial_condition == rx.Commercial_condition &&
                   EqualityComparer<int?>.Default.Equals(id_Commercial_condition, rx.id_Commercial_condition) &&
                   EqualityComparer<int?>.Default.Equals(Total_products, rx.Total_products) &&
                   EqualityComparer<int?>.Default.Equals(Situation, rx.Situation) &&
                   EqualityComparer<int?>.Default.Equals(StatusOrder, rx.StatusOrder) &&
                   EqualityComparer<IEnumerable<OrderProductRx>>.Default.Equals(Products, rx.Products) &&
                   EqualityComparer<List<Error>>.Default.Equals(Errors, rx.Errors);
        }

        public override int GetHashCode()
        {
            var hashCode = 2143854693;
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Id);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Grouped_order_code);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Client_CNPJ);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Client_Code);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(MotiveOrder);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Subsidiary_CNPJ);
            hashCode = hashCode * -1521134295 + Total_Value.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(SubsidiaryId);
            hashCode = hashCode * -1521134295 + EqualityComparer<double?>.Default.GetHashCode(Discount_Value);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Commercial_condition);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(id_Commercial_condition);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Total_products);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Situation);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(StatusOrder);
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<OrderProductRx>>.Default.GetHashCode(Products);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<Error>>.Default.GetHashCode(Errors);
            return hashCode;
        }
    }
}
