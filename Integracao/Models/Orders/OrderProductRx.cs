﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Core.Models;
using Newtonsoft.Json;

namespace Core.Models.Order
{

    public class OrderProductRx : IEquatable<OrderProductRx>
    {
        [JsonProperty(PropertyName = "id")]
        public int? Id { get; set; }
        [JsonProperty(PropertyName = "ean")]
        public string EAN { get; set; }
        [JsonProperty(PropertyName = "quantity")]
        public int? Quantity { get; set; }
        [JsonProperty(PropertyName = "percent_discount")]
        public double? Discount { get; set; }
        [DefaultValue(0.00)]
        [JsonProperty(PropertyName = "discount_Value")]
        public double? Discount_Value { get; set; }
        [DefaultValue(0.00)]
        [JsonProperty(PropertyName = "unitprice")]
        public double? UnitPrice { get; set; }
        [JsonProperty(PropertyName = "id_commercial_condition")]
        public int? Id_Commercial_condition { get; set; }
        [JsonProperty(PropertyName = "responseQuantity")]
        public int? ResponseQuantity { get; set; }
        [JsonProperty(PropertyName = "statusProduct")]
        public int? StatusProduct { get; set; }
        [JsonProperty(PropertyName = "monitored")]
        public Boolean? Monitored { get; set; }
        [JsonProperty(PropertyName = "errors")]
        public List<Error> Errors { get; set; }
        [JsonProperty(PropertyName = "mustcut")]
        public bool MustCut { get; set; }
        
        public int? Log_id_produto { get; set; }
        
        public int? Id_Order_Log { get; set; }
        

        protected OrderProductRx()
        {
            Errors = new List<Error>();
            StatusProduct = null;
        }


        public bool Equals(OrderProductRx other)
        {
            return this.Equals(other);
        }

        public override bool Equals(object obj)
        {
            var rx = obj as OrderProductRx;
            return rx != null &&
                   EqualityComparer<int?>.Default.Equals(Id, rx.Id) &&
                   EAN == rx.EAN &&
                   Quantity == rx.Quantity &&
                   Discount == rx.Discount &&
                   EqualityComparer<double?>.Default.Equals(Discount_Value, rx.Discount_Value) &&
                   UnitPrice == rx.UnitPrice &&
                   EqualityComparer<int?>.Default.Equals(Id_Commercial_condition, rx.Id_Commercial_condition) &&
                   EqualityComparer<int?>.Default.Equals(ResponseQuantity, rx.ResponseQuantity) &&
                   EqualityComparer<int?>.Default.Equals(StatusProduct, rx.StatusProduct) &&
                   EqualityComparer<bool?>.Default.Equals(Monitored, rx.Monitored) &&
                   EqualityComparer<List<Error>>.Default.Equals(Errors, rx.Errors) &&
                   MustCut == rx.MustCut;
        }

        public override int GetHashCode()
        {
            var hashCode = -1321234351;
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Id);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(EAN);
            hashCode = hashCode * -1521134295 + Quantity.GetHashCode();
            hashCode = hashCode * -1521134295 + Discount.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<double?>.Default.GetHashCode(Discount_Value);
            hashCode = hashCode * -1521134295 + UnitPrice.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Id_Commercial_condition);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(ResponseQuantity);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(StatusProduct);
            hashCode = hashCode * -1521134295 + EqualityComparer<bool?>.Default.GetHashCode(Monitored);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<Error>>.Default.GetHashCode(Errors);
            hashCode = hashCode * -1521134295 + MustCut.GetHashCode();
            return hashCode;
        }
    }
}
