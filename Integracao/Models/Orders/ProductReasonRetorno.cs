﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Integracao.Models.Orders
{
    public class ProductReasonRetorno
    {
        private  Dictionary<int, string>  listProductReasonsRetorno = new Dictionary<int, string>();

        public ProductReasonRetorno() {

            listProductReasonsRetorno[1] = "PRODUCT_SUCCESSFULLY_ACCEPTED";
            listProductReasonsRetorno[2] = "PRODUCT_NOT_REGISTERED";
            listProductReasonsRetorno[3] = "PARTIALLY_ACCEPTED_PRODUCT";
            listProductReasonsRetorno[4] = "UNBILLED_PRODUCT";
            listProductReasonsRetorno[5] = "STOCK_SHORTAGE";
            listProductReasonsRetorno[6] = "PRODUCT_BLOCKED";
            listProductReasonsRetorno[7] = "DISCONTINUED_PRODUCT";
            listProductReasonsRetorno[8] = "INVALID_ORDERED_AMOUNT";
            listProductReasonsRetorno[9] = "CLIENT_AND_OR_PRODUCT_NOT_REGISTERED_ON_NEGOTIATION";
            listProductReasonsRetorno[10] = "PRODUCT_BLOCKED_BY_BRIEF_EXPIRATION";
            listProductReasonsRetorno[11] = "DATE_PRODUCT_BLOCKED_FOR_THIS_OPERATION";
            listProductReasonsRetorno[12] = "PRODUCT_BLOCKED_CLINICAL";
            listProductReasonsRetorno[13] = "PRODUCT_BLOCKED_BY_PRICE_DIVERGENCE";
            listProductReasonsRetorno[14] = "PRODUCT_BLOCKED_BY_LOGISTICS";
            listProductReasonsRetorno[15] = "PRODUCT NOT REGISTERED ON PROMOTION";

        }

        public int getProductIndiceReasonRetorno(string status)
        {
            return listProductReasonsRetorno.Where(item => item.Value == status).Select(item => item.Key).Single();
        }

        public string getStringProductReason(int indice) {
            return listProductReasonsRetorno[indice];
        }

        internal JToken getProductIndiceReasonRetorno(JToken value)
        {
            return this.getProductIndiceReasonRetorno(value.ToString());
        }
    }
}
