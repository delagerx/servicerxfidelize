﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Integracao.Models.Orders
{
    public class OrderStatusRetorno
    {
        private  Dictionary<int, string>  listOrderStatusRetorno = new Dictionary<int, string>();

        public OrderStatusRetorno() {
            listOrderStatusRetorno[1] = "BELOW_THE_MINIMUM";
            listOrderStatusRetorno[2] = "PARTIALLY_BILLED_ORDER";
            listOrderStatusRetorno[3] = "ORDER_SUCCESSFULLY_ACCEPTED";
            listOrderStatusRetorno[4] = "STOCK_SHORTAGE";
            listOrderStatusRetorno[5] = "CREDIT_SHORTAGE";
            listOrderStatusRetorno[6] = "CLIENT_NOT_REGISTERED";
            listOrderStatusRetorno[7] = "TIME_EXCEEDED";
            listOrderStatusRetorno[8] = "CLIENT_AND_OR_PRODUCT_NOT_REGISTERED_ON_NEGOTIATION";
            listOrderStatusRetorno[9] = "DUPLICATE_ORDER";
            listOrderStatusRetorno[10] = "LICENSE_DATE_EXPIRED";
            listOrderStatusRetorno[11] = "REGISTRATION_PROBLEMS";
            listOrderStatusRetorno[12] = "CLIENT_BLOCKED";
            listOrderStatusRetorno[13] = "INVALID_PAYMENT_CONDITION";
            listOrderStatusRetorno[14] = "LOGISTICAL_OPERATION_NOT_ALLOWED";
            listOrderStatusRetorno[15] = "CLIENT_NOT_REGISTERED_ON_PROMOTION";
            listOrderStatusRetorno[16] = "INVALID_LAYOUT";
            listOrderStatusRetorno[17] = "ORDER_BILLED_WITH_DIFFERENT_CONDITION";
        }

        public int getIndiceStatusRetorno(string status)
        {
            return listOrderStatusRetorno.Where(item => item.Value == status).Select(item => item.Key).Single();
        }

        public string getStringStatusRetorno(int? indice) {
            if (indice.Equals(0) || indice == null) {
                indice = 11;
            }
            return listOrderStatusRetorno[indice.GetValueOrDefault()];
        }

        internal JToken getIndiceStatusRetorno(JToken value)
        {
            return this.getIndiceStatusRetorno(value.ToString());
        }
    }
}
