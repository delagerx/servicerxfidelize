﻿using System;
using System.Collections.Generic;
using Core.Models;
using Newtonsoft.Json;

namespace Core.Models.Order
{
    public class OrderProductApi : IEquatable<OrderProductApi>
    {
        [JsonProperty(PropertyName ="ean")]
        public string EAN { get; set; }
        [JsonProperty(PropertyName = "ordered_quantity")]
        public int? Ordered_Quantity { get; set; }
        [JsonProperty(PropertyName = "wholesaler_discount")]
        public decimal? WholeSaler_Discount { get; set; }
        [JsonProperty(PropertyName = "order_discount")]
        public decimal? Order_Discount { get; set; }
        [JsonProperty(PropertyName = "unit_net_price")]
        public decimal? Unit_Net_Price { get; set; }
        [JsonProperty(PropertyName = "industry_order_code")]
        public int? Industry_Order_Code { get; set; }
        [JsonProperty(PropertyName = "product_reason")]
        public string Product_Reason { get; set; }
        [JsonProperty(PropertyName = "total_value")]
        public double? Total_Value { get; set; }
        [JsonProperty(PropertyName = "discount_value")]
        public double? Discount_Value { get; set; }
        [JsonProperty(PropertyName = "response_quantity")]
        public int? Response_Quantity { get; set; }

        //[JsonIgnore]
        public int? Id_Order_Log { get; set; }
        //[JsonIgnore]
        public int? Log_id_produto { get; set; }
        [JsonIgnore]
        public List<Error> Errors { get; set; }
        [JsonIgnore]
        public int? Id { get; set; }
        [JsonIgnore]
        public int? Id_Commercial_condition { get; set; }

        public bool ShouldSerializeId_Order_Log()
        {
            return false;
        }
        public bool ShouldSerializeLog_id_produto()
        {
            return false;
        }

        protected OrderProductApi()
        {
            Errors = new List<Error>();
            Product_Reason = null;
        }

        public override bool Equals(object obj)
        {
            var api = obj as OrderProductApi;
            return api != null &&
                   EAN == api.EAN &&
                   EqualityComparer<int?>.Default.Equals(Ordered_Quantity, api.Ordered_Quantity) &&
                   EqualityComparer<decimal?>.Default.Equals(WholeSaler_Discount, api.WholeSaler_Discount) &&
                   EqualityComparer<decimal?>.Default.Equals(Order_Discount, api.Order_Discount) &&
                   EqualityComparer<decimal?>.Default.Equals(Unit_Net_Price, api.Unit_Net_Price) &&
                   EqualityComparer<int?>.Default.Equals(Industry_Order_Code, api.Industry_Order_Code) &&
                   Product_Reason == api.Product_Reason &&
                   EqualityComparer<double?>.Default.Equals(Total_Value, api.Total_Value) &&
                   EqualityComparer<double?>.Default.Equals(Discount_Value, api.Discount_Value) &&
                   EqualityComparer<int?>.Default.Equals(Response_Quantity, api.Response_Quantity) &&
                   EqualityComparer<List<Error>>.Default.Equals(Errors, api.Errors) &&
                   EqualityComparer<int?>.Default.Equals(Id, api.Id) &&
                   EqualityComparer<int?>.Default.Equals(Id_Order_Log, api.Id_Order_Log) &&
                   EqualityComparer<int?>.Default.Equals(Log_id_produto, api.Log_id_produto) && 
                   EqualityComparer<int?>.Default.Equals(Id_Commercial_condition, api.Id_Commercial_condition);
        }

        public override int GetHashCode()
        {
            var hashCode = 1653553753;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(EAN);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Ordered_Quantity);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(WholeSaler_Discount);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(Order_Discount);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(Unit_Net_Price);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Industry_Order_Code);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Product_Reason);
            hashCode = hashCode * -1521134295 + EqualityComparer<double?>.Default.GetHashCode(Total_Value);
            hashCode = hashCode * -1521134295 + EqualityComparer<double?>.Default.GetHashCode(Discount_Value);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Response_Quantity);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<Error>>.Default.GetHashCode(Errors);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Id);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Id_Commercial_condition);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Id_Order_Log);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Log_id_produto);
            return hashCode;
        }


        public bool Equals(OrderProductApi other)
         {
             return this.Equals(other);
         }
    }
}
