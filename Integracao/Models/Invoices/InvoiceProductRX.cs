﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.Invoices
{
    public class InvoiceProductRX : IEquatable<InvoiceProductRX>
    {
        public int? Id { get; set; }
        public string EAN { get; set; }
        public int Quantity { get; set; }
        public float Percent_discount_product { get; set; }
        public float DiscountValueProduct { get; set; }
        public float UnitPriceProduct { get; set; }
        public int Id_Commercial_condition { get; set; }
        public Decimal Icms_tax_calc_base { get; set; }
        public Decimal Tax_substitution_icms_tax_calc_base { get; set; }
        public Decimal Icms_percentage { get; set; }
        public Decimal Ipi_percentage { get; set; }
        public Decimal Icms_tax_value { get; set; }
        public Decimal Tax_substitution_value { get; set; }
        public Decimal Passed_along_icms_tax_value { get; set; }
       // public int Payment_term { get; set; }
        public Decimal Passed_along_value { get; set; }
        public Decimal Total_value { get; set; }
        public Decimal Total_value_with_taxes { get; set; }
        public Decimal Financial_discount_percentage { get; set; }
        public Decimal Financial_discount_value { get; set; }
        public int Tax_substitution_code { get; set; }
        public int Classification { get; set; }
        public int Cfop_code { get; set; }
        public char Tax_substitution_flag { get; set; }
        public char Positive_list_flag { get; set; }
        public bool Monitored { get; set; }
        public int? Log_id_produto { get; set; }
        public int? Id_Order_Log { get; set; }


        public override bool Equals(object obj)
        {
            var rX = obj as InvoiceProductRX;
            return rX != null &&
                   EqualityComparer<int?>.Default.Equals(Id, rX.Id) &&
                   EAN == rX.EAN &&
                   Quantity == rX.Quantity &&
                   Percent_discount_product == rX.Percent_discount_product &&
                   DiscountValueProduct == rX.DiscountValueProduct &&
                   UnitPriceProduct == rX.UnitPriceProduct &&
                   Id_Commercial_condition == rX.Id_Commercial_condition &&
                   Icms_tax_calc_base == rX.Icms_tax_calc_base &&
                   Tax_substitution_icms_tax_calc_base == rX.Tax_substitution_icms_tax_calc_base &&
                   Icms_percentage == rX.Icms_percentage &&
                   Ipi_percentage == rX.Ipi_percentage &&
                   Icms_tax_value == rX.Icms_tax_value &&
                   Tax_substitution_value == rX.Tax_substitution_value &&
                   Passed_along_icms_tax_value == rX.Passed_along_icms_tax_value &&
                   Passed_along_value == rX.Passed_along_value &&
                   Total_value == rX.Total_value &&
                   Total_value_with_taxes == rX.Total_value_with_taxes &&
                   Financial_discount_percentage == rX.Financial_discount_percentage &&
                   Financial_discount_value == rX.Financial_discount_value &&
                   Tax_substitution_code == rX.Tax_substitution_code &&
                   Classification == rX.Classification &&
                   Cfop_code == rX.Cfop_code &&
                   Tax_substitution_flag == rX.Tax_substitution_flag &&
                   Positive_list_flag == rX.Positive_list_flag &&
                   Monitored == rX.Monitored;
                    
        }

        public bool Equals(InvoiceProductRX other)
        {
            return this.Equals(other);
        }

        public override int GetHashCode()
        {
            var hashCode = 1204269152;
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Id);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(EAN);
            hashCode = hashCode * -1521134295 + Quantity.GetHashCode();
            hashCode = hashCode * -1521134295 + Percent_discount_product.GetHashCode();
            hashCode = hashCode * -1521134295 + DiscountValueProduct.GetHashCode();
            hashCode = hashCode * -1521134295 + UnitPriceProduct.GetHashCode();
            hashCode = hashCode * -1521134295 + Id_Commercial_condition.GetHashCode();
            hashCode = hashCode * -1521134295 + Icms_tax_calc_base.GetHashCode();
            hashCode = hashCode * -1521134295 + Tax_substitution_icms_tax_calc_base.GetHashCode();
            hashCode = hashCode * -1521134295 + Icms_percentage.GetHashCode();
            hashCode = hashCode * -1521134295 + Ipi_percentage.GetHashCode();
            hashCode = hashCode * -1521134295 + Icms_tax_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Tax_substitution_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Passed_along_icms_tax_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Passed_along_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Total_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Total_value_with_taxes.GetHashCode();
            hashCode = hashCode * -1521134295 + Financial_discount_percentage.GetHashCode();
            hashCode = hashCode * -1521134295 + Financial_discount_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Tax_substitution_code.GetHashCode();
            hashCode = hashCode * -1521134295 + Classification.GetHashCode();
            hashCode = hashCode * -1521134295 + Cfop_code.GetHashCode();
            hashCode = hashCode * -1521134295 + Tax_substitution_flag.GetHashCode();
            hashCode = hashCode * -1521134295 + Positive_list_flag.GetHashCode();
            hashCode = hashCode * -1521134295 + Monitored.GetHashCode();
            return hashCode;
        }
    }
}
