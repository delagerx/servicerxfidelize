﻿using Core.Models.Invoices;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.Invoices
{
    public class InvoiceRX : IEquatable<InvoiceRX>
    {
        public int InvoiceId { get; set; }
        public int OrderId { get; set; }
        public string Client_CNPJ { get; set; }
        public string Subsidiary_CNPJ { get; set; }
        public int SubsidiaryId { get; set; }
        public int Client_Code { get; set; }
        public DateTime Emission { get; set; }
        public string Invoice_number { get; set; }
        public string old_Invoice_Number { get; set; }
        public Decimal Tax_substitution_icms_tax_calc_base { get; set; }
        public Decimal Icms_tax_calc_base { get; set; }
        public int Invoice_Model { get; set; }
        public int Invoice_Serie { get; set; }
        public string Invoice_danfe_key { get; set; }
        public Decimal Invoice_Value { get; set; }
        public Decimal Invoice_discount { get; set; }
        public Decimal Invoice_passed_along_icms_tax_value { get; set; }
        public Decimal Invoice_products_total_value​ { get; set; }
        public Decimal Invoice_ipi_tax_value { get; set; }
        public Decimal Invoice_freight_value { get; set; }
        public Decimal invoice_icms_tax_retained_total_value { get; set; }
        public Decimal Invoice_icms_tax_total_value { get; set; }
        public Decimal Invoice_volume_amount { get; set; }
        public IEnumerable<InvoiceProductRX> Products { get; set; }
        public int? Log_id { get; set; }

        public override bool Equals(object obj)
        {
            return Equals(obj as InvoiceRX);
        }

        public bool Equals(InvoiceRX other)
        {
            return other != null &&
                   InvoiceId == other.InvoiceId &&
                   OrderId == other.OrderId &&
                   Client_CNPJ == other.Client_CNPJ &&
                   Subsidiary_CNPJ == other.Subsidiary_CNPJ &&
                   SubsidiaryId == other.SubsidiaryId &&
                   Client_Code == other.Client_Code &&
                   Emission == other.Emission &&
                   Invoice_number == other.Invoice_number &&
                   Tax_substitution_icms_tax_calc_base == other.Tax_substitution_icms_tax_calc_base &&
                   Icms_tax_calc_base == other.Icms_tax_calc_base &&
                   Invoice_Model == other.Invoice_Model &&
                   Invoice_Serie == other.Invoice_Serie &&
                   Invoice_danfe_key == other.Invoice_danfe_key &&
                   Invoice_Value == other.Invoice_Value &&
                   Invoice_discount == other.Invoice_discount &&
                   Invoice_passed_along_icms_tax_value == other.Invoice_passed_along_icms_tax_value &&
                   Invoice_products_total_value == other.Invoice_products_total_value &&
                   Invoice_ipi_tax_value == other.Invoice_ipi_tax_value &&
                   Invoice_freight_value == other.Invoice_freight_value &&
                   invoice_icms_tax_retained_total_value == other.invoice_icms_tax_retained_total_value &&
                   Invoice_icms_tax_total_value == other.Invoice_icms_tax_total_value &&
                   Invoice_volume_amount == other.Invoice_volume_amount &&
                   EqualityComparer<IEnumerable<InvoiceProductRX>>.Default.Equals(Products, other.Products);
        }

        public override int GetHashCode()
        {
            var hashCode = 346728659;
            hashCode = hashCode * -1521134295 + InvoiceId.GetHashCode();
            hashCode = hashCode * -1521134295 + OrderId.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Client_CNPJ);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Subsidiary_CNPJ);
            hashCode = hashCode * -1521134295 + SubsidiaryId.GetHashCode();
            hashCode = hashCode * -1521134295 + Client_Code.GetHashCode();
            hashCode = hashCode * -1521134295 + Emission.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Invoice_number);
            hashCode = hashCode * -1521134295 + Tax_substitution_icms_tax_calc_base.GetHashCode();
            hashCode = hashCode * -1521134295 + Icms_tax_calc_base.GetHashCode();
            hashCode = hashCode * -1521134295 + Invoice_Model.GetHashCode();
            hashCode = hashCode * -1521134295 + Invoice_Serie.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Invoice_danfe_key);
            hashCode = hashCode * -1521134295 + Invoice_Value.GetHashCode();
            hashCode = hashCode * -1521134295 + Invoice_discount.GetHashCode();
            hashCode = hashCode * -1521134295 + Invoice_passed_along_icms_tax_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Invoice_products_total_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Invoice_ipi_tax_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Invoice_freight_value.GetHashCode();
            hashCode = hashCode * -1521134295 + invoice_icms_tax_retained_total_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Invoice_icms_tax_total_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Invoice_volume_amount.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<InvoiceProductRX>>.Default.GetHashCode(Products);
            return hashCode;
        }
    }   
}
