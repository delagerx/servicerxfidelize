﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Integracao.Models.Orders
{
    public class InvoiceStatus
    {
        private Dictionary<int, string> listInvoiceStatus = new Dictionary<int, string>();

        public InvoiceStatus()
        {
            listInvoiceStatus[1] = "AWAITING_PROCESSING";
            listInvoiceStatus[2] = "PROCESSING";
            listInvoiceStatus[3] = "PROCESSED";
            listInvoiceStatus[4] = "AWAITING_ORDER_RESPONSE";
            listInvoiceStatus[5] = "AWAITING_ORDER_INVOICE";
            listInvoiceStatus[6] = "INVOICE_RECEIVED";
            listInvoiceStatus[7] = "AWAITING_CANCELLATION_PROCESSING";
            listInvoiceStatus[8] = "PROCESSING_CANCELLATION";
            listInvoiceStatus[9] = "CANCELLED";

        }

        public int getIndiceInvoiceStatus(string status)
        {
            return listInvoiceStatus.Where(item => item.Value == status).Select(item => item.Key).Single();
        }

        public string getStringInvoiceStatus(int indice)
        {
            return listInvoiceStatus[indice];
        }

        internal JToken getIndiceInvoiceStatus(JToken value)
        {
            return this.getIndiceInvoiceStatus(value.ToString());
        }
    }
}
