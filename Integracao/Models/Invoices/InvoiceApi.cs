﻿using Core.Models.Invoices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.Invoices
{
    public class InvoiceApi  :IEquatable<InvoiceApi>
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "client_identification")]
        public string Client_identification { get; set; }
        [JsonProperty(PropertyName = "wholesaler")]
        public string Wholesaler { get; set; }
        [JsonProperty(PropertyName = "subsidiaryId")]
        public int SubsidiaryId { get; set; }
        [JsonProperty(PropertyName = "client_code")]
        public int Client_Code { get; set; }
        [JsonProperty(PropertyName = "invoice_issue_date")]
        public DateTime Invoice_issue_date { get; set; }
        [JsonProperty(PropertyName = "invoice_number")]
        public string Invoice_number { get; set; }
        [JsonProperty(PropertyName = "removed_invoice")]
        public string Removed_invoice { get; set; }
        [JsonProperty(PropertyName = "base_calculation_icms_tax_substitution")]
        public Decimal Base_calculation_icms_tax_substitution { get; set; }
        [JsonProperty(PropertyName = "base_calculation_icms")]
        public Decimal Base_calculation_icms { get; set; }
        [JsonProperty(PropertyName = "invoice_model")]
        public int? Invoice_Model { get; set; }
        [JsonProperty(PropertyName = "invoice_serie")]
        public int? Invoice_Serie { get; set; }
        [JsonProperty(PropertyName = "danfe")]
        public string Danfe { get; set; }
        [JsonProperty(PropertyName = "invoice_value")]
        public Decimal Invoice_value { get; set; }
        [JsonProperty(PropertyName = "invoice_discount_value")]
        public Decimal Invoice_discount_value { get; set; }
        [JsonProperty(PropertyName = "icms_value_transferred")]
        public Decimal Icms_value_transferred { get; set; }
        [JsonProperty(PropertyName = "products_total_value")]
        public Decimal Products_total_value { get; set; }
        [JsonProperty(PropertyName = "icms_total_value_withheld")]
        public Decimal Icms_total_value_withheld { get; set; }
        [JsonProperty(PropertyName = "icms_total_value")]
        public Decimal Icms_total_value { get; set; }
        [JsonProperty(PropertyName = "volume_quantity")]
        public Double Volume_quantity { get; set; }
        [JsonProperty(PropertyName = "invoice_products")]
        public IEnumerable<InvoiceProductApi> Invoice_products { get; set; }
        public int? Log_id { get; set; }
        public int? id_Commercial_condition { get; set; }

        protected InvoiceApi()
        {
          //  Errors = new List<Error>();
        }

        public bool ShouldSerializeLog_id()
        {
            return false;
        }



        public override bool Equals(object obj)
        {
            var api = obj as InvoiceApi;
            return api != null &&
                   Id == api.Id &&
                   Client_identification == api.Client_identification &&
                   Wholesaler == api.Wholesaler &&
                   SubsidiaryId == api.SubsidiaryId &&
                   Client_Code == api.Client_Code &&
                   Invoice_issue_date == api.Invoice_issue_date &&
                   Invoice_number == api.Invoice_number &&
                   Base_calculation_icms_tax_substitution == api.Base_calculation_icms_tax_substitution &&
                   Base_calculation_icms == api.Base_calculation_icms &&
                   Invoice_Model == api.Invoice_Model &&
                   Invoice_Serie == api.Invoice_Serie &&
                   Danfe == api.Danfe &&
                   Invoice_value == api.Invoice_value &&
                   Invoice_discount_value == api.Invoice_discount_value &&
                   Icms_value_transferred == api.Icms_value_transferred &&
                   Products_total_value == api.Products_total_value &&
                   Icms_total_value_withheld == api.Icms_total_value_withheld &&
                   Icms_total_value == api.Icms_total_value &&
                   Volume_quantity == api.Volume_quantity &&
                   EqualityComparer<IEnumerable<InvoiceProductApi>>.Default.Equals(Invoice_products, api.Invoice_products);
        }

        public override int GetHashCode()
        {
            var hashCode = -344567522;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Client_identification);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Wholesaler);
            hashCode = hashCode * -1521134295 + SubsidiaryId.GetHashCode();
            hashCode = hashCode * -1521134295 + Client_Code.GetHashCode();
            hashCode = hashCode * -1521134295 + Invoice_issue_date.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Invoice_number);
            hashCode = hashCode * -1521134295 + Base_calculation_icms_tax_substitution.GetHashCode();
            hashCode = hashCode * -1521134295 + Base_calculation_icms.GetHashCode();
            hashCode = hashCode * -1521134295 + Invoice_Model.GetHashCode();
            hashCode = hashCode * -1521134295 + Invoice_Serie.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Danfe);
            hashCode = hashCode * -1521134295 + Invoice_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Invoice_discount_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Icms_value_transferred.GetHashCode();
            hashCode = hashCode * -1521134295 + Products_total_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Icms_total_value_withheld.GetHashCode();
            hashCode = hashCode * -1521134295 + Icms_total_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Volume_quantity.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<InvoiceProductApi>>.Default.GetHashCode(Invoice_products);
            return hashCode;
        }

        public bool Equals(InvoiceApi other)
        {
            return this.Equals(other);
        }
    }   
}
