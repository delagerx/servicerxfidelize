﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.Invoices
{
    public class InvoiceProductApi: IEquatable<InvoiceProductApi>
    {
        [JsonProperty(PropertyName = "id")]
        public int? Id { get; set; }
        [JsonProperty(PropertyName = "ean")]
        public string EAN { get; set; }
        [JsonProperty(PropertyName = "invoice_quantity")]
        public int Invoice_quantity { get; set; }
        [JsonProperty(PropertyName = "percent_discount")]
        public Decimal Percent_discount { get; set; }
        [JsonProperty(PropertyName = "unit_discount_price")]
        public Decimal Unit_discount_price { get; set; }
        [JsonProperty(PropertyName = "unit_net_price")]
        public Decimal Unit_net_price { get; set; }
        [JsonProperty(PropertyName = "base_calculation_icms")]
        public Decimal Base_calculation_icms { get; set; }
        [JsonProperty(PropertyName = "base_calculation_icms_tax_substitution")]
        public Decimal Base_calculation_icms_tax_substitution { get; set; }
        [JsonProperty(PropertyName = "percent_icms_aliquot")]
        public Decimal Percent_icms_aliquot { get; set; }
        [JsonProperty(PropertyName = "percent_ipi_aliquot")]
        public Decimal Percent_ipi_aliquot { get; set; }
        [JsonProperty(PropertyName = "icms_value")]
        public Decimal Icms_value { get; set; }
        [JsonProperty(PropertyName = "st_value")]
        public Decimal St_value { get; set; }
        [JsonProperty(PropertyName = "icms_transferred_value")]
        public Decimal Icms_transferred_value { get; set; }
        [JsonProperty(PropertyName = "transfer_value")]
        public Decimal Transfer_value { get; set; }
        [JsonProperty(PropertyName = "product_total_value_charges")]
        public Decimal Product_total_value_charges { get; set; }
        [JsonProperty(PropertyName = "percent_financial_discount")]
        public Decimal Percent_financial_discount { get; set; }
        [JsonProperty(PropertyName = "percent_unit_financial_discount")]
        public Decimal Percent_unit_financial_discount { get; set; }
        [JsonProperty(PropertyName = "product_classification")]
        public int? Product_classification { get; set; }
        [JsonProperty(PropertyName = "cfop")]
        public int Cfop { get; set; }
        [JsonProperty(PropertyName = "tax_substitution_flag")]
        public char Tax_substitution_flag { get; set; }
        [JsonProperty(PropertyName = "positive_list_identifier")]
        public char Positive_list_identifier { get; set; }
        //[JsonProperty(PropertyName = "id_commercial_condition")]
        [JsonIgnore]
        public int? id_Commercial_condition { get; set; }
        public int? Id_Order_Log { get; set; }
        public int? Log_id_produto { get; set; }

        public override bool Equals(object obj)
        {
            return Equals(obj as InvoiceProductApi);
        }

        public bool ShouldSerializeId_Order_Log()
        {
            return false;
        }

        public bool ShouldSerializeLog_id_produto()
        {
            return false;
        }

        public bool Equals(InvoiceProductApi other)
        {
            return other != null &&
                   EqualityComparer<int?>.Default.Equals(Id, other.Id) &&
                   EAN == other.EAN &&
                   Invoice_quantity == other.Invoice_quantity &&
                   Percent_discount == other.Percent_discount &&
                   Unit_discount_price == other.Unit_discount_price &&
                   Unit_net_price == other.Unit_net_price &&
                   Base_calculation_icms == other.Base_calculation_icms &&
                   Base_calculation_icms_tax_substitution == other.Base_calculation_icms_tax_substitution &&
                   Percent_icms_aliquot == other.Percent_icms_aliquot &&
                   Percent_ipi_aliquot == other.Percent_ipi_aliquot &&
                   Icms_value == other.Icms_value &&
                   St_value == other.St_value &&
                   Icms_transferred_value == other.Icms_transferred_value &&
                   Transfer_value == other.Transfer_value &&
                   Product_total_value_charges == other.Product_total_value_charges &&
                   Percent_financial_discount == other.Percent_financial_discount &&
                   Percent_unit_financial_discount == other.Percent_unit_financial_discount &&
                   Product_classification == other.Product_classification &&
                   Cfop == other.Cfop &&
                   Tax_substitution_flag == other.Tax_substitution_flag &&
                   Positive_list_identifier == other.Positive_list_identifier &&
                   id_Commercial_condition == other.Id;
        }

        public override int GetHashCode()
        {
            var hashCode = -1841127462;
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Id);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(EAN);
            hashCode = hashCode * -1521134295 + Invoice_quantity.GetHashCode();
            hashCode = hashCode * -1521134295 + Percent_discount.GetHashCode();
            hashCode = hashCode * -1521134295 + Unit_discount_price.GetHashCode();
            hashCode = hashCode * -1521134295 + Unit_net_price.GetHashCode();
            hashCode = hashCode * -1521134295 + Base_calculation_icms.GetHashCode();
            hashCode = hashCode * -1521134295 + Base_calculation_icms_tax_substitution.GetHashCode();
            hashCode = hashCode * -1521134295 + Percent_icms_aliquot.GetHashCode();
            hashCode = hashCode * -1521134295 + Percent_ipi_aliquot.GetHashCode();
            hashCode = hashCode * -1521134295 + Icms_value.GetHashCode();
            hashCode = hashCode * -1521134295 + St_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Icms_transferred_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Transfer_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Product_total_value_charges.GetHashCode();
            hashCode = hashCode * -1521134295 + Percent_financial_discount.GetHashCode();
            hashCode = hashCode * -1521134295 + Percent_unit_financial_discount.GetHashCode();
            hashCode = hashCode * -1521134295 + Product_classification.GetHashCode();
            hashCode = hashCode * -1521134295 + Cfop.GetHashCode();
            hashCode = hashCode * -1521134295 + Tax_substitution_flag.GetHashCode();
            hashCode = hashCode * -1521134295 + Positive_list_identifier.GetHashCode();
            hashCode = hashCode * -1521134295 + id_Commercial_condition.GetHashCode();

            return hashCode;
        }
    }
}
