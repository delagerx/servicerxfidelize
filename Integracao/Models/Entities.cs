﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Core.Models
{
    public class Entities : IEquatable<Entities>
    {
        public int EntityCode { get; set; }
        public string Name { get; set; }
        public string Nickname { get; set; }
        public string Document { get; set; }
        public DateTime CreationTime { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public int AddressTypeCode { get; set; }
        public string AddressType { get; set; }
        public string City { get; set; }
        public string Neighborhood { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Route { get; set; }
        public int SituationCode { get; set; }
        public string Situation { get; set; }
        public int TypeCode { get; set; }
        public string Type { get; set; }
        public string AdicionalInformation { get; set; }
        public string StateRegistration { get; set; }
        public string Flag { get; set; }
        public List<Error> Errors { get; set; }

        public Entities()
        {
            Errors = new List<Error>();
        }

        public bool Equals(Entities entities)
        {
            if (ReferenceEquals(null, entities)) return false;
            if (ReferenceEquals(this, entities)) return true;
            return EntityCode == entities.EntityCode && 
                string.Equals(Name, entities.Name, StringComparison.OrdinalIgnoreCase) && 
                string.Equals(Nickname, entities.Nickname, StringComparison.OrdinalIgnoreCase) && 
                string.Equals(Document, entities.Document, StringComparison.OrdinalIgnoreCase) && 
                CreationTime.Equals(entities.CreationTime) && 
                string.Equals(AddressLine1, entities.AddressLine1, StringComparison.OrdinalIgnoreCase) && 
                string.Equals(AddressLine2, entities.AddressLine2, StringComparison.OrdinalIgnoreCase) && 
                AddressTypeCode == entities.AddressTypeCode && 
                string.Equals(AddressType, entities.AddressType, StringComparison.OrdinalIgnoreCase) && 
                string.Equals(City, entities.City, StringComparison.OrdinalIgnoreCase) && 
                string.Equals(Neighborhood, entities.Neighborhood, StringComparison.OrdinalIgnoreCase) && 
                string.Equals(State, entities.State, StringComparison.OrdinalIgnoreCase) && 
                string.Equals(ZipCode, entities.ZipCode, StringComparison.OrdinalIgnoreCase) && 
                string.Equals(Route, entities.Route, StringComparison.OrdinalIgnoreCase) && 
                SituationCode == entities.SituationCode && 
                string.Equals(Situation, entities.Situation, StringComparison.OrdinalIgnoreCase) && 
                TypeCode == entities.TypeCode && string.Equals(Type, entities.Type, StringComparison.OrdinalIgnoreCase) && 
                string.Equals(AdicionalInformation, entities.AdicionalInformation, StringComparison.OrdinalIgnoreCase) && 
                string.Equals(StateRegistration, entities.StateRegistration, StringComparison.OrdinalIgnoreCase) && 
                string.Equals(Flag, entities.Flag, StringComparison.OrdinalIgnoreCase);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = EntityCode;
                hashCode = (hashCode * 397) ^ (Name != null ? StringComparer.OrdinalIgnoreCase.GetHashCode(Name) : 0);
                hashCode = (hashCode * 397) ^ (Nickname != null ? StringComparer.OrdinalIgnoreCase.GetHashCode(Nickname) : 0);
                hashCode = (hashCode * 397) ^ (Document != null ? StringComparer.OrdinalIgnoreCase.GetHashCode(Document) : 0);
                hashCode = (hashCode * 397) ^ CreationTime.GetHashCode();
                hashCode = (hashCode * 397) ^ (AddressLine1 != null ? StringComparer.OrdinalIgnoreCase.GetHashCode(AddressLine1) : 0);
                hashCode = (hashCode * 397) ^ (AddressLine2 != null ? StringComparer.OrdinalIgnoreCase.GetHashCode(AddressLine2) : 0);
                hashCode = (hashCode * 397) ^ AddressTypeCode;
                hashCode = (hashCode * 397) ^ (AddressType != null ? StringComparer.OrdinalIgnoreCase.GetHashCode(AddressType) : 0);
                hashCode = (hashCode * 397) ^ (City != null ? StringComparer.OrdinalIgnoreCase.GetHashCode(City) : 0);
                hashCode = (hashCode * 397) ^ (Neighborhood != null ? StringComparer.OrdinalIgnoreCase.GetHashCode(Neighborhood) : 0);
                hashCode = (hashCode * 397) ^ (State != null ? StringComparer.OrdinalIgnoreCase.GetHashCode(State) : 0);
                hashCode = (hashCode * 397) ^ (ZipCode != null ? StringComparer.OrdinalIgnoreCase.GetHashCode(ZipCode) : 0);
                hashCode = (hashCode * 397) ^ (Route != null ? StringComparer.OrdinalIgnoreCase.GetHashCode(Route) : 0);
                hashCode = (hashCode * 397) ^ SituationCode;
                hashCode = (hashCode * 397) ^ (Situation != null ? StringComparer.OrdinalIgnoreCase.GetHashCode(Situation) : 0);
                hashCode = (hashCode * 397) ^ TypeCode;
                hashCode = (hashCode * 397) ^ (Type != null ? StringComparer.OrdinalIgnoreCase.GetHashCode(Type) : 0);
                hashCode = (hashCode * 397) ^ (AdicionalInformation != null ? StringComparer.OrdinalIgnoreCase.GetHashCode(AdicionalInformation) : 0);
                hashCode = (hashCode * 397) ^ (StateRegistration != null ? StringComparer.OrdinalIgnoreCase.GetHashCode(StateRegistration) : 0);
                hashCode = (hashCode * 397) ^ (Flag != null ? StringComparer.OrdinalIgnoreCase.GetHashCode(Flag) : 0);
                return hashCode;
            }
        }
    }
}
