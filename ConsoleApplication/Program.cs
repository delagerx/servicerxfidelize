﻿using Core;
using System.Net.Http;
using Topshelf;
using Integracao.Calls;
using System;
using System.Timers;
using System.Configuration;
using ConsoleApplication.TimerFluxo;
using System.Threading.Tasks;

namespace ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<Service>(s =>
                {
                    s.ConstructUsing(n => new Service());
                    s.WhenStarted(service => service.InitializeService());
                    s.WhenStopped(service => service.Stop());
                });

                x.RunAsLocalSystem();
                x.UseAssemblyInfoForServiceInfo();
            });
        }

        public class Service
        {
            public void InitializeService()
            {
                TaskFluxo1 Fluxo1 = new TaskFluxo1();
                TaskFluxo3 Fluxo3 = new TaskFluxo3();
                TaskFluxo4 Fluxo4 = new TaskFluxo4();
                TaskFluxo5 Fluxo5 = new TaskFluxo5();
                TaskFluxo6 Fluxo6 = new TaskFluxo6();
                TaskFluxo7 Fluxo7 = new TaskFluxo7();

                //Task taskFluxo1 = Task.Factory.StartNew(() => Fluxo1.StartTaskFluxo1());
                Task taskFluxo3 = Task.Factory.StartNew(() => Fluxo3.StartTaskFluxo3());
                //Task taskFluxo4 = Task.Factory.StartNew(() => Fluxo4.StartTaskFluxo4());
                //Task taskFluxo5 = Task.Factory.StartNew(() => Fluxo5.StartTaskFluxo5());
                //Task taskFluxo6 = Task.Factory.StartNew(() => Fluxo6.StartTaskFluxo6());
                //Task taskFluxo7 = Task.Factory.StartNew(() => Fluxo7.StartTaskFluxo7());
            }



            public async void Stop()
            {
            }
        }
    }
}
