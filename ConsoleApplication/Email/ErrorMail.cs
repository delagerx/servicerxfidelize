﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Configuration;

namespace ConsoleApplication.Email
{
    public class ErrorMail
    {
        public void SendErrorMail(string task, string error, int? codProduto)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["ServidorSMTP"]);

                mail.From = new MailAddress(ConfigurationManager.AppSettings["Remetente"]);
                mail.To.Add(ConfigurationManager.AppSettings["Destinatario"]);

                if(Int32.Parse(ConfigurationManager.AppSettings["CC"]) == 1)
                {
                    mail.CC.Add(ConfigurationManager.AppSettings["DestinatarioCC"]);
                }

                mail.Subject = "Erro na Integração";
                mail.Body = createBody(task, error);

                SmtpServer.Port = Int32.Parse(ConfigurationManager.AppSettings["PortaSMTP"]);
                SmtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["Remetente"], ConfigurationManager.AppSettings["SenhaRemetente"]);
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public string createBody(string task, string error)
        {
            StringBuilder body = new StringBuilder();
            string taskName = "";

            switch(task)
            {
                case "TaskFluxo1":
                    taskName = "Fluxo 1";
                    break;

                case "TaskFluxo2":
                    taskName = "Fluxo 2";
                    break;

                case "TaskFluxo3":
                    taskName = "Fluxo 3";
                    break;

                case "TaskFluxo4":
                    taskName = "Fluxo 4";
                    break;

                case "TaskFluxo5":
                    taskName = "Fluxo 5";
                    break;

                case "TaskFluxo6":
                    taskName = "Fluxo 6";
                    break;

                default:
                    break;
            }

            body.AppendLine("Data: " + DateTime.Now.ToString("dd/mm/yyyy - hh:mm:ss") + "\n");
            body.AppendLine("\n");
            body.AppendLine("Olá, \n");
            body.AppendLine("você está recebendo este e-mail pois ocorreu um erro no sistema de integração. Os detalhes estão exibidos abaixo. \n");
            body.AppendLine("\n");
            body.AppendLine("\n");
            body.AppendLine("Módulo: " + taskName + "\n");
            body.AppendLine("\n");
            body.AppendLine("Descrição: " + error + "\n");
            body.AppendLine("\n");
            body.AppendLine("Pedido: \n");

            return body.ToString();
        }
    }
}
