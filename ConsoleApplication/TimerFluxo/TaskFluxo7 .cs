﻿using ConsoleApplication.LogClass;
using Integracao.Calls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplication.TimerFluxo
{
    public class TaskFluxo7
    {
        private static Timer timerFluxo7;
        private static Log logFluxo = new Log("Fluxo7Log", "Fluxo7");
        public void StartTaskFluxo7()
        {
                timerFluxo7 = new Timer(InitializeFluxo7Async,
                null,
                Int32.Parse(ConfigurationManager.AppSettings["TimerFluxo7"]),
                Timeout.Infinite);
        }

        public async void InitializeFluxo7Async(Object state)
        {
            try
            {
                bool y = await CallNotas.RequestInvoiceReversalRxAsync();// Fluxo 7
                timerFluxo7.Change(Int32.Parse(ConfigurationManager.AppSettings["TimerFluxo7"]), Timeout.Infinite);
            }
            catch (Exception ex)
            {
                logFluxo.WriteEntry(ex);
            }
        }
    }
}
