﻿using ConsoleApplication.LogClass;
using Integracao.Calls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplication.TimerFluxo
{
    public class TaskFluxo5
    {
        private static Timer timerFluxo5;
        private static Log logFluxo = new Log("Fluxo5Log", "Fluxo5");
        public void StartTaskFluxo5()
        {
                timerFluxo5 = new Timer(InitializeFluxo5Async,
                null,
                Int32.Parse(ConfigurationManager.AppSettings["TimerFluxo5"]),
                Timeout.Infinite);
        }

        public async void InitializeFluxo5Async(Object state)
        {
            try
            {
                Boolean y = await CallNotas.RequestInvoiceRxAsync();// Fluxo 5
                timerFluxo5.Change(Int32.Parse(ConfigurationManager.AppSettings["TimerFluxo5"]), Timeout.Infinite);
            }
            catch (Exception ex)
            {
                logFluxo.WriteEntry(ex);
                timerFluxo5.Change(Int32.Parse(ConfigurationManager.AppSettings["TimerFluxo5"]), Timeout.Infinite);
            }
        }
    }
}
