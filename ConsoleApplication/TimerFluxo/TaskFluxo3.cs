﻿using ConsoleApplication.LogClass;
using Integracao.Calls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplication.TimerFluxo
{
    public class TaskFluxo3
    {
        private static Timer timerFluxo3;
        private static Log logFluxo = new Log("Fluxo3Log", "Fluxo3");

        public void StartTaskFluxo3()
        {
            timerFluxo3 = new Timer(InitializeFluxo3Async,
                null,
                Int32.Parse(ConfigurationManager.AppSettings["TimerFluxo3"]),
                Timeout.Infinite);
        }

        public async void InitializeFluxo3Async(Object state)
        {
            try
            {
                Boolean y = await CallPedidos.UpdateOrderStatus();// Fluxo 3
                timerFluxo3.Change(Int32.Parse(ConfigurationManager.AppSettings["TimerFluxo3"]), Timeout.Infinite);
            }
            catch (Exception ex) {
               logFluxo.WriteEntry(ex);
            }
        }

    }
}
