﻿using ConsoleApplication.LogClass;
using Integracao.Calls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplication.TimerFluxo
{
    public class TaskFluxo4
    {
        private static Timer timerFluxo4;
        private static Log logFluxo = new Log("Fluxo4Log", "Fluxo4");
        public void StartTaskFluxo4()
        {
            timerFluxo4 = new Timer(InitializeFluxo4Async,
            null,
            Int32.Parse(ConfigurationManager.AppSettings["TimerFluxo4"]),
            Timeout.Infinite);
        }

        public async void InitializeFluxo4Async(Object state)
        {
            try {
                Boolean y = await CallPedidos.RequestOrderRetornoRxAsync();// Fluxo 4
                timerFluxo4.Change(Int32.Parse(ConfigurationManager.AppSettings["TimerFluxo4"]), Timeout.Infinite);
            }catch (Exception ex) {
                logFluxo.WriteEntry(ex);
            }

            }
    }
}
