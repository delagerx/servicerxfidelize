﻿using Integracao.Calls;
using ConsoleApplication.Email;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ConsoleApplication.LogClass;


namespace ConsoleApplication.TimerFluxo
{
    public class TaskFluxo1 
    {
        private static Timer timerFluxo1;
        private static Log logFluxo = new Log("Fluxo1Log","Fluxo1");

        

        public void StartTaskFluxo1()
        {
            timerFluxo1 = new Timer(InitializeFluxo1Async,
                null,
                Int32.Parse(ConfigurationManager.AppSettings["TimerFluxo1"]),
                Timeout.Infinite);
        }
       
        public async void InitializeFluxo1Async(Object state)
        {
           try
            {

                Boolean x = await CallPedidos.RequestOrderRxAsync();// Fluxo 1 e 2
                timerFluxo1.Change(Int32.Parse(ConfigurationManager.AppSettings["TimerFluxo1"]), Timeout.Infinite);
            }
            catch (Exception ex) {
                logFluxo.WriteEntry(ex);
            }
        }
    }
}
