﻿using ConsoleApplication.LogClass;
using Integracao.Calls;
using System;
using System.Configuration;
using System.Threading;

namespace ConsoleApplication
{
    public class TaskFluxo6
    {
        public Timer timerFluxo6 { get; private set; }
        private static Log logFluxo = new Log("Fluxo6Log", "Fluxo6");
        public void StartTaskFluxo6()
        {
            timerFluxo6 = new Timer(InitializeFluxo6Async,
            null,
            int.Parse(ConfigurationManager.AppSettings["TimerFluxo6"]),
            Timeout.Infinite);
        }

        public async void InitializeFluxo6Async(Object state)
        {
            try
            {
                bool y = await CallPedidos.RequestCancellationRxAsync();// Fluxo 6
                timerFluxo6.Change(int.Parse(ConfigurationManager.AppSettings["TimerFluxo6"]), Timeout.Infinite);
            }
            catch (Exception ex)
            {
                logFluxo.WriteEntry(ex);
            }
        }
    }
}