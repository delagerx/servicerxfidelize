﻿namespace ConsoleApplication
{
    public class ActorName
    {
        public const string AGENTE_DIARIO_LOTE_ESTOQUE = "agenteDiarioLoteEstoque";
        public const string AGENTE_DIARIO_PRODUTO_ESTOQUE = "agenteDiarioProdutoEstoque";
        public const string AGENTE_ENTIDADE = "agenteEntidade";
        public const string AGENTE_PRODUTO = "agenteProduto";
        public const string AGENTE_PRODUTO_AUDITORIA_MOV_INV = "agenteProdutoAuditoriaMovInv";
        public const string AGENTE_MENSAGEM_INTEGRACAO = "agenteMensagemIntegracao";
        public const string AGENTE_PEDIDO_CANCELAMENTO = "agentePedidoCancelamento";
        public const string AGENTE_PEDIDO_ERP = "agentePedidoErp";
        public const string AGENTE_PEDIDO_SITUACAO_OPERACAO_SAIDA = "agentePedidoSituacaoOperacaoSaida";
        public const string AGENTE_PEDIDO_WMS = "agentePedidoWms";
        public const string AGENTE_TITULO_OPERACAO_SAIDA = "agenteTituloOperacaoSaida";
        public const string COORDENADOR_IMPORTACAO = "coordenadorImportacao";
        public const string ERP_DIARO_LOTE_ESTOQUE = "erpEscritorDiarioLoteEstoque";
        public const string ERP_DIARO_PRODUTO_ESTOQUE = "erpEscritorDiarioProdutoEstoque";
        public const string ERP_ESCRITOR_MENSAGEM_INTEGRACAO = "escritorMensagemIntegracao";
        public const string ERP_ESCRITOR_PEDIDO = "erpEscritorPedido";
        public const string ERP_ESCRITOR_PEDIDO_SITUACAO_OPERACAO_SAIDA = "erpEscritorPedidoSituacaoOperacaoSaida";
        public const string ERP_ESCRITOR_PRODUTO_AUDITORIA_MOV_INV = "erpEscritorProdutoAuditoriaMovInv";
        public const string ERP_LEITOR_ENTIDADE = "erpLeitorEntidade";
        public const string ERP_LEITOR_MENSAGEM_INTEGRACAO = "leitorMensagemIntegracao";
        public const string ERP_LEITOR_PEDIDO_CANCELAMENTO = "erpLeitorPedidoCancelamento";
        public const string ERP_LEITOR_PEDIDO = "erpLeitorPedido";
        public const string ERP_LEITOR_PRODUTO = "erpLeitorProduto";
        public const string ERP_LEITOR_TITULO_OPERACAO_SAIDA = "erpLeitorTituloOperacaoSaida";
        public const string WMS_ESCRITOR_ENTIDADE = "wmsEscritorEntidade";
        public const string WMS_ESCRITOR_PRODUTO = "wmsEscritorProduto";
        public const string WMS_ESCRITOR_PEDIDO = "wmsEscritorPedido";
        public const string WMS_ESCRITOR_PEDIDO_CANCELAMENTO = "wmsEscritorPedidoCancelamento";
        public const string WMS_ESCRITOR_TITULO_OPERACAO_SAIDA = "wmsEscritorTituloOperacaoSaida";
        public const string WMS_LEITOR_PEDIDO = "wmsLeitorPedido";
        public const string WMS_LEITOR_DIARIO_PRODUTO_ESTOQUE = "wmsLeitorDiarioProdutoEstoque";
        public const string WMS_LEITOR_PRODUTO_AUDITORIA_MOV_INV = "wmsLeitorProdutoAuditoriaMovInv";
        public const string WMS_LEITOR_PEDIDO_SITUACAO_OPERACAO_SAIDA = "wmsLeitorPedidoSituacaoOpercaoSaida";

    }
}
