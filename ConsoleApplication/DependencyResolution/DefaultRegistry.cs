﻿using StructureMap;
using StructureMap.Graph;
using System;

namespace ConsoleApplication.DependencyResolution
{
    public class DefaultRegistry : Registry
    {
        public DefaultRegistry(Action<IAssemblyScanner> assembliesToScan)
        {
            Scan(s =>
            {
                assembliesToScan(s);
                s.WithDefaultConventions();
                s.RegisterConcreteTypesAgainstTheFirstInterface();
            });

         
        }
    }
}
