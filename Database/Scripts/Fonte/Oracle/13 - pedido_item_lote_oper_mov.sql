CREATE TABLE pedido_item_lote_oper_mov(
	id_pedido_item_lote_oper_mov Number(10) NOT NULL,
	id_pedido_item_oper_mov Number(10) NOT NULL,
	lote_orig Varchar2(50) NOT NULL,
	lote Varchar2(50) NOT NULL,
	id_classificacao_origem_polo Number(10) NOT NULL,
	id_classificacao_destino_polo Number(10) NOT NULL,
	quantidade Number NOT NULL,
	validade_orig Timestamp(0) NULL,
	validade Timestamp(0) NULL,
	fabricacao Timestamp(0) NULL,
	cod_situacao Number(10) NULL,
	obs Varchar2(255) NULL,
	classificacao_origem Varchar2(50) NULL,
	classificacao_destino Varchar2(50) NULL,
 CONSTRAINT UQ_pedido_item_lote_oper_mov UNIQUE 
(
	id_pedido_item_oper_mov,
	lote_orig,
	lote,
	id_classificacao_origem_polo,
	id_classificacao_destino_polo 
) 
);



ALTER TABLE pedido_item_lote_oper_mov ADD (
  CONSTRAINT pk_pedido_item_lote_oper_mov PRIMARY KEY (id_pedido_item_lote_oper_mov));
CREATE SEQUENCE pedido_item_lote_oper_mov_seq;


CREATE OR REPLACE TRIGGER pedido_item_lote_oper_mov_bir 
BEFORE INSERT ON pedido_item_lote_oper_mov 
FOR EACH ROW
WHEN (new.id_pedido_item_lote_oper_mov IS NULL)
BEGIN
  SELECT pedido_item_lote_oper_mov_seq.NEXTVAL
  INTO   :new.id_pedido_item_lote_oper_mov
  FROM   dual;
END;

ALTER TABLE pedido_item_lote_oper_mov
ADD CONSTRAINT FK_pedido_item_oper_mov FOREIGN KEY (id_pedido_item_oper_mov) 
references pedido_item_oper_movimentacao (id_pedido_item_oper_mov);


