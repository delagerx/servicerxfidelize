

CREATE TABLE produto_shelf_life(
	id_produto_shelf_life Number(10)  NOT NULL,
	id_produto Number(10) NOT NULL,
	grupo_shelf_life Varchar2(30) NOT NULL,
	validade_minima int NULL,
	validade_maxima int NULL,
 CONSTRAINT UQ_produto_shelf_life UNIQUE 
(
	id_produto,
	grupo_shelf_life 
) 
);

ALTER TABLE produto_shelf_life ADD (
  CONSTRAINT pk_produto_shelf_life PRIMARY KEY (id_produto_shelf_life));
CREATE SEQUENCE produto_shelf_life_seq;




CREATE OR REPLACE TRIGGER produto_shelf_life_bir 
BEFORE INSERT ON produto_shelf_life 
FOR EACH ROW
WHEN (new.id_produto_shelf_life IS NULL)
BEGIN
  SELECT produto_shelf_life_seq.NEXTVAL
  INTO   :new.id_produto_shelf_life
  FROM   dual;
END;



ALTER TABLE produto_shelf_life ADD 
  CONSTRAINT FK_prod_shelf_life_prod FOREIGN KEY (id_produto) REFERENCES produto (id_produto) ON DELETE CASCADE;
