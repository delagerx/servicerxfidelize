

CREATE TABLE pedido_item_oper_movimentacao(
	id_pedido_item_oper_mov Number(10)  NOT NULL,
	id_pedido_operacao_mov Number(10) NOT NULL,
	id_produto Number(10) NOT NULL,
	quantidade Number  not NULL,
	separado Number NULL,
	cod_motivo Number(10) NULL,
	motivo Varchar2(30) NULL,
	numero_item Number(10) NULL,
	obs Varchar2(255) NULL,
	ordem_conf Number(10) NULL,
 CONSTRAINT UQ_pedido_item_oper_mov UNIQUE 
(
	id_pedido_operacao_mov,
	id_produto 
) 
);




ALTER TABLE pedido_item_oper_movimentacao ADD (
  CONSTRAINT pk_pedido_item_oper_mov PRIMARY KEY (id_pedido_item_oper_mov));
CREATE SEQUENCE pedido_item_oper_mov_seq;


CREATE OR REPLACE TRIGGER pedido_item_oper_mov_bir 
BEFORE INSERT ON pedido_item_oper_movimentacao 
FOR EACH ROW
WHEN (new.id_pedido_item_oper_mov IS NULL)
BEGIN
  SELECT pedido_item_oper_mov_seq.NEXTVAL
  INTO   :new.id_pedido_item_oper_mov
  FROM   dual;
END;





ALTER TABLE pedido_item_oper_movimentacao
ADD CONSTRAINT ck_pedido_item_oper_mov_qtde
CHECK (quantidade>=(0));


ALTER TABLE pedido_item_oper_movimentacao
ADD CONSTRAINT ck_ped_item_oper_mov_separado
CHECK (separado>=(0));


ALTER TABLE pedido_item_oper_movimentacao
ADD CONSTRAINT FK_ped_item_oper_mov_produto FOREIGN KEY (id_produto) references produto (id_produto);

