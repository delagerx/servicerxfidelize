CREATE TABLE produto_cod_barra(
	id_produto_cod_barra Number(10)  NOT NULL,
	cod_barra Varchar2(20) NOT NULL,
	id_produto Number(10) NOT NULL,
	quantidade Number NOT NULL,
	cod_tipo Number(10) NOT NULL,
	tipo Varchar2(30) NULL,
	altura Number NULL,
	largura Number NULL,
	comprimento Number NULL,
	peso Number NULL,
	principal Number(10) NULL,
 CONSTRAINT UQ_produto_Cod_barra UNIQUE 
(
	cod_barra,
	id_produto 
) 
);



ALTER TABLE produto_Cod_barra ADD (
  CONSTRAINT pk_produto_Cod_barra PRIMARY KEY (id_produto_Cod_barra));
CREATE SEQUENCE produto_Cod_barra_seq;




CREATE OR REPLACE TRIGGER produto_Cod_barra_bir 
BEFORE INSERT ON produto_Cod_barra 
FOR EACH ROW
WHEN (new.id_produto_Cod_barra IS NULL)
BEGIN
  SELECT produto_Cod_barra_seq.NEXTVAL
  INTO   :new.id_produto_Cod_barra
  FROM   dual;
END;


ALTER TABLE produto_Cod_barra
ADD CONSTRAINT ck_produto_Cod_barra_qtde
CHECK (quantidade>=(0)); 


ALTER TABLE produto_Cod_barra ADD 
  CONSTRAINT FK_produto_Cod_barra_produto FOREIGN KEY (id_produto) REFERENCES produto (id_produto) ON DELETE CASCADE;
