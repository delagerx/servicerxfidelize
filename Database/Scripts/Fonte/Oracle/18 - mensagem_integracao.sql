create table mensagem_integracao (
  Id_mensagem_integracao Number(10)  NOT NULL,
	Origem  Number(10) not null, --1 - ERP, 2 - WMSRX
	tabela_relacionada Varchar2(30) NOT NULL,
	Id_tabela_relacionada  Number(10) NOT NULL,	
	Data_cadastro Timestamp(0) DEFAULT SYSDATE,
	Data_processamento Timestamp(0) NULL,
	Cod_rejeicao  Number(10),
	Motivo_rejeicao Varchar2(100),
	Rejeitado number(3) default '0' check (Rejeitado in ('1','0') )
);



ALTER TABLE mensagem_integracao ADD (
  CONSTRAINT pk_mensagem_integracao PRIMARY KEY (Id_mensagem_integracao));
CREATE SEQUENCE mensagem_integracao_seq;



CREATE OR REPLACE TRIGGER mensagem_integracao_bir 
BEFORE INSERT ON mensagem_integracao 
FOR EACH ROW
WHEN (new.Id_mensagem_integracao IS NULL)
BEGIN
  SELECT mensagem_integracao_seq.NEXTVAL
  INTO   :new.Id_mensagem_integracao
  FROM   dual;
END;

