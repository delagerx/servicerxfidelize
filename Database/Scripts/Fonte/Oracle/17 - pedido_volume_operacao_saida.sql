

CREATE TABLE pedido_volume_operacao_saida(
	id_pedido_volume_oper_saida Number(10) NOT NULL,
	id_pedido_operacao_saida Number(10) NOT NULL,
	volume Number(10) NOT NULL,
	lacre Varchar2(50) NOT NULL,
	data_volume_embarcado Timestamp(0) NULL,
	tipo_caixa Number(5) NULL,
 CONSTRAINT UQ_pedido_volume_oper_saida UNIQUE 
(
	id_pedido_operacao_saida,
	volume 
) 
);
  
  
ALTER TABLE pedido_volume_operacao_saida ADD (
  CONSTRAINT pk_pedido_volume_oper_saida PRIMARY KEY (id_pedido_volume_oper_saida));
  
CREATE SEQUENCE pedido_volume_oper_saida_seq;


CREATE OR REPLACE TRIGGER pedido_volume_oper_saida_bir 
BEFORE INSERT ON pedido_volume_operacao_saida 
FOR EACH ROW
WHEN (new.id_pedido_volume_oper_saida IS NULL)
BEGIN
  SELECT pedido_volume_oper_saida_seq.NEXTVAL
  INTO   :new.id_pedido_volume_oper_saida
  FROM   dual;
END;



ALTER TABLE pedido_volume_operacao_saida ADD CONSTRAINT FK_pedido_operacao_saida_vol
FOREIGN KEY (id_pedido_operacao_saida) references pedido_operacao_saida (id_pedido_operacao_saida) 
ON DELETE CASCADE;