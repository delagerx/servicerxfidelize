

CREATE TABLE pedido_volume_operacao_mov(
	id_pedido_volume_oper_mov Number(10) NOT NULL,
	id_pedido_operacao_mov Number(10) NOT NULL,
	volume Number(10) not NULL,
	lacre Varchar2(50) NOT NULL,
	data_volume_embarcado Timestamp(0) NULL,
	tipo_caixa Number(5) NULL,
 CONSTRAINT UQ_pedido_volume_oper_mov UNIQUE 
(
	id_pedido_operacao_mov,
	volume 
) 
);




  
ALTER TABLE pedido_volume_operacao_mov ADD (
  CONSTRAINT pk_pedido_volume_oper_mov PRIMARY KEY (id_pedido_volume_oper_mov));
  
CREATE SEQUENCE pedido_volume_oper_mov_seq;


CREATE OR REPLACE TRIGGER pedido_volume_oper_mov_bir 
BEFORE INSERT ON pedido_volume_operacao_mov 
FOR EACH ROW
WHEN (new.id_pedido_volume_oper_mov IS NULL)
BEGIN
  SELECT pedido_volume_operacao_mov_seq.NEXTVAL
  INTO   :new.id_pedido_volume_oper_mov
  FROM   dual;
END;




ALTER TABLE pedido_volume_operacao_mov ADD CONSTRAINT FK_pedido_operacao_mov
FOREIGN KEY (id_pedido_operacao_mov) references pedido_operacao_movimentacao (id_pedido_operacao_mov) 
ON DELETE CASCADE;
