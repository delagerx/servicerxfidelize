

CREATE TABLE entidade(
	id_entidade Number(10)  NOT NULL,
	cod_entidade_polo Varchar2(30) NOT NULL,
	cpf_cnpj Varchar2(30) NULL,
	razao_social Varchar2(255) NOT NULL,
	cod_tipo_pessoa Number(10) NULL,
	fantasia Varchar2(50) default null,
	tipo Varchar2(50) NOT NULL,
	subtipo Varchar2(50) NULL,
	bandeira Varchar2(50) NULL,
	cod_situacao Number(10) NULL,
	obs Varchar2(500) DEFAULT NULL,
	endereco Varchar2(100) NULL,
	cod_tipo_endereco Number(5) NULL,
	bairro Varchar2(60) NULL,
	cep Varchar2(9) NULL,
	cod_rota Varchar2(7) NULL,
	descr_rota Varchar2(50) NULL,
	complemento Varchar2(100) NULL,
	cidade Varchar2(50) NULL,
	sigla_estado char(10) NULL,
	inscricao_estadual Varchar2(30) NULL,
	telefone Varchar2(20) NULL,
 CONSTRAINT UQ_entidade UNIQUE (cod_entidade_polo, tipo ) );


ALTER TABLE entidade ADD (
  CONSTRAINT pk_entidade PRIMARY KEY (id_entidade));
CREATE SEQUENCE entidade_seq;

CREATE OR REPLACE TRIGGER entidade_bir 
BEFORE INSERT ON entidade 
FOR EACH ROW
WHEN (new.id_entidade IS NULL)
BEGIN
  SELECT entidade_seq.NEXTVAL
  INTO   :new.id_entidade
  FROM   dual;
END;
