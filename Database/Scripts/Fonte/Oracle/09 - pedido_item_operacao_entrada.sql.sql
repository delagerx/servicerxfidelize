
CREATE TABLE pedido_item_operacao_entrada(
	id_pedido_item_oper_entrada Number(10)  NOT NULL,
	id_pedido_operacao_entrada Number(10) NOT NULL,
	id_produto Number(10) NOT NULL,
	quantidade Number not NULL,
	separado Number NULL,
	cod_motivo Number(10) NULL,
	motivo Varchar2(30) NULL,
	numero_item Number(10) NULL,
	obs Varchar2(255) NULL,
	ordem_conf Number(10) NULL,
 CONSTRAINT UQ_pedido_item_oper_entrada UNIQUE 
(
	id_pedido_operacao_entrada,
	id_produto 
) 
);




ALTER TABLE pedido_item_operacao_entrada ADD (
  CONSTRAINT pk_pedido_item_oper_entrada PRIMARY KEY (id_pedido_item_oper_entrada));
CREATE SEQUENCE pedido_item_oper_entrada_seq;


CREATE OR REPLACE TRIGGER pedido_item_oper_entrada_bir 
BEFORE INSERT ON pedido_item_operacao_entrada 
FOR EACH ROW
WHEN (new.id_pedido_item_oper_entrada IS NULL)
BEGIN
  SELECT pedido_item_oper_entrada_seq.NEXTVAL
  INTO   :new.id_pedido_item_oper_entrada
  FROM   dual;
END;


ALTER TABLE pedido_item_operacao_entrada
ADD CONSTRAINT ck_ped_item_oper_entrad_qtde
CHECK (quantidade>=(0));


ALTER TABLE pedido_item_operacao_entrada
ADD CONSTRAINT ck_ped_item_oper_ent_separad
CHECK (separado>=(0));


ALTER TABLE pedido_item_operacao_entrada
ADD CONSTRAINT FK_pedido_item_oper_ent_prod FOREIGN KEY (id_produto) references produto (id_produto);


