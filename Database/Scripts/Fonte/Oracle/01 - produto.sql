CREATE TABLE produto(
	id_produto Number(10)  NOT NULL,
	cod_produto_polo Varchar2(30) NOT NULL,
	cod_forn_polo Varchar2(30) NULL,
	descricao Varchar2(80) default null NOT NULL,
	generico Varchar2(80) NULL,
	grupo Varchar2(50) NULL,
	subgrupo Varchar2(50) NULL,
	cod_situacao Number(10) NULL,
	emb_compra Number  default 1,
	emb_saida Number default 1 NOT NULL,
	validade Number(10) default 356,
	linha_produto Varchar2(50) NULL,
	controle_lote Number(3) default '0' check (controle_lote in ('1','0') ),
	unidade Varchar2(5) default 'UN',
	casas_decimais Number(10) DEFAULT 0,
	altura Number DEFAULT 1,
	largura Number DEFAULT 1,
	comprimento Number DEFAULT 1,
	peso Number NULL,
	preco Number NULL,
 CONSTRAINT UQ_produto UNIQUE (cod_produto_polo ) 
);


ALTER TABLE produto ADD (
  CONSTRAINT pk_produto PRIMARY KEY (id_produto));
CREATE SEQUENCE produto_seq;



CREATE OR REPLACE TRIGGER produto_bir 
BEFORE INSERT ON produto 
FOR EACH ROW
WHEN (new.id_produto IS NULL)
BEGIN
  SELECT produto_seq.NEXTVAL
  INTO   :new.id_produto
  FROM   dual;
END;
END IF;
