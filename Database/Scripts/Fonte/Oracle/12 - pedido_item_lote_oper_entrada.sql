CREATE TABLE pedido_item_lote_oper_entrada(
	id_ped_item_lote_oper_entrada Number(10) NOT NULL,
	id_pedido_item_oper_entrada Number(10) NOT NULL,
	lote_orig Varchar2(50) NOT NULL,
	lote Varchar2(50) NOT NULL,
	id_classificacao_origem_polo Number(10) NOT NULL,
	id_classificacao_destino_polo Number(10) NOT NULL,
	quantidade Number NOT NULL,
	validade_orig Timestamp(0) NULL,
	validade Timestamp(0) NULL,
	fabricacao Timestamp(0) NULL,
	cod_situacao Number(10) NULL,
	obs Varchar2(255) NULL,
	classificacao_origem Varchar2(50) NULL,
	classificacao_destino Varchar2(50) NULL,
 CONSTRAINT UQ_ped_item_lote_oper_entr UNIQUE 
(
	id_pedido_item_oper_entrada,
	lote_orig,
	lote,
	id_classificacao_origem_polo,
	id_classificacao_dest_polo 
) 
);



ALTER TABLE pedido_item_lote_oper_entrada ADD (
  CONSTRAINT pk_ped_item_lote_oper_ent PRIMARY KEY (id_ped_item_lote_oper_entrada));
CREATE SEQUENCE ped_item_lote_oper_entrada_seq;


CREATE OR REPLACE TRIGGER ped_item_lot_oper_entrad_bir 
BEFORE INSERT ON pedido_item_lote_oper_entrada 
FOR EACH ROW
WHEN (new.id_ped_item_lote_oper_entrada IS NULL)
BEGIN
  SELECT ped_item_lote_oper_entrada_seq.NEXTVAL
  INTO   :new.id_ped_item_lote_oper_entrada
  FROM   dual;
END;



ALTER TABLE pedido_item_lote_oper_entrada
ADD CONSTRAINT FK_pedido_item_oper_entrada FOREIGN KEY (id_pedido_item_oper_entrada) 
references pedido_item_operacao_entrada (id_pedido_item_oper_entrada);
