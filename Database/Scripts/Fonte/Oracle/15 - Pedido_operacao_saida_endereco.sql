

CREATE TABLE pedido_operacao_saida_endereco(
	id_pedido_oper_saida_endereco Number(10) NOT NULL,
	cod_pedido_polo Varchar2(30) NOT NULL,
	cod_operacao_logistica_polo Varchar2(20) NULL,
	cod_tipo_endereco Number(5) NULL,
	bairro Varchar2(60) NULL,
	cep Varchar2(9) NOT NULL,
	cod_rota Varchar2(7) NULL,
	descr_rota Varchar2(50) NULL,
	complemento Varchar2(100) NULL,
	cidade Varchar2(50) NULL,
	sigla_estado char(10) NULL,
 CONSTRAINT UQ_pedido_oper_saida_endereco UNIQUE 
(
	id_pedido_operacao_saida,
	cep 
) 
);



  
ALTER TABLE pedido_operacao_saida_endereco ADD (
  CONSTRAINT pk_pedido_oper_saida_ender PRIMARY KEY (id_pedido_oper_saida_endereco));
  
CREATE SEQUENCE pedido_oper_saida_ender_seq;


CREATE OR REPLACE TRIGGER pedido_oper_saida_endereco_bi 
BEFORE INSERT ON pedido_operacao_saida_endereco 
FOR EACH ROW
WHEN (new.id_pedido_oper_saida_endereco IS NULL)
BEGIN
  SELECT pedido_oper_saida_endereco_seq.NEXTVAL
  INTO   :new.id_pedido_oper_saida_endereco
  FROM   dual;
END;




