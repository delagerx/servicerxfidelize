CREATE TABLE produto_curva(
	id_produto_curva Number(10)  NOT NULL,
	id_produto Number(10) NOT NULL,
	cod_operacao_logistica_polo Varchar2(20) NOT NULL,
	curva Varchar2(10) NOT NULL,
 CONSTRAINT UQ_produto_curva UNIQUE 
(
	id_produto,
	cod_operacao_logistica_polo,
	curva 
) 
);

ALTER TABLE produto_curva ADD (
  CONSTRAINT pk_produto_curva PRIMARY KEY (id_produto_curva));
CREATE SEQUENCE produto_curva_seq;




CREATE OR REPLACE TRIGGER produto_curva_bir 
BEFORE INSERT ON produto_curva 
FOR EACH ROW
WHEN (new.id_produto_curva IS NULL)
BEGIN
  SELECT produto_curva_seq.NEXTVAL
  INTO   :new.id_produto_curva
  FROM   dual;
END;


ALTER TABLE produto_curva ADD 
  CONSTRAINT FK_produto_curva_produto FOREIGN KEY (id_produto) REFERENCES produto (id_produto) ON DELETE CASCADE;