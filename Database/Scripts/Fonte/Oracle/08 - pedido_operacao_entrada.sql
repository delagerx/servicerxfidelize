CREATE TABLE pedido_operacao_entrada(
	id_pedido_operacao_entrada Number(10)  NOT NULL,
	cod_pedido_polo Varchar2(30) NOT NULL,
	cod_operacao_logistica_polo Varchar2(20) NOT NULL,
	id_entidade Number(10) NOT NULL,
	cod_prenota Number(10)  NULL,	
	operacao Number(10) NOT null,
	cod_situacao Number(10) NULL,
	situacao Varchar2(50) NULL,
	digitacao Timestamp(0) NULL,
	data_liberacao Timestamp(0) NULL,
	prenota Timestamp(0) NULL,
	encerramento_PN Timestamp(0) NULL,
	quem_digitou Varchar2(30) NULL,
	quem_liberou Varchar2(30) NULL,
	cod_origem_ped Number(10) NULL,
	origem_ped Varchar2(50) NULL,
	volumes Number(10) NULL,
	valor_total Number NULL,
	obs_PN Varchar2(500) NULL,
	suboperacao Varchar2(50) NULL,
	motivo_suboperacao Varchar2(50) NULL,
	numero_titulo Varchar2(13) NULL,
	serie_nf Varchar2(3) NULL,
	cod_titulo_polo Varchar2(30) NULL,
	peso Binary_float NULL,
	qtdItens Number(10) NULL,
	obs Varchar2(255) NULL,
	id_cancelamento Number(10) NULL,
	data_solicitacao_cancelamento Timestamp(3) NULL,
    	data_confirmacao_cancelamento Timestamp(3) NULL,
CONSTRAINT fk_pedido_operacao_entrada
	FOREIGN KEY (id_entidade)
	REFERENCES entidade (id_entidade),

 CONSTRAINT UQ_pedido_operacao_entrada UNIQUE 
(
	cod_pedido_polo,
	cod_operacao_logistica_polo 
));

ALTER TABLE pedido_operacao_entrada ADD (
  CONSTRAINT pk_pedido_operacao_entrada PRIMARY KEY (id_pedido_operacao_entrada));
CREATE SEQUENCE pedido_operacao_entrada_seq;


CREATE OR REPLACE TRIGGER pedido_operacao_entrada_bir 
BEFORE INSERT ON pedido_operacao_entrada 
FOR EACH ROW
WHEN (new.id_pedido_operacao_entrada IS NULL)
BEGIN
  SELECT pedido_operacao_entrada_seq.NEXTVAL
  INTO   :new.id_pedido_operacao_entrada
  FROM   dual;
END;


