

CREATE TABLE pedido_item_lote_oper_saida(
	id_pedido_item_lote_oper_saida Number(10) NOT NULL,
	id_pedido_item_operacao_saida Number(10) NOT NULL,
	lote_orig Varchar2(50) NOT NULL,
	lote Varchar2(50) NOT NULL,
	id_classificacao_origem_polo Number(10) NOT NULL,
	id_classificacao_destino_polo Number(10) NOT NULL,
	quantidade Number NOT NULL,
	validade_orig Timestamp(0) NULL,
	validade Timestamp(0) NULL,
	fabricacao Timestamp(0) NULL,
	cod_situacao Number(10) NULL,
	obs Varchar2(255) NULL,
	classificacao_origem Varchar2(50) NULL,
	classificacao_destino Varchar2(50) NULL,
 CONSTRAINT UQ_ped_item_lote_oper_saida UNIQUE 
(
	id_pedido_item_operacao_saida,
	lote_orig,
	lote,
	id_classificacao_origem_polo,
	id_classificacao_destino_polo 
) 
);



ALTER TABLE pedido_item_lote_oper_saida ADD (
  CONSTRAINT pk_ped_item_lote_oper_saida PRIMARY KEY (id_pedido_item_lote_oper_saida));
CREATE SEQUENCE ped_item_lote_oper_saida_seq;


CREATE OR REPLACE TRIGGER ped_item_lote_oper_saida_bir 
BEFORE INSERT ON pedido_item_lote_oper_saida 
FOR EACH ROW
WHEN (new.id_pedido_item_lote_oper_saida IS NULL)
BEGIN
  SELECT ped_item_lote_oper_saida_seq.NEXTVAL
  INTO   :new.id_pedido_item_lote_oper_saida
  FROM   dual;
END;


ALTER TABLE pedido_item_lote_oper_saida
ADD CONSTRAINT ck_ped_item_lot_oper_sai_qtd
CHECK (quantidade>=(0));


ALTER TABLE pedido_item_lote_oper_saida
ADD CONSTRAINT FK_pedido_item_operacao_said FOREIGN KEY (id_pedido_item_operacao_saida) 
references pedido_item_operacao_saida (id_pedido_item_operacao_saida);