

CREATE TABLE pedido_item_operacao_saida(
	id_pedido_item_operacao_saida Number(10)  NOT NULL,
	id_pedido_operacao_saida Number(10) NOT NULL,
	id_produto Number(10) NOT NULL,
	quantidade Number NOT NULL,
	separado Number NULL,
	cod_motivo Number(10) NULL,
	motivo Varchar2(30) NULL,
	numero_item Number(10) NULL,
	obs Varchar2(255) NULL,
	ordem_conf Number(10) NULL,
 CONSTRAINT UQ_pedido_item_operacao_saida UNIQUE 
(
	id_pedido_operacao_saida,
	id_produto 
) 
);




ALTER TABLE pedido_item_operacao_saida ADD (
  CONSTRAINT pk_pedido_item_operacao_saida PRIMARY KEY (id_pedido_item_operacao_saida));
CREATE SEQUENCE pedido_item_oper_saida_seq;


CREATE OR REPLACE TRIGGER pedido_item_operacao_saida_bir 
BEFORE INSERT ON pedido_item_operacao_saida 
FOR EACH ROW
WHEN (new.id_pedido_item_operacao_saida IS NULL)
BEGIN
  SELECT pedido_item_operacao_saida_seq.NEXTVAL
  INTO   :new.id_pedido_item_operacao_saida
  FROM   dual;
END;


ALTER TABLE pedido_item_operacao_saida
ADD CONSTRAINT ck_pedido_item_operacao_saida
CHECK (separado>=(0));



ALTER TABLE pedido_item_operacao_saida
ADD CONSTRAINT FK_ped_item_oper_saida_prod FOREIGN KEY (id_produto) references produto (id_produto);