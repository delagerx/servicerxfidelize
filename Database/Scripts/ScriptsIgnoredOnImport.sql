﻿
IF OBJECT_ID('entidade') is null
BEGIN


--The following statement was imported into the database project as a schema object and named dbo.entidade.
--CREATE TABLE entidade(
--	id_entidade INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_entidade primary key,
--	cod_entidade_polo VARCHAR(30) NOT NULL, 
--	cpf_cnpj VARCHAR(30), 
--	razao_social VARCHAR(255) NOT NULL,
--	cod_tipo_pessoa INT,
--	fantasia VARCHAR(50) NULL,
--	tipo VARCHAR(50) NOT NULL,
--	subtipo VARCHAR(50),
--   bandeira VARCHAR(50) NOT NULL,
--   cod_situacao int,
--   obs VARCHAR(500),
--   endereco VARCHAR(100),
--   cod_tipo_endereco smallint,
--   bairro VARCHAR(60),
--   cep VARCHAR(9),
--   cod_rota varchar(7),
--   descr_rota VARCHAR(50),
--   complemento VARCHAR(100),
--   cidade VARCHAR(50),
--   sigla_estado CHAR(10),
--   inscricao_estadual VARCHAR(30),
--   telefone VARCHAR(20),
--   
-- CONSTRAINT UQ_entidade UNIQUE (cod_entidade_polo, tipo)
--
-- 
--
--)





END
GO

IF OBJECT_ID('produto') IS NULL
BEGIN
   
   --The following statement was imported into the database project as a schema object and named dbo.produto.
--CREATE TABLE produto (
--     id_produto	INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_produto PRIMARY KEY,
--     cod_produto_polo	VARCHAR(30) CONSTRAINT UQ_produto UNIQUE,
--     cod_forn_polo	VARCHAR(20),
--     descricao	VARCHAR(80) NOT NULL,
--     generico	varchar(80),
--     grupo	VARCHAR(50),
--     subgrupo	VARCHAR(50),
--     cod_situacao	int,
--     emb_compra	money,
--     emb_saida	money,
--     validade	INT,
--     linha_produto	varchar(50),
--     controle_lote	bit,
--     unidade	VARCHAR(5),
--     casas_decimais	INT,
--     altura	MONEY,
--     largura	MONEY,
--     comprimento	MONEY,
--     peso	money,
--     preco_venda	money
--
--   
--   )


   

END
GO

IF OBJECT_ID('produto_cod_barra') IS NULL
BEGIN


  --The following statement was imported into the database project as a schema object and named dbo.produto_cod_barra.
--CREATE TABLE produto_cod_barra (
--     id_produto_cod_barra INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_produto_cod_barra PRIMARY KEY, 
--     cod_barra varchar(20) NOT NULL,
--     id_produto int NOT NULL,
--     quantidade	money NOT NULL,
--     cod_tipo	int NOT NULL,
--     tipo	varchar(30),
--     altura	money,
--     largura	money,
--     comprimento	money,
--     peso	money,
--     principal	int,
--     CONSTRAINT UQ_produto_Cod_barra UNIQUE (cod_barra, id_produto)
--
--  )

END
GO

IF OBJECT_ID('produto_curva') IS NULL
BEGIN
  --The following statement was imported into the database project as a schema object and named dbo.produto_curva.
--CREATE TABLE produto_curva (
--  id_produto_curva INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_produto_curva PRIMARY KEY,
--  id_produto int NOT NULL,
--  cod_operacao_logistica_polo	varchar(20) NOT NULL,
--  curva	varchar(10) NOT NULL, 
--
--  CONSTRAINT UQ_produto_curva UNIQUE (id_produto, cod_operacao_logistica_polo,curva)
-- 
-- )



END
GO

IF OBJECT_ID('produto_shelf_life') is null
BEGIN
  
   --The following statement was imported into the database project as a schema object and named dbo.produto_shelf_life.
--CREATE TABLE produto_shelf_life (
--   id_produto_shelf_life INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_produto_shelf_life PRIMARY KEY,
--   id_produto	int NOT NULL,
--   grupo_shelf_life	varchar(30) NOT NULL,
--   validade_minima	smalldatetime,
--   validade_maxima	smalldatetime,
--    CONSTRAINT UQ_produto_shelf_life UNIQUE (id_produto, grupo_shelf_life)
-- 
-- )

  
END
GO

IF OBJECT_ID('pedido_operacao_entrada') IS NULL
BEGIN

  --The following statement was imported into the database project as a schema object and named dbo.pedido_operacao_entrada.
--CREATE TABLE pedido_operacao_entrada (
--  
--    id_pedido_operacao_entrada	int not null identity (1,1) CONSTRAINT PK_pedido_operacao_entrada PRIMARY KEY,
--    cod_pedido_polo VARCHAR(30) NOT NULL, 
--    cod_operacao_logistica_polo	varchar(20),
--    cod_prenota	int,
--    cod_entidade_polo VARCHAR(30) NOT NULL,
--    Cod_tipo_entidade VARCHAR(50) NOT NULL,
--    operacao	int NOT NULL,
--    cod_situacao	INT,
--    situacao	VARCHAR(50),
--    digitacao	SMALLDATETIME,
--    data_liberacao	SMALLDATETIME,
--    prenota	SMALLDATETIME,
--    encerramento_PN	SMALLDATETIME,
--    quem_digitou	varchar(30),
--    quem_liberou	varchar(30),
--    cod_origem_ped	int,
--    origem_ped	varchar(50),
--    volumes	INT,
--    valor_total	MONEY,
--    obs_PN	VARCHAR(500),
--    suboperacao	VARCHAR(50),
--    motivo_suboperacao	VARCHAR(50),
--    cod_entrega	int,
--    numero_titulo	varchar(13),
--    serie_nf	VARCHAR(3),
--    cod_titulo_polo	varchar(30),
--    peso	real,
--    qtdItens	int,
--    obs	varchar(255),
--    id_cancelamento	int,
--    data_solicitacao_cancelamento	DATETIME,
--    data_confirmacao_cancelamento	DATETIME,
--    CONSTRAINT UQ_pedido_operacao_entrada UNIQUE (cod_pedido_polo, cod_operacao_logistica_polo)
-- 
-- )


  
 

END
GO

IF OBJECT_ID('pedido_operacao_saida') IS NULL
BEGIN

  --The following statement was imported into the database project as a schema object and named dbo.pedido_operacao_saida.
--CREATE TABLE pedido_operacao_saida (
--  
--    id_pedido_operacao_saida	int not null identity (1,1) CONSTRAINT PK_pedido_operacao_saida PRIMARY KEY,
--    cod_pedido_polo VARCHAR(30) NOT NULL, 
--    cod_operacao_logistica_polo	varchar(20),
--    cod_prenota	int,
--    cod_entidade_polo VARCHAR(30) NOT NULL ,
--    Cod_tipo_entidade VARCHAR(50) NOT NULL ,
--    operacao	int NOT NULL,
--    cod_situacao	INT,
--    situacao	VARCHAR(50),
--    digitacao	SMALLDATETIME,
--    data_liberacao	SMALLDATETIME,
--    prenota	SMALLDATETIME,
--    encerramento_PN	SMALLDATETIME,
--    quem_digitou	varchar(30),
--    quem_liberou	varchar(30),
--    cod_origem_ped	int,
--    origem_ped	varchar(50),
--    volumes	INT,
--    valor_total	MONEY,
--    obs_PN	VARCHAR(500),
--    suboperacao	VARCHAR(50),
--    motivo_suboperacao	VARCHAR(50),
--    cod_entrega	int,
--    numero_titulo	varchar(13),
--    serie_nf	VARCHAR(3),
--    cod_titulo_polo	varchar(30),
--    peso	real,
--    qtdItens	int,
--    obs	varchar(255),
--    id_cancelamento	int,
--    data_solicitacao_cancelamento	DATETIME,
--        data_confirmacao_cancelamento	DATETIME,
--    CONSTRAINT UQ_pedido_operacao_saida UNIQUE (cod_pedido_polo, cod_operacao_logistica_polo)
-- 
-- )


  
 

END
GO

IF OBJECT_ID('Pedido_operacao_saida_endereco') is null
BEGIN

  --The following statement was imported into the database project as a schema object and named dbo.Pedido_operacao_saida_endereco.
--CREATE TABLE Pedido_operacao_saida_endereco (
--  
--   Id_pedido_oper_saida_endereco	INT NOT NULL CONSTRAINT PK_Pedido_operacao_saida_endereco PRIMARY KEY,
--   cod_pedido_polo VARCHAR(30) NOT NULL, 
--   cod_operacao_logistica_polo	varchar(20),
--   cod_tipo_endereco	smallint,
--   bairro	varchar(60),
--   cep	varchar(9)	,
--   cod_rota	varchar(7)	,
--   descr_rota	varchar(50)	,
--   complemento	varchar(100),
--   cidade	varchar(50),
--   sigla_estado	char(10)	,
--   CONSTRAINT UQ_Pedido_operacao_saida_endereco unique (cod_pedido_polo, cod_operacao_logistica_polo)
--
--  
--  )



END
GO

IF OBJECT_ID('pedido_operacao_movimentacao') IS NULL
BEGIN

  --The following statement was imported into the database project as a schema object and named dbo.pedido_operacao_movimentacao.
--CREATE TABLE pedido_operacao_movimentacao (
--  
--    id_pedido_operacao_mov	int not null identity (1,1) CONSTRAINT PK_pedido_operacao_movimentacao PRIMARY KEY,
--    cod_pedido_polo VARCHAR(30) NOT NULL, 
--    cod_operacao_logistica_polo	varchar(20),
--    cod_prenota	int,
--    cod_entidade_polo VARCHAR(30) NOT NULL ,
--    Cod_tipo_entidade VARCHAR(50) NOT NULL ,
--    operacao	int NOT NULL,
--    cod_situacao	INT,
--    situacao	VARCHAR(50),
--    digitacao	SMALLDATETIME,
--    data_liberacao	SMALLDATETIME,
--    prenota	SMALLDATETIME,
--    encerramento_PN	SMALLDATETIME,
--    quem_digitou	varchar(30),
--    quem_liberou	varchar(30),
--    cod_origem_ped	int,
--    origem_ped	varchar(50),
--    volumes	INT,
--    valor_total	MONEY,
--    obs_PN	VARCHAR(500),
--    suboperacao	VARCHAR(50),
--    motivo_suboperacao	VARCHAR(50),
--    cod_entrega	int,
--    numero_titulo	varchar(13),
--    serie_nf	VARCHAR(3),
--    cod_titulo_polo	varchar(30),
--    peso	real,
--    qtdItens	int,
--    obs	varchar(255),
--    id_cancelamento	int,
--    data_solicitacao_cancelamento	DATETIME,
--        data_confirmacao_cancelamento	DATETIME,
--    CONSTRAINT UQ_pedido_operacao_movimentacao UNIQUE (cod_pedido_polo, cod_operacao_logistica_polo)
-- 
-- )


  
 

END
GO

IF OBJECT_ID('pedido_item_operacao_entrada') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_operacao_entrada.
--CREATE TABLE pedido_item_operacao_entrada (
--      id_pedido_item_oper_entrada INT not null identity (1,1) CONSTRAINT PK_pedido_item_operacao_entrada PRIMARY KEY,
--      id_pedido_operacao_entrada	INT NOT NULL,
--      id_produto	INT NOT NULL,
--      quantidade	MONEY,
--      separado	MONEY,
--      cod_motivo	INT,
--      motivo	VARCHAR(30),
--      numero_item	INT,
--      obs	VARCHAR(255),
--      ordem_conf	INT,
--
--      CONSTRAINT UQ_pedido_item_operacao_entrada UNIQUE (id_pedido_operacao_entrada,id_produto)
--   )

end
GO

IF OBJECT_ID('pedido_item_operacao_saida') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_operacao_saida.
--CREATE TABLE pedido_item_operacao_saida (
--      id_pedido_item_operacao_saida INT not null identity (1,1) CONSTRAINT PK_pedido_item_operacao_saida PRIMARY KEY,
--      id_pedido_operacao_saida	INT NOT NULL,
--      id_produto	INT NOT NULL,
--      quantidade	MONEY,
--      separado	MONEY,
--      cod_motivo	INT,
--      motivo	VARCHAR(30),
--      numero_item	INT,
--      obs	VARCHAR(255),
--      ordem_conf	INT,
--
--      CONSTRAINT UQ_pedido_item_operacao_saida UNIQUE (id_pedido_operacao_saida,id_produto)
--   )

end
GO

IF OBJECT_ID('pedido_item_oper_mov') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_oper_mov.
--CREATE TABLE pedido_item_oper_mov (
--      id_pedido_item_oper_mov INT not null identity (1,1) CONSTRAINT PK_pedido_item_operacao_movimentacao PRIMARY KEY,
--      id_pedido_operacao_mov INT NOT NULL,
--      id_produto	INT NOT NULL,
--      quantidade	MONEY,
--      separado	MONEY,
--      cod_motivo	INT,
--      motivo	VARCHAR(30),
--      numero_item	INT,
--      obs	VARCHAR(255),
--      ordem_conf	INT,
--
--      CONSTRAINT UQ_pedido_item_operacao_movimentacao UNIQUE (id_pedido_operacao_mov,id_produto)
--   )

end
GO

if OBJECT_ID('pedido_item_lote_oper_entrada') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_lote_oper_entrada.
--CREATE TABLE pedido_item_lote_oper_entrada (
--      id_ped_item_lote_oper_entrada INT NOT NULL CONSTRAINT PK_ped_item_lote_oper_entrada PRIMARY KEY,
--      id_pedido_item_oper_entrada INT NOT NULL CONSTRAINT FK_pedido_item_operacao_entrada_pedido_item_operacao_entrada FOREIGN KEY REFERENCES pedido_item_operacao_entrada (id_pedido_item_oper_entrada),
--      lote_orig	VARCHAR(50) NOT NULL,
--      lote	varchar(50) NOT NULL,
--      id_classificacao_origem_polo	INT NOT NULL,
--      id_classificacao_dest_polo	INT NOT NULL,
--      quantidade	MONEY NOT NULL,
--      validade_orig	SMALLDATETIME,
--      validade	SMALLDATETIME,
--      fabricacao	SMALLDATETIME,
--      cod_situacao	INT,
--      obs	VARCHAR(255),
--      classificacao_origem	VARCHAR(50),
--      classificacao_destino	VARCHAR(50),
--
--      CONSTRAINT UQ_ped_item_lote_oper_entrada UNIQUE (id_pedido_item_oper_entrada, lote_orig, lote,id_classificacao_origem_polo,id_classificacao_dest_polo )
--   
--   )


END
GO

if OBJECT_ID('pedido_item_lote_oper_saida') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_lote_oper_saida.
--CREATE TABLE pedido_item_lote_oper_saida (
--      id_pedido_item_lote_oper_saida INT NOT NULL CONSTRAINT PK_pedido_item_lote_operacao_saida PRIMARY KEY,
--      id_pedido_item_operacao_saida INT NOT NULL CONSTRAINT FK_pedido_item_lote_operacao_saida_pedido_item_operacao_saida FOREIGN KEY REFERENCES pedido_item_operacao_saida (id_pedido_item_operacao_saida),
--      lote_orig	VARCHAR(50) NOT NULL,
--      lote	varchar(50) NOT NULL,
--      id_classificacao_origem_polo	INT NOT NULL,
--      id_classificacao_destino_polo	INT NOT NULL,
--      quantidade	MONEY NOT NULL,
--      validade_orig	SMALLDATETIME,
--      validade	SMALLDATETIME,
--      fabricacao	SMALLDATETIME,
--      cod_situacao	INT,
--      obs	VARCHAR(255),
--      classificacao_origem	VARCHAR(50),
--      classificacao_destino	VARCHAR(50),
--
--      CONSTRAINT UQ_pedido_item_lote_oper_saida UNIQUE (id_pedido_item_operacao_saida, lote_orig, lote,id_classificacao_origem_polo,id_classificacao_destino_polo )
--   
--   )


END
GO

if OBJECT_ID('pedido_item_lote_oper_mov') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_lote_oper_mov.
--CREATE TABLE pedido_item_lote_oper_mov (
--      id_pedido_item_lote_oper_movo INT NOT NULL CONSTRAINT PK_pedido_item_lote_operacao_movimentacao PRIMARY KEY,
--      id_pedido_item_oper_mov INT NOT NULL CONSTRAINT FK_pedido_item_lote_operacao_movimentacao_pedido_item_operacao_movimentacao FOREIGN KEY REFERENCES pedido_item_oper_mov (id_pedido_item_oper_mov),
--      lote_orig	VARCHAR(50) NOT NULL,
--      lote	varchar(50) NOT NULL,
--      id_classificacao_origem_polo	INT NOT NULL,
--      id_classificacao_destino_polo	INT NOT NULL,
--      quantidade	MONEY NOT NULL,
--      validade_orig	SMALLDATETIME,
--      validade	SMALLDATETIME,
--      fabricacao	SMALLDATETIME,
--      cod_situacao	INT,
--      obs	VARCHAR(255),
--      classificacao_origem	VARCHAR(50),
--      classificacao_destino	VARCHAR(50),
--
--      CONSTRAINT UQ_pedido_item_lote_operacao_movimentacao UNIQUE (id_pedido_item_oper_mov, lote_orig, lote,id_classificacao_origem_polo,id_classificacao_destino_polo )
--   
--   )


END
GO

IF OBJECT_ID('pedido_volume_operacao_saida') IS NULL
BEGIN

   --The following statement was imported into the database project as a schema object and named dbo.pedido_volume_operacao_saida.
--CREATE TABLE pedido_volume_operacao_saida(
--     id_pedido_volume_oper_saida INT NOT NULL CONSTRAINT PK_pedido_volume_operacao_saida PRIMARY KEY,
--     id_pedido_operacao_saida	int NOT NULL,
--     volume	int,
--     lacre	varchar(50) NOT NULL,
--     data_volume_embarcado	smalldatetime,
--     tipo_caixa	smallint,
--     CONSTRAINT UQ_pedido_volume_operacao_saida UNIQUE (id_pedido_operacao_saida, volume)
--     
--)
 


--The following statement was imported into the database project as a schema object and named dbo.FK_pedido_volume_operacao_saida_pedido_operacao_Saida.
--ALTER TABLE pedido_volume_operacao_saida  WITH CHECK ADD 
-- CONSTRAINT FK_pedido_volume_operacao_saida_pedido_operacao_Saida 
-- FOREIGN KEY(id_pedido_operacao_saida)
--REFERENCES pedido_operacao_saida (id_pedido_operacao_saida)
--ON UPDATE CASCADE
--ON DELETE CASCADE

   

END
GO

IF OBJECT_ID('pedido_volume_operacao_mov') IS NULL
BEGIN

   --The following statement was imported into the database project as a schema object and named dbo.pedido_volume_operacao_mov.
--CREATE TABLE pedido_volume_operacao_mov(
--     id_pedido_volume_oper_mov INT NOT NULL CONSTRAINT PK_pedido_volume_operacao_movimentacao PRIMARY KEY,
--     id_pedido_operacao_mov int NOT NULL,
--     volume	int,
--     lacre	varchar(50) NOT NULL,
--     data_volume_embarcado	smalldatetime,
--     tipo_caixa	smallint,
--     CONSTRAINT UQ_pedido_volume_operacao_movimentacao UNIQUE (id_pedido_operacao_mov, volume)
--     
--)
 


--The following statement was imported into the database project as a schema object and named dbo.FK_pedido_volume_operacao_movimentacao_pedido_operacao_movimentacao.
--ALTER TABLE pedido_volume_operacao_mov  WITH CHECK ADD  CONSTRAINT 
--FK_pedido_volume_operacao_movimentacao_pedido_operacao_movimentacao FOREIGN KEY(id_pedido_operacao_mov)
--REFERENCES pedido_operacao_movimentacao (id_pedido_operacao_mov)
--ON UPDATE CASCADE
--ON DELETE CASCADE

   

END
GO

IF OBJECT_ID('entidade') is null
BEGIN


--The following statement was imported into the database project as a schema object and named dbo.entidade.
--CREATE TABLE entidade(
--	id_entidade INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_entidade primary key,
--	cod_entidade_polo VARCHAR(30) NOT NULL, 
--	cpf_cnpj VARCHAR(30), 
--	razao_social VARCHAR(255) NOT NULL,
--	cod_tipo_pessoa INT,
--	fantasia VARCHAR(50) NULL,
--	tipo VARCHAR(50) NOT NULL,
--	subtipo VARCHAR(50),
--   bandeira VARCHAR(50) NOT NULL,
--   cod_situacao int,
--   obs VARCHAR(500),
--   endereco VARCHAR(100),
--   cod_tipo_endereco smallint,
--   bairro VARCHAR(60),
--   cep VARCHAR(9),
--   cod_rota varchar(7),
--   descr_rota VARCHAR(50),
--   complemento VARCHAR(100),
--   cidade VARCHAR(50),
--   sigla_estado CHAR(10),
--   inscricao_estadual VARCHAR(30),
--   telefone VARCHAR(20),
--   
-- CONSTRAINT UQ_entidade UNIQUE (cod_entidade_polo, tipo)
--
-- 
--
--)





END
GO

IF OBJECT_ID('produto') IS NULL
BEGIN
   
   --The following statement was imported into the database project as a schema object and named dbo.produto.
--CREATE TABLE produto (
--     id_produto	INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_produto PRIMARY KEY,
--     cod_produto_polo	VARCHAR(30) CONSTRAINT UQ_produto UNIQUE,
--     cod_forn_polo	VARCHAR(20),
--     descricao	VARCHAR(80) NOT NULL,
--     generico	varchar(80),
--     grupo	VARCHAR(50),
--     subgrupo	VARCHAR(50),
--     cod_situacao	int,
--     emb_compra	money,
--     emb_saida	money,
--     validade	INT,
--     linha_produto	varchar(50),
--     controle_lote	bit,
--     unidade	VARCHAR(5),
--     casas_decimais	INT,
--     altura	MONEY,
--     largura	MONEY,
--     comprimento	MONEY,
--     peso	money,
--     preco_venda	money
--
--   
--   )


   

END
GO

IF OBJECT_ID('produto_cod_barra') IS NULL
BEGIN


  --The following statement was imported into the database project as a schema object and named dbo.produto_cod_barra.
--CREATE TABLE produto_cod_barra (
--     id_produto_cod_barra INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_produto_cod_barra PRIMARY KEY, 
--     cod_barra varchar(20) NOT NULL,
--     id_produto int NOT NULL,
--     quantidade	money NOT NULL,
--     cod_tipo	int NOT NULL,
--     tipo	varchar(30),
--     altura	money,
--     largura	money,
--     comprimento	money,
--     peso	money,
--     principal	int,
--     CONSTRAINT UQ_produto_Cod_barra UNIQUE (cod_barra, id_produto)
--
--  )

END
GO

IF OBJECT_ID('produto_curva') IS NULL
BEGIN
  --The following statement was imported into the database project as a schema object and named dbo.produto_curva.
--CREATE TABLE produto_curva (
--  id_produto_curva INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_produto_curva PRIMARY KEY,
--  id_produto int NOT NULL,
--  cod_operacao_logistica_polo	varchar(20) NOT NULL,
--  curva	varchar(10) NOT NULL, 
--
--  CONSTRAINT UQ_produto_curva UNIQUE (id_produto, cod_operacao_logistica_polo,curva)
-- 
-- )



END
GO

IF OBJECT_ID('produto_shelf_life') is null
BEGIN
  
   --The following statement was imported into the database project as a schema object and named dbo.produto_shelf_life.
--CREATE TABLE produto_shelf_life (
--   id_produto_shelf_life INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_produto_shelf_life PRIMARY KEY,
--   id_produto	int NOT NULL,
--   grupo_shelf_life	varchar(30) NOT NULL,
--   validade_minima	smalldatetime,
--   validade_maxima	smalldatetime,
--    CONSTRAINT UQ_produto_shelf_life UNIQUE (id_produto, grupo_shelf_life)
-- 
-- )

  
END
GO

IF OBJECT_ID('pedido_operacao_entrada') IS NULL
BEGIN

  --The following statement was imported into the database project as a schema object and named dbo.pedido_operacao_entrada.
--CREATE TABLE pedido_operacao_entrada (
--  
--    id_pedido_operacao_entrada	int not null identity (1,1) CONSTRAINT PK_pedido_operacao_entrada PRIMARY KEY,
--    cod_pedido_polo VARCHAR(30) NOT NULL, 
--    cod_operacao_logistica_polo	varchar(20),
--    cod_prenota	int,
--    cod_entidade_polo VARCHAR(30) NOT NULL,
--    Cod_tipo_entidade VARCHAR(50) NOT NULL,
--    operacao	int NOT NULL,
--    cod_situacao	INT,
--    situacao	VARCHAR(50),
--    digitacao	SMALLDATETIME,
--    data_liberacao	SMALLDATETIME,
--    prenota	SMALLDATETIME,
--    encerramento_PN	SMALLDATETIME,
--    quem_digitou	varchar(30),
--    quem_liberou	varchar(30),
--    cod_origem_ped	int,
--    origem_ped	varchar(50),
--    volumes	INT,
--    valor_total	MONEY,
--    obs_PN	VARCHAR(500),
--    suboperacao	VARCHAR(50),
--    motivo_suboperacao	VARCHAR(50),
--    cod_entrega	int,
--    numero_titulo	varchar(13),
--    serie_nf	VARCHAR(3),
--    cod_titulo_polo	varchar(30),
--    peso	real,
--    qtdItens	int,
--    obs	varchar(255),
--    id_cancelamento	int,
--    data_solicitacao_cancelamento	DATETIME,
--    data_confirmacao_cancelamento	DATETIME,
--    CONSTRAINT UQ_pedido_operacao_entrada UNIQUE (cod_pedido_polo, cod_operacao_logistica_polo)
-- 
-- )


  
 

END
GO

IF OBJECT_ID('pedido_operacao_saida') IS NULL
BEGIN

  --The following statement was imported into the database project as a schema object and named dbo.pedido_operacao_saida.
--CREATE TABLE pedido_operacao_saida (
--  
--    id_pedido_operacao_saida	int not null identity (1,1) CONSTRAINT PK_pedido_operacao_saida PRIMARY KEY,
--    cod_pedido_polo VARCHAR(30) NOT NULL, 
--    cod_operacao_logistica_polo	varchar(20),
--    cod_prenota	int,
--    cod_entidade_polo VARCHAR(30) NOT NULL ,
--    Cod_tipo_entidade VARCHAR(50) NOT NULL ,
--    operacao	int NOT NULL,
--    cod_situacao	INT,
--    situacao	VARCHAR(50),
--    digitacao	SMALLDATETIME,
--    data_liberacao	SMALLDATETIME,
--    prenota	SMALLDATETIME,
--    encerramento_PN	SMALLDATETIME,
--    quem_digitou	varchar(30),
--    quem_liberou	varchar(30),
--    cod_origem_ped	int,
--    origem_ped	varchar(50),
--    volumes	INT,
--    valor_total	MONEY,
--    obs_PN	VARCHAR(500),
--    suboperacao	VARCHAR(50),
--    motivo_suboperacao	VARCHAR(50),
--    cod_entrega	int,
--    numero_titulo	varchar(13),
--    serie_nf	VARCHAR(3),
--    cod_titulo_polo	varchar(30),
--    peso	real,
--    qtdItens	int,
--    obs	varchar(255),
--    id_cancelamento	int,
--    data_solicitacao_cancelamento	DATETIME,
--        data_confirmacao_cancelamento	DATETIME,
--    CONSTRAINT UQ_pedido_operacao_saida UNIQUE (cod_pedido_polo, cod_operacao_logistica_polo)
-- 
-- )


  
 

END
GO

IF OBJECT_ID('Pedido_operacao_saida_endereco') is null
BEGIN

  --The following statement was imported into the database project as a schema object and named dbo.Pedido_operacao_saida_endereco.
--CREATE TABLE Pedido_operacao_saida_endereco (
--  
--   Id_pedido_oper_saida_endereco	INT NOT NULL CONSTRAINT PK_Pedido_operacao_saida_endereco PRIMARY KEY,
--   cod_pedido_polo VARCHAR(30) NOT NULL, 
--   cod_operacao_logistica_polo	varchar(20),
--   cod_tipo_endereco	smallint,
--   bairro	varchar(60),
--   cep	varchar(9)	,
--   cod_rota	varchar(7)	,
--   descr_rota	varchar(50)	,
--   complemento	varchar(100),
--   cidade	varchar(50),
--   sigla_estado	char(10)	,
--   CONSTRAINT UQ_Pedido_operacao_saida_endereco unique (cod_pedido_polo, cod_operacao_logistica_polo)
--
--  
--  )



END
GO

IF OBJECT_ID('pedido_operacao_movimentacao') IS NULL
BEGIN

  --The following statement was imported into the database project as a schema object and named dbo.pedido_operacao_movimentacao.
--CREATE TABLE pedido_operacao_movimentacao (
--  
--    id_pedido_operacao_mov	int not null identity (1,1) CONSTRAINT PK_pedido_operacao_movimentacao PRIMARY KEY,
--    cod_pedido_polo VARCHAR(30) NOT NULL, 
--    cod_operacao_logistica_polo	varchar(20),
--    cod_prenota	int,
--    cod_entidade_polo VARCHAR(30) NOT NULL ,
--    Cod_tipo_entidade VARCHAR(50) NOT NULL ,
--    operacao	int NOT NULL,
--    cod_situacao	INT,
--    situacao	VARCHAR(50),
--    digitacao	SMALLDATETIME,
--    data_liberacao	SMALLDATETIME,
--    prenota	SMALLDATETIME,
--    encerramento_PN	SMALLDATETIME,
--    quem_digitou	varchar(30),
--    quem_liberou	varchar(30),
--    cod_origem_ped	int,
--    origem_ped	varchar(50),
--    volumes	INT,
--    valor_total	MONEY,
--    obs_PN	VARCHAR(500),
--    suboperacao	VARCHAR(50),
--    motivo_suboperacao	VARCHAR(50),
--    cod_entrega	int,
--    numero_titulo	varchar(13),
--    serie_nf	VARCHAR(3),
--    cod_titulo_polo	varchar(30),
--    peso	real,
--    qtdItens	int,
--    obs	varchar(255),
--    id_cancelamento	int,
--    data_solicitacao_cancelamento	DATETIME,
--        data_confirmacao_cancelamento	DATETIME,
--    CONSTRAINT UQ_pedido_operacao_movimentacao UNIQUE (cod_pedido_polo, cod_operacao_logistica_polo)
-- 
-- )


  
 

END
GO

IF OBJECT_ID('pedido_item_operacao_entrada') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_operacao_entrada.
--CREATE TABLE pedido_item_operacao_entrada (
--      id_pedido_item_oper_entrada INT not null identity (1,1) CONSTRAINT PK_pedido_item_operacao_entrada PRIMARY KEY,
--      id_pedido_operacao_entrada	INT NOT NULL,
--      id_produto	INT NOT NULL,
--      quantidade	MONEY,
--      separado	MONEY,
--      cod_motivo	INT,
--      motivo	VARCHAR(30),
--      numero_item	INT,
--      obs	VARCHAR(255),
--      ordem_conf	INT,
--
--      CONSTRAINT UQ_pedido_item_operacao_entrada UNIQUE (id_pedido_operacao_entrada,id_produto)
--   )

end
GO

IF OBJECT_ID('pedido_item_operacao_saida') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_operacao_saida.
--CREATE TABLE pedido_item_operacao_saida (
--      id_pedido_item_operacao_saida INT not null identity (1,1) CONSTRAINT PK_pedido_item_operacao_saida PRIMARY KEY,
--      id_pedido_operacao_saida	INT NOT NULL,
--      id_produto	INT NOT NULL,
--      quantidade	MONEY,
--      separado	MONEY,
--      cod_motivo	INT,
--      motivo	VARCHAR(30),
--      numero_item	INT,
--      obs	VARCHAR(255),
--      ordem_conf	INT,
--
--      CONSTRAINT UQ_pedido_item_operacao_saida UNIQUE (id_pedido_operacao_saida,id_produto)
--   )

end
GO

IF OBJECT_ID('pedido_item_oper_mov') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_oper_mov.
--CREATE TABLE pedido_item_oper_mov (
--      id_pedido_item_oper_mov INT not null identity (1,1) CONSTRAINT PK_pedido_item_operacao_movimentacao PRIMARY KEY,
--      id_pedido_operacao_mov INT NOT NULL,
--      id_produto	INT NOT NULL,
--      quantidade	MONEY,
--      separado	MONEY,
--      cod_motivo	INT,
--      motivo	VARCHAR(30),
--      numero_item	INT,
--      obs	VARCHAR(255),
--      ordem_conf	INT,
--
--      CONSTRAINT UQ_pedido_item_operacao_movimentacao UNIQUE (id_pedido_operacao_mov,id_produto)
--   )

end
GO

if OBJECT_ID('pedido_item_lote_oper_entrada') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_lote_oper_entrada.
--CREATE TABLE pedido_item_lote_oper_entrada (
--      id_ped_item_lote_oper_entrada INT NOT NULL CONSTRAINT PK_ped_item_lote_oper_entrada PRIMARY KEY,
--      id_pedido_item_oper_entrada INT NOT NULL CONSTRAINT FK_pedido_item_operacao_entrada_pedido_item_operacao_entrada FOREIGN KEY REFERENCES pedido_item_operacao_entrada (id_pedido_item_oper_entrada),
--      lote_orig	VARCHAR(50) NOT NULL,
--      lote	varchar(50) NOT NULL,
--      id_classificacao_origem_polo	INT NOT NULL,
--      id_classificacao_dest_polo	INT NOT NULL,
--      quantidade	MONEY NOT NULL,
--      validade_orig	SMALLDATETIME,
--      validade	SMALLDATETIME,
--      fabricacao	SMALLDATETIME,
--      cod_situacao	INT,
--      obs	VARCHAR(255),
--      classificacao_origem	VARCHAR(50),
--      classificacao_destino	VARCHAR(50),
--
--      CONSTRAINT UQ_ped_item_lote_oper_entrada UNIQUE (id_pedido_item_oper_entrada, lote_orig, lote,id_classificacao_origem_polo,id_classificacao_dest_polo )
--   
--   )


END
GO

if OBJECT_ID('pedido_item_lote_oper_saida') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_lote_oper_saida.
--CREATE TABLE pedido_item_lote_oper_saida (
--      id_pedido_item_lote_oper_saida INT NOT NULL CONSTRAINT PK_pedido_item_lote_operacao_saida PRIMARY KEY,
--      id_pedido_item_operacao_saida INT NOT NULL CONSTRAINT FK_pedido_item_lote_operacao_saida_pedido_item_operacao_saida FOREIGN KEY REFERENCES pedido_item_operacao_saida (id_pedido_item_operacao_saida),
--      lote_orig	VARCHAR(50) NOT NULL,
--      lote	varchar(50) NOT NULL,
--      id_classificacao_origem_polo	INT NOT NULL,
--      id_classificacao_destino_polo	INT NOT NULL,
--      quantidade	MONEY NOT NULL,
--      validade_orig	SMALLDATETIME,
--      validade	SMALLDATETIME,
--      fabricacao	SMALLDATETIME,
--      cod_situacao	INT,
--      obs	VARCHAR(255),
--      classificacao_origem	VARCHAR(50),
--      classificacao_destino	VARCHAR(50),
--
--      CONSTRAINT UQ_pedido_item_lote_oper_saida UNIQUE (id_pedido_item_operacao_saida, lote_orig, lote,id_classificacao_origem_polo,id_classificacao_destino_polo )
--   
--   )


END
GO

if OBJECT_ID('pedido_item_lote_oper_mov') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_lote_oper_mov.
--CREATE TABLE pedido_item_lote_oper_mov (
--      id_pedido_item_lote_oper_movo INT NOT NULL CONSTRAINT PK_pedido_item_lote_operacao_movimentacao PRIMARY KEY,
--      id_pedido_item_oper_mov INT NOT NULL CONSTRAINT FK_pedido_item_lote_operacao_movimentacao_pedido_item_operacao_movimentacao FOREIGN KEY REFERENCES pedido_item_oper_mov (id_pedido_item_oper_mov),
--      lote_orig	VARCHAR(50) NOT NULL,
--      lote	varchar(50) NOT NULL,
--      id_classificacao_origem_polo	INT NOT NULL,
--      id_classificacao_destino_polo	INT NOT NULL,
--      quantidade	MONEY NOT NULL,
--      validade_orig	SMALLDATETIME,
--      validade	SMALLDATETIME,
--      fabricacao	SMALLDATETIME,
--      cod_situacao	INT,
--      obs	VARCHAR(255),
--      classificacao_origem	VARCHAR(50),
--      classificacao_destino	VARCHAR(50),
--
--      CONSTRAINT UQ_pedido_item_lote_operacao_movimentacao UNIQUE (id_pedido_item_oper_mov, lote_orig, lote,id_classificacao_origem_polo,id_classificacao_destino_polo )
--   
--   )


END
GO

IF OBJECT_ID('pedido_volume_operacao_saida') IS NULL
BEGIN

   --The following statement was imported into the database project as a schema object and named dbo.pedido_volume_operacao_saida.
--CREATE TABLE pedido_volume_operacao_saida(
--     id_pedido_volume_oper_saida INT NOT NULL CONSTRAINT PK_pedido_volume_operacao_saida PRIMARY KEY,
--     id_pedido_operacao_saida	int NOT NULL,
--     volume	int,
--     lacre	varchar(50) NOT NULL,
--     data_volume_embarcado	smalldatetime,
--     tipo_caixa	smallint,
--     CONSTRAINT UQ_pedido_volume_operacao_saida UNIQUE (id_pedido_operacao_saida, volume)
--     
--)
 


--The following statement was imported into the database project as a schema object and named dbo.FK_pedido_volume_operacao_saida_pedido_operacao_Saida.
--ALTER TABLE pedido_volume_operacao_saida  WITH CHECK ADD 
-- CONSTRAINT FK_pedido_volume_operacao_saida_pedido_operacao_Saida 
-- FOREIGN KEY(id_pedido_operacao_saida)
--REFERENCES pedido_operacao_saida (id_pedido_operacao_saida)
--ON UPDATE CASCADE
--ON DELETE CASCADE

   

END
GO

IF OBJECT_ID('pedido_volume_operacao_mov') IS NULL
BEGIN

   --The following statement was imported into the database project as a schema object and named dbo.pedido_volume_operacao_mov.
--CREATE TABLE pedido_volume_operacao_mov(
--     id_pedido_volume_oper_mov INT NOT NULL CONSTRAINT PK_pedido_volume_operacao_movimentacao PRIMARY KEY,
--     id_pedido_operacao_mov int NOT NULL,
--     volume	int,
--     lacre	varchar(50) NOT NULL,
--     data_volume_embarcado	smalldatetime,
--     tipo_caixa	smallint,
--     CONSTRAINT UQ_pedido_volume_operacao_movimentacao UNIQUE (id_pedido_operacao_mov, volume)
--     
--)
 


--The following statement was imported into the database project as a schema object and named dbo.FK_pedido_volume_operacao_movimentacao_pedido_operacao_movimentacao.
--ALTER TABLE pedido_volume_operacao_mov  WITH CHECK ADD  CONSTRAINT 
--FK_pedido_volume_operacao_movimentacao_pedido_operacao_movimentacao FOREIGN KEY(id_pedido_operacao_mov)
--REFERENCES pedido_operacao_movimentacao (id_pedido_operacao_mov)
--ON UPDATE CASCADE
--ON DELETE CASCADE

   

END
GO

IF OBJECT_ID('entidade') is null
BEGIN


--The following statement was imported into the database project as a schema object and named dbo.entidade.
--CREATE TABLE entidade(
--	id_entidade INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_entidade primary key,
--	cod_entidade_polo VARCHAR(30) NOT NULL, 
--	cpf_cnpj VARCHAR(30), 
--	razao_social VARCHAR(255) NOT NULL,
--	cod_tipo_pessoa INT,
--	fantasia VARCHAR(50) NULL,
--	tipo VARCHAR(50) NOT NULL,
--	subtipo VARCHAR(50),
--   bandeira VARCHAR(50) NOT NULL,
--   cod_situacao int,
--   obs VARCHAR(500),
--   endereco VARCHAR(100),
--   cod_tipo_endereco smallint,
--   bairro VARCHAR(60),
--   cep VARCHAR(9),
--   cod_rota varchar(7),
--   descr_rota VARCHAR(50),
--   complemento VARCHAR(100),
--   cidade VARCHAR(50),
--   sigla_estado CHAR(10),
--   inscricao_estadual VARCHAR(30),
--   telefone VARCHAR(20),
--   
-- CONSTRAINT UQ_entidade UNIQUE (cod_entidade_polo, tipo)
--
-- 
--
--)





END
GO

IF OBJECT_ID('produto') IS NULL
BEGIN
   
   --The following statement was imported into the database project as a schema object and named dbo.produto.
--CREATE TABLE produto (
--     id_produto	INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_produto PRIMARY KEY,
--     cod_produto_polo	VARCHAR(30) CONSTRAINT UQ_produto UNIQUE,
--     cod_forn_polo	VARCHAR(20),
--     descricao	VARCHAR(80) NOT NULL,
--     generico	varchar(80),
--     grupo	VARCHAR(50),
--     subgrupo	VARCHAR(50),
--     cod_situacao	int,
--     emb_compra	money,
--     emb_saida	money,
--     validade	INT,
--     linha_produto	varchar(50),
--     controle_lote	bit,
--     unidade	VARCHAR(5),
--     casas_decimais	INT,
--     altura	MONEY,
--     largura	MONEY,
--     comprimento	MONEY,
--     peso	money,
--     preco_venda	money
--
--   
--   )


   

END
GO

IF OBJECT_ID('produto_cod_barra') IS NULL
BEGIN


  --The following statement was imported into the database project as a schema object and named dbo.produto_cod_barra.
--CREATE TABLE produto_cod_barra (
--     id_produto_cod_barra INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_produto_cod_barra PRIMARY KEY, 
--     cod_barra varchar(20) NOT NULL,
--     id_produto int NOT NULL,
--     quantidade	money NOT NULL,
--     cod_tipo	int NOT NULL,
--     tipo	varchar(30),
--     altura	money,
--     largura	money,
--     comprimento	money,
--     peso	money,
--     principal	int,
--     CONSTRAINT UQ_produto_Cod_barra UNIQUE (cod_barra, id_produto)
--
--  )

END
GO

IF OBJECT_ID('produto_curva') IS NULL
BEGIN
  --The following statement was imported into the database project as a schema object and named dbo.produto_curva.
--CREATE TABLE produto_curva (
--  id_produto_curva INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_produto_curva PRIMARY KEY,
--  id_produto int NOT NULL,
--  cod_operacao_logistica_polo	varchar(20) NOT NULL,
--  curva	varchar(10) NOT NULL, 
--
--  CONSTRAINT UQ_produto_curva UNIQUE (id_produto, cod_operacao_logistica_polo,curva)
-- 
-- )



END
GO

IF OBJECT_ID('produto_shelf_life') is null
BEGIN
  
   --The following statement was imported into the database project as a schema object and named dbo.produto_shelf_life.
--CREATE TABLE produto_shelf_life (
--   id_produto_shelf_life INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_produto_shelf_life PRIMARY KEY,
--   id_produto	int NOT NULL,
--   grupo_shelf_life	varchar(30) NOT NULL,
--   validade_minima	smalldatetime,
--   validade_maxima	smalldatetime,
--    CONSTRAINT UQ_produto_shelf_life UNIQUE (id_produto, grupo_shelf_life)
-- 
-- )

  
END
GO

IF OBJECT_ID('pedido_operacao_entrada') IS NULL
BEGIN

  --The following statement was imported into the database project as a schema object and named dbo.pedido_operacao_entrada.
--CREATE TABLE pedido_operacao_entrada (
--  
--    id_pedido_operacao_entrada	int not null identity (1,1) CONSTRAINT PK_pedido_operacao_entrada PRIMARY KEY,
--    cod_pedido_polo VARCHAR(30) NOT NULL, 
--    cod_operacao_logistica_polo	varchar(20),
--    cod_prenota	int,
--    cod_entidade_polo VARCHAR(30) NOT NULL,
--    Cod_tipo_entidade VARCHAR(50) NOT NULL,
--    operacao	int NOT NULL,
--    cod_situacao	INT,
--    situacao	VARCHAR(50),
--    digitacao	SMALLDATETIME,
--    data_liberacao	SMALLDATETIME,
--    prenota	SMALLDATETIME,
--    encerramento_PN	SMALLDATETIME,
--    quem_digitou	varchar(30),
--    quem_liberou	varchar(30),
--    cod_origem_ped	int,
--    origem_ped	varchar(50),
--    volumes	INT,
--    valor_total	MONEY,
--    obs_PN	VARCHAR(500),
--    suboperacao	VARCHAR(50),
--    motivo_suboperacao	VARCHAR(50),
--    cod_entrega	int,
--    numero_titulo	varchar(13),
--    serie_nf	VARCHAR(3),
--    cod_titulo_polo	varchar(30),
--    peso	real,
--    qtdItens	int,
--    obs	varchar(255),
--    id_cancelamento	int,
--    data_solicitacao_cancelamento	DATETIME,
--    data_confirmacao_cancelamento	DATETIME,
--    CONSTRAINT UQ_pedido_operacao_entrada UNIQUE (cod_pedido_polo, cod_operacao_logistica_polo)
-- 
-- )


  
 

END
GO

IF OBJECT_ID('pedido_operacao_saida') IS NULL
BEGIN

  --The following statement was imported into the database project as a schema object and named dbo.pedido_operacao_saida.
--CREATE TABLE pedido_operacao_saida (
--  
--    id_pedido_operacao_saida	int not null identity (1,1) CONSTRAINT PK_pedido_operacao_saida PRIMARY KEY,
--    cod_pedido_polo VARCHAR(30) NOT NULL, 
--    cod_operacao_logistica_polo	varchar(20),
--    cod_prenota	int,
--    cod_entidade_polo VARCHAR(30) NOT NULL ,
--    Cod_tipo_entidade VARCHAR(50) NOT NULL ,
--    operacao	int NOT NULL,
--    cod_situacao	INT,
--    situacao	VARCHAR(50),
--    digitacao	SMALLDATETIME,
--    data_liberacao	SMALLDATETIME,
--    prenota	SMALLDATETIME,
--    encerramento_PN	SMALLDATETIME,
--    quem_digitou	varchar(30),
--    quem_liberou	varchar(30),
--    cod_origem_ped	int,
--    origem_ped	varchar(50),
--    volumes	INT,
--    valor_total	MONEY,
--    obs_PN	VARCHAR(500),
--    suboperacao	VARCHAR(50),
--    motivo_suboperacao	VARCHAR(50),
--    cod_entrega	int,
--    numero_titulo	varchar(13),
--    serie_nf	VARCHAR(3),
--    cod_titulo_polo	varchar(30),
--    peso	real,
--    qtdItens	int,
--    obs	varchar(255),
--    id_cancelamento	int,
--    data_solicitacao_cancelamento	DATETIME,
--        data_confirmacao_cancelamento	DATETIME,
--    CONSTRAINT UQ_pedido_operacao_saida UNIQUE (cod_pedido_polo, cod_operacao_logistica_polo)
-- 
-- )


  
 

END
GO

IF OBJECT_ID('Pedido_operacao_saida_endereco') is null
BEGIN

  --The following statement was imported into the database project as a schema object and named dbo.Pedido_operacao_saida_endereco.
--CREATE TABLE Pedido_operacao_saida_endereco (
--  
--   Id_pedido_oper_saida_endereco	INT NOT NULL CONSTRAINT PK_Pedido_operacao_saida_endereco PRIMARY KEY,
--   cod_pedido_polo VARCHAR(30) NOT NULL, 
--   cod_operacao_logistica_polo	varchar(20),
--   cod_tipo_endereco	smallint,
--   bairro	varchar(60),
--   cep	varchar(9)	,
--   cod_rota	varchar(7)	,
--   descr_rota	varchar(50)	,
--   complemento	varchar(100),
--   cidade	varchar(50),
--   sigla_estado	char(10)	,
--   CONSTRAINT UQ_Pedido_operacao_saida_endereco unique (cod_pedido_polo, cod_operacao_logistica_polo)
--
--  
--  )



END
GO

IF OBJECT_ID('pedido_operacao_movimentacao') IS NULL
BEGIN

  --The following statement was imported into the database project as a schema object and named dbo.pedido_operacao_movimentacao.
--CREATE TABLE pedido_operacao_movimentacao (
--  
--    id_pedido_operacao_mov	int not null identity (1,1) CONSTRAINT PK_pedido_operacao_movimentacao PRIMARY KEY,
--    cod_pedido_polo VARCHAR(30) NOT NULL, 
--    cod_operacao_logistica_polo	varchar(20),
--    cod_prenota	int,
--    cod_entidade_polo VARCHAR(30) NOT NULL ,
--    Cod_tipo_entidade VARCHAR(50) NOT NULL ,
--    operacao	int NOT NULL,
--    cod_situacao	INT,
--    situacao	VARCHAR(50),
--    digitacao	SMALLDATETIME,
--    data_liberacao	SMALLDATETIME,
--    prenota	SMALLDATETIME,
--    encerramento_PN	SMALLDATETIME,
--    quem_digitou	varchar(30),
--    quem_liberou	varchar(30),
--    cod_origem_ped	int,
--    origem_ped	varchar(50),
--    volumes	INT,
--    valor_total	MONEY,
--    obs_PN	VARCHAR(500),
--    suboperacao	VARCHAR(50),
--    motivo_suboperacao	VARCHAR(50),
--    cod_entrega	int,
--    numero_titulo	varchar(13),
--    serie_nf	VARCHAR(3),
--    cod_titulo_polo	varchar(30),
--    peso	real,
--    qtdItens	int,
--    obs	varchar(255),
--    id_cancelamento	int,
--    data_solicitacao_cancelamento	DATETIME,
--        data_confirmacao_cancelamento	DATETIME,
--    CONSTRAINT UQ_pedido_operacao_movimentacao UNIQUE (cod_pedido_polo, cod_operacao_logistica_polo)
-- 
-- )


  
 

END
GO

IF OBJECT_ID('pedido_item_operacao_entrada') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_operacao_entrada.
--CREATE TABLE pedido_item_operacao_entrada (
--      id_pedido_item_oper_entrada INT not null identity (1,1) CONSTRAINT PK_pedido_item_operacao_entrada PRIMARY KEY,
--      id_pedido_operacao_entrada	INT NOT NULL,
--      id_produto	INT NOT NULL,
--      quantidade	MONEY,
--      separado	MONEY,
--      cod_motivo	INT,
--      motivo	VARCHAR(30),
--      numero_item	INT,
--      obs	VARCHAR(255),
--      ordem_conf	INT,
--
--      CONSTRAINT UQ_pedido_item_operacao_entrada UNIQUE (id_pedido_operacao_entrada,id_produto)
--   )

end
GO

IF OBJECT_ID('pedido_item_operacao_saida') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_operacao_saida.
--CREATE TABLE pedido_item_operacao_saida (
--      id_pedido_item_operacao_saida INT not null identity (1,1) CONSTRAINT PK_pedido_item_operacao_saida PRIMARY KEY,
--      id_pedido_operacao_saida	INT NOT NULL,
--      id_produto	INT NOT NULL,
--      quantidade	MONEY,
--      separado	MONEY,
--      cod_motivo	INT,
--      motivo	VARCHAR(30),
--      numero_item	INT,
--      obs	VARCHAR(255),
--      ordem_conf	INT,
--
--      CONSTRAINT UQ_pedido_item_operacao_saida UNIQUE (id_pedido_operacao_saida,id_produto)
--   )

end
GO

IF OBJECT_ID('pedido_item_oper_mov') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_oper_mov.
--CREATE TABLE pedido_item_oper_mov (
--      id_pedido_item_oper_mov INT not null identity (1,1) CONSTRAINT PK_pedido_item_operacao_movimentacao PRIMARY KEY,
--      id_pedido_operacao_mov INT NOT NULL,
--      id_produto	INT NOT NULL,
--      quantidade	MONEY,
--      separado	MONEY,
--      cod_motivo	INT,
--      motivo	VARCHAR(30),
--      numero_item	INT,
--      obs	VARCHAR(255),
--      ordem_conf	INT,
--
--      CONSTRAINT UQ_pedido_item_operacao_movimentacao UNIQUE (id_pedido_operacao_mov,id_produto)
--   )

end
GO

if OBJECT_ID('pedido_item_lote_oper_entrada') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_lote_oper_entrada.
--CREATE TABLE pedido_item_lote_oper_entrada (
--      id_ped_item_lote_oper_entrada INT NOT NULL CONSTRAINT PK_ped_item_lote_oper_entrada PRIMARY KEY,
--      id_pedido_item_oper_entrada INT NOT NULL CONSTRAINT FK_pedido_item_operacao_entrada_pedido_item_operacao_entrada FOREIGN KEY REFERENCES pedido_item_operacao_entrada (id_pedido_item_oper_entrada),
--      lote_orig	VARCHAR(50) NOT NULL,
--      lote	varchar(50) NOT NULL,
--      id_classificacao_origem_polo	INT NOT NULL,
--      id_classificacao_dest_polo	INT NOT NULL,
--      quantidade	MONEY NOT NULL,
--      validade_orig	SMALLDATETIME,
--      validade	SMALLDATETIME,
--      fabricacao	SMALLDATETIME,
--      cod_situacao	INT,
--      obs	VARCHAR(255),
--      classificacao_origem	VARCHAR(50),
--      classificacao_destino	VARCHAR(50),
--
--      CONSTRAINT UQ_ped_item_lote_oper_entrada UNIQUE (id_pedido_item_oper_entrada, lote_orig, lote,id_classificacao_origem_polo,id_classificacao_dest_polo )
--   
--   )


END
GO

if OBJECT_ID('pedido_item_lote_oper_saida') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_lote_oper_saida.
--CREATE TABLE pedido_item_lote_oper_saida (
--      id_pedido_item_lote_oper_saida INT NOT NULL CONSTRAINT PK_pedido_item_lote_operacao_saida PRIMARY KEY,
--      id_pedido_item_operacao_saida INT NOT NULL CONSTRAINT FK_pedido_item_lote_operacao_saida_pedido_item_operacao_saida FOREIGN KEY REFERENCES pedido_item_operacao_saida (id_pedido_item_operacao_saida),
--      lote_orig	VARCHAR(50) NOT NULL,
--      lote	varchar(50) NOT NULL,
--      id_classificacao_origem_polo	INT NOT NULL,
--      id_classificacao_destino_polo	INT NOT NULL,
--      quantidade	MONEY NOT NULL,
--      validade_orig	SMALLDATETIME,
--      validade	SMALLDATETIME,
--      fabricacao	SMALLDATETIME,
--      cod_situacao	INT,
--      obs	VARCHAR(255),
--      classificacao_origem	VARCHAR(50),
--      classificacao_destino	VARCHAR(50),
--
--      CONSTRAINT UQ_pedido_item_lote_oper_saida UNIQUE (id_pedido_item_operacao_saida, lote_orig, lote,id_classificacao_origem_polo,id_classificacao_destino_polo )
--   
--   )


END
GO

if OBJECT_ID('pedido_item_lote_oper_mov') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_lote_oper_mov.
--CREATE TABLE pedido_item_lote_oper_mov (
--      id_pedido_item_lote_oper_movo INT NOT NULL CONSTRAINT PK_pedido_item_lote_operacao_movimentacao PRIMARY KEY,
--      id_pedido_item_oper_mov INT NOT NULL CONSTRAINT FK_pedido_item_lote_operacao_movimentacao_pedido_item_operacao_movimentacao FOREIGN KEY REFERENCES pedido_item_oper_mov (id_pedido_item_oper_mov),
--      lote_orig	VARCHAR(50) NOT NULL,
--      lote	varchar(50) NOT NULL,
--      id_classificacao_origem_polo	INT NOT NULL,
--      id_classificacao_destino_polo	INT NOT NULL,
--      quantidade	MONEY NOT NULL,
--      validade_orig	SMALLDATETIME,
--      validade	SMALLDATETIME,
--      fabricacao	SMALLDATETIME,
--      cod_situacao	INT,
--      obs	VARCHAR(255),
--      classificacao_origem	VARCHAR(50),
--      classificacao_destino	VARCHAR(50),
--
--      CONSTRAINT UQ_pedido_item_lote_operacao_movimentacao UNIQUE (id_pedido_item_oper_mov, lote_orig, lote,id_classificacao_origem_polo,id_classificacao_destino_polo )
--   
--   )


END
GO

IF OBJECT_ID('pedido_volume_operacao_saida') IS NULL
BEGIN

   --The following statement was imported into the database project as a schema object and named dbo.pedido_volume_operacao_saida.
--CREATE TABLE pedido_volume_operacao_saida(
--     id_pedido_volume_oper_saida INT NOT NULL CONSTRAINT PK_pedido_volume_operacao_saida PRIMARY KEY,
--     id_pedido_operacao_saida	int NOT NULL,
--     volume	int,
--     lacre	varchar(50) NOT NULL,
--     data_volume_embarcado	smalldatetime,
--     tipo_caixa	smallint,
--     CONSTRAINT UQ_pedido_volume_operacao_saida UNIQUE (id_pedido_operacao_saida, volume)
--     
--)
 


--The following statement was imported into the database project as a schema object and named dbo.FK_pedido_volume_operacao_saida_pedido_operacao_Saida.
--ALTER TABLE pedido_volume_operacao_saida  WITH CHECK ADD 
-- CONSTRAINT FK_pedido_volume_operacao_saida_pedido_operacao_Saida 
-- FOREIGN KEY(id_pedido_operacao_saida)
--REFERENCES pedido_operacao_saida (id_pedido_operacao_saida)
--ON UPDATE CASCADE
--ON DELETE CASCADE

   

END
GO

IF OBJECT_ID('pedido_volume_operacao_mov') IS NULL
BEGIN

   --The following statement was imported into the database project as a schema object and named dbo.pedido_volume_operacao_mov.
--CREATE TABLE pedido_volume_operacao_mov(
--     id_pedido_volume_oper_mov INT NOT NULL CONSTRAINT PK_pedido_volume_operacao_movimentacao PRIMARY KEY,
--     id_pedido_operacao_mov int NOT NULL,
--     volume	int,
--     lacre	varchar(50) NOT NULL,
--     data_volume_embarcado	smalldatetime,
--     tipo_caixa	smallint,
--     CONSTRAINT UQ_pedido_volume_operacao_movimentacao UNIQUE (id_pedido_operacao_mov, volume)
--     
--)
 


--The following statement was imported into the database project as a schema object and named dbo.FK_pedido_volume_operacao_movimentacao_pedido_operacao_movimentacao.
--ALTER TABLE pedido_volume_operacao_mov  WITH CHECK ADD  CONSTRAINT 
--FK_pedido_volume_operacao_movimentacao_pedido_operacao_movimentacao FOREIGN KEY(id_pedido_operacao_mov)
--REFERENCES pedido_operacao_movimentacao (id_pedido_operacao_mov)
--ON UPDATE CASCADE
--ON DELETE CASCADE

   

END
GO

IF OBJECT_ID('entidade') is null
BEGIN


--The following statement was imported into the database project as a schema object and named dbo.entidade.
--CREATE TABLE entidade(
--	id_entidade INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_entidade primary key,
--	cod_entidade_polo VARCHAR(30) NOT NULL, 
--	cpf_cnpj VARCHAR(30), 
--	razao_social VARCHAR(255) NOT NULL,
--	cod_tipo_pessoa INT,
--	fantasia VARCHAR(50) NULL,
--	tipo VARCHAR(50) NOT NULL,
--	subtipo VARCHAR(50),
--   bandeira VARCHAR(50) NOT NULL,
--   cod_situacao int,
--   obs VARCHAR(500),
--   endereco VARCHAR(100),
--   cod_tipo_endereco smallint,
--   bairro VARCHAR(60),
--   cep VARCHAR(9),
--   cod_rota varchar(7),
--   descr_rota VARCHAR(50),
--   complemento VARCHAR(100),
--   cidade VARCHAR(50),
--   sigla_estado CHAR(10),
--   inscricao_estadual VARCHAR(30),
--   telefone VARCHAR(20),
--   
-- CONSTRAINT UQ_entidade UNIQUE (cod_entidade_polo, tipo)
--
-- 
--
--)





END
GO

IF OBJECT_ID('produto') IS NULL
BEGIN
   
   --The following statement was imported into the database project as a schema object and named dbo.produto.
--CREATE TABLE produto (
--     id_produto	INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_produto PRIMARY KEY,
--     cod_produto_polo	VARCHAR(30) CONSTRAINT UQ_produto UNIQUE,
--     cod_forn_polo	VARCHAR(20),
--     descricao	VARCHAR(80) NOT NULL,
--     generico	varchar(80),
--     grupo	VARCHAR(50),
--     subgrupo	VARCHAR(50),
--     cod_situacao	int,
--     emb_compra	money,
--     emb_saida	money,
--     validade	INT,
--     linha_produto	varchar(50),
--     controle_lote	bit,
--     unidade	VARCHAR(5),
--     casas_decimais	INT,
--     altura	MONEY,
--     largura	MONEY,
--     comprimento	MONEY,
--     peso	money,
--     preco_venda	money
--
--   
--   )


   

END
GO

IF OBJECT_ID('produto_cod_barra') IS NULL
BEGIN


  --The following statement was imported into the database project as a schema object and named dbo.produto_cod_barra.
--CREATE TABLE produto_cod_barra (
--     id_produto_cod_barra INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_produto_cod_barra PRIMARY KEY, 
--     cod_barra varchar(20) NOT NULL,
--     id_produto int NOT NULL,
--     quantidade	money NOT NULL,
--     cod_tipo	int NOT NULL,
--     tipo	varchar(30),
--     altura	money,
--     largura	money,
--     comprimento	money,
--     peso	money,
--     principal	int,
--     CONSTRAINT UQ_produto_Cod_barra UNIQUE (cod_barra, id_produto)
--
--  )

END
GO

IF OBJECT_ID('produto_curva') IS NULL
BEGIN
  --The following statement was imported into the database project as a schema object and named dbo.produto_curva.
--CREATE TABLE produto_curva (
--  id_produto_curva INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_produto_curva PRIMARY KEY,
--  id_produto int NOT NULL,
--  cod_operacao_logistica_polo	varchar(20) NOT NULL,
--  curva	varchar(10) NOT NULL, 
--
--  CONSTRAINT UQ_produto_curva UNIQUE (id_produto, cod_operacao_logistica_polo,curva)
-- 
-- )



END
GO

IF OBJECT_ID('produto_shelf_life') is null
BEGIN
  
   --The following statement was imported into the database project as a schema object and named dbo.produto_shelf_life.
--CREATE TABLE produto_shelf_life (
--   id_produto_shelf_life INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_produto_shelf_life PRIMARY KEY,
--   id_produto	int NOT NULL,
--   grupo_shelf_life	varchar(30) NOT NULL,
--   validade_minima	smalldatetime,
--   validade_maxima	smalldatetime,
--    CONSTRAINT UQ_produto_shelf_life UNIQUE (id_produto, grupo_shelf_life)
-- 
-- )

  
END
GO

IF OBJECT_ID('pedido_operacao_entrada') IS NULL
BEGIN

  --The following statement was imported into the database project as a schema object and named dbo.pedido_operacao_entrada.
--CREATE TABLE pedido_operacao_entrada (
--  
--    id_pedido_operacao_entrada	int not null identity (1,1) CONSTRAINT PK_pedido_operacao_entrada PRIMARY KEY,
--    cod_pedido_polo VARCHAR(30) NOT NULL, 
--    cod_operacao_logistica_polo	varchar(20),
--    cod_prenota	int,
--    cod_entidade_polo VARCHAR(30) NOT NULL,
--    Cod_tipo_entidade VARCHAR(50) NOT NULL,
--    operacao	int NOT NULL,
--    cod_situacao	INT,
--    situacao	VARCHAR(50),
--    digitacao	SMALLDATETIME,
--    data_liberacao	SMALLDATETIME,
--    prenota	SMALLDATETIME,
--    encerramento_PN	SMALLDATETIME,
--    quem_digitou	varchar(30),
--    quem_liberou	varchar(30),
--    cod_origem_ped	int,
--    origem_ped	varchar(50),
--    volumes	INT,
--    valor_total	MONEY,
--    obs_PN	VARCHAR(500),
--    suboperacao	VARCHAR(50),
--    motivo_suboperacao	VARCHAR(50),
--    cod_entrega	int,
--    numero_titulo	varchar(13),
--    serie_nf	VARCHAR(3),
--    cod_titulo_polo	varchar(30),
--    peso	real,
--    qtdItens	int,
--    obs	varchar(255),
--    id_cancelamento	int,
--    data_solicitacao_cancelamento	DATETIME,
--    data_confirmacao_cancelamento	DATETIME,
--    CONSTRAINT UQ_pedido_operacao_entrada UNIQUE (cod_pedido_polo, cod_operacao_logistica_polo)
-- 
-- )


  
 

END
GO

IF OBJECT_ID('pedido_operacao_saida') IS NULL
BEGIN

  --The following statement was imported into the database project as a schema object and named dbo.pedido_operacao_saida.
--CREATE TABLE pedido_operacao_saida (
--  
--    id_pedido_operacao_saida	int not null identity (1,1) CONSTRAINT PK_pedido_operacao_saida PRIMARY KEY,
--    cod_pedido_polo VARCHAR(30) NOT NULL, 
--    cod_operacao_logistica_polo	varchar(20),
--    cod_prenota	int,
--    cod_entidade_polo VARCHAR(30) NOT NULL ,
--    Cod_tipo_entidade VARCHAR(50) NOT NULL ,
--    operacao	int NOT NULL,
--    cod_situacao	INT,
--    situacao	VARCHAR(50),
--    digitacao	SMALLDATETIME,
--    data_liberacao	SMALLDATETIME,
--    prenota	SMALLDATETIME,
--    encerramento_PN	SMALLDATETIME,
--    quem_digitou	varchar(30),
--    quem_liberou	varchar(30),
--    cod_origem_ped	int,
--    origem_ped	varchar(50),
--    volumes	INT,
--    valor_total	MONEY,
--    obs_PN	VARCHAR(500),
--    suboperacao	VARCHAR(50),
--    motivo_suboperacao	VARCHAR(50),
--    cod_entrega	int,
--    numero_titulo	varchar(13),
--    serie_nf	VARCHAR(3),
--    cod_titulo_polo	varchar(30),
--    peso	real,
--    qtdItens	int,
--    obs	varchar(255),
--    id_cancelamento	int,
--    data_solicitacao_cancelamento	DATETIME,
--        data_confirmacao_cancelamento	DATETIME,
--    CONSTRAINT UQ_pedido_operacao_saida UNIQUE (cod_pedido_polo, cod_operacao_logistica_polo)
-- 
-- )


  
 

END
GO

IF OBJECT_ID('Pedido_operacao_saida_endereco') is null
BEGIN

  --The following statement was imported into the database project as a schema object and named dbo.Pedido_operacao_saida_endereco.
--CREATE TABLE Pedido_operacao_saida_endereco (
--  
--   Id_pedido_oper_saida_endereco	INT NOT NULL CONSTRAINT PK_Pedido_operacao_saida_endereco PRIMARY KEY,
--   cod_pedido_polo VARCHAR(30) NOT NULL, 
--   cod_operacao_logistica_polo	varchar(20),
--   cod_tipo_endereco	smallint,
--   bairro	varchar(60),
--   cep	varchar(9)	,
--   cod_rota	varchar(7)	,
--   descr_rota	varchar(50)	,
--   complemento	varchar(100),
--   cidade	varchar(50),
--   sigla_estado	char(10)	,
--   CONSTRAINT UQ_Pedido_operacao_saida_endereco unique (cod_pedido_polo, cod_operacao_logistica_polo)
--
--  
--  )



END
GO

IF OBJECT_ID('pedido_operacao_movimentacao') IS NULL
BEGIN

  --The following statement was imported into the database project as a schema object and named dbo.pedido_operacao_movimentacao.
--CREATE TABLE pedido_operacao_movimentacao (
--  
--    id_pedido_operacao_mov	int not null identity (1,1) CONSTRAINT PK_pedido_operacao_movimentacao PRIMARY KEY,
--    cod_pedido_polo VARCHAR(30) NOT NULL, 
--    cod_operacao_logistica_polo	varchar(20),
--    cod_prenota	int,
--    cod_entidade_polo VARCHAR(30) NOT NULL ,
--    Cod_tipo_entidade VARCHAR(50) NOT NULL ,
--    operacao	int NOT NULL,
--    cod_situacao	INT,
--    situacao	VARCHAR(50),
--    digitacao	SMALLDATETIME,
--    data_liberacao	SMALLDATETIME,
--    prenota	SMALLDATETIME,
--    encerramento_PN	SMALLDATETIME,
--    quem_digitou	varchar(30),
--    quem_liberou	varchar(30),
--    cod_origem_ped	int,
--    origem_ped	varchar(50),
--    volumes	INT,
--    valor_total	MONEY,
--    obs_PN	VARCHAR(500),
--    suboperacao	VARCHAR(50),
--    motivo_suboperacao	VARCHAR(50),
--    cod_entrega	int,
--    numero_titulo	varchar(13),
--    serie_nf	VARCHAR(3),
--    cod_titulo_polo	varchar(30),
--    peso	real,
--    qtdItens	int,
--    obs	varchar(255),
--    id_cancelamento	int,
--    data_solicitacao_cancelamento	DATETIME,
--        data_confirmacao_cancelamento	DATETIME,
--    CONSTRAINT UQ_pedido_operacao_movimentacao UNIQUE (cod_pedido_polo, cod_operacao_logistica_polo)
-- 
-- )


  
 

END
GO

IF OBJECT_ID('pedido_item_operacao_entrada') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_operacao_entrada.
--CREATE TABLE pedido_item_operacao_entrada (
--      id_pedido_item_oper_entrada INT not null identity (1,1) CONSTRAINT PK_pedido_item_operacao_entrada PRIMARY KEY,
--      id_pedido_operacao_entrada	INT NOT NULL,
--      id_produto	INT NOT NULL,
--      quantidade	MONEY,
--      separado	MONEY,
--      cod_motivo	INT,
--      motivo	VARCHAR(30),
--      numero_item	INT,
--      obs	VARCHAR(255),
--      ordem_conf	INT,
--
--      CONSTRAINT UQ_pedido_item_operacao_entrada UNIQUE (id_pedido_operacao_entrada,id_produto)
--   )

end
GO

IF OBJECT_ID('pedido_item_operacao_saida') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_operacao_saida.
--CREATE TABLE pedido_item_operacao_saida (
--      id_pedido_item_operacao_saida INT not null identity (1,1) CONSTRAINT PK_pedido_item_operacao_saida PRIMARY KEY,
--      id_pedido_operacao_saida	INT NOT NULL,
--      id_produto	INT NOT NULL,
--      quantidade	MONEY,
--      separado	MONEY,
--      cod_motivo	INT,
--      motivo	VARCHAR(30),
--      numero_item	INT,
--      obs	VARCHAR(255),
--      ordem_conf	INT,
--
--      CONSTRAINT UQ_pedido_item_operacao_saida UNIQUE (id_pedido_operacao_saida,id_produto)
--   )

end
GO

IF OBJECT_ID('pedido_item_oper_mov') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_oper_mov.
--CREATE TABLE pedido_item_oper_mov (
--      id_pedido_item_oper_mov INT not null identity (1,1) CONSTRAINT PK_pedido_item_operacao_movimentacao PRIMARY KEY,
--      id_pedido_operacao_mov INT NOT NULL,
--      id_produto	INT NOT NULL,
--      quantidade	MONEY,
--      separado	MONEY,
--      cod_motivo	INT,
--      motivo	VARCHAR(30),
--      numero_item	INT,
--      obs	VARCHAR(255),
--      ordem_conf	INT,
--
--      CONSTRAINT UQ_pedido_item_operacao_movimentacao UNIQUE (id_pedido_operacao_mov,id_produto)
--   )

end
GO

if OBJECT_ID('pedido_item_lote_oper_entrada') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_lote_oper_entrada.
--CREATE TABLE pedido_item_lote_oper_entrada (
--      id_ped_item_lote_oper_entrada INT NOT NULL CONSTRAINT PK_ped_item_lote_oper_entrada PRIMARY KEY,
--      id_pedido_item_oper_entrada INT NOT NULL CONSTRAINT FK_pedido_item_operacao_entrada_pedido_item_operacao_entrada FOREIGN KEY REFERENCES pedido_item_operacao_entrada (id_pedido_item_oper_entrada),
--      lote_orig	VARCHAR(50) NOT NULL,
--      lote	varchar(50) NOT NULL,
--      id_classificacao_origem_polo	INT NOT NULL,
--      id_classificacao_dest_polo	INT NOT NULL,
--      quantidade	MONEY NOT NULL,
--      validade_orig	SMALLDATETIME,
--      validade	SMALLDATETIME,
--      fabricacao	SMALLDATETIME,
--      cod_situacao	INT,
--      obs	VARCHAR(255),
--      classificacao_origem	VARCHAR(50),
--      classificacao_destino	VARCHAR(50),
--
--      CONSTRAINT UQ_ped_item_lote_oper_entrada UNIQUE (id_pedido_item_oper_entrada, lote_orig, lote,id_classificacao_origem_polo,id_classificacao_dest_polo )
--   
--   )


END
GO

if OBJECT_ID('pedido_item_lote_oper_saida') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_lote_oper_saida.
--CREATE TABLE pedido_item_lote_oper_saida (
--      id_pedido_item_lote_oper_saida INT NOT NULL CONSTRAINT PK_pedido_item_lote_operacao_saida PRIMARY KEY,
--      id_pedido_item_operacao_saida INT NOT NULL CONSTRAINT FK_pedido_item_lote_operacao_saida_pedido_item_operacao_saida FOREIGN KEY REFERENCES pedido_item_operacao_saida (id_pedido_item_operacao_saida),
--      lote_orig	VARCHAR(50) NOT NULL,
--      lote	varchar(50) NOT NULL,
--      id_classificacao_origem_polo	INT NOT NULL,
--      id_classificacao_destino_polo	INT NOT NULL,
--      quantidade	MONEY NOT NULL,
--      validade_orig	SMALLDATETIME,
--      validade	SMALLDATETIME,
--      fabricacao	SMALLDATETIME,
--      cod_situacao	INT,
--      obs	VARCHAR(255),
--      classificacao_origem	VARCHAR(50),
--      classificacao_destino	VARCHAR(50),
--
--      CONSTRAINT UQ_pedido_item_lote_oper_saida UNIQUE (id_pedido_item_operacao_saida, lote_orig, lote,id_classificacao_origem_polo,id_classificacao_destino_polo )
--   
--   )


END
GO

if OBJECT_ID('pedido_item_lote_oper_mov') IS NULL
BEGIN
   --The following statement was imported into the database project as a schema object and named dbo.pedido_item_lote_oper_mov.
--CREATE TABLE pedido_item_lote_oper_mov (
--      id_pedido_item_lote_oper_movo INT NOT NULL CONSTRAINT PK_pedido_item_lote_operacao_movimentacao PRIMARY KEY,
--      id_pedido_item_oper_mov INT NOT NULL CONSTRAINT FK_pedido_item_lote_operacao_movimentacao_pedido_item_operacao_movimentacao FOREIGN KEY REFERENCES pedido_item_oper_mov (id_pedido_item_oper_mov),
--      lote_orig	VARCHAR(50) NOT NULL,
--      lote	varchar(50) NOT NULL,
--      id_classificacao_origem_polo	INT NOT NULL,
--      id_classificacao_destino_polo	INT NOT NULL,
--      quantidade	MONEY NOT NULL,
--      validade_orig	SMALLDATETIME,
--      validade	SMALLDATETIME,
--      fabricacao	SMALLDATETIME,
--      cod_situacao	INT,
--      obs	VARCHAR(255),
--      classificacao_origem	VARCHAR(50),
--      classificacao_destino	VARCHAR(50),
--
--      CONSTRAINT UQ_pedido_item_lote_operacao_movimentacao UNIQUE (id_pedido_item_oper_mov, lote_orig, lote,id_classificacao_origem_polo,id_classificacao_destino_polo )
--   
--   )


END
GO

IF OBJECT_ID('pedido_volume_operacao_saida') IS NULL
BEGIN

   --The following statement was imported into the database project as a schema object and named dbo.pedido_volume_operacao_saida.
--CREATE TABLE pedido_volume_operacao_saida(
--     id_pedido_volume_oper_saida INT NOT NULL CONSTRAINT PK_pedido_volume_operacao_saida PRIMARY KEY,
--     id_pedido_operacao_saida	int NOT NULL,
--     volume	int,
--     lacre	varchar(50) NOT NULL,
--     data_volume_embarcado	smalldatetime,
--     tipo_caixa	smallint,
--     CONSTRAINT UQ_pedido_volume_operacao_saida UNIQUE (id_pedido_operacao_saida, volume)
--     
--)
 


--The following statement was imported into the database project as a schema object and named dbo.FK_pedido_volume_operacao_saida_pedido_operacao_Saida.
--ALTER TABLE pedido_volume_operacao_saida  WITH CHECK ADD 
-- CONSTRAINT FK_pedido_volume_operacao_saida_pedido_operacao_Saida 
-- FOREIGN KEY(id_pedido_operacao_saida)
--REFERENCES pedido_operacao_saida (id_pedido_operacao_saida)
--ON UPDATE CASCADE
--ON DELETE CASCADE

   

END
GO

IF OBJECT_ID('pedido_volume_operacao_mov') IS NULL
BEGIN

   --The following statement was imported into the database project as a schema object and named dbo.pedido_volume_operacao_mov.
--CREATE TABLE pedido_volume_operacao_mov(
--     id_pedido_volume_oper_mov INT NOT NULL CONSTRAINT PK_pedido_volume_operacao_movimentacao PRIMARY KEY,
--     id_pedido_operacao_mov int NOT NULL,
--     volume	int,
--     lacre	varchar(50) NOT NULL,
--     data_volume_embarcado	smalldatetime,
--     tipo_caixa	smallint,
--     CONSTRAINT UQ_pedido_volume_operacao_movimentacao UNIQUE (id_pedido_operacao_mov, volume)
--     
--)
 


--The following statement was imported into the database project as a schema object and named dbo.FK_pedido_volume_operacao_movimentacao_pedido_operacao_movimentacao.
--ALTER TABLE pedido_volume_operacao_mov  WITH CHECK ADD  CONSTRAINT 
--FK_pedido_volume_operacao_movimentacao_pedido_operacao_movimentacao FOREIGN KEY(id_pedido_operacao_mov)
--REFERENCES pedido_operacao_movimentacao (id_pedido_operacao_mov)
--ON UPDATE CASCADE
--ON DELETE CASCADE

   

END
GO

IF OBJECT_ID('mensagem_integracao') IS NULL
BEGIN

   --The following statement was imported into the database project as a schema object and named dbo.mensagem_integracao.
--CREATE TABLE mensagem_integracao(
--     id_mensagem_integracao INT NOT NULL CONSTRAINT PK_mensagem_integracao PRIMARY KEY,
--     origem INT NOT NULL, --1 - ERP, 2 - WMSRX
--     tabela_relacionada	varchar(30) NOT NULL,
--	 id_tabela_relacionada INT NOT NULL,
--	 data_cadastro smalldatetime,
--	 data_processamento smalldatetime,
--	 cod_rejeicao INT,
--	 motivo_rejeicao varchar(100),
--	 rejeitado int NOT NULL DEFAULT 0
--)
 
  
END
GO
