﻿using Entidades;
using Persistencia.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Persistencia
{
    public interface IRepository<TEntity> where TEntity : EntidadeBase
    {
        IQueryable<TEntity> Items { get; }

        long Add(TEntity entity);

        void Delete(TEntity entity);

        TEntity Get(long id);

        void Update(TEntity entity);

        void Commit();

        void Rollback();
    }

    public class Repository<T> : IRepository<T> where T : EntidadeBase
    {
        protected static object _locker = new object();
        protected DatabaseContext _context;

        public Repository(DatabaseContext context)
        {
            _context = context ?? ContextContainer.Context;
        }

        public IQueryable<T> Items
        {
            get
            {
                lock (_locker)
                {
                    return _context.Set<T>();
                }
            }
        }

        public long Add(T entity)
        {
            lock (_locker)
            {
                T item = _context.Set<T>().Add(entity);

                Commit();

                return item.Id;
            }
        }

        public void Delete(T entity)
        {
            lock (_locker)
            {
                _context.Set<T>().Remove(entity);
                _context.SaveChanges();
            }
        }

        public T Get(long id)
        {
            lock (_locker)
            {
                return _context.Set<T>().SingleOrDefault(x => x.Id == id);
            }
        }

        public IQueryable<T> Get(Func<T, bool> query)
        {
            lock (_locker)
            {
                return _context.Set<T>().Where(query).AsQueryable();
            }
        }

        public IEnumerable<T> NativeQuery(string query)
        {
            lock (_locker)
            {
                List<T> results = _context.Database.SqlQuery<T>(query).ToList();
                return results;
            }
        }

        public void Update(T entity)
        {
            lock (_locker)
            {
                var entry = _context.Entry(entity);
                entry.State = EntityState.Modified;
                Commit();
            }
        }

        public void Commit()
        {
            lock (_locker)
            {
                _context.SaveChanges();
            }
        }

        public void ClearCache(T entity)
        {
            var entry = _context.Entry(entity);

            if (entry.State == EntityState.Modified)
                _context.Entry(entity).Reload();
            if (entry.State == EntityState.Added || entry.State == EntityState.Deleted)
                entry.State = EntityState.Unchanged;
        }

        public void Rollback()
        {
            _context.ChangeTracker.DetectChanges();

            foreach (var entry in _context.ChangeTracker.Entries().Where(x => x.State != EntityState.Unchanged))
            {
                switch (entry.State)
                {
                    case EntityState.Deleted:
                    case EntityState.Modified:
                        {
                            entry.Reload();
                            break;
                        }
                    case EntityState.Added:
                        {
                            entry.State = EntityState.Unchanged;
                            break;
                        }
                }
            } 
        }
    }
}
