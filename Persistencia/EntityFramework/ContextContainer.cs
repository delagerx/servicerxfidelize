﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.EntityFramework
{
    public static class ContextContainer
    {
        public static DatabaseContext Context { get; set; }
    }
}
