﻿using Entidades;
using Entidades.Enum;
using Persistencia.Mapping;
using System.Configuration;
using System.Data.Entity;

namespace Persistencia.EntityFramework
{
    public class DatabaseContext : DbContext
    {
        public DbSet<DiarioLoteEstoque>                     DiarioLoteEstoqueDbSet { get; set; }
        public DbSet<DiarioProdutoEstoque>                  DiarioProdutoEstoqueDbSet { get; set; }
        public DbSet<Entidade>                              EntidadeDbSet { get; set; }
        public DbSet<MensagemIntegracao>                    MensagemIntegracaoDbSet { get; set; }
        public DbSet<PedidoAtributoOperacaoSaida>           PedidoAtributoOperacaoSaidaDbSet { get; set; }
        public DbSet<PedidoCancelamento>                    PedidoCancelamentoDbSet { get; set; }
        public DbSet<PedidoEnderecoOperacaoSaida>           PedidoEnderecoOperacaoSaidaDbSet { get; set; }
        public DbSet<PedidoItemOperacaoEntrada>             PedidoItemOperacaoEntradaDbSet { get; set; }
        public DbSet<PedidoItemOperacaoMovimentacao>        PedidoItemOperacaoMovimentacaoDbSet { get; set; }
        public DbSet<PedidoItemOperacaoSaida>               PedidoItemOperacaoSaidaDbSet { get; set; }
        public DbSet<PedidoItemKitOperacaoSaida>            PedidoItemKitOperacaoSaidaDbSet { get; set; }
        public DbSet<PedidoItemLoteOperacaoEntrada>         PedidoItemLoteOperacaoEntradaDbSet { get; set; }
        public DbSet<PedidoItemLoteOperacaoMovimentacao>    PedidoItemLoteOperacaoMovimentacaoDbSet { get; set; }
        public DbSet<PedidoItemLoteOperacaoSaida>           PedidoItemLoteOperacaoSaidaDbSet { get; set; }
        public DbSet<PedidoOperacaoEntrada>                 PedidoOperacaoEntradaDbSet { get; set; }
        public DbSet<PedidoOperacaoMovimentacao>            PedidoOperacaoMovimentacaoDbSet { get; set; }
        public DbSet<PedidoOperacaoSaida>                   PedidoOperacaoSaidaDbSet { get; set; }
        public DbSet<PedidoSituacaoOperacaoSaida>           PedidoSituacaoOperacaoSaidaDbSet { get; set; }
        public DbSet<PedidoItemVolumeOperacaoMovimentacao>  PedidoItemVolumeOperacaoMovimentacaoDbSet { get; set; }
        public DbSet<PedidoItemVolumeOperacaoSaida>         PedidoItemVolumeOperacaoSaidaDbSet { get; set; }
        public DbSet<PedidoVolumeOperacaoMovimentacao>      PedidoVolumeOperacaoMovimentacaoDbSet { get; set; }
        public DbSet<PedidoVolumeOperacaoSaida>             PedidoVolumeOperacaoSaidaDbSet { get; set; }
        public DbSet<ProdutoAuditoriaMovInv>                ProdutoAuditoriaMovInvDbSet { get; set; }
        public DbSet<Produto>                               ProdutoDbSet { get; set; }
        public DbSet<ProdutoCodBarra>                       ProdutoCodBarraDbSet { get; set; }
        public DbSet<ProdutoCurva>                          ProdutoCurvaDbSet { get; set; }
        public DbSet<ProdutoKit>                            ProdutoKitDbSet { get; set; }
        public DbSet<ProdutoShelfLife>                      ProdutoShelfLifeDbSet { get; set; }
        public DbSet<TituloOperacaoSaida>                   TituloOperacaoSaidaDbSet { get; set; }

        public DatabaseContext() : base("DefaultConnection") 
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<DatabaseContext>(null);

            modelBuilder.Configurations.Add(new DiarioLoteEstoqueMapping());
            modelBuilder.Configurations.Add(new DiarioProdutoEstoqueMapping());
            modelBuilder.Configurations.Add(new EntidadeMapping());
            modelBuilder.Configurations.Add(new MensagemIntegracaoMapping());
            modelBuilder.Configurations.Add(new ProdutoMapping());
            modelBuilder.Configurations.Add(new PedidoAtributoOperacaoSaidaMapping());
            modelBuilder.Configurations.Add(new PedidoCancelamentoMapping());
            modelBuilder.Configurations.Add(new PedidoEnderecoOperacaoSaidaMapping());
            modelBuilder.Configurations.Add(new PedidoSituacaoOperacaoSaidaMapping());
            modelBuilder.Configurations.Add(new PedidoItemOperacaoEntradaMapping());
            modelBuilder.Configurations.Add(new PedidoItemOperacaoMovimentacaoMapping());
            modelBuilder.Configurations.Add(new PedidoItemOperacaoSaidaMapping());
            modelBuilder.Configurations.Add(new PedidoItemKitOperacaoSaidaMapping());
            modelBuilder.Configurations.Add(new PedidoItemLoteOperacaoEntradaMapping());
            modelBuilder.Configurations.Add(new PedidoItemLoteOperacaoMovimentacaoMapping());
            modelBuilder.Configurations.Add(new PedidoItemLoteOperacaoSaidaMapping());
            modelBuilder.Configurations.Add(new PedidoItemVolumeOperacaoMovimentacaoMapping());
            modelBuilder.Configurations.Add(new PedidoItemVolumeOperacaoSaidaMapping());
            modelBuilder.Configurations.Add(new PedidoOperacaoEntradaMapping());
            modelBuilder.Configurations.Add(new PedidoOperacaoMovimentacaoMapping());
            modelBuilder.Configurations.Add(new PedidoOperacaoSaidaMapping());
            modelBuilder.Configurations.Add(new PedidoVolumeOperacaoMovimentacaoMapping());
            modelBuilder.Configurations.Add(new PedidoVolumeOperacaoSaidaMapping());
            modelBuilder.Configurations.Add(new ProdutoAuditoriaMovInvMapping());
            modelBuilder.Configurations.Add(new ProdutoCodBarraMapping());
            modelBuilder.Configurations.Add(new ProdutoCurvaMapping());
            modelBuilder.Configurations.Add(new ProdutoKitMapping());
            modelBuilder.Configurations.Add(new ProdutoShelfLifeMapping());
            modelBuilder.Configurations.Add(new ShelfLifeSaidaMapping());
            modelBuilder.Configurations.Add(new TituloOperacaoSaidaMapping());

            modelBuilder.HasDefaultSchema(Configuracao.DatabaseSchema);
        }
    }
}
