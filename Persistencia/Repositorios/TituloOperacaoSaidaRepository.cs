﻿using System;
using System.Collections.Generic;
using Entidades;
using Persistencia.EntityFramework;
using System.Linq;

namespace Persistencia
{
    public interface ITituloOperacaoSaidaRepository : IRepository<TituloOperacaoSaida>
    {
    }

    public class TituloOperacaoSaidaRepository : Repository<TituloOperacaoSaida>, ITituloOperacaoSaidaRepository
    {
        public TituloOperacaoSaidaRepository(DatabaseContext context) : base(context)
        {
        }

        public new TituloOperacaoSaida Get(long id)
        {
            lock (_locker)
            {
                return _context.Set<TituloOperacaoSaida>().AsNoTracking().SingleOrDefault(x => x.Id == id);
            }
        }
    }
}
