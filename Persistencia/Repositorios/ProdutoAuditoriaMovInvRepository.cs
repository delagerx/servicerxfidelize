﻿using Entidades;
using Mensagens.Resources;
using Persistencia.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System;

namespace Persistencia
{
    public interface IProdutoAuditoriaMovInvRepository: IRepository<ProdutoAuditoriaMovInv>
    {
    }

    public class ProdutoAuditoriaMovInvRepository : Repository<ProdutoAuditoriaMovInv>, IProdutoAuditoriaMovInvRepository
    {
        private IRepository<Produto> _produtoRepositorio;

        public ProdutoAuditoriaMovInvRepository(DatabaseContext context) : base(context)
        {
            _produtoRepositorio = new Repository<Produto>(context);
        }

        public new Entidade Get(long id)
        {
            lock (_locker)
            {
                return _context.Set<Entidade>().AsNoTracking().SingleOrDefault(x => x.Id == id);
            }
        }

        public long Salvar(ProdutoAuditoriaMovInv registro)
        {
            AssociarProdutoAoRegistro(registro);

            return Add(registro);
        }

        public ProdutoAuditoriaMovInv BuscarRegistroExistente(ProdutoAuditoriaMovInv produtoAuditoriaMovInv)
        {
            return Items.Where(x =>
                            x.CodOperacaoLogisticaPolo == produtoAuditoriaMovInv.CodOperacaoLogisticaPolo &&
                            x.IdPedidoOperacaoEntrada == produtoAuditoriaMovInv.IdPedidoOperacaoEntrada &&
                            x.IdPedidoOperacaoSaida == produtoAuditoriaMovInv.IdPedidoOperacaoSaida &&
                            x.Lote == produtoAuditoriaMovInv.Lote &&
                            x.Endereco == produtoAuditoriaMovInv.Endereco
                        ).FirstOrDefault();
        }

        private void AssociarProdutoAoRegistro(ProdutoAuditoriaMovInv registro)
        {
            Produto produto = _produtoRepositorio.Items.Where(x => x.CodProdutoPolo == registro.Produto.CodProdutoPolo).FirstOrDefault();

            if (produto == null)
                throw new ValidationException(string.Format(MensagensValidacao.PRODUTO_NAO_EXISTE, registro.Produto.CodProdutoPolo));

            registro.Produto = produto;
            registro.IdProduto = produto.Id;
        }
    }
}
