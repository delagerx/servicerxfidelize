﻿using Entidades;
using Entidades.Enum;
using Oracle.ManagedDataAccess.Client;
using Persistencia.EntityFramework;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System;
using Mensagens.Resources;
using System.ComponentModel.DataAnnotations;

namespace Persistencia
{
    public interface IPedidoOperacaoEntradaRepository : IRepository<PedidoOperacaoEntrada>
    {
        long Salvar(PedidoOperacaoEntrada pedido);

    }

    public class PedidoOperacaoEntradaRepository : Repository<PedidoOperacaoEntrada>, IPedidoOperacaoEntradaRepository
    {
        private IProdutoRepository _produtoRepositorio;
        private IEntidadeRepository _entidadeRepositorio;

        public PedidoOperacaoEntradaRepository(DatabaseContext context, IProdutoRepository produtoRepositorio, IEntidadeRepository entidadeRepositorio) : base(context)
        {
            _produtoRepositorio = produtoRepositorio;
            _entidadeRepositorio = entidadeRepositorio;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public new PedidoOperacaoEntrada Get(long id)
        {
            var pedidoOperacaoEntrada = Items
                                        .Include("PedidoItemOperacaoEntradaList.Produto")
                                        .Include("PedidoItemOperacaoEntradaList.PedidoItemLoteOperacaoEntradaList")
                                        .Include("Entidade")
                                        .Where(pedido => pedido.Id == id).FirstOrDefault();

            if (pedidoOperacaoEntrada == null)
                return null;

            var quantidadeDeItens = pedidoOperacaoEntrada.PedidoItemOperacaoEntradaList.Count;
            var tamanhoDaPaginaDeItens = quantidadeDeItens / 1000.0;

            // Ao tentar realizar o SELECT de um pedido com muitos itens (> 6000), o Entity Framework 
            // utiliza o operador APPLY, que não é compatível com Oracle 11, causando uma exceção na hora 
            // da leitura. O trecho de código a seguir, faz com que a consulta de itens seja feita de 
            // 1000 em 1000 de modo a forçar o Entity Framework a não utilizar o operador APPLY.
            for (var paginaDeItens = 0; paginaDeItens < tamanhoDaPaginaDeItens; paginaDeItens++)
            {
                List<long> idProdutoList = pedidoOperacaoEntrada
                    .PedidoItemOperacaoEntradaList.Skip(paginaDeItens * 1000).Take(1000)
                    .Select(i => i.Produto.Id)
                    .ToList();

                _produtoRepositorio.Items
                    .Include("ProdutoCurvaList")
                    .Include("ProdutoKitList")
                    .Include("ProdutoInsumoList")
                    .Include("ProdutoShelfLifeList")
                    .Include("ProdutoCodBarraList")
                    .Where(x => idProdutoList.Contains(x.Id))
                    .ToList();
            }

			foreach (var item in pedidoOperacaoEntrada.PedidoItemOperacaoEntradaList)
			{
				item.Produto.ProdutoCurvaList.ToList();
				item.Produto.ProdutoKitList.ToList();
				item.Produto.ProdutoShelfLifeList.ToList();
				item.Produto.ProdutoCodBarraList.ToList();
			}

            return pedidoOperacaoEntrada;
        }

        public long Salvar(PedidoOperacaoEntrada pedido)
        {
            OracleConnection sqlConnection = (OracleConnection)_context.Database.Connection;
            
            if (sqlConnection.State != ConnectionState.Open)
            {
                sqlConnection.Close();
                sqlConnection.Open();
            }

            using (OracleTransaction transaction = sqlConnection.BeginTransaction())
            {
                try
                {
                    _context.Database.UseTransaction(transaction);

                    VerificarExistenciaDoPedidoComMesmoStatus(pedido);

                    AssociarEntidadeAoPedido(pedido);

                    IList<PedidoItemOperacaoEntrada> itensDoPedidoEmMemoria = pedido.PedidoItemOperacaoEntradaList.ToList();
                    pedido.PedidoItemOperacaoEntradaList.Clear();

                    pedido.Id = Add(pedido);

                    AssociarProdutosAosItensDoPedido(itensDoPedidoEmMemoria);

                    SalvarItensDoPedido(itensDoPedidoEmMemoria, pedido, sqlConnection, transaction);
                    SalvarLotesDoPedido(itensDoPedidoEmMemoria, pedido, sqlConnection, transaction);

                    transaction.Commit();

                    return pedido.Id;
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        private void SalvarItensDoPedido(IList<PedidoItemOperacaoEntrada> itens, PedidoOperacaoEntrada pedido, OracleConnection sqlConnection, OracleTransaction transaction)
        {
            int pageSize = 100;
            int pages = itens.Count / pageSize;
            int lastPageSize = pageSize;

            if (itens.Count % pageSize > 0)
            {
                pages++;
                lastPageSize = itens.Count % pageSize;
            }

            for (int page = 0; page < pages; page++)
            {
                using (OracleCommand sqlCommand = new OracleCommand())
                {
                    sqlCommand.Transaction = transaction;
                    sqlCommand.Connection = sqlConnection;

                    sqlCommand.CommandText = "INSERT ALL ";

                    int currentPageSize = (page == (pages - 1)) ? lastPageSize : pageSize;

                    for (int currentPageIndex = 0; currentPageIndex < currentPageSize; currentPageIndex++)
                    {
                        PedidoItemOperacaoEntrada item = itens.ElementAt((page * pageSize) + currentPageIndex);

                        string commandText = $"INTO PEDIDO_ITEM_OPERACAO_ENTRADA (COD_MOTIVO, MOTIVO, NUMERO_ITEM, OBS, ORDEM_CONF, ID_PEDIDO_OPERACAO_ENTRADA, ID_PRODUTO, QUANTIDADE, SEPARADO) VALUES (:codMotivo{currentPageIndex}, :motivo{currentPageIndex}, :numeroItem{currentPageIndex}, :obs{currentPageIndex}, :ordemConf{currentPageIndex}, :idPedidoOperacaoEntrada{currentPageIndex}, :idProduto{currentPageIndex}, :quantidade{currentPageIndex}, :separado{currentPageIndex}) ";

                        sqlCommand.CommandText += commandText;

                        sqlCommand.Parameters.Add(new OracleParameter("codMotivo" + currentPageIndex, item.CodMotivo));
                        sqlCommand.Parameters.Add(new OracleParameter("motivo" + currentPageIndex, item.Motivo));
                        sqlCommand.Parameters.Add(new OracleParameter("numeroItem" + currentPageIndex, item.NumeroItem));
                        sqlCommand.Parameters.Add(new OracleParameter("obs" + currentPageIndex, item.Obs));
                        sqlCommand.Parameters.Add(new OracleParameter("ordemConf" + currentPageIndex, item.OrdemConf));
                        sqlCommand.Parameters.Add(new OracleParameter("idPedidoOperacaoEntrada" + currentPageIndex, pedido.Id));
                        sqlCommand.Parameters.Add(new OracleParameter("idProduto" + currentPageIndex, item.IdProduto));
                        sqlCommand.Parameters.Add(new OracleParameter("quantidade" + currentPageIndex, item.Quantidade));
                        sqlCommand.Parameters.Add(new OracleParameter("separado" + currentPageIndex, item.Separado));
                    }

                    sqlCommand.CommandText += " SELECT * FROM dual";

                    sqlCommand.ExecuteNonQuery();
                }
            }
        }

        private void SalvarLotesDoPedido(IList<PedidoItemOperacaoEntrada> itensDoPedidoEmMemoria, PedidoOperacaoEntrada pedido, OracleConnection sqlConnection, OracleTransaction transaction)
        {
            IDictionary<long, long> itensDoPedidoGravados = new Dictionary<long, long>();

            using (OracleCommand sqlCommand = new OracleCommand())
            {
                sqlCommand.Transaction = transaction;
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = $"SELECT ID_PEDIDO_ITEM_OPER_ENTRADA, ID_PRODUTO FROM PEDIDO_ITEM_OPERACAO_ENTRADA WHERE ID_PEDIDO_OPERACAO_ENTRADA = :idPedido";
                sqlCommand.Parameters.Add(new OracleParameter("idPedido", pedido.Id));

                using (OracleDataReader reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        itensDoPedidoGravados.Add((long)reader["ID_PRODUTO"], (long)reader["ID_PEDIDO_ITEM_OPER_ENTRADA"]);
                    }
                }
            }

            IList<PedidoItemLoteOperacaoEntrada> lotesDoPedidoEmMemoria = new List<PedidoItemLoteOperacaoEntrada>();

            foreach (PedidoItemOperacaoEntrada itemEmMemoria in itensDoPedidoEmMemoria)
            {
                long idItemGravado = itensDoPedidoGravados[itemEmMemoria.IdProduto];

                foreach (PedidoItemLoteOperacaoEntrada lote in itemEmMemoria.PedidoItemLoteOperacaoEntradaList)
                {
                    lote.PedidoItem = new PedidoItemOperacaoEntrada { Id = idItemGravado };
                    lotesDoPedidoEmMemoria.Add(lote);
                }
            }

            int pageSize = 100;
            int pages = lotesDoPedidoEmMemoria.Count / pageSize;
            int lastPageSize = pageSize;

            if (lotesDoPedidoEmMemoria.Count % pageSize > 0)
            {
                pages++;
                lastPageSize = lotesDoPedidoEmMemoria.Count % pageSize;
            }

            for (int page = 0; page < pages; page++)
            {
                using (OracleCommand sqlCommand = new OracleCommand())
                {
                    sqlCommand.Transaction = transaction;
                    sqlCommand.Connection = sqlConnection;

                    sqlCommand.CommandText = "INSERT ALL ";

                    int currentPageSize = (page == (pages - 1)) ? lastPageSize : pageSize;

                    for (int currentPageIndex = 0; currentPageIndex < currentPageSize; currentPageIndex++)
                    {
                        PedidoItemLoteOperacaoEntrada lote = lotesDoPedidoEmMemoria.ElementAt((page * pageSize) + currentPageIndex);

                        string commandText = $"INTO PEDIDO_ITEM_LOTE_OPER_ENTRADA " +
                            "(CLASSIFICACAO_DESTINO, CLASSIFICACAO_ORIGEM, COD_SITUACAO, FABRICACAO, ID_CLASSIFICACAO_DEST, ID_CLASSIFICACAO_DEST_POLO, ID_CLASSIFICACAO_ORIGEM, ID_CLASSIFICACAO_ORIGEM_POLO, LOTE, LOTE_ORIG, NUMERO_LPN, OBS, ID_PEDIDO_ITEM_OPER_ENTRADA, QUANTIDADE, VALIDADE, VALIDADE_ORIG) " +
                            $"VALUES (:classificacaoDestino{currentPageIndex}, :classificacaoOrigem{currentPageIndex}, :codSituacao{currentPageIndex}, :fabricacao{currentPageIndex}, :idClassificacaoDest{currentPageIndex}, :idClassificacaoDestPolo{currentPageIndex}, :idClassificacaoOrigem{currentPageIndex}, :idClassificacaoOrigemPolo{currentPageIndex}, :lote{currentPageIndex}, :loteOrig{currentPageIndex}, :numeroLpn{currentPageIndex}, :obs{currentPageIndex}, :idPedidoItemOperacaoEntrada{currentPageIndex}, :quantidade{currentPageIndex}, :validade{currentPageIndex}, :validadeOrig{currentPageIndex}) ";

                        sqlCommand.CommandText += commandText;

                        sqlCommand.Parameters.Add(new OracleParameter("classificacaoDestino" + currentPageIndex, lote.ClassificacaoDestino));
                        sqlCommand.Parameters.Add(new OracleParameter("classificacaoOrigem" + currentPageIndex, lote.ClassificacaoOrigem));
                        sqlCommand.Parameters.Add(new OracleParameter("codSituacao" + currentPageIndex, lote.CodSituacao));
                        sqlCommand.Parameters.Add(new OracleParameter("fabricacao" + currentPageIndex, lote.Fabricacao));
                        sqlCommand.Parameters.Add(new OracleParameter("idClassificacaoDest" + currentPageIndex, lote.IdClassificacaoDestino));
                        sqlCommand.Parameters.Add(new OracleParameter("idClassificacaoDestPolo" + currentPageIndex, lote.IdClassificacaoDestinoPolo));
                        sqlCommand.Parameters.Add(new OracleParameter("idClassificacaoOrigem" + currentPageIndex, lote.IdClassificacaoOrigem));
                        sqlCommand.Parameters.Add(new OracleParameter("idClassificacaoOrigemPolo" + currentPageIndex, lote.IdClassificacaoOrigemPolo));
                        sqlCommand.Parameters.Add(new OracleParameter("lote" + currentPageIndex, lote.Lote));
                        sqlCommand.Parameters.Add(new OracleParameter("loteOrig" + currentPageIndex, lote.LoteOrig));
                        sqlCommand.Parameters.Add(new OracleParameter("numeroLpn" + currentPageIndex, lote.NumeroLpn));
                        sqlCommand.Parameters.Add(new OracleParameter("obs" + currentPageIndex, lote.Obs));
                        sqlCommand.Parameters.Add(new OracleParameter("idPedidoItemOperacaoEntrada" + currentPageIndex, lote.PedidoItem.Id));
                        sqlCommand.Parameters.Add(new OracleParameter("quantidade" + currentPageIndex, lote.Quantidade));
                        sqlCommand.Parameters.Add(new OracleParameter("validade" + currentPageIndex, lote.Validade));
                        sqlCommand.Parameters.Add(new OracleParameter("validadeOrig" + currentPageIndex, lote.ValidadeOrig));
                    }

                    sqlCommand.CommandText += " SELECT * FROM dual";

                    sqlCommand.ExecuteNonQuery();
                }
            }
        }

        private void VerificarExistenciaDoPedidoComMesmoStatus(PedidoOperacaoEntrada pedido)
        {
            var pedidoJaCadastradoComSituacaoMovimentadoOuConcluido = Items.Any(x => x.CodPedidoPolo == pedido.CodPedidoPolo &&
                                                                                     (x.CodSituacao == (int)Situacao.MOVIMENTADO || x.CodSituacao == (int)Situacao.CONCLUIDO || x.CodSituacao == (int)Situacao.CANCELADO) &&
                                                                                     x.CodSituacao == pedido.CodSituacao);
            if (pedidoJaCadastradoComSituacaoMovimentadoOuConcluido)
                throw new ValidationException(MensagensValidacao.PEDIDO_OPERACAO_ENTRADA_COM_MESMO_STATUS_JA_EXISTE);
        }

        private void AssociarEntidadeAoPedido(PedidoOperacaoEntrada pedido)
        {
            Entidade entidade = _entidadeRepositorio.Items.Where(x => x.CodEntidadePolo == pedido.Entidade.CodEntidadePolo).FirstOrDefault();

            if (entidade == null)
                throw new ValidationException(string.Format(MensagensValidacao.ENTIDADE_NAO_EXISTE, pedido.Entidade.CodEntidadePolo));

            pedido.Entidade = entidade;
            pedido.IdEntidade = entidade.Id;
        }

        private void AssociarProdutosAosItensDoPedido(IList<PedidoItemOperacaoEntrada> itensDoPedidoEmMemoria)
        {
            List<string> codProdutoPoloList = itensDoPedidoEmMemoria
                            .Select(item => item.Produto.CodProdutoPolo)
                            .ToList();

            List<Produto> produtosDoPedido = _produtoRepositorio.Items.Where(produto => codProdutoPoloList.Contains(produto.CodProdutoPolo)).ToList();

            foreach (PedidoItemOperacaoEntrada itemDoPedido in itensDoPedidoEmMemoria)
            {
                Produto produtoDoItem = produtosDoPedido.Where(produto => produto.CodProdutoPolo == itemDoPedido.Produto.CodProdutoPolo).FirstOrDefault();

                if (produtoDoItem == null)
                    throw new ValidationException(string.Format(MensagensValidacao.PRODUTO_NAO_EXISTE, itemDoPedido.Produto.CodProdutoPolo));
                else
                {
                    itemDoPedido.Produto = produtoDoItem;
                    itemDoPedido.IdProduto = produtoDoItem.Id;
                }
            }
        }
    }
}
