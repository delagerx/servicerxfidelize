﻿using Entidades;
using Persistencia.EntityFramework;

namespace Persistencia
{
    public interface IMensagemIntegracaoRepository : IRepository<MensagemIntegracao>
    {
        void Atualizar(MensagemIntegracao mensagemIntegracaoAtualizada);
    }

    public class MensagemIntegracaoRepository : Repository<MensagemIntegracao>, IMensagemIntegracaoRepository
    {
        public MensagemIntegracaoRepository(DatabaseContext context) : base(context)
        {
        }

        public void Atualizar(MensagemIntegracao mensagemIntegracaoAtualizada)
        {
            MensagemIntegracao mensagemIntegracao = Get(mensagemIntegracaoAtualizada.Id);

            mensagemIntegracao.CodRejeicao = mensagemIntegracaoAtualizada.CodRejeicao;
            mensagemIntegracao.DataProcessamento = mensagemIntegracaoAtualizada.DataProcessamento;
            mensagemIntegracao.MotivoRejeicao = mensagemIntegracaoAtualizada.MotivoRejeicao;
            mensagemIntegracao.Rejeitado = mensagemIntegracaoAtualizada.Rejeitado;

            Update(mensagemIntegracao);
        }
    }
}
