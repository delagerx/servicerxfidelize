﻿using System;
using System.Collections.Generic;
using Entidades;
using Persistencia.EntityFramework;
using System.Linq;

namespace Persistencia
{
    public interface IProdutoRepository : IRepository<Produto>
    {
    }

    public class ProdutoRepository : Repository<Produto>, IProdutoRepository
    {
        public ProdutoRepository(DatabaseContext context) : base(context)
        {
        }

        public new Produto Get(long id)
        {
            lock (_locker)
            {
                Produto produto = _context.Set<Produto>().AsNoTracking().SingleOrDefault(x => x.Id == id);

                produto.ProdutoCodBarraList.ToList();
                produto.ProdutoCurvaList.ToList();
                produto.ProdutoInsumoList.ToList();
                produto.ProdutoKitList.ToList();
                produto.ProdutoShelfLifeList.ToList();

                return produto;
            }
        }
    }
}
