﻿using Entidades;
using Persistencia.EntityFramework;
using System.Linq;

namespace Persistencia
{
    public interface IEntidadeRepository : IRepository<Entidade>
    {
    }
    public class EntidadeRepository : Repository<Entidade>, IEntidadeRepository
    {
        public EntidadeRepository(DatabaseContext context) : base(context)
        {
        }

        public new Entidade Get(long id)
        {
            lock (_locker)
            {
                return _context.Set<Entidade>().AsNoTracking().SingleOrDefault(x => x.Id == id);
            }
        }
    }
}
