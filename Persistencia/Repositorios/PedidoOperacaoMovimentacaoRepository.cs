﻿using Entidades;
using Persistencia.EntityFramework;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System;
using Oracle.ManagedDataAccess.Client;
using System.ComponentModel.DataAnnotations;
using Mensagens.Resources;
using Entidades.Enum;
using System.Data;

namespace Persistencia
{
    public interface IPedidoOperacaoMovimentacaoRepository : IRepository<PedidoOperacaoMovimentacao>
    {
        long Salvar(PedidoOperacaoMovimentacao pedido);

    }
    public class PedidoOperacaoMovimentacaoRepository : Repository<PedidoOperacaoMovimentacao>, IPedidoOperacaoMovimentacaoRepository
    {
        private IProdutoRepository _produtoRepositorio;
        private IEntidadeRepository _entidadeRepositorio;

        public PedidoOperacaoMovimentacaoRepository(DatabaseContext context, IProdutoRepository produtoRepositorio, IEntidadeRepository entidadeRepositorio) : base(context)
        {
            _produtoRepositorio = produtoRepositorio;
            _entidadeRepositorio = entidadeRepositorio;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public new PedidoOperacaoMovimentacao Get(long id)
        {
            var pedidoOperacaoMovimentacao = Items
                                        .Include("PedidoItemOperacaoMovimentacaoList.Produto")
                                        .Include("PedidoItemOperacaoMovimentacaoList.PedidoItemLoteOperacaoMovimentacaoList")
                                        .Include("PedidoVolumeOperacaoMovimentacaoList")
                                        .Include("Entidade")
                                        .Where(pedido => pedido.Id == id).FirstOrDefault();

            if (pedidoOperacaoMovimentacao == null)
                return null;

            var quantidadeDeItens = pedidoOperacaoMovimentacao.PedidoItemOperacaoMovimentacaoList.Count;
            var tamanhoDaPaginaDeItens = quantidadeDeItens / 1000.0;

            // Ao tentar realizar o SELECT de um pedido com muitos itens (> 6000), o Entity Framework 
            // utiliza o operador APPLY, que não é compatível com Oracle 11, causando uma exceção na hora 
            // da leitura. O trecho de código a seguir, faz com que a consulta de itens seja feita de 
            // 1000 em 1000 de modo a forçar o Entity Framework a não utilizar o operador APPLY.
            for (var paginaDeItens = 0; paginaDeItens < tamanhoDaPaginaDeItens; paginaDeItens++)
            {
                List<long> idProdutoList = pedidoOperacaoMovimentacao
                .PedidoItemOperacaoMovimentacaoList.Skip(paginaDeItens * 1000).Take(1000)
                .Select(item => item.IdProduto)
                .ToList();

                _produtoRepositorio.Items
					.Include("ProdutoCurvaList")
					.Include("ProdutoKitList")
					.Include("ProdutoInsumoList")
					.Include("ProdutoShelfLifeList")
					.Include("ProdutoCodBarraList")
					.Where(x => idProdutoList.Contains(x.Id))
					.ToList();
			}

			foreach (var item in pedidoOperacaoMovimentacao.PedidoItemOperacaoMovimentacaoList)
			{
				item.Produto.ProdutoCurvaList.ToList();
				item.Produto.ProdutoKitList.ToList();
				item.Produto.ProdutoInsumoList.ToList();
				item.Produto.ProdutoShelfLifeList.ToList();
				item.Produto.ProdutoCodBarraList.ToList();
			}

            return pedidoOperacaoMovimentacao;
        }

        public long Salvar(PedidoOperacaoMovimentacao pedido)
        {
            OracleConnection sqlConnection = (OracleConnection)_context.Database.Connection;

            if (sqlConnection.State != ConnectionState.Open)
            {
                sqlConnection.Close();
                sqlConnection.Open();
            }

            using (OracleTransaction transaction = sqlConnection.BeginTransaction())
            {
                try
                {
                    _context.Database.UseTransaction(transaction);

                    VerificarExistenciaDoPedidoComMesmoStatus(pedido);

                    AssociarEntidadeAoPedido(pedido);

                    IList<PedidoItemOperacaoMovimentacao> itensDoPedidoEmMemoria = pedido.PedidoItemOperacaoMovimentacaoList.ToList();
                    pedido.PedidoItemOperacaoMovimentacaoList.Clear();

                    IList<PedidoVolumeOperacaoMovimentacao> volumesDoPedidoEmMemoria = pedido.PedidoVolumeOperacaoMovimentacaoList.ToList();
                    pedido.PedidoVolumeOperacaoMovimentacaoList.Clear();

                    IList<PedidoItemVolumeOperacaoMovimentacao> itensDoVolumeDoPedidoEmMemoria = pedido.PedidoItemVolumeOperacaoMovimentacaoList.ToList();
                    pedido.PedidoItemVolumeOperacaoMovimentacaoList.Clear();

                    pedido.Id = Add(pedido);

                    AssociarProdutosAosItensDoPedido(itensDoPedidoEmMemoria);
                    AssociarProdutosAosItensDosVolumesDoPedido(itensDoVolumeDoPedidoEmMemoria);

                    SalvarItensDoPedido(itensDoPedidoEmMemoria, pedido, sqlConnection, transaction);
                    SalvarLotesDoPedido(itensDoPedidoEmMemoria, pedido, sqlConnection, transaction);
                    SalvarVolumesDoPedido(volumesDoPedidoEmMemoria, pedido, sqlConnection, transaction);
                    SalvarItensDosVolumesDoPedido(itensDoVolumeDoPedidoEmMemoria, pedido, sqlConnection, transaction);

                    transaction.Commit();

                    return pedido.Id;
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        private void SalvarItensDoPedido(IList<PedidoItemOperacaoMovimentacao> itens, PedidoOperacaoMovimentacao pedido, OracleConnection sqlConnection, OracleTransaction transaction)
        {
            int pageSize = 100;
            int pages = itens.Count / pageSize;
            int lastPageSize = pageSize;

            if (itens.Count % pageSize > 0)
            {
                pages++;
                lastPageSize = itens.Count % pageSize;
            }

            for (int page = 0; page < pages; page++)
            {
                using (OracleCommand sqlCommand = new OracleCommand())
                {
                    sqlCommand.Transaction = transaction;
                    sqlCommand.Connection = sqlConnection;

                    sqlCommand.CommandText = "INSERT ALL ";

                    int currentPageSize = (page == (pages - 1)) ? lastPageSize : pageSize;

                    for (int currentPageIndex = 0; currentPageIndex < currentPageSize; currentPageIndex++)
                    {
                        PedidoItemOperacaoMovimentacao item = itens.ElementAt((page * pageSize) + currentPageIndex);

                        string commandText = $"INTO PEDIDO_ITEM_OPERACAO_SAIDA (COD_MOTIVO, MOTIVO, NUMERO_ITEM, OBS, ORDEM_CONF, ID_PEDIDO_OPERACAO_SAIDA, ID_PRODUTO, QUANTIDADE, SEPARADO) VALUES (:codMotivo{currentPageIndex}, :motivo{currentPageIndex}, :numeroItem{currentPageIndex}, :obs{currentPageIndex}, :ordemConf{currentPageIndex}, :idPedidoOperacaoMovimentacao{currentPageIndex}, :idProduto{currentPageIndex}, :quantidade{currentPageIndex}, :separado{currentPageIndex}) ";

                        sqlCommand.CommandText += commandText;

                        sqlCommand.Parameters.Add(new OracleParameter("codMotivo" + currentPageIndex, item.CodMotivo));
                        sqlCommand.Parameters.Add(new OracleParameter("motivo" + currentPageIndex, item.Motivo));
                        sqlCommand.Parameters.Add(new OracleParameter("numeroItem" + currentPageIndex, item.NumeroItem));
                        sqlCommand.Parameters.Add(new OracleParameter("obs" + currentPageIndex, item.Obs));
                        sqlCommand.Parameters.Add(new OracleParameter("ordemConf" + currentPageIndex, item.OrdemConf));
                        sqlCommand.Parameters.Add(new OracleParameter("idPedidoOperacaoMovimentacao" + currentPageIndex, pedido.Id));
                        sqlCommand.Parameters.Add(new OracleParameter("idProduto" + currentPageIndex, item.IdProduto));
                        sqlCommand.Parameters.Add(new OracleParameter("quantidade" + currentPageIndex, item.Quantidade));
                        sqlCommand.Parameters.Add(new OracleParameter("separado" + currentPageIndex, item.Separado));
                    }

                    sqlCommand.CommandText += " SELECT * FROM dual";

                    sqlCommand.ExecuteNonQuery();
                }
            }
        }

        private void SalvarLotesDoPedido(IList<PedidoItemOperacaoMovimentacao> itensDoPedidoEmMemoria, PedidoOperacaoMovimentacao pedido, OracleConnection sqlConnection, OracleTransaction transaction)
        {
            IDictionary<long, long> itensDoPedidoGravados = new Dictionary<long, long>();

            using (OracleCommand sqlCommand = new OracleCommand())
            {
                sqlCommand.Transaction = transaction;
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = $"SELECT ID_PEDIDO_ITEM_OPERACAO_SAIDA, ID_PRODUTO FROM PEDIDO_ITEM_OPERACAO_SAIDA WHERE ID_PEDIDO_OPERACAO_SAIDA = :idPedido";
                sqlCommand.Parameters.Add(new OracleParameter("idPedido", pedido.Id));

                using (OracleDataReader reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        itensDoPedidoGravados.Add((long)reader["ID_PRODUTO"], (long)reader["ID_PEDIDO_ITEM_OPERACAO_SAIDA"]);
                    }
                }
            }

            IList<PedidoItemLoteOperacaoMovimentacao> lotesDoPedidoEmMemoria = new List<PedidoItemLoteOperacaoMovimentacao>();

            foreach (PedidoItemOperacaoMovimentacao itemEmMemoria in itensDoPedidoEmMemoria)
            {
                long idItemGravado = itensDoPedidoGravados[itemEmMemoria.IdProduto];

                foreach (PedidoItemLoteOperacaoMovimentacao lote in itemEmMemoria.PedidoItemLoteOperacaoMovimentacaoList)
                {
                    lote.PedidoItem = new PedidoItemOperacaoMovimentacao { Id = idItemGravado };
                    lotesDoPedidoEmMemoria.Add(lote);
                }
            }

            int pageSize = 100;
            int pages = lotesDoPedidoEmMemoria.Count / pageSize;
            int lastPageSize = pageSize;

            if (lotesDoPedidoEmMemoria.Count % pageSize > 0)
            {
                pages++;
                lastPageSize = lotesDoPedidoEmMemoria.Count % pageSize;
            }

            for (int page = 0; page < pages; page++)
            {
                using (OracleCommand sqlCommand = new OracleCommand())
                {
                    sqlCommand.Transaction = transaction;
                    sqlCommand.Connection = sqlConnection;

                    sqlCommand.CommandText = "INSERT ALL ";

                    int currentPageSize = (page == (pages - 1)) ? lastPageSize : pageSize;

                    for (int currentPageIndex = 0; currentPageIndex < currentPageSize; currentPageIndex++)
                    {
                        PedidoItemLoteOperacaoMovimentacao lote = lotesDoPedidoEmMemoria.ElementAt((page * pageSize) + currentPageIndex);

                        string commandText = $"INTO PEDIDO_ITEM_LOTE_OPER_SAIDA " +
                            "(CLASSIFICACAO_DESTINO, CLASSIFICACAO_ORIGEM, COD_SITUACAO, FABRICACAO, ID_CLASSIFICACAO_DEST, ID_CLASSIFICACAO_DEST_POLO, ID_CLASSIFICACAO_ORIGEM, ID_CLASSIFICACAO_ORIGEM_POLO, LOTE, LOTE_ORIG, OBS, ID_PEDIDO_ITEM_OPERACAO_SAIDA, QUANTIDADE, VALIDADE, VALIDADE_ORIG) " +
                            $"VALUES (:classificacaoDestino{currentPageIndex}, :classificacaoOrigem{currentPageIndex}, :codSituacao{currentPageIndex}, :fabricacao{currentPageIndex}, :idClassificacaoDest{currentPageIndex}, :idClassificacaoDestPolo{currentPageIndex}, :idClassificacaoOrigem{currentPageIndex}, :idClassificacaoOrigemPolo{currentPageIndex}, :lote{currentPageIndex}, :loteOrig{currentPageIndex}, :obs{currentPageIndex}, :idPedidoItemOperacaoMovimentacao{currentPageIndex}, :quantidade{currentPageIndex}, :validade{currentPageIndex}, :validadeOrig{currentPageIndex}) ";

                        sqlCommand.CommandText += commandText;

                        sqlCommand.Parameters.Add(new OracleParameter("classificacaoDestino" + currentPageIndex, lote.ClassificacaoDestino));
                        sqlCommand.Parameters.Add(new OracleParameter("classificacaoOrigem" + currentPageIndex, lote.ClassificacaoOrigem));
                        sqlCommand.Parameters.Add(new OracleParameter("codSituacao" + currentPageIndex, lote.CodSituacao));
                        sqlCommand.Parameters.Add(new OracleParameter("fabricacao" + currentPageIndex, lote.Fabricacao));
                        sqlCommand.Parameters.Add(new OracleParameter("idClassificacaoDest" + currentPageIndex, lote.IdClassificacaoDestino));
                        sqlCommand.Parameters.Add(new OracleParameter("idClassificacaoDestPolo" + currentPageIndex, lote.IdClassificacaoDestinoPolo));
                        sqlCommand.Parameters.Add(new OracleParameter("idClassificacaoOrigem" + currentPageIndex, lote.IdClassificacaoOrigem));
                        sqlCommand.Parameters.Add(new OracleParameter("idClassificacaoOrigemPolo" + currentPageIndex, lote.IdClassificacaoOrigemPolo));
                        sqlCommand.Parameters.Add(new OracleParameter("lote" + currentPageIndex, lote.Lote));
                        sqlCommand.Parameters.Add(new OracleParameter("loteOrig" + currentPageIndex, lote.LoteOrig));
                        sqlCommand.Parameters.Add(new OracleParameter("obs" + currentPageIndex, lote.Obs));
                        sqlCommand.Parameters.Add(new OracleParameter("idPedidoItemOperacaoMovimentacao" + currentPageIndex, lote.PedidoItem.Id));
                        sqlCommand.Parameters.Add(new OracleParameter("quantidade" + currentPageIndex, lote.Quantidade));
                        sqlCommand.Parameters.Add(new OracleParameter("validade" + currentPageIndex, lote.Validade));
                        sqlCommand.Parameters.Add(new OracleParameter("validadeOrig" + currentPageIndex, lote.ValidadeOrig));
                    }

                    sqlCommand.CommandText += " SELECT * FROM dual";

                    sqlCommand.ExecuteNonQuery();
                }
            }
        }

        private void SalvarVolumesDoPedido(IList<PedidoVolumeOperacaoMovimentacao> volumes, PedidoOperacaoMovimentacao pedido, OracleConnection sqlConnection, OracleTransaction transaction)
        {
            int pageSize = 100;
            int pages = volumes.Count / pageSize;
            int lastPageSize = pageSize;

            if (volumes.Count % pageSize > 0)
            {
                pages++;
                lastPageSize = volumes.Count % pageSize;
            }

            for (int page = 0; page < pages; page++)
            {
                using (OracleCommand sqlCommand = new OracleCommand())
                {
                    sqlCommand.Transaction = transaction;
                    sqlCommand.Connection = sqlConnection;

                    sqlCommand.CommandText = "INSERT ALL ";

                    int currentPageSize = (page == (pages - 1)) ? lastPageSize : pageSize;

                    for (int currentPageIndex = 0; currentPageIndex < currentPageSize; currentPageIndex++)
                    {
                        PedidoVolumeOperacaoMovimentacao volume = volumes.ElementAt((page * pageSize) + currentPageIndex);

                        string commandText = $"INTO PEDIDO_VOLUME_OPERACAO_SAIDA (DATA_VOLUME_EMBARCADO, LACRE, ID_PEDIDO_OPERACAO_SAIDA, TIPO_CAIXA, VOLUME, LINHA_SEPARACAO) VALUES (:dataVolumeEmbarcado{currentPageIndex}, :lacre{currentPageIndex}, :idPedidoOperacaoMovimentacao{currentPageIndex}, :tipoCaixa{currentPageIndex}, :volume{currentPageIndex}, :linhaSeparacao{currentPageIndex}) ";

                        sqlCommand.CommandText += commandText;

                        sqlCommand.Parameters.Add(new OracleParameter("dataVolumeEmbarcado" + currentPageIndex, volume.DataVolumeEmbarcado));
                        sqlCommand.Parameters.Add(new OracleParameter("lacre" + currentPageIndex, volume.Lacre));
                        sqlCommand.Parameters.Add(new OracleParameter("idPedidoOperacaoMovimentacao" + currentPageIndex, pedido.Id));
                        sqlCommand.Parameters.Add(new OracleParameter("tipoCaixa" + currentPageIndex, volume.TipoCaixa));
                        sqlCommand.Parameters.Add(new OracleParameter("volume" + currentPageIndex, volume.Volume));
                        sqlCommand.Parameters.Add(new OracleParameter("linhaSeparacao" + currentPageIndex, volume.LinhaSeparacao));
                    }

                    sqlCommand.CommandText += " SELECT * FROM dual";

                    sqlCommand.ExecuteNonQuery();
                }
            }
        }

        private void SalvarItensDosVolumesDoPedido(IList<PedidoItemVolumeOperacaoMovimentacao> itensVolume, PedidoOperacaoMovimentacao pedido, OracleConnection sqlConnection, OracleTransaction transaction)
        {
            IDictionary<long, long> volumesDoPedidoGravados = new Dictionary<long, long>();

            using (OracleCommand sqlCommand = new OracleCommand())
            {
                sqlCommand.Transaction = transaction;
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = $"SELECT ID_PEDIDO_VOLUME_OPER_MOV, VOLUME FROM PEDIDO_VOLUME_OPERACAO_MOV WHERE ID_PEDIDO_OPERACAO_MOV = :idPedido";
                sqlCommand.Parameters.Add(new OracleParameter("idPedido", pedido.Id));

                using (OracleDataReader reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        volumesDoPedidoGravados.Add((long)reader["VOLUME"], (long)reader["ID_PEDIDO_VOLUME_OPER_MOV"]);
                    }
                }
            }

            foreach (PedidoItemVolumeOperacaoMovimentacao itemVolumeEmMemoria in itensVolume)
            {
                long idVolumeGravado = volumesDoPedidoGravados[itemVolumeEmMemoria.Volume];

                itemVolumeEmMemoria.IdPedidoVolume = idVolumeGravado;
            }

            int pageSize = 100;
            int pages = itensVolume.Count / pageSize;
            int lastPageSize = pageSize;

            if (itensVolume.Count % pageSize > 0)
            {
                pages++;
                lastPageSize = itensVolume.Count % pageSize;
            }

            for (int page = 0; page < pages; page++)
            {
                using (OracleCommand sqlCommand = new OracleCommand())
                {
                    sqlCommand.Transaction = transaction;
                    sqlCommand.Connection = sqlConnection;

                    sqlCommand.CommandText = "INSERT ALL ";

                    int currentPageSize = (page == (pages - 1)) ? lastPageSize : pageSize;

                    for (int currentPageIndex = 0; currentPageIndex < currentPageSize; currentPageIndex++)
                    {
                        PedidoItemVolumeOperacaoMovimentacao itemVolume = itensVolume.ElementAt((page * pageSize) + currentPageIndex);

                        string commandText = $"INTO PEDIDO_ITEM_VOLUME_OPER_MOV (" +
                                                 $"ID_PEDIDO_VOLUME_OPER_MOV, " +
                                                 $"ID_PEDIDO_OPERACAO_MOV, " +
                                                 $"ID_PRODUTO, " +
                                                 $"VOLUME, " +
                                                 $"LOTE_ORIGEM, " +
                                                 $"LOTE_DEST, " +
                                                 $"QUANTIDADE) " +
                                             $"VALUES ( :idPedidoOperacaoMov{currentPageIndex}, " +
                                             $"         :idPedidoVolumeOperMov{currentPageIndex}, " +
                                             $"         :idProduto{currentPageIndex}, " +
                                             $"         :volume{currentPageIndex}, " +
                                             $"         :loteOrigem{currentPageIndex}, " +
                                             $"         :loteDest{currentPageIndex}, " +
                                             $"         :quantidade{currentPageIndex}) ";

                        sqlCommand.CommandText += commandText;

                        sqlCommand.Parameters.Add(new OracleParameter("idPedidoOperacaoMov" + currentPageIndex, pedido.Id));
                        sqlCommand.Parameters.Add(new OracleParameter("idPedidoVolumeOperMov" + currentPageIndex, itemVolume.IdPedidoVolume));
                        sqlCommand.Parameters.Add(new OracleParameter("idProduto" + currentPageIndex, itemVolume.IdProduto));
                        sqlCommand.Parameters.Add(new OracleParameter("volume" + currentPageIndex, itemVolume.Volume));
                        sqlCommand.Parameters.Add(new OracleParameter("loteOrigem" + currentPageIndex, itemVolume.LoteOrigem));
                        sqlCommand.Parameters.Add(new OracleParameter("loteDest" + currentPageIndex, itemVolume.LoteDestino));
                        sqlCommand.Parameters.Add(new OracleParameter("quantidade" + currentPageIndex, itemVolume.Quantidade));
                    }

                    sqlCommand.CommandText += " SELECT * FROM dual";

                    sqlCommand.ExecuteNonQuery();
                }
            }
        }

        private void VerificarExistenciaDoPedidoComMesmoStatus(PedidoOperacaoMovimentacao pedido)
        {
            var pedidoJaCadastradoComSituacaoMovimentadoOuConcluido = Items.Any(x => x.CodPedidoPolo == pedido.CodPedidoPolo && (x.CodSituacao == (int)Situacao.MOVIMENTADO || x.CodSituacao == (int)Situacao.CONCLUIDO));

            if (pedidoJaCadastradoComSituacaoMovimentadoOuConcluido)
                throw new ValidationException(MensagensValidacao.PEDIDO_OPERACAO_SAIDA_COM_MESMO_STATUS_JA_EXISTE);
        }

        private void AssociarEntidadeAoPedido(PedidoOperacaoMovimentacao pedido)
        {
            Entidade entidade = _entidadeRepositorio.Items.Where(x => x.CodEntidadePolo == pedido.Entidade.CodEntidadePolo).FirstOrDefault();

            if (entidade == null)
                throw new ValidationException(string.Format(MensagensValidacao.ENTIDADE_NAO_EXISTE, pedido.Entidade.CodEntidadePolo));

            pedido.Entidade = entidade;
            pedido.IdEntidade = entidade.Id;
        }

        private void AssociarProdutosAosItensDoPedido(IList<PedidoItemOperacaoMovimentacao> itensDoPedidoEmMemoria)
        {
            List<string> codProdutoPoloList = itensDoPedidoEmMemoria
                            .Select(item => item.Produto.CodProdutoPolo)
                            .ToList();

            List<Produto> produtosDoPedido = _produtoRepositorio.Items.Where(produto => codProdutoPoloList.Contains(produto.CodProdutoPolo)).ToList();

            foreach (PedidoItemOperacaoMovimentacao itemDoPedido in itensDoPedidoEmMemoria)
            {
                Produto produtoDoItem = produtosDoPedido.Where(produto => produto.CodProdutoPolo == itemDoPedido.Produto.CodProdutoPolo).FirstOrDefault();

                if (produtoDoItem == null)
                    throw new ValidationException(string.Format(MensagensValidacao.PRODUTO_NAO_EXISTE, itemDoPedido.Produto.CodProdutoPolo));
                else
                {
                    itemDoPedido.Produto = produtoDoItem;
                    itemDoPedido.IdProduto = produtoDoItem.Id;
                }
            }
        }

        private void AssociarProdutosAosItensDosVolumesDoPedido(IList<PedidoItemVolumeOperacaoMovimentacao> listaItemVolumeDoPedidoEmMemoria)
        {
            List<string> codProdutoPoloList = listaItemVolumeDoPedidoEmMemoria
                            .Select(item => item.Produto.CodProdutoPolo)
                            .ToList();

            List<Produto> produtosDoPedido = _produtoRepositorio.Items.Where(produto => codProdutoPoloList.Contains(produto.CodProdutoPolo)).ToList();

            foreach (PedidoItemVolumeOperacaoMovimentacao itemVolumeDoPedido in listaItemVolumeDoPedidoEmMemoria)
            {
                Produto produtoDoItemVolume = produtosDoPedido.Where(produto => produto.CodProdutoPolo == itemVolumeDoPedido.Produto.CodProdutoPolo).FirstOrDefault();

                if (produtoDoItemVolume == null)
                    throw new ValidationException(string.Format(MensagensValidacao.PRODUTO_NAO_EXISTE, itemVolumeDoPedido.Produto.CodProdutoPolo));
                else
                {
                    itemVolumeDoPedido.Produto = produtoDoItemVolume;
                    itemVolumeDoPedido.IdProduto = produtoDoItemVolume.Id;
                }
            }
        }
    }
}
