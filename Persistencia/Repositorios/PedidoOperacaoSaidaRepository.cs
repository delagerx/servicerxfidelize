﻿using Entidades;
using Entidades.Enum;
using Mensagens.Resources;
using Oracle.ManagedDataAccess.Client;
using Persistencia.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Persistencia
{
    public interface IPedidoOperacaoSaidaRepository : IRepository<PedidoOperacaoSaida>
    {
        long Salvar(PedidoOperacaoSaida pedido);
    }

    public class PedidoOperacaoSaidaRepository : Repository<PedidoOperacaoSaida>, IPedidoOperacaoSaidaRepository
    {
        private IProdutoRepository _produtoRepositorio;
        private IEntidadeRepository _entidadeRepositorio;

        public PedidoOperacaoSaidaRepository(DatabaseContext context, IProdutoRepository produtoRepositorio, IEntidadeRepository entidadeRepositorio) : base(context)
        {
            _produtoRepositorio = produtoRepositorio;
            _entidadeRepositorio = entidadeRepositorio;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public new PedidoOperacaoSaida Get(long id)
        {
            // exibe log do Entity Framework no console
            //_context.Database.Log = Console.Write;

            var pedidoOperacaoSaida = Items
                                        .Include("PedidoItemOperacaoSaidaList.Produto")
                                        .Include("PedidoItemOperacaoSaidaList.PedidoItemLoteOperacaoSaidaList")
                                        .Include("PedidoVolumeOperacaoSaidaList")
                                        .Include("PedidoItemKitOperacaoSaidaList")
                                        .Include("PedidoItemVolumeOperacaoSaidaList")
                                        .Include("PedidoOperacaoSaidaEndereco")
                                        .Include("Entidade")
                                        .Where(pedido => pedido.Id == id).FirstOrDefault();

            if (pedidoOperacaoSaida == null)
                return null;

            var quantidadeDeItens = pedidoOperacaoSaida.PedidoItemOperacaoSaidaList.Count;
            var tamanhoDaPaginaDeItens = quantidadeDeItens / 1000.0;

            // Ao tentar realizar o SELECT de um pedido com muitos itens (> 6000), o Entity Framework 
            // utiliza o operador APPLY, que não é compatível com Oracle 11, causando uma exceção na hora 
            // da leitura. O trecho de código a seguir, faz com que a consulta de itens seja feita de 
            // 1000 em 1000 de modo a forçar o Entity Framework a não utilizar o operador APPLY.
            for (var paginaDeItens = 0; paginaDeItens < tamanhoDaPaginaDeItens; paginaDeItens++)
            {
                List<long> idProdutoList = pedidoOperacaoSaida
                .PedidoItemOperacaoSaidaList.Skip(paginaDeItens * 1000).Take(1000)
                .Select(item => item.IdProduto)
                .ToList();
				
                _produtoRepositorio.Items
                    .Include("ProdutoCurvaList")
                    .Include("ProdutoKitList")
                    .Include("ProdutoInsumoList")
                    .Include("ProdutoShelfLifeList")
                    .Include("ProdutoCodBarraList")
                    .Where(x => idProdutoList.Contains(x.Id))
                    .ToList();
            }

			foreach (var item in pedidoOperacaoSaida.PedidoItemOperacaoSaidaList)
			{
				item.Produto.ProdutoCurvaList.ToList();
				item.Produto.ProdutoKitList.ToList();
				item.Produto.ProdutoInsumoList.ToList();
				item.Produto.ProdutoShelfLifeList.ToList();
				item.Produto.ProdutoCodBarraList.ToList();
			}

            return pedidoOperacaoSaida;
        }

        public long Salvar(PedidoOperacaoSaida pedido)
        {
            OracleConnection sqlConnection = (OracleConnection)_context.Database.Connection;

            if (sqlConnection.State != ConnectionState.Open)
            {
                sqlConnection.Close();
                sqlConnection.Open();
            }

            using (OracleTransaction transaction = sqlConnection.BeginTransaction())
            {
                try
                {
                    _context.Database.UseTransaction(transaction);

                    VerificarExistenciaDoPedidoComMesmoStatus(pedido);

                    AssociarEntidadeAoPedido(pedido);

                    IList<Produto> produtosAssociadosAoPedido = ObterProdutosAssociadosAoPedido(pedido);

                    IList<PedidoItemOperacaoSaida> pedidoItemListaEmMemoria = pedido.PedidoItemOperacaoSaidaList.ToList();
                    pedido.PedidoItemOperacaoSaidaList.Clear();

                    IList<PedidoVolumeOperacaoSaida> pedidoVolumeListaEmMemoria = pedido.PedidoVolumeOperacaoSaidaList.ToList();
                    pedido.PedidoVolumeOperacaoSaidaList.Clear();

                    IList<PedidoItemVolumeOperacaoSaida> pedidoItemVolumeListaEmMemoria = pedido.PedidoItemVolumeOperacaoSaidaList.ToList();
                    pedido.PedidoItemVolumeOperacaoSaidaList.Clear();

                    IList<PedidoItemKitOperacaoSaida> pedidoItemKitListaEmMemoria = pedido.PedidoItemKitOperacaoSaidaList.ToList();
                    pedido.PedidoItemKitOperacaoSaidaList.Clear();

                    pedido.Id = Add(pedido);

                    AssociarProdutosPedidoItem(pedidoItemListaEmMemoria, produtosAssociadosAoPedido);
                    AssociarProdutosPedidoItemKit(pedidoItemKitListaEmMemoria, produtosAssociadosAoPedido);
                    AssociarProdutoKitsPedidoItemKit(pedidoItemKitListaEmMemoria, produtosAssociadosAoPedido);
                    AssociarProdutosPedidoItemVolume(pedidoItemVolumeListaEmMemoria, produtosAssociadosAoPedido);

                    SalvarListaDePedidoItem(pedidoItemListaEmMemoria, pedido, sqlConnection, transaction);

                    IDictionary<long, long> listaDePedidoItemGravados  = ObterListaDePedidoItemGravados(pedido, sqlConnection, transaction);

                    SalvarListaDePedidoItemLote(pedidoItemListaEmMemoria, listaDePedidoItemGravados, pedido, sqlConnection, transaction);
                    SalvarListaDePedidoVolume(pedidoVolumeListaEmMemoria, pedido, sqlConnection, transaction);
                    SalvarListaDePedidoItemVolume(pedidoItemVolumeListaEmMemoria, pedido, sqlConnection, transaction);
                    SalvarListaDePedidoItemKit(pedidoItemKitListaEmMemoria, pedido, sqlConnection, transaction);

                    transaction.Commit();

                    return pedido.Id;
                }
                catch(Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        private IList<Produto> ObterProdutosAssociadosAoPedido(PedidoOperacaoSaida pedido)
        {
            List<string> codProdutoPoloList = new List<string>();

            var produtosAssociadosPedidoItem = pedido.PedidoItemOperacaoSaidaList.Select(pedidoItem => pedidoItem.Produto.CodProdutoPolo).ToList();
            var produtosAssociadosPedidoItemKit = pedido.PedidoItemKitOperacaoSaidaList.Select(pedidoItemKit => pedidoItemKit.Produto.CodProdutoPolo).ToList();
            var produtosAssociadosPedidoItemVolume = pedido.PedidoItemVolumeOperacaoSaidaList.Select(pedidoItemVolume => pedidoItemVolume.Produto.CodProdutoPolo).ToList();

            codProdutoPoloList.AddRange(produtosAssociadosPedidoItem);
            codProdutoPoloList.AddRange(produtosAssociadosPedidoItemKit);
            codProdutoPoloList.AddRange(produtosAssociadosPedidoItemVolume);

            codProdutoPoloList = codProdutoPoloList.Distinct().ToList();

            List<Produto> produtosAssociadosAoPedido = _produtoRepositorio.Items.Where(produto => codProdutoPoloList.Contains(produto.CodProdutoPolo)).ToList();

            return produtosAssociadosAoPedido;
        }

        private IDictionary<long, long> ObterListaDePedidoItemGravados(PedidoOperacaoSaida pedido, OracleConnection sqlConnection, OracleTransaction transaction)
        {
            IDictionary<long, long> itensDoPedidoGravados = new Dictionary<long, long>();

            using (OracleCommand sqlCommand = new OracleCommand())
            {
                sqlCommand.Transaction = transaction;
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = $"SELECT ID_PEDIDO_ITEM_OPERACAO_SAIDA, ID_PRODUTO FROM PEDIDO_ITEM_OPERACAO_SAIDA WHERE ID_PEDIDO_OPERACAO_SAIDA = :idPedido";
                sqlCommand.Parameters.Add(new OracleParameter("idPedido", pedido.Id));

                using (OracleDataReader reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        itensDoPedidoGravados.Add((long)reader["ID_PRODUTO"], (long)reader["ID_PEDIDO_ITEM_OPERACAO_SAIDA"]);
                    }
                }
            }

            return itensDoPedidoGravados;
        }

        private void VerificarExistenciaDoPedidoComMesmoStatus(PedidoOperacaoSaida pedido)
        {
            var pedidoJaCadastradoComSituacaoMovimentadoOuConcluido = Items.Any(x => x.CodPedidoPolo == pedido.CodPedidoPolo && 
                (x.CodSituacao == (int)Situacao.MOVIMENTADO || x.CodSituacao == (int)Situacao.CONCLUIDO || x.CodSituacao == (int)Situacao.CANCELADO) && 
                x.CodSituacao == pedido.CodSituacao);

            if (pedidoJaCadastradoComSituacaoMovimentadoOuConcluido)
                throw new ValidationException(MensagensValidacao.PEDIDO_OPERACAO_SAIDA_COM_MESMO_STATUS_JA_EXISTE);
        }

        private void AssociarEntidadeAoPedido(PedidoOperacaoSaida pedido)
        {
            Entidade entidade = _entidadeRepositorio.Items.Where(x => x.CodEntidadePolo == pedido.Entidade.CodEntidadePolo).FirstOrDefault();

            if (entidade == null)
                throw new ValidationException(string.Format(MensagensValidacao.ENTIDADE_NAO_EXISTE, pedido.Entidade.CodEntidadePolo));

            pedido.Entidade = entidade;
            pedido.IdEntidade = entidade.Id;
        }

        private void AssociarProdutosPedidoItem(IList<PedidoItemOperacaoSaida> pedidoItemListaEmMemoria, IList<Produto> produtosAssociadosAoPedido)
        {
            foreach (PedidoItemOperacaoSaida pedidoItem in pedidoItemListaEmMemoria)
            {
                Produto produtoAssociadoAoPedidoItem = produtosAssociadosAoPedido.Where(produto => produto.CodProdutoPolo == pedidoItem.Produto.CodProdutoPolo).FirstOrDefault();

                if (produtoAssociadoAoPedidoItem == null)
                    throw new ValidationException(string.Format(MensagensValidacao.PRODUTO_NAO_EXISTE, pedidoItem.Produto.CodProdutoPolo));
                else
                {
                    pedidoItem.Produto = produtoAssociadoAoPedidoItem;
                    pedidoItem.IdProduto = produtoAssociadoAoPedidoItem.Id;
                }
            }
        }

        private void AssociarProdutosPedidoItemVolume(IList<PedidoItemVolumeOperacaoSaida> pedidoItemVolumeListaEmMemoria, IList<Produto> produtosAssociadosAoPedido)
        {
            foreach (PedidoItemVolumeOperacaoSaida pedidoItemVolume in pedidoItemVolumeListaEmMemoria)
            {
                Produto produtoAssociadoAoPedidoItemVolume = produtosAssociadosAoPedido.Where(produto => produto.CodProdutoPolo == pedidoItemVolume.Produto.CodProdutoPolo).FirstOrDefault();

                if (produtoAssociadoAoPedidoItemVolume == null)
                    throw new ValidationException(string.Format(MensagensValidacao.PRODUTO_NAO_EXISTE, pedidoItemVolume.Produto.CodProdutoPolo));
                else
                {
                    pedidoItemVolume.Produto = produtoAssociadoAoPedidoItemVolume;
                    pedidoItemVolume.IdProduto = produtoAssociadoAoPedidoItemVolume.Id;
                }
            }
        }

        private void AssociarProdutosPedidoItemKit(IList<PedidoItemKitOperacaoSaida> pedidoItemKitListaEmMemoria, IList<Produto> produtosAssociadosAoPedido)
        {
            foreach (PedidoItemKitOperacaoSaida pedidoItemKit in pedidoItemKitListaEmMemoria)
            {
                Produto produtoAssociadoAoPedidoItemKit = produtosAssociadosAoPedido.Where(produto => produto.CodProdutoPolo == pedidoItemKit.Produto.CodProdutoPolo).FirstOrDefault();

                if (produtoAssociadoAoPedidoItemKit == null)
                    throw new ValidationException(string.Format(MensagensValidacao.PRODUTO_NAO_EXISTE, pedidoItemKit.Produto.CodProdutoPolo));
                else
                {
                    pedidoItemKit.Produto = produtoAssociadoAoPedidoItemKit;
                    pedidoItemKit.IdProduto = produtoAssociadoAoPedidoItemKit.Id;
                }
            }
        }

        private void AssociarProdutoKitsPedidoItemKit(IList<PedidoItemKitOperacaoSaida> pedidoItemKitListaEmMemoria, IList<Produto> produtosAssociadosAoPedido)
        {
            foreach (PedidoItemKitOperacaoSaida pedidoItemKit in pedidoItemKitListaEmMemoria)
            {
                Produto produtoAssociadoAoPedidoItemKit = produtosAssociadosAoPedido.Where(produto => produto.CodProdutoPolo == pedidoItemKit.ProdutoKit.CodProdutoPolo).FirstOrDefault();

                if (produtoAssociadoAoPedidoItemKit == null)
                    throw new ValidationException(string.Format(MensagensValidacao.PRODUTO_NAO_EXISTE, pedidoItemKit.ProdutoKit.CodProdutoPolo));
                else
                {
                    pedidoItemKit.ProdutoKit = produtoAssociadoAoPedidoItemKit;
                    pedidoItemKit.IdProdutoKit = produtoAssociadoAoPedidoItemKit.Id;
                }
            }
        }

        private void SalvarListaDePedidoItem(IList<PedidoItemOperacaoSaida> listaPedidoItemEmMemoria, PedidoOperacaoSaida pedido, OracleConnection sqlConnection, OracleTransaction transaction)
        {
            int pageSize = 100;
            int pages = listaPedidoItemEmMemoria.Count / pageSize;
            int lastPageSize = pageSize;

            if (listaPedidoItemEmMemoria.Count % pageSize > 0)
            {
                pages++;
                lastPageSize = listaPedidoItemEmMemoria.Count % pageSize;
            }

            for (int page = 0; page < pages; page++)
            {
                using (OracleCommand sqlCommand = new OracleCommand())
                {
                    sqlCommand.Transaction = transaction;
                    sqlCommand.Connection = sqlConnection;

                    sqlCommand.CommandText = "INSERT ALL ";

                    int currentPageSize = (page == (pages - 1)) ? lastPageSize : pageSize;

                    for (int currentPageIndex = 0; currentPageIndex < currentPageSize; currentPageIndex++)
                    {
                        PedidoItemOperacaoSaida item = listaPedidoItemEmMemoria.ElementAt((page * pageSize) + currentPageIndex);

                        string commandText = $"INTO PEDIDO_ITEM_OPERACAO_SAIDA (COD_MOTIVO, MOTIVO, NUMERO_ITEM, OBS, ORDEM_CONF, ID_PEDIDO_OPERACAO_SAIDA, ID_PRODUTO, QUANTIDADE, SEPARADO) VALUES (:codMotivo{currentPageIndex}, :motivo{currentPageIndex}, :numeroItem{currentPageIndex}, :obs{currentPageIndex}, :ordemConf{currentPageIndex}, :idPedidoOperacaoSaida{currentPageIndex}, :idProduto{currentPageIndex}, :quantidade{currentPageIndex}, :separado{currentPageIndex}) ";

                        sqlCommand.CommandText += commandText;

                        sqlCommand.Parameters.Add(new OracleParameter("codMotivo" + currentPageIndex, item.CodMotivo));
                        sqlCommand.Parameters.Add(new OracleParameter("motivo" + currentPageIndex, item.Motivo));
                        sqlCommand.Parameters.Add(new OracleParameter("numeroItem" + currentPageIndex, item.NumeroItem));
                        sqlCommand.Parameters.Add(new OracleParameter("obs" + currentPageIndex, item.Obs));
                        sqlCommand.Parameters.Add(new OracleParameter("ordemConf" + currentPageIndex, item.OrdemConf));
                        sqlCommand.Parameters.Add(new OracleParameter("idPedidoOperacaoSaida" + currentPageIndex, pedido.Id));
                        sqlCommand.Parameters.Add(new OracleParameter("idProduto" + currentPageIndex, item.IdProduto));
                        sqlCommand.Parameters.Add(new OracleParameter("quantidade" + currentPageIndex, item.Quantidade));
                        sqlCommand.Parameters.Add(new OracleParameter("separado" + currentPageIndex, item.Separado));
                    }

                    sqlCommand.CommandText += " SELECT * FROM dual";

                    sqlCommand.ExecuteNonQuery();
                }
            }
        }

        private void SalvarListaDePedidoItemKit(IList<PedidoItemKitOperacaoSaida> listaPedidoItemKitEmMemoria, PedidoOperacaoSaida pedido, OracleConnection sqlConnection, OracleTransaction transaction)
        {

            int pageSize = 100;
            int pages = listaPedidoItemKitEmMemoria.Count / pageSize;
            int lastPageSize = pageSize;

            if (listaPedidoItemKitEmMemoria.Count % pageSize > 0)
            {
                pages++;
                lastPageSize = listaPedidoItemKitEmMemoria.Count % pageSize;
            }

            for (int page = 0; page < pages; page++)
            {
                using (OracleCommand sqlCommand = new OracleCommand())
                {
                    sqlCommand.Transaction = transaction;
                    sqlCommand.Connection = sqlConnection;

                    sqlCommand.CommandText = "INSERT ALL ";

                    int currentPageSize = (page == (pages - 1)) ? lastPageSize : pageSize;

                    for (int currentPageIndex = 0; currentPageIndex < currentPageSize; currentPageIndex++)
                    {
                        PedidoItemKitOperacaoSaida itemKit = listaPedidoItemKitEmMemoria.ElementAt((page * pageSize) + currentPageIndex);

                        string commandText = $"INTO pedido_item_kit_operacao_saida PEDIDO_ITEM_KIT_OPER_SAIDA (" +
                                                 $"ID_PEDIDO_OPERACAO_SAIDA, " +
                                                 $"ID_PRODUTO, " +
                                                 $"QUANTIDADE, " +
                                                 $"SEPARADO, " +
                                                 $"COD_MOTIVO, " +
                                                 $"MOTIVO, " +
                                                 $"ID_PRODUTO_KIT, " +
                                                 $"CORTA_KIT_TOTAL) " +
                                             $"VALUES ( :idPedidoOperacaoSaida{currentPageIndex}, " +
                                             $"         :idProduto{currentPageIndex}, " +
                                             $"         :quantidade{currentPageIndex}, " +
                                             $"         :separado{currentPageIndex}, " +
                                             $"         :codMotivo{currentPageIndex}, " +
                                             $"         :motivo{currentPageIndex}, " +
                                             $"         :idProdutoKit{currentPageIndex}, " +
                                             $"         :cortaKitTotal{currentPageIndex}) ";

                        sqlCommand.CommandText += commandText;

                        sqlCommand.Parameters.Add(new OracleParameter("idPedidoOperacaoSaida" + currentPageIndex, pedido.Id));
                        sqlCommand.Parameters.Add(new OracleParameter("idProduto" + currentPageIndex, itemKit.IdProduto));
                        sqlCommand.Parameters.Add(new OracleParameter("quantidade" + currentPageIndex, itemKit.Quantidade));
                        sqlCommand.Parameters.Add(new OracleParameter("separado" + currentPageIndex, itemKit.Separado));
                        sqlCommand.Parameters.Add(new OracleParameter("codMotivo" + currentPageIndex, itemKit.CodMotivo));
                        sqlCommand.Parameters.Add(new OracleParameter("motivo" + currentPageIndex, itemKit.Motivo));
                        sqlCommand.Parameters.Add(new OracleParameter("idProdutoKit" + currentPageIndex, itemKit.IdProdutoKit));
                        sqlCommand.Parameters.Add(new OracleParameter("cortaKitTotal" + currentPageIndex, itemKit.CortaKitTotal));
                    }

                    sqlCommand.CommandText += " SELECT * FROM dual";

                    sqlCommand.ExecuteNonQuery();
                }
            }
        }

        private void SalvarListaDePedidoItemLote(IList<PedidoItemOperacaoSaida> listaPedidoItemLoteEmMemoria, IDictionary<long, long> listaDePedidoItemGravados, PedidoOperacaoSaida pedido, OracleConnection sqlConnection, OracleTransaction transaction)
        {
            IList<PedidoItemLoteOperacaoSaida> lotesDoPedidoEmMemoria = new List<PedidoItemLoteOperacaoSaida>();

            foreach (PedidoItemOperacaoSaida itemEmMemoria in listaPedidoItemLoteEmMemoria)
            {
                long idItemGravado = listaDePedidoItemGravados[itemEmMemoria.IdProduto];

                foreach (PedidoItemLoteOperacaoSaida lote in itemEmMemoria.PedidoItemLoteOperacaoSaidaList)
                {
                    lote.PedidoItem = new PedidoItemOperacaoSaida { Id = idItemGravado };
                    lotesDoPedidoEmMemoria.Add(lote);
                }
            }

            int pageSize = 100;
            int pages = lotesDoPedidoEmMemoria.Count / pageSize;
            int lastPageSize = pageSize;

            if (lotesDoPedidoEmMemoria.Count % pageSize > 0)
            {
                pages++;
                lastPageSize = lotesDoPedidoEmMemoria.Count % pageSize;
            }

            for (int page = 0; page < pages; page++)
            {
                using (OracleCommand sqlCommand = new OracleCommand())
                {
                    sqlCommand.Transaction = transaction;
                    sqlCommand.Connection = sqlConnection;

                    sqlCommand.CommandText = "INSERT ALL ";

                    int currentPageSize = (page == (pages - 1)) ? lastPageSize : pageSize;

                    for (int currentPageIndex = 0; currentPageIndex < currentPageSize; currentPageIndex++)
                    {
                        PedidoItemLoteOperacaoSaida lote = lotesDoPedidoEmMemoria.ElementAt((page * pageSize) + currentPageIndex);

                        string commandText = $"INTO PEDIDO_ITEM_LOTE_OPER_SAIDA " +
                            "(CLASSIFICACAO_DESTINO, CLASSIFICACAO_ORIGEM, COD_SITUACAO, FABRICACAO, ID_CLASSIFICACAO_DEST, ID_CLASSIFICACAO_DEST_POLO, ID_CLASSIFICACAO_ORIGEM, ID_CLASSIFICACAO_ORIGEM_POLO, LOTE, LOTE_ORIG, OBS, ID_PEDIDO_ITEM_OPERACAO_SAIDA, QUANTIDADE, VALIDADE, VALIDADE_ORIG) " +
                            $"VALUES (:classificacaoDestino{currentPageIndex}, :classificacaoOrigem{currentPageIndex}, :codSituacao{currentPageIndex}, :fabricacao{currentPageIndex}, :idClassificacaoDest{currentPageIndex}, :idClassificacaoDestPolo{currentPageIndex}, :idClassificacaoOrigem{currentPageIndex}, :idClassificacaoOrigemPolo{currentPageIndex}, :lote{currentPageIndex}, :loteOrig{currentPageIndex}, :obs{currentPageIndex}, :idPedidoItemOperacaoSaida{currentPageIndex}, :quantidade{currentPageIndex}, :validade{currentPageIndex}, :validadeOrig{currentPageIndex}) ";

                        sqlCommand.CommandText += commandText;

                        sqlCommand.Parameters.Add(new OracleParameter("classificacaoDestino" + currentPageIndex, lote.ClassificacaoDestino));
                        sqlCommand.Parameters.Add(new OracleParameter("classificacaoOrigem" + currentPageIndex, lote.ClassificacaoOrigem));
                        sqlCommand.Parameters.Add(new OracleParameter("codSituacao" + currentPageIndex, lote.CodSituacao));
                        sqlCommand.Parameters.Add(new OracleParameter("fabricacao" + currentPageIndex, lote.Fabricacao));
                        sqlCommand.Parameters.Add(new OracleParameter("idClassificacaoDest" + currentPageIndex, lote.IdClassificacaoDestino));
                        sqlCommand.Parameters.Add(new OracleParameter("idClassificacaoDestPolo" + currentPageIndex, lote.IdClassificacaoDestinoPolo));
                        sqlCommand.Parameters.Add(new OracleParameter("idClassificacaoOrigem" + currentPageIndex, lote.IdClassificacaoOrigem));
                        sqlCommand.Parameters.Add(new OracleParameter("idClassificacaoOrigemPolo" + currentPageIndex, lote.IdClassificacaoOrigemPolo));
                        sqlCommand.Parameters.Add(new OracleParameter("lote" + currentPageIndex, lote.Lote));
                        sqlCommand.Parameters.Add(new OracleParameter("loteOrig" + currentPageIndex, lote.LoteOrig));
                        sqlCommand.Parameters.Add(new OracleParameter("obs" + currentPageIndex, lote.Obs));
                        sqlCommand.Parameters.Add(new OracleParameter("idPedidoItemOperacaoSaida" + currentPageIndex, lote.PedidoItem.Id));
                        sqlCommand.Parameters.Add(new OracleParameter("quantidade" + currentPageIndex, lote.Quantidade));
                        sqlCommand.Parameters.Add(new OracleParameter("validade" + currentPageIndex, lote.Validade));
                        sqlCommand.Parameters.Add(new OracleParameter("validadeOrig" + currentPageIndex, lote.ValidadeOrig));
                    }

                    sqlCommand.CommandText += " SELECT * FROM dual";

                    sqlCommand.ExecuteNonQuery();
                }
            }
        }

        private void SalvarListaDePedidoItemVolume(IList<PedidoItemVolumeOperacaoSaida> listaPedidoItemVolumeEmMemoria, PedidoOperacaoSaida pedido, OracleConnection sqlConnection, OracleTransaction transaction)
        {
            IDictionary<long, long> volumesDoPedidoGravados = new Dictionary<long, long>();

            using (OracleCommand sqlCommand = new OracleCommand())
            {
                sqlCommand.Transaction = transaction;
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = $"SELECT ID_PEDIDO_VOLUME_OPER_SAIDA, VOLUME FROM PEDIDO_VOLUME_OPERACAO_SAIDA WHERE ID_PEDIDO_OPERACAO_SAIDA = :idPedido";
                sqlCommand.Parameters.Add(new OracleParameter("idPedido", pedido.Id));

                using (OracleDataReader reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        volumesDoPedidoGravados.Add((long)reader["VOLUME"], (long)reader["ID_PEDIDO_VOLUME_OPER_SAIDA"]);
                    }
                }
            }

            foreach (PedidoItemVolumeOperacaoSaida itemVolumeEmMemoria in listaPedidoItemVolumeEmMemoria)
            {
                long idVolumeGravado = volumesDoPedidoGravados[itemVolumeEmMemoria.Volume];

                itemVolumeEmMemoria.IdPedidoVolume = idVolumeGravado;
            }

            int pageSize = 100;
            int pages = listaPedidoItemVolumeEmMemoria.Count / pageSize;
            int lastPageSize = pageSize;

            if (listaPedidoItemVolumeEmMemoria.Count % pageSize > 0)
            {
                pages++;
                lastPageSize = listaPedidoItemVolumeEmMemoria.Count % pageSize;
            }

            for (int page = 0; page < pages; page++)
            {
                using (OracleCommand sqlCommand = new OracleCommand())
                {
                    sqlCommand.Transaction = transaction;
                    sqlCommand.Connection = sqlConnection;

                    sqlCommand.CommandText = "INSERT ALL ";

                    int currentPageSize = (page == (pages - 1)) ? lastPageSize : pageSize;

                    for (int currentPageIndex = 0; currentPageIndex < currentPageSize; currentPageIndex++)
                    {
                        PedidoItemVolumeOperacaoSaida volumeItem = listaPedidoItemVolumeEmMemoria.ElementAt((page * pageSize) + currentPageIndex);

                        string commandText = $"INTO PEDIDO_ITEM_VOLUME_OPER_SAIDA (" +
                                                 $"ID_PEDIDO_OPERACAO_SAIDA, " +
                                                 $"ID_PEDIDO_VOLUME_OPER_SAIDA, " +
                                                 $"ID_PRODUTO, " +
                                                 $"VOLUME, " +
                                                 $"LOTE_ORIGEM, " +
                                                 $"LOTE_DEST, " +
                                                 $"QUANTIDADE) " +
                                             $"VALUES ( :idPedidoOperacaoSaida{currentPageIndex}, " +
                                             $"         :idPedidoVolumeOperSaida{currentPageIndex}, " +
                                             $"         :idProduto{currentPageIndex}, " +
                                             $"         :volume{currentPageIndex}, " +
                                             $"         :loteOrigem{currentPageIndex}, " +
                                             $"         :loteDest{currentPageIndex}, " +
                                             $"         :quantidade{currentPageIndex}) ";

                        sqlCommand.CommandText += commandText;

                        sqlCommand.Parameters.Add(new OracleParameter("idPedidoOperacaoSaida" + currentPageIndex, pedido.Id));
                        sqlCommand.Parameters.Add(new OracleParameter("idPedidoVolumeOperSaida" + currentPageIndex, volumeItem.IdPedidoVolume));
                        sqlCommand.Parameters.Add(new OracleParameter("idProduto" + currentPageIndex, volumeItem.IdProduto));
                        sqlCommand.Parameters.Add(new OracleParameter("volume" + currentPageIndex, volumeItem.Volume));
                        sqlCommand.Parameters.Add(new OracleParameter("loteOrigem" + currentPageIndex, volumeItem.LoteOrigem));
                        sqlCommand.Parameters.Add(new OracleParameter("loteDest" + currentPageIndex, volumeItem.LoteDestino));
                        sqlCommand.Parameters.Add(new OracleParameter("quantidade" + currentPageIndex, volumeItem.Quantidade));
                    }

                    sqlCommand.CommandText += " SELECT * FROM dual";

                    sqlCommand.ExecuteNonQuery();
                }
            }
        }

        private void SalvarListaDePedidoVolume(IList<PedidoVolumeOperacaoSaida> listaPedidoVolumeEmMemoria, PedidoOperacaoSaida pedido, OracleConnection sqlConnection, OracleTransaction transaction)
        {
            int pageSize = 100;
            int pages = listaPedidoVolumeEmMemoria.Count / pageSize;
            int lastPageSize = pageSize;

            if (listaPedidoVolumeEmMemoria.Count % pageSize > 0)
            {
                pages++;
                lastPageSize = listaPedidoVolumeEmMemoria.Count % pageSize;
            }

            for (int page = 0; page < pages; page++)
            {
                using (OracleCommand sqlCommand = new OracleCommand())
                {
                    sqlCommand.Transaction = transaction;
                    sqlCommand.Connection = sqlConnection;

                    sqlCommand.CommandText = "INSERT ALL ";

                    int currentPageSize = (page == (pages - 1)) ? lastPageSize : pageSize;

                    for (int currentPageIndex = 0; currentPageIndex < currentPageSize; currentPageIndex++)
                    {
                        PedidoVolumeOperacaoSaida volume = listaPedidoVolumeEmMemoria.ElementAt((page * pageSize) + currentPageIndex);

                        string commandText = $"INTO PEDIDO_VOLUME_OPERACAO_SAIDA (DATA_VOLUME_EMBARCADO, LACRE, ID_PEDIDO_OPERACAO_SAIDA, TIPO_CAIXA, VOLUME, LINHA_SEPARACAO) VALUES (:dataVolumeEmbarcado{currentPageIndex}, :lacre{currentPageIndex}, :idPedidoOperacaoSaida{currentPageIndex}, :tipoCaixa{currentPageIndex}, :volume{currentPageIndex}, :linhaSeparacao{currentPageIndex}) ";

                        sqlCommand.CommandText += commandText;

                        sqlCommand.Parameters.Add(new OracleParameter("dataVolumeEmbarcado" + currentPageIndex, volume.DataVolumeEmbarcado));
                        sqlCommand.Parameters.Add(new OracleParameter("lacre" + currentPageIndex, volume.Lacre));
                        sqlCommand.Parameters.Add(new OracleParameter("idPedidoOperacaoSaida" + currentPageIndex, pedido.Id));
                        sqlCommand.Parameters.Add(new OracleParameter("tipoCaixa" + currentPageIndex, volume.TipoCaixa));
                        sqlCommand.Parameters.Add(new OracleParameter("volume" + currentPageIndex, volume.Volume));
                        sqlCommand.Parameters.Add(new OracleParameter("linhaSeparacao" + currentPageIndex, volume.LinhaSeparacao));
                    }

                    sqlCommand.CommandText += " SELECT * FROM dual";

                    sqlCommand.ExecuteNonQuery();
                }
            }
        }
    }
}
