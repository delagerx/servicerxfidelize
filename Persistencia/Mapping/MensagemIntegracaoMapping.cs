﻿using Entidades;
using System.Data.Entity.ModelConfiguration;

namespace Persistencia.Mapping
{
    public class MensagemIntegracaoMapping : EntityTypeConfiguration<MensagemIntegracao>
    {
        public MensagemIntegracaoMapping()
        {
            ToTable("mensagem_integracao".ToUpper());

            HasKey(x => x.Id);

            Ignore(x => x.DbLinkId);
            Ignore(x => x.DbLinkInicioProcessamento);

            Property(x => x.Id).HasColumnName("id_mensagem_integracao".ToUpper());
            Property(x => x.Origem).HasColumnName("origem".ToUpper());
            Property(x => x.TabelaRelacionada).HasColumnName("tabela_relacionada".ToUpper());
            Property(x => x.IdTabelaRelacionada).HasColumnName("id_tabela_relacionada".ToUpper());
            Property(x => x.DataCadastro).HasColumnName("data_cadastro".ToUpper());
            Property(x => x.DataProcessamento).HasColumnName("data_processamento".ToUpper());
            Property(x => x.DataAnaliseProcessamento).HasColumnName("data_analise_processamento".ToUpper());
            Property(x => x.CodRejeicao).HasColumnName("cod_rejeicao".ToUpper());
            Property(x => x.MotivoRejeicao).HasColumnName("motivo_rejeicao".ToUpper());
            Property(x => x.Rejeitado).HasColumnName("rejeitado".ToUpper());
        }
    }
}
