﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class PedidoVolumeMapping<T> : EntityTypeConfiguration<T> where T : PedidoVolume
    {
        public PedidoVolumeMapping()
        {
            HasKey(x => x.Id);

            Property(x => x.Volume).HasColumnName("volume".ToUpper());
            Property(x => x.Lacre).HasColumnName("lacre".ToUpper());
            Property(x => x.DataVolumeEmbarcado).HasColumnName("data_volume_embarcado".ToUpper());
            Property(x => x.TipoCaixa).HasColumnName("tipo_caixa".ToUpper());
            Property(x => x.LinhaSeparacao).HasColumnName("linha_separacao".ToUpper());
        }
    }
}
