﻿using Entidades;

namespace Persistencia.Mapping
{
    public class PedidoVolumeOperacaoSaidaMapping : PedidoVolumeMapping<PedidoVolumeOperacaoSaida>
    {
        public PedidoVolumeOperacaoSaidaMapping()
        {
            ToTable("pedido_volume_operacao_saida".ToUpper());

            HasRequired(x => x.Pedido)
                .WithMany(x => x.PedidoVolumeOperacaoSaidaList)
                .Map(map => map.MapKey("id_pedido_operacao_saida".ToUpper()));

            Property(x => x.Id).HasColumnName("id_pedido_volume_oper_saida".ToUpper());
            Property(x => x.PesoCaixa).HasColumnName("peso_caixa".ToUpper());
            Property(x => x.PesoItensVolume).HasColumnName("peso_itens_volume".ToUpper());
        }
    }
}
