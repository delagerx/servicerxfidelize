﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class PedidoVolumeOperacaoMovimentacaoMapping : PedidoVolumeMapping<PedidoVolumeOperacaoMovimentacao>
    {
        public PedidoVolumeOperacaoMovimentacaoMapping()
        {
            ToTable("pedido_volume_operacao_mov".ToUpper());

            HasRequired(x => x.Pedido)
                .WithMany(x => x.PedidoVolumeOperacaoMovimentacaoList)
                .Map(map => map.MapKey("id_pedido_operacao_mov".ToUpper()));

            Property(x => x.Id).HasColumnName("id_pedido_volume_oper_mov".ToUpper());
        }
    }
}
