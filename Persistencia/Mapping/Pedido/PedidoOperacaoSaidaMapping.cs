﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class PedidoOperacaoSaidaMapping : PedidoMapping<PedidoOperacaoSaida>
    {
        public PedidoOperacaoSaidaMapping()
        {
            ToTable("pedido_operacao_saida".ToUpper());

            Property(x => x.Id).HasColumnName("id_pedido_operacao_saida".ToUpper());
            Property(x => x.CodTransportadoraPolo).HasColumnName("cod_transportadora_polo".ToUpper());
            Property(x => x.CodOnda).HasColumnName("cod_onda".ToUpper());
            Property(x => x.DiasEntrega).HasColumnName("dias_entrega".ToUpper());
            Property(x => x.Modalidade).HasColumnName("modalidade".ToUpper());
            Property(x => x.ObsEntrega).HasColumnName("obs_entrega".ToUpper());
        }
    }
}
