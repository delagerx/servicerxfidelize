﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class PedidoOperacaoEntradaMapping : PedidoMapping<PedidoOperacaoEntrada>
    {
        public PedidoOperacaoEntradaMapping()
        {
            ToTable("pedido_operacao_entrada".ToUpper());

            Property(x => x.Id).HasColumnName("id_pedido_operacao_entrada".ToUpper());
        }
    }
}
