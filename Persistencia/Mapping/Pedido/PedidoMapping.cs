﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class PedidoMapping<T> : EntityTypeConfiguration<T> where T : Pedido
    {
        public PedidoMapping()
        {
            HasKey(x => x.Id);
            
            Ignore(x => x.IdWms);

            HasOptional(x => x.Entidade).WithMany().HasForeignKey(x => x.IdEntidade);

            Property(x => x.CodPedidoPolo).HasColumnName("cod_pedido_polo".ToUpper());
            Property(x => x.CodOperacaoLogisticaPolo).HasColumnName("cod_operacao_logistica_polo".ToUpper());
            Property(x => x.IdEntidade).HasColumnName("id_entidade".ToUpper());
            Property(x => x.CodPrenota).HasColumnName("cod_prenota".ToUpper());
            Property(x => x.Operacao).HasColumnName("operacao".ToUpper());
            Property(x => x.CodSituacao).HasColumnName("cod_situacao".ToUpper());
            Property(x => x.Situacao).HasColumnName("situacao".ToUpper());
            Property(x => x.Digitacao).HasColumnName("digitacao".ToUpper());
            Property(x => x.DataLiberacao).HasColumnName("data_liberacao".ToUpper());
            Property(x => x.Prenota).HasColumnName("prenota".ToUpper());
            Property(x => x.EncerramentoPN).HasColumnName("encerramento_PN".ToUpper());
            Property(x => x.QuemDigitou).HasColumnName("quem_digitou".ToUpper());
            Property(x => x.QuemLiberou).HasColumnName("quem_liberou".ToUpper());
            Property(x => x.CodOrigemPed).HasColumnName("cod_origem_ped".ToUpper());
            Property(x => x.OrigemPed).HasColumnName("origem_ped".ToUpper());
            Property(x => x.Volumes).HasColumnName("volumes".ToUpper());
            Property(x => x.ValorTotal).HasColumnName("valor_total".ToUpper());
            Property(x => x.ObsPN).HasColumnName("obs_PN".ToUpper());
            Property(x => x.Suboperacao).HasColumnName("suboperacao".ToUpper());
            Property(x => x.MotivoSuboperacao).HasColumnName("motivo_suboperacao".ToUpper());
            Property(x => x.NumeroTitulo).HasColumnName("numero_titulo".ToUpper());
            Property(x => x.SerieNf).HasColumnName("serie_nf".ToUpper());
            Property(x => x.CodTituloPolo).HasColumnName("cod_titulo_polo".ToUpper());
            Property(x => x.Peso).HasColumnName("peso".ToUpper());
            Property(x => x.QtdItens).HasColumnName("qtdItens".ToUpper());
            Property(x => x.Obs).HasColumnName("obs".ToUpper());
            Property(x => x.IdCancelamento).HasColumnName("id_cancelamento".ToUpper());
            Property(x => x.DataSolicitacaoCancelamento).HasColumnName("data_solicitacao_cancelamento".ToUpper());
            Property(x => x.DataConfirmacaoCancelamento).HasColumnName("data_confirmacao_cancelamento".ToUpper());
        }
    }
}
