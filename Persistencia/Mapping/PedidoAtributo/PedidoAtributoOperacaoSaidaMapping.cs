﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class PedidoAtributoOperacaoSaidaMapping : PedidoAtributoMapping<PedidoAtributoOperacaoSaida>
    {
        public PedidoAtributoOperacaoSaidaMapping()
        {
            ToTable("pedido_atributo_operacao_saida".ToUpper());

            HasRequired(x => x.Pedido)
                .WithMany(x => x.PedidoAtributoOperacaoSaidaList)
                .Map(map => map.MapKey("id_pedido_operacao_saida".ToUpper()));
                
            Property(x => x.Id).HasColumnName("id_atributo_operacao_saida".ToUpper());
        }
    }
}
