﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class PedidoAtributoMapping<T> : EntityTypeConfiguration<T> where T : PedidoAtributo
    {
        public PedidoAtributoMapping()
        {
            HasKey(x => x.Id);

            Property(x => x.DescricaoAtributo).HasColumnName("descricao_atributo".ToUpper());
            Property(x => x.OpcaoAtributo).HasColumnName("opcao_atributo".ToUpper());
        }
    }
}
