﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class ProdutoCodBarraMapping : EntityTypeConfiguration<ProdutoCodBarra>
    {
        public ProdutoCodBarraMapping()
        {
            ToTable("produto_cod_barra".ToUpper());

            HasRequired(x => x.Produto)
                .WithMany(x => x.ProdutoCodBarraList)
                .Map(map => map.MapKey("id_produto".ToUpper()));

            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("id_produto_cod_barra".ToUpper());
            Property(x => x.CodBarra).HasColumnName("cod_barra".ToUpper());
            Property(x => x.Quantidade).HasColumnName("quantidade".ToUpper());
            Property(x => x.CodTipo).HasColumnName("cod_tipo".ToUpper());
            Property(x => x.Tipo).HasColumnName("tipo".ToUpper());
            Property(x => x.Altura).HasColumnName("altura".ToUpper());
            Property(x => x.Largura).HasColumnName("largura".ToUpper());
            Property(x => x.Comprimento).HasColumnName("comprimento".ToUpper());
            Property(x => x.Peso).HasColumnName("peso".ToUpper());
            Property(x => x.Principal).HasColumnName("principal".ToUpper());
        }
    }
}
