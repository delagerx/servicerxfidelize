﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class EntidadeMapping : EntityTypeConfiguration<Entidade>
    {
        public EntidadeMapping()
        {
            ToTable("entidade".ToUpper());

            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("id_entidade".ToUpper());
            Property(x => x.CodEntidadePolo).HasColumnName("cod_entidade_polo".ToUpper());
            Property(x => x.CpfCnpj).HasColumnName("cpf_cnpj".ToUpper());
            Property(x => x.RazaoSocial).HasColumnName("razao_social".ToUpper());
            Property(x => x.CodTipoPessoa).HasColumnName("cod_tipo_pessoa".ToUpper());
            Property(x => x.Fantasia).HasColumnName("fantasia".ToUpper());
            Property(x => x.Tipo).HasColumnName("tipo".ToUpper());
            Property(x => x.SubTipo).HasColumnName("subtipo".ToUpper());
            Property(x => x.Bandeira).HasColumnName("bandeira".ToUpper());
            Property(x => x.CodSituacao).HasColumnName("cod_situacao".ToUpper());
            Property(x => x.Obs).HasColumnName("obs".ToUpper());
            Property(x => x.Endereco).HasColumnName("endereco".ToUpper());
            Property(x => x.CodTipoEndereco).HasColumnName("cod_tipo_endereco".ToUpper());
            Property(x => x.Bairro).HasColumnName("bairro".ToUpper());
            Property(x => x.Cep).HasColumnName("cep".ToUpper());
            Property(x => x.CodRota).HasColumnName("cod_rota".ToUpper());
            Property(x => x.DescrRota).HasColumnName("descr_rota".ToUpper());
            Property(x => x.Complemento).HasColumnName("complemento".ToUpper());
            Property(x => x.Cidade).HasColumnName("cidade".ToUpper());
            Property(x => x.SiglaEstado).HasColumnName("sigla_estado".ToUpper());
            Property(x => x.InscricaoEstadual).HasColumnName("inscricao_estadual".ToUpper());
            Property(x => x.Telefone).HasColumnName("telefone".ToUpper());
        }
    }
}
