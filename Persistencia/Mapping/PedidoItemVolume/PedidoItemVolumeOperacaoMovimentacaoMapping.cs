﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class PedidoItemVolumeOperacaoMovimentacaoMapping : PedidoItemVolumeMapping<PedidoItemVolumeOperacaoMovimentacao>
    {
        public PedidoItemVolumeOperacaoMovimentacaoMapping()
        {
            ToTable("pedido_item_volume_oper_mov".ToUpper());

            HasRequired(x => x.Pedido)
                .WithMany(x => x.PedidoItemVolumeOperacaoMovimentacaoList)
                .Map(map => map.MapKey("id_pedido_operacao_mov".ToUpper()));

            Property(x => x.Id).HasColumnName("id_pedido_item_volume_oper_mov".ToUpper());
            Property(x => x.IdPedidoVolume).HasColumnName("id_pedido_volume_oper_mov".ToUpper());
        }
    }
}
