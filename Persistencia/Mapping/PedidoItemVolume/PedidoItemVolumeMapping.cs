﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class PedidoItemVolumeMapping<T> : EntityTypeConfiguration<T> where T : PedidoItemVolume
    {
        public PedidoItemVolumeMapping()
        {
            HasKey(x => x.Id);

            HasOptional(x => x.Produto).WithMany().HasForeignKey(x => x.IdProduto);

            Property(x => x.IdProduto).HasColumnName("id_produto".ToUpper());
            Property(x => x.Volume).HasColumnName("volume".ToUpper());
            Property(x => x.LoteOrigem).HasColumnName("lote_origem".ToUpper());
            Property(x => x.LoteDestino).HasColumnName("lote_dest".ToUpper());
            Property(x => x.Quantidade).HasColumnName("quantidade".ToUpper());
        }
    }
}
