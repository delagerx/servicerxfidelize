﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class PedidoItemVolumeOperacaoSaidaMapping : PedidoItemVolumeMapping<PedidoItemVolumeOperacaoSaida>
    {
        public PedidoItemVolumeOperacaoSaidaMapping()
        {
            ToTable("pedido_item_volume_oper_saida".ToUpper());

            HasRequired(x => x.Pedido)
                .WithMany(x => x.PedidoItemVolumeOperacaoSaidaList)
                .Map(map => map.MapKey("id_pedido_operacao_saida".ToUpper()));

            Property(x => x.Id).HasColumnName("id_pedido_item_vol_oper_saida".ToUpper());
            Property(x => x.IdPedidoVolume).HasColumnName("id_pedido_volume_oper_saida".ToUpper());
        }
    }
}
