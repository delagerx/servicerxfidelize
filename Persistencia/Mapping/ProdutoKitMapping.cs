﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class ProdutoKitMapping : EntityTypeConfiguration<ProdutoKit>
    {
        public ProdutoKitMapping()
        {
            ToTable("produto_kit".ToUpper());

            HasRequired(x => x.Produto)
                .WithMany(x => x.ProdutoKitList)
                .Map(map => map.MapKey("id_produto".ToUpper()));

            HasOptional(x => x.ProdutoInsumo)
                .WithMany(x => x.ProdutoInsumoList)
                .Map(map => map.MapKey("id_produto_insumo".ToUpper()));

            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("id_produto_kit".ToUpper());
            Property(x => x.Quantidade).HasColumnName("quantidade".ToUpper());
        }
    }
}
