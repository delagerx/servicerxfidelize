﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class PedidoItemLoteOperacaoEntradaMapping : PedidoItemLoteMapping<PedidoItemLoteOperacaoEntrada>
    {
        public PedidoItemLoteOperacaoEntradaMapping()
        {
            ToTable("pedido_item_lote_oper_entrada".ToUpper());

            HasRequired(x => x.PedidoItem)
                .WithMany(x => x.PedidoItemLoteOperacaoEntradaList)
                .Map(map => map.MapKey("id_pedido_item_oper_entrada".ToUpper()));

            Property(x => x.Id).HasColumnName("id_ped_item_lote_oper_entrada".ToUpper());
            Property(x => x.NumeroLpn).HasColumnName("numero_lpn".ToUpper());
        }
    }
}
