﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class PedidoItemLoteMapping<T> : EntityTypeConfiguration<T> where T : PedidoItemLote
    {
        public PedidoItemLoteMapping()
        {
            HasKey(x => x.Id);

            Property(x => x.LoteOrig).HasColumnName("lote_orig".ToUpper());
            Property(x => x.Lote).HasColumnName("lote".ToUpper());
            Property(x => x.Quantidade).HasColumnName("quantidade".ToUpper());
            Property(x => x.ValidadeOrig).HasColumnName("validade_orig".ToUpper());
            Property(x => x.Validade).HasColumnName("validade".ToUpper());
            Property(x => x.Fabricacao).HasColumnName("fabricacao".ToUpper());
            Property(x => x.CodSituacao).HasColumnName("cod_situacao".ToUpper());
            Property(x => x.Obs).HasColumnName("obs".ToUpper());
            Property(x => x.ClassificacaoOrigem).HasColumnName("classificacao_origem".ToUpper());
            Property(x => x.ClassificacaoDestino).HasColumnName("classificacao_destino".ToUpper());
            Property(x => x.IdClassificacaoOrigemPolo).HasColumnName("id_classificacao_origem_polo".ToUpper());
            Property(x => x.IdClassificacaoDestinoPolo).HasColumnName("id_classificacao_dest_polo".ToUpper());
            Property(x => x.IdClassificacaoOrigem).HasColumnName("id_classificacao_origem".ToUpper());
            Property(x => x.IdClassificacaoDestino).HasColumnName("id_classificacao_dest".ToUpper());
        }
    }
}
