﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class PedidoItemLoteOperacaoMovimentacaoMapping : PedidoItemLoteMapping<PedidoItemLoteOperacaoMovimentacao>
    {
        public PedidoItemLoteOperacaoMovimentacaoMapping()
        {
            ToTable("pedido_item_lote_oper_mov".ToUpper());

            HasRequired(x => x.PedidoItem)
                .WithMany(x => x.PedidoItemLoteOperacaoMovimentacaoList)
                .Map(map => map.MapKey("id_pedido_item_oper_mov".ToUpper()));

            Property(x => x.Id).HasColumnName("id_pedido_item_lote_oper_mov".ToUpper());
        }
    }
}
