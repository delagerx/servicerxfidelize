﻿using Entidades;

namespace Persistencia.Mapping
{
    public class PedidoItemLoteOperacaoSaidaMapping : PedidoItemLoteMapping<PedidoItemLoteOperacaoSaida>
    {
        public PedidoItemLoteOperacaoSaidaMapping()
        {
            ToTable("pedido_item_lote_oper_saida".ToUpper());

            HasRequired(x => x.PedidoItem)
                .WithMany(x => x.PedidoItemLoteOperacaoSaidaList)
                .Map(map => map.MapKey("id_pedido_item_operacao_saida".ToUpper()));

            Property(x => x.Id).HasColumnName("id_pedido_item_lote_oper_saida".ToUpper());
            Property(x => x.PesoItem).HasColumnName("peso_item".ToUpper());
        }
    }
}
