﻿using Entidades;
using System.Data.Entity.ModelConfiguration;

namespace Persistencia.Mapping
{
    public class ShelfLifeSaidaMapping : EntityTypeConfiguration<ShelfLifeSaida>
    {
        public ShelfLifeSaidaMapping()
        {
            ToTable("shelflife_ent_grupo_sub_prod".ToUpper());

            HasKey(x => x.Id);

            HasRequired(x => x.Entidade)
                .WithOptional(x => x.ShelfLifeSaida)
                .Map(map => map.MapKey("id_entidade".ToUpper()));

            Property(x => x.Id)
                .HasColumnName("idshelflifeentgruposubprod".ToUpper())
                ;

            Property(x => x.Grupo).HasColumnName("grupo".ToUpper());

            Property(x => x.Subgrupo).HasColumnName("subgrupo".ToUpper());
        }
    }
}
