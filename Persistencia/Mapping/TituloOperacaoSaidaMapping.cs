﻿using Entidades;
using System.Data.Entity.ModelConfiguration;

namespace Persistencia.Mapping
{
    public class TituloOperacaoSaidaMapping : EntityTypeConfiguration<TituloOperacaoSaida>
    {
        public TituloOperacaoSaidaMapping()
        {
            ToTable("titulo_operacao_saida".ToUpper());

            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("id_titulo_operacao_saida".ToUpper());
            Property(x => x.ChaveAcesso).HasColumnName("chave_acesso".ToUpper());
            Property(x => x.CodOperacaoLogisticaPolo).HasColumnName("cod_operacao_logistica_polo".ToUpper());
            Property(x => x.CodPedidoPolo).HasColumnName("cod_pedido_polo".ToUpper());
            Property(x => x.CodTituloPolo).HasColumnName("cod_titulo_polo".ToUpper());
            Property(x => x.NumeroTitulo).HasColumnName("numero_titulo".ToUpper());
            Property(x => x.Peso).HasColumnName("peso".ToUpper());
            Property(x => x.SerieNf).HasColumnName("serie_nf".ToUpper());
            Property(x => x.ValorMercadoria).HasColumnName("valor_mercadoria".ToUpper());
            Property(x => x.ValorTotal).HasColumnName("valor_total".ToUpper());
        }
    }
}