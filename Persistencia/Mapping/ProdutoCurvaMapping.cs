﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class ProdutoCurvaMapping : EntityTypeConfiguration<ProdutoCurva>
    {
        public ProdutoCurvaMapping()
        {
            ToTable("produto_curva".ToUpper());

            HasRequired(x => x.Produto)
                .WithMany(x => x.ProdutoCurvaList)
                .Map(map => map.MapKey("id_produto".ToUpper()));

            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("id_produto_curva".ToUpper());
            Property(x => x.CodOperacaoLogisticaPolo).HasColumnName("cod_operacao_logistica_polo".ToUpper());
            Property(x => x.Curva).HasColumnName("curva".ToUpper());
        }
    }
}
