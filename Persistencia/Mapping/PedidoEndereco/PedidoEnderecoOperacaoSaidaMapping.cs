﻿using Entidades;

namespace Persistencia.Mapping
{
    public class PedidoEnderecoOperacaoSaidaMapping : PedidoEnderecoMapping<PedidoEnderecoOperacaoSaida>
    {
        public PedidoEnderecoOperacaoSaidaMapping()
        {
            ToTable("pedido_operacao_saida_endereco".ToUpper());

            HasRequired(x => x.Pedido)
                .WithOptional(x => x.PedidoOperacaoSaidaEndereco)
                .Map(map => map.MapKey("id_pedido_operacao_saida".ToUpper()));

            Property(x => x.Id)
                .HasColumnName("id_pedido_oper_saida_endereco".ToUpper())
                ;
        }
    }
}
