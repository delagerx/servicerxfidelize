﻿using Entidades;
using System.Data.Entity.ModelConfiguration;

namespace Persistencia.Mapping
{
    public class PedidoEnderecoMapping<T> : EntityTypeConfiguration<T> where T : PedidoEndereco
    {
        public PedidoEnderecoMapping()
        {
            HasKey(x => x.Id);

            Property(x => x.CodTipoEndereco).HasColumnName("cod_tipo_endereco".ToUpper()).IsOptional();
            Property(x => x.Endereco).HasColumnName("endereco".ToUpper());
            Property(x => x.Bairro).HasColumnName("bairro".ToUpper()).IsOptional();
            Property(x => x.Cep).HasColumnName("cep".ToUpper());
            Property(x => x.CodRota).HasColumnName("cod_rota".ToUpper()).IsOptional();
            Property(x => x.DescrRota).HasColumnName("descr_rota".ToUpper()).IsOptional();
            Property(x => x.Complemento).HasColumnName("complemento".ToUpper()).IsOptional();
            Property(x => x.Cidade).HasColumnName("cidade".ToUpper()).IsOptional();
            Property(x => x.SiglaEstado).HasColumnName("sigla_estado".ToUpper()).IsOptional();
        }
    }
}
