﻿using Entidades;
using System.Data.Entity.ModelConfiguration;

namespace Persistencia.Mapping
{
    public class PedidoCancelamentoMapping : EntityTypeConfiguration<PedidoCancelamento>
    {
        public PedidoCancelamentoMapping()
        {
            ToTable("pedido_cancelamento".ToUpper());

            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("id_pedido_cancelamento".ToUpper());
            Property(x => x.CodPedidoPolo).HasColumnName("cod_pedido_polo".ToUpper());
            Property(x => x.CodOperacaoLogisticaPolo).HasColumnName("cod_operacao_logistica_polo".ToUpper());
            Property(x => x.IdCancelamento).HasColumnName("id_cancelamento".ToUpper());
            Property(x => x.DataSolicitacaoCancelamento).HasColumnName("data_solicitacao_cancelamento".ToUpper());
            Property(x => x.DescricaoCancelamento).HasColumnName("descricao_cancelamento".ToUpper());
        }
    }
}
