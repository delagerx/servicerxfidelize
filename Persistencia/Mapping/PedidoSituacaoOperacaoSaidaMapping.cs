﻿using Entidades;
using System.Data.Entity.ModelConfiguration;

namespace Persistencia.Mapping
{
    public class PedidoSituacaoOperacaoSaidaMapping : EntityTypeConfiguration<PedidoSituacaoOperacaoSaida>
    {
        public PedidoSituacaoOperacaoSaidaMapping()
        {
            ToTable("pedido_situacao_operacao_saida".ToUpper());

            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("id_pedido_situacao_oper_saida".ToUpper());
            Property(x => x.ChaveAcesso).HasColumnName("chave_acesso".ToUpper());
            Property(x => x.CodOperacaoLogisticaPolo).HasColumnName("cod_operacao_logistica_polo".ToUpper());
            Property(x => x.CodPedido).HasColumnName("cod_pedido".ToUpper());
            Property(x => x.CodPedidoPolo).HasColumnName("cod_pedido_polo".ToUpper());
            Property(x => x.CodPrenota).HasColumnName("cod_prenota".ToUpper());
            Property(x => x.CodSituacao).HasColumnName("cod_situacao".ToUpper());
            Property(x => x.Fatura).HasColumnName("fatura".ToUpper());
            Property(x => x.NumeroTitulo).HasColumnName("numero_titulo".ToUpper());
            Property(x => x.Situacao).HasColumnName("situacao".ToUpper());
        }
    }
}
