﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class ProdutoMapping : EntityTypeConfiguration<Produto>
    {
        public ProdutoMapping()
        {
            ToTable("produto".ToUpper());

            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("id_produto".ToUpper());
            Property(x => x.CodProdutoPolo).HasColumnName("cod_produto_polo".ToUpper());
            Property(x => x.CodFornPolo).HasColumnName("cod_forn_polo".ToUpper());
            Property(x => x.Descricao).HasColumnName("descricao".ToUpper());
            Property(x => x.Generico).HasColumnName("generico".ToUpper());
            Property(x => x.Grupo).HasColumnName("grupo".ToUpper());
            Property(x => x.Subgrupo).HasColumnName("subgrupo".ToUpper());
            Property(x => x.CodSituacao).HasColumnName("cod_situacao".ToUpper());
            Property(x => x.EmbCompra).HasColumnName("emb_compra".ToUpper());
            Property(x => x.EmbSaida).HasColumnName("emb_saida".ToUpper());
            Property(x => x.Validade).HasColumnName("validade".ToUpper());
            Property(x => x.ValidadeMinima).HasColumnName("validade_minima_subgrupo".ToUpper());
            Property(x => x.LinhaProduto).HasColumnName("linha_produto".ToUpper());
            Property(x => x.ControleLote).HasColumnName("controle_lote".ToUpper());
            Property(x => x.Unidade).HasColumnName("unidade".ToUpper());
            Property(x => x.CasasDecimais).HasColumnName("casas_decimais".ToUpper());
            Property(x => x.Altura).HasColumnName("altura".ToUpper());
            Property(x => x.Largura).HasColumnName("largura".ToUpper());
            Property(x => x.Comprimento).HasColumnName("comprimento".ToUpper());
            Property(x => x.Peso).HasColumnName("peso".ToUpper());
            Property(x => x.Preco).HasColumnName("preco".ToUpper());
            Property(x => x.EhKit).HasColumnName("eh_kit".ToUpper());
        }
    }
}
