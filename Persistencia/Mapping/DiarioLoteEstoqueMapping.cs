﻿using Entidades;
using System.Data.Entity.ModelConfiguration;

namespace Persistencia.Mapping
{
    public class DiarioLoteEstoqueMapping : EntityTypeConfiguration<DiarioLoteEstoque>
    {
        public DiarioLoteEstoqueMapping()
        {
            ToTable("diario_lote_estoque".ToUpper());

            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("id_diario_lote_estoque".ToUpper());
            Property(x => x.IdProduto).HasColumnName("id_produto".ToUpper());
            Property(x => x.CodOperacaoLogisticaPolo).HasColumnName("cod_operacao_logistica_polo".ToUpper());
            Property(x => x.IdClassificacao).HasColumnName("id_classificacao".ToUpper());
            Property(x => x.IdClassificacaoPolo).HasColumnName("id_classificacao_polo".ToUpper());
            Property(x => x.Lote).HasColumnName("lote".ToUpper());
            Property(x => x.Estoque).HasColumnName("estoque".ToUpper());
            Property(x => x.DataCadastro).HasColumnName("data_cadastro".ToUpper());
        }
    }
}
