﻿using Entidades;
using System.Data.Entity.ModelConfiguration;

namespace Persistencia.Mapping
{
    public class ProdutoAuditoriaMovInvMapping : EntityTypeConfiguration<ProdutoAuditoriaMovInv>
    {
        public ProdutoAuditoriaMovInvMapping()
        {
            ToTable("produto_auditoria_mov_inv".ToUpper());

            HasKey(x => x.Id);

            HasOptional(x => x.Produto).WithMany();

            Property(x => x.Id).HasColumnName("id_produto_auditoria_mov_inv".ToUpper());
            Property(x => x.CodOperacaoLogisticaPolo).HasColumnName("cod_operacao_logistica_polo".ToUpper());
            Property(x => x.IdPedidoOperacaoEntrada).HasColumnName("id_pedido_operacao_entrada".ToUpper());
            Property(x => x.IdPedidoOperacaoSaida).HasColumnName("id_pedido_operacao_saida".ToUpper());
            Property(x => x.IdProduto).HasColumnName("id_produto".ToUpper());
            Property(x => x.Lote).HasColumnName("lote".ToUpper());
            Property(x => x.Endereco).HasColumnName("endereco".ToUpper());
            Property(x => x.Quantidade).HasColumnName("quantidade".ToUpper());
            Property(x => x.IdInventario).HasColumnName("id_inventario".ToUpper());
            Property(x => x.DescricaoClassificacao).HasColumnName("descricao_classificacao".ToUpper());
            Property(x => x.IdAuditoriaInventarioDocumento).HasColumnName("id_auditoria_inventario_documento".ToUpper());
            Property(x => x.IdLoteClassificacao).HasColumnName("id_lote_classificacao".ToUpper());
            Property(x => x.IdLoteClassificacaoRel).HasColumnName("id_lote_classificacao_rel".ToUpper());
        }
    }
}
