﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class PedidoItemOperacaoEntradaMapping : PedidoItemMapping<PedidoItemOperacaoEntrada>
    {
        public PedidoItemOperacaoEntradaMapping()
        {
            ToTable("pedido_item_operacao_entrada".ToUpper());

            HasRequired(x => x.Pedido)
                .WithMany(x => x.PedidoItemOperacaoEntradaList)
                .Map(map => map.MapKey("id_pedido_operacao_entrada".ToUpper()));

            Property(x => x.Id).HasColumnName("id_pedido_item_oper_entrada".ToUpper());
        }
    }
}
