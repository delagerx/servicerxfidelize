﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class PedidoItemMapping<T> : EntityTypeConfiguration<T> where T : PedidoItem
    {
        public PedidoItemMapping()
        {
            HasKey(x => x.Id);

            HasOptional(x => x.Produto).WithMany().HasForeignKey(x => x.IdProduto);

            Property(x => x.IdProduto).HasColumnName("id_produto".ToUpper());
            Property(x => x.Quantidade).HasColumnName("quantidade".ToUpper());
            Property(x => x.Separado).HasColumnName("separado".ToUpper());
            Property(x => x.CodMotivo).HasColumnName("cod_motivo".ToUpper());
            Property(x => x.Motivo).HasColumnName("motivo".ToUpper());
            Property(x => x.NumeroItem).HasColumnName("numero_item".ToUpper());
            Property(x => x.Obs).HasColumnName("obs".ToUpper());
            Property(x => x.OrdemConf).HasColumnName("ordem_conf".ToUpper());
        }
    }
}
