﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class PedidoItemOperacaoMovimentacaoMapping : PedidoItemMapping<PedidoItemOperacaoMovimentacao>
    {
        public PedidoItemOperacaoMovimentacaoMapping()
        {
            ToTable("pedido_item_oper_movimentacao".ToUpper());

            HasRequired(x => x.Pedido)
                .WithMany(x => x.PedidoItemOperacaoMovimentacaoList)
                .Map(map => map.MapKey("id_pedido_operacao_mov".ToUpper()));

            Property(x => x.Id).HasColumnName("id_pedido_item_oper_mov".ToUpper());
        }
    }
}
