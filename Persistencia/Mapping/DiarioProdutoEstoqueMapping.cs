﻿using Entidades;
using System.Data.Entity.ModelConfiguration;

namespace Persistencia.Mapping
{
    public class DiarioProdutoEstoqueMapping : EntityTypeConfiguration<DiarioProdutoEstoque>
    {
        public DiarioProdutoEstoqueMapping()
        {
            ToTable("diario_produto_estoque".ToUpper());

            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("id_diario_produto_estoque".ToUpper());
            Property(x => x.IdProduto).HasColumnName("id_produto".ToUpper());
            Property(x => x.CodOperacaoLogisticaPolo).HasColumnName("cod_operacao_logistica_polo".ToUpper());
            Property(x => x.Estoque).HasColumnName("estoque".ToUpper());
            Property(x => x.Retencao).HasColumnName("retencao".ToUpper());
            Property(x => x.EstoqueEmPedidoSeg).HasColumnName("estoque_em_pedido_seg".ToUpper());
            Property(x => x.EstoqueEmPicking).HasColumnName("estoque_em_picking".ToUpper());
            Property(x => x.EmEntrada).HasColumnName("em_entrada".ToUpper());
            Property(x => x.EmEntradaSeg).HasColumnName("em_entrada_seg".ToUpper());
            Property(x => x.DataCadastro).HasColumnName("data_cadastro".ToUpper());
        }
    }
}
