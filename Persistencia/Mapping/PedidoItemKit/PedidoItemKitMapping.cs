﻿using Entidades;
using System.Data.Entity.ModelConfiguration;

namespace Persistencia.Mapping
{
    public class PedidoItemKitMapping<T> : EntityTypeConfiguration<T> where T : PedidoItemKit
    {
        public PedidoItemKitMapping()
        {
            HasKey(x => x.Id);

            HasOptional(x => x.Produto).WithMany().HasForeignKey(x => x.IdProduto);
            HasOptional(x => x.ProdutoKit).WithMany().HasForeignKey(x => x.IdProdutoKit);

            Property(x => x.CodMotivo).HasColumnName("cod_motivo".ToUpper());
            Property(x => x.CortaKitTotal).HasColumnName("corta_kit_total".ToUpper());
            Property(x => x.IdProduto).HasColumnName("id_produto".ToUpper());
            Property(x => x.IdProdutoKit).HasColumnName("id_produto_kit".ToUpper());
            Property(x => x.Motivo).HasColumnName("motivo".ToUpper());
            Property(x => x.Quantidade).HasColumnName("quantidade".ToUpper());
            Property(x => x.Separado).HasColumnName("separado".ToUpper());
            Property(x => x.QuantidadeKit).HasColumnName("quantidade_kit".ToUpper());
        }
    }
}
