﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class PedidoItemKitOperacaoSaidaMapping : PedidoItemKitMapping<PedidoItemKitOperacaoSaida>
    {
        public PedidoItemKitOperacaoSaidaMapping()
        {
            ToTable("pedido_item_kit_oper_saida".ToUpper());

            HasRequired(x => x.Pedido)
                .WithMany(x => x.PedidoItemKitOperacaoSaidaList)
                .Map(map => map.MapKey("id_pedido_operacao_saida".ToUpper()));

            Property(x => x.Id).HasColumnName("id_pedido_item_kit_oper_saida".ToUpper());
        }
    }
}
