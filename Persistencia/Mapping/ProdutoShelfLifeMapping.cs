﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Mapping
{
    public class ProdutoShelfLifeMapping : EntityTypeConfiguration<ProdutoShelfLife>
    {
        public ProdutoShelfLifeMapping()
        {
            ToTable("produto_shelf_life".ToUpper());

            HasRequired(x => x.Produto)
                .WithMany(x => x.ProdutoShelfLifeList)
                .Map(map => map.MapKey("id_produto".ToUpper()));

            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("id_produto_shelf_life".ToUpper());
            Property(x => x.GrupoShelfLife).HasColumnName("grupo_shelf_life".ToUpper());
            Property(x => x.ValidadeMinima).HasColumnName("validade_minima".ToUpper());
            Property(x => x.ValidadeMaxima).HasColumnName("validade_maxima".ToUpper());
        }
    }
}
